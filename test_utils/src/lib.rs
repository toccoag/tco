use nix::fcntl;
use std::fs::{self, File};
use tco::ansible;
use tco::ansible::config::Config;
use tco::cache;
use tco::utils::drop_all_caches;
use tokio::sync::OnceCell;
use tokio::task;

pub async fn memory_cached_ansible_config() -> &'static Config {
    static ONCE: OnceCell<Config> = OnceCell::const_new();
    ONCE.get_or_init(|| async {
        let _guard = lock_caches(LockMode::Shared).await;
        let repo = ansible::git::repo().await.unwrap();
        ansible::config::load(&repo).await.unwrap()
    })
    .await
}

#[must_use]
pub async fn lock_caches(mode: LockMode) -> LockGuard {
    task::spawn_blocking(move || lock_caches_sync(mode))
        .await
        .unwrap()
}

#[must_use]
pub fn lock_caches_sync(mode: LockMode) -> LockGuard {
    let dir = cache::cache_dir();
    match fs::create_dir_all(dir) {
        Ok(()) => (),
        Err(e) if e.kind() == std::io::ErrorKind::NotFound => (),
        Err(e) => panic!("{e}"),
    }
    let path = dir.with_extension("lock");
    let file = File::create(path).unwrap();
    let fcntl_mode = match mode {
        LockMode::Exclusive => fcntl::FlockArg::LockExclusive,
        LockMode::Shared => fcntl::FlockArg::LockShared,
    };
    let flock = fcntl::Flock::lock(file, fcntl_mode)
        .map_err(|(_, errno)| errno)
        .unwrap();
    LockGuard {
        _lock_file: flock,
        drop_caches_on_lock_release: false,
        exclusive: matches!(mode, LockMode::Exclusive),
    }
}

pub struct LockGuard {
    _lock_file: fcntl::Flock<File>,
    drop_caches_on_lock_release: bool,
    exclusive: bool,
}

#[derive(Clone, Copy, Eq, PartialEq)]
pub enum LockMode {
    Exclusive,
    Shared,
}

impl LockGuard {
    pub async fn drop_caches(&self) {
        assert!(self.exclusive, "exclusive lock required to drop caches");
        drop_all_caches().await.unwrap();
    }

    pub fn drop_caches_on_lock_release(&mut self, value: bool) -> &mut Self {
        self.drop_caches_on_lock_release = value;
        self
    }
}

impl Drop for LockGuard {
    fn drop(&mut self) {
        if self.drop_caches_on_lock_release {
            futures::executor::block_on(self.drop_caches());
        }
    }
}
