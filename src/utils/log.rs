//! Utilities to easly printing logs
use log::Level;
use std::borrow::Cow;
use std::ffi::OsStr;
use std::fmt;
use std::process::{Command, ExitStatus};
use std::sync::atomic::{AtomicBool, Ordering};
use std::time::Duration;
use tokio::task;

static VPN_NEEDED_LOGGED: LogOnce = LogOnce::new();

struct LogOnce {
    logged: AtomicBool,
}

impl LogOnce {
    const fn new() -> Self {
        Self {
            logged: AtomicBool::new(false),
        }
    }
}

macro_rules! log_once {
    ($once:expr, $level:expr, $($arg:tt)+) => {
        let once: &LogOnce = $once;

        if once.logged.compare_exchange(false, true, Ordering::AcqRel, Ordering::Relaxed).is_ok() {
            log::log!($level, $($arg)+);
        }
    };
}

/// Hint to user that VPN may need to be enabled.
///
/// `status` is the exit code from SSH. Message is printed if
/// exit code is 255. From ssh(1):
///
/// > ssh exits with the exit status of the remote command or with 255 if an error occurred.
///
/// Message is printed an most once.
pub fn vpn_needed_info_message_on_ssh_timeout(status: ExitStatus) {
    if status.code() == Some(255) {
        log_once!(
            &VPN_NEEDED_LOGGED,
            Level::Info,
            "SSH reported an error: is VPN enabled?"
        );
    }
}

/// Handle which cancels scheduled message if dropped.
///
/// See [`schedule_vpn_needed_info_message`]
#[must_use]
pub(crate) struct CancelVpnMessage(task::AbortHandle);

impl Drop for CancelVpnMessage {
    fn drop(&mut self) {
        self.0.abort();
    }
}

/// Schedule message to be printed about need to enable VPN.
///
/// Used to print a `info!()` message hinting to the user that VPN may need
/// to be enabled. Printing of the message is canceled, if the returned
/// [`CancelVpnMessage`] is dropped within `duration`.
///
/// Message is printed at most once. If called with different `msg`es,
/// the message for the first call to exceed its `duration` is printed.
pub(crate) fn schedule_vpn_needed_info_message(
    duration: Duration,
    msg: &'static str,
) -> CancelVpnMessage {
    let handle = task::spawn(async move {
        tokio::time::sleep(duration).await;
        log_once!(&VPN_NEEDED_LOGGED, Level::Info, "{}", msg,);
    });
    CancelVpnMessage(handle.abort_handle())
}

/// Schedule `info!()` message to be printed in five seconds hinting
/// that VPN me be needed.
///
/// See [`schedule_vpn_needed_info_message`] for details.
pub(crate) fn schedule_vpn_needed_for_ssh_info_message() -> CancelVpnMessage {
    schedule_vpn_needed_info_message(
        Duration::from_secs(5),
        "SSH connection could not be established within 5 seconds: is VPN enabled?",
    )
}

/// [`unreachable!()`] which only panic!()s in tests
///
/// If `cfg!(test)` or `cfg!(feature = "panic-on-soft-unreachable")`, panics
/// with given message. Otherwise, message is printed as [`warn!()`]ing.
macro_rules! soft_unreachable {
    ($($arg:tt)+) => {
        if cfg!(any(test, feature = "panic-on-soft-unreachable")) {
            ::std::panic!($($arg)+);
        } else {
            ::log::warn!($($arg)+);
        }
    };
}
pub(crate) use soft_unreachable;

/// Helper to print [`Command`] with proper shell Quoting.
pub struct DisplayCommand<'a>(pub &'a Command);

impl fmt::Display for DisplayCommand<'_> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fn quote(value: &OsStr) -> Cow<'_, str> {
            shlex::try_quote(value.to_str().unwrap_or("<non-UTF8>")).expect("NUL character")
        }

        for (key, value) in self.0.get_envs() {
            write!(
                f,
                "{}={} ",
                key.to_str().unwrap_or("'<non-UTF8>'"),
                value.map(|v| quote(v)).unwrap_or(Cow::Borrowed("")),
            )?;
        }
        write!(f, "{}", quote(self.0.get_program()))?;
        for arg in self.0.get_args() {
            write!(f, " {}", quote(arg))?;
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    #[should_panic = "one test 2"]
    fn soft_unreachable_empty_with_message() {
        soft_unreachable!("{} test {}", "one", 2);
    }

    #[test]
    fn display_command_unquoted() {
        let mut cmd = Command::new("ls");
        assert_eq!(format!("{}", DisplayCommand(&cmd)), "ls");

        cmd.arg("-lh");
        cmd.arg("/");
        assert_eq!(format!("{}", DisplayCommand(&cmd)), "ls -lh /");

        cmd.env("DEBUG", "");
        cmd.env("TERM", "xterm");
        assert_eq!(
            format!("{}", DisplayCommand(&cmd)),
            "DEBUG='' TERM=xterm ls -lh /"
        );
    }

    #[test]
    fn display_command_quoted() {
        let mut cmd = Command::new("a command");
        assert_eq!(format!("{}", DisplayCommand(&cmd)), "'a command'");

        #[allow(clippy::suspicious_command_arg_space)]
        cmd.arg("--a option");
        cmd.arg("with spaces");
        assert_eq!(
            format!("{}", DisplayCommand(&cmd)),
            "'a command' '--a option' 'with spaces'"
        );

        cmd.env("DEBUG", r#"some value with "quotes""#);
        assert_eq!(
            format!("{}", DisplayCommand(&cmd)),
            r#"DEBUG='some value with "quotes"' 'a command' '--a option' 'with spaces'"#
        );
    }
}
