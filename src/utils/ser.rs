//! Utilities for deserialization
use semver::Version;
use serde::Serializer;

/// Serialize [`semver::Version`]
pub(crate) fn version<S>(value: &Version, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    serializer.serialize_str(&value.to_string())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn version() {
        #[derive(serde::Serialize)]
        struct Data {
            #[serde(serialize_with = "super::version")]
            version: Version,
        }

        let data = Data {
            version: Version::new(3, 12, 0),
        };
        let json = serde_json::to_string(&data).unwrap();
        assert_eq!(json, r#"{"version":"3.12.0"}"#);
    }
}
