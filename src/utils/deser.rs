//! Utilities for deserialization
use semver::Version;
use serde::Deserialize;
use serde::de;
use std::any;
use std::fmt;
use std::str::FromStr;

/// Serialize as `String`, then convert via [`FromStr`]
pub(crate) fn parse_str<'de, D, T>(deserializer: D) -> Result<T, D::Error>
where
    D: de::Deserializer<'de>,
    T: FromStr,
    <T as FromStr>::Err: fmt::Display,
{
    let s: String = Deserialize::deserialize(deserializer)?;
    s.parse().map_err(|e| {
        de::Error::custom(format!(
            "Failed to parse string as {}: {e}",
            any::type_name::<T>(),
        ))
    })
}

/// Deserialize [`semver::Version`] using [`lenient_semver::parse()`]
pub(crate) fn version<'de, D>(deserializer: D) -> Result<Version, D::Error>
where
    D: de::Deserializer<'de>,
{
    let s: String = Deserialize::deserialize(deserializer)?;
    lenient_semver::parse(&s)
        .map_err(|e| de::Error::custom(format!("Failed to parse string as Version: {e}")))
}

/// Deserialize non-empty string
///
/// Treats an empty string as [`None`].
pub(crate) fn non_empty_string<'de, D>(deserializer: D) -> Result<Option<String>, D::Error>
where
    D: de::Deserializer<'de>,
{
    let s: String = Deserialize::deserialize(deserializer)?;
    if s.is_empty() { Ok(None) } else { Ok(Some(s)) }
}

/// Deserialize string where hyphen is an absent value
///
/// Treats strings `"-"` and `""` as [`None`].
pub(crate) fn u64_with_hyphen_as_none<'de, D>(deserializer: D) -> Result<Option<u64>, D::Error>
where
    D: de::Deserializer<'de>,
{
    let s: String = Deserialize::deserialize(deserializer)?;
    if s.is_empty() || s == "-" {
        Ok(None)
    } else {
        Ok(Some(s.parse().map_err(|e| {
            de::Error::custom(format!("Failed to parse string as u64: {e}"))
        })?))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_str() {
        #[derive(serde::Deserialize)]
        struct Data {
            #[serde(deserialize_with = "super::parse_str")]
            number: u64,
            #[serde(deserialize_with = "super::parse_str")]
            boolean: bool,
        }

        let json = r#"{ "number": "12", "boolean": "true" }"#;
        let data: Data = serde_json::from_str(json).unwrap();
        assert_eq!(data.number, 12);
        assert!(data.boolean);
    }

    #[test]
    fn version() {
        #[derive(Deserialize)]
        struct Data {
            #[serde(deserialize_with = "super::version")]
            version: Version,
        }

        let json = r#"{ "version": "3.12" }"#;
        let data: Data = serde_json::from_str(json).unwrap();
        assert_eq!(data.version, Version::new(3, 12, 0));
    }
}
