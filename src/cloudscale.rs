//! Interact with cloudscale API
//!
//! ## ID
//!
//! A tag named `"id"` is used as identifier rather than using
//! the random ID provided by Cloudscale. This allows us to
//! set a well-defined ID.
//!
//! It's technically possibly for multiple users to have the
//! same ID. An error is returned if an attempt is made
//! to create a second user with the same ID. This check is
//! done client-side and is not race-free.
//!
//! ## See Also
//!
//! [API documentation](https://www.cloudscale.ch/en/api).

use crate::{Error, Result};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fmt;

const USERS_ENDPOINT: &str = "https://api.cloudscale.ch/v1/objects-users";

/// Object / S3 user
#[derive(Deserialize)]
pub struct ObjectUser {
    /// ID used by Cloudscale internally.
    ///
    /// See [`ObjectUser::id()`] for the ID we, Tocco, uses.
    #[serde(rename = "id")]
    pub cloudscale_id: String,
    pub href: String,
    pub display_name: String,
    pub keys: Vec<Key>,
    pub tags: HashMap<String, String>,
}

impl ObjectUser {
    /// Display [`ObjectUser`], including secrets, using a user-readable format
    #[must_use]
    pub fn multi_line_display(&self) -> MultiLineUserDisplay {
        MultiLineUserDisplay { inner: self }
    }

    #[allow(clippy::doc_markdown)]
    /// Set old_key tag on object user.
    ///
    /// old_key tag is set to the current key to facilitate key
    /// rotation. An error is returned if there isn't exactly
    /// one key or if there is two keys and neither is set
    /// as old_key already.
    pub async fn set_old_key(self, api_token: &str) -> Result<()> {
        let old_key = self.tags.get("old_key");
        let key = match &self.keys[..] {
            [] => return Err(Error::Custom("user has no key".to_string())),
            [key] if Some(&key.access_key) == old_key => {
                debug!(
                    "For user {:?}, only key already set as old_key. Nothing left to do.",
                    self.id().unwrap_or(&self.display_name)
                );
                return Ok(());
            }
            [key] => key,
            [key, _] | [_, key] if Some(&key.access_key) == old_key => {
                debug!(
                    "For user {:?}, one of two keys ({:?}) already set as old_key. Nothing left \
                     to do.",
                    self.id().unwrap_or(&self.display_name),
                    key.access_key,
                );
                return Ok(());
            }
            [_, _, ..] => return Err(Error::Custom("user has multiple keys".to_string())),
        };

        debug!(
            "For user {:?}, setting key {:?} as old_key.",
            self.id().unwrap_or(&self.display_name),
            key.access_key
        );
        let mut payload = TagChangeRequest { tags: self.tags };
        payload
            .tags
            .insert("old_key".to_string(), key.access_key.clone());
        crate::http_client()
            .patch(self.href)
            .bearer_auth(api_token)
            .json(&payload)
            .send()
            .await?
            .error_for_status()?;
        Ok(())
    }

    #[allow(clippy::doc_markdown)]
    /// Remove old_key tag on object user.
    ///
    /// See also ['ObjectUser::set_old_key`]
    pub async fn unset_old_key(mut self, api_token: &str) -> Result<()> {
        if self.tags.remove("old_key").is_none() {
            debug!(
                "For user {:?}, no old_key was set. Nothing left to do.",
                self.id().unwrap_or(&self.display_name)
            );
            return Ok(());
        }

        let payload = TagChangeRequest { tags: self.tags };
        crate::http_client()
            .patch(self.href)
            .bearer_auth(api_token)
            .json(&payload)
            .send()
            .await?
            .error_for_status()?;
        Ok(())
    }
}

impl ObjectUser {
    /// Get ID of user
    #[must_use]
    pub fn id(&self) -> Option<&str> {
        self.tags.get("id").map(|i| i.as_str())
    }

    /// Get ID of user falling back to `display_name` if needed.
    #[must_use]
    pub fn id_or_display(&self) -> &str {
        self.id().unwrap_or(&self.display_name)
    }
}

/// Display [`ObjectUser`] in user-friendly format
///
/// See [`ObjectUser::multi_line_display()`]
pub struct MultiLineUserDisplay<'a> {
    inner: &'a ObjectUser,
}

impl fmt::Display for MultiLineUserDisplay<'_> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if let Some(id) = self.inner.id() {
            writeln!(f, "{id}:")?;
        } else {
            writeln!(f, "{}: (display_name)", &self.inner.display_name)?;
        }
        writeln!(f, "    id: {}", self.inner.id().unwrap_or("unknown"))?;
        writeln!(f, "    display_name: {}", &self.inner.display_name)?;
        writeln!(f, "    cloudscale_id: {}", &self.inner.cloudscale_id)?;
        for (no, key) in self.inner.keys.iter().enumerate() {
            writeln!(f, "    key {no}:")?;
            writeln!(f, "        access_key: {}", key.access_key)?;
            writeln!(f, "        secret_key: {}", key.secret_key)?;
        }
        writeln!(f, "    tags:")?;
        for (key, value) in &self.inner.tags {
            writeln!(f, "        {key}: {value:?}")?;
        }
        Ok(())
    }
}

/// Object / S3 key
#[derive(Deserialize)]
pub struct Key {
    pub access_key: String,
    pub secret_key: String,
}

/// Get list of all object / S3 users
pub async fn get_object_users(api_token: &str) -> Result<Vec<ObjectUser>> {
    Ok(crate::http_client()
        .get(USERS_ENDPOINT)
        .bearer_auth(api_token)
        .send()
        .await?
        .error_for_status()?
        .json()
        .await?)
}

/// Get object / S3 user with given ID
///
/// # Panics
///
/// Panics if more than one user has given ID.
pub async fn get_object_user_by_id(api_token: &str, id: &str) -> Result<Option<ObjectUser>> {
    let users: Vec<ObjectUser> = crate::http_client()
        .get(USERS_ENDPOINT)
        .bearer_auth(api_token)
        .query(&[("tag:id", id)])
        .send()
        .await?
        .error_for_status()?
        .json()
        .await?;
    match users.len() {
        0 => Ok(None),
        1 => Ok(Some(users.into_iter().next().expect("empty list of users"))),
        _ => panic!("more than one object user with ID {id}"),
    }
}

#[derive(Serialize)]
struct CreateUserRequest<'a> {
    display_name: &'a str,
    tags: CreateUserTags<'a>,
}

#[derive(Serialize)]
struct CreateUserTags<'a> {
    id: &'a str,
}

/// Create object user with given ID.
///
/// `id` is set as `"id"` tag and display name.
///
/// An error is returned if the user already exists.
pub async fn create_object_user(api_token: &str, id: &str) -> Result<ObjectUser> {
    if get_object_user_by_id(api_token, id).await?.is_some() {
        return Err(Error::Custom(format!("user with ID {id} already exists")));
    }
    let payload = CreateUserRequest {
        display_name: id,
        tags: CreateUserTags { id },
    };
    Ok(crate::http_client()
        .post(USERS_ENDPOINT)
        .bearer_auth(api_token)
        .json(&payload)
        .send()
        .await?
        .error_for_status()?
        .json()
        .await?)
}

/// Delete user with given ID.
pub async fn delete_object_user(api_token: &str, id: &str) -> Result<()> {
    let Some(user) = get_object_user_by_id(api_token, id).await? else {
        return Err(Error::Custom("no such user".to_string()));
    };
    crate::http_client()
        .delete(user.href)
        .bearer_auth(api_token)
        .send()
        .await?
        .error_for_status()?;
    Ok(())
}

#[derive(Serialize)]
struct TagChangeRequest {
    tags: HashMap<String, String>,
}

#[cfg(test)]
#[cfg(feature = "test-with-secrets")]
mod tests {
    use super::*;

    #[tokio::test]
    async fn tests() {
        tokio::join!(
            get_user(),
            get_users(),
            create_and_delete_user(),
            set_and_unset_old_key(),
        );
    }

    // Called from tests() to avoid "runtime dropped the dispatch task" error
    // #[tokio::test]
    async fn get_user() {
        let api_token = api_token().await;
        let user = get_object_user_by_id(api_token, "nice-test")
            .await
            .unwrap()
            .unwrap();
        assert_eq!(user.id(), Some("nice-test"));
        assert_eq!(user.tags["id"], "nice-test");
        assert_eq!(user.display_name, "nice-test");
        assert!(!user.keys.is_empty());
    }

    // Called from tests() to avoid "runtime dropped the dispatch task" error
    // #[tokio::test]
    async fn get_users() {
        let api_token = api_token().await;
        let users = get_object_users(api_token).await.unwrap();
        assert!(users.iter().any(|i| i.id() == Some("nice-test")));
        assert!(users.len() > 20);
    }

    // Called from tests() to avoid "runtime dropped the dispatch task" error
    // #[tokio::test]
    async fn create_and_delete_user() {
        let api_token = api_token().await;
        let user_id = "tco-integration-test-byk2phvvvf";

        // remove remnants from previous run
        let _ = delete_object_user(api_token, user_id).await;
        assert!(
            get_object_user_by_id(api_token, user_id)
                .await
                .unwrap()
                .is_none()
        );

        // create user
        let user = create_object_user(api_token, user_id).await.unwrap();
        assert_eq!(user.id(), Some(user_id));
        assert_eq!(user.tags["id"], user_id);

        // user already exists
        assert!(create_object_user(api_token, user_id).await.is_err());
        assert!(
            get_object_user_by_id(api_token, user_id)
                .await
                .unwrap()
                .is_some()
        );

        // remove user
        delete_object_user(api_token, user_id).await.unwrap();
        assert!(
            get_object_user_by_id(api_token, user_id)
                .await
                .unwrap()
                .is_none()
        );
    }

    // Called from tests() to avoid "runtime dropped the dispatch task" error
    // #[tokio::test]
    async fn set_and_unset_old_key() {
        let api_token = api_token().await;
        let user_id = "tco-integration-test-isy99re7c5";

        // remove remnants from previous run
        let _ = delete_object_user(api_token, user_id).await;
        assert!(
            get_object_user_by_id(api_token, user_id)
                .await
                .unwrap()
                .is_none()
        );

        // create user
        let user = create_object_user(api_token, user_id).await.unwrap();
        assert!(!user.tags.contains_key("old_key"));

        // set old_key
        user.set_old_key(api_token).await.unwrap();
        let user = get_object_user_by_id(api_token, user_id)
            .await
            .unwrap()
            .unwrap();
        assert_eq!(user.keys[0].access_key, user.tags["old_key"]);

        // unset old_key
        user.unset_old_key(api_token).await.unwrap();
        let user = get_object_user_by_id(api_token, user_id)
            .await
            .unwrap()
            .unwrap();
        assert!(!user.tags.contains_key("old_key"));

        // remove user
        delete_object_user(api_token, user_id).await.unwrap();
    }

    async fn api_token() -> &'static str {
        let config = test_utils::memory_cached_ansible_config().await;
        &config
            .installations
            .get("master")
            .expect("installation master not found")
            .cloudscale_api_token
    }
}
