//! Extract settings from *ansible*'s config.yml, global.yml and secrets2.yml.
//!
//! # Caveats
//!
//! This implementation is very slow. If possible, use
//! [`config_quick`](`super::config_quick`).
//!
//! Unlike `config_quick`, this module supports evaluating Jinja2 expressions
//! `{{ expr }}`, and access to Ansible Vault.
//!
//! # Fetching variables from Ansible
//!
//! `ansible_vars!{}` below defines what variables are fetched from Ansible
//! and made available via [`InstallConfig`].
//!
//! Example:
//!
//! ```no_compile
//! ansible_vars!{
//!     pub my_var: Vec<String> , "some_dict.keys()|list",
//!     //
//!     //  ^^^^^^
//!     //  name of field in InstallConfig.
//!     //
//!     //          ^^^^^^^^^^^
//!     //          Type to which to deserialize.
//!     //
//!     //                        ^^^^^^^^^^^^^^^^^^^^^^^
//!     //                        Jinja2 expression evaluated in Ansible
//!     //                        to obtain value.
//!
//! }
//! ```
//!
//! # Extracting Data from Ansible
//!
//! There is no easy way to extract variables directly from the setting file because [Jinja2][]
//! expressions need to be evaluated and the [Ansible Vault][] decrypted. Ansible does
//! not offer an API to access this data. Rather a [playbook][] is created and it's
//! output printed on stdout using a well-defined format. This output is then parsed.
//!
//! To prevent concurrency issues, a fresh [Git working tree] is created is created
//! in a temporary folder, *master* branch checked out, and a temporary playbook
//! written and executed.
//!
//! # Caching
//!
//! Extracting data from Ansible is a very slow process. Thus, all settings, for all
//! installations, are extracted from Ansible and cached.
//!
//! The cache is stored at *~/.cache/tocco-tco/ansible-config.cache*
//!
//! ## Outdated Cache
//!
//! The cache includes the Git commit ID of the working tree used to create
//! it. The cache is invalidated when *master* no longer points to this ID.
//!
//! ## Schema Changes
//!
//! The cache can become invalid when the (de)serialization format changes, Jinja2
//! expressions are adjusted, types change, fields change, etc. To detect such
//! changes, a hash over this file, generated at compile-time, is included in
//! the cache file before the start of the serialized data.
//!
//! ## Concurrency-Safety
//!
//! Cache is written to a *\*.tmp* file first, and [synced][] to disk to prevent
//! a truncated cache. The file is then renamed, an atomic operation.  The cache,
//! once written, is modified again making it safe to load the cache concurrently
//! without locking.
//!
//! Writes to the *\*.tmp*, however, are not concurrency-safe, and a *\*.lock*
//! file is created to prevent concurrent writes.
//!
//! [Ansible Vault]: https://docs.ansible.com/ansible/latest/cli/ansible-vault.html
//! [Git working tree]: https://git-scm.com/docs/git-worktree
//! [Jinja2]: https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_templating.html
//! [playbook]: https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_intro.html
//! [synced]: `std::fs::File::sync_all()`

use crate::cache;
use crate::git::Repository;
use crate::nice;
use crate::utils;
use crate::utils::log::DisplayCommand;
use crate::{Error, Result};
use semver::Version;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fmt;
use std::io;
use std::path::Path;
use std::process::Stdio;
use tokio::io::AsyncWriteExt;
use tokio::{fs, task};

const CACHE_NAME: &str = "ansible_config";
const CACHE_SCHEMA_HASH: cache::SchemaHash =
    cache::cache_schema_hash(include_bytes!("config.rs"), 0);
const REMOTE_BRANCH: &str = "master";

macro_rules! ansible_vars {
    ( $( $(#[$meta:meta])* $pub:vis $name:ident: $type:ty, $jinja_expr:expr ),*$(,)? ) => {
        /// Fields in `InstallConfig`.
        const INSTALL_CONFIG_FIELDS: &[&str] = &[
        $(
            stringify!($name),
        )*
        ];

        /// Variables which are extracted from Ansible.
        const ANSIBLE_JINJA_EXPR: &[&str] = &[
        $(
            $jinja_expr,
        )*
        ];

        /// Configuration as defined in Ansible's config.yml,
        /// global.yml and secrets2.yml.
        #[derive(Clone, Debug, Serialize, Deserialize)]
        pub struct InstallConfig {
        $(
           $(#[$meta])*
           $pub $name: $type,
        )*
        }
    };
}

ansible_vars! {
    /// Git branch in *nice2* repository deployed.
    pub branch: String, "branch",

    /// Name of the customer to which this installation belongs
    pub customer_name: String, "customer_name",

    /// API token for Cloudscale API.
    ///
    /// [API documentation](https://www.cloudscale.ch/en/api)
    pub cloudscale_api_token: String, "secrets2.cloudscale_write_api_token",

    /// Name of the customer in the *nice2* repository.
    ///
    /// Customer-specific code is located at `"customer/{customer_name_in_repo}/"`
    /// in *nice2* repository.
    pub customer_name_in_repo: String, "customer_name_in_repo",

    pub db_name: String, "db_name",
    pub db_server: String, "db_server",
    // default if not overridden for domain
    default_dkim_selector_name: String, "dkim_selector_name",
    // default if not overridden for domain
    default_dkim_value: String, "dkim_value",
    pub domains: Vec<String>, "routes.keys()",

    /// Password of Elasticsearch user "admin"
    pub elasticsearch_admin_password: String, "secrets2.elasticsearch_api_password",

    /// Elasticsearch servers ordered lexographically
    ///
    /// Endpoints are used round-robin style by Nice.
    pub elasticsearch_servers: Vec<String>, "elasticsearch_servers | sort",

    /// Elasticsearch index name
    pub elasticsearch_index_name: String, "elasticsearch_index_name",

    /// Whether an installation is prod or test
    ///
    /// Corresponds to `run_env_binary` in Ansible.
    pub environment: nice::Env, "run_env_binary",

    pub installation_name: String, "installation_name",

    /// Domains configured for outgoing mail.
    mail_domains: HashMap<String, Option<MailConfigInternal>>, "mail_domains",

    /// Configured memory
    ///
    /// This corresponds approximately to how much memory the
    /// application is expected to use during normal operations.
    ///
    /// This is the `memory` set in Ansible's config.yml.
    pub set_memory_bytes: u64, "memory|unit_to_bytes|int",

    /// Java heap memory
    ///
    /// Limit set via `java -xmx …`.
    pub heap_memory_bytes: u64, "memory_heap|unit_to_bytes|int",

    /// Memory request set on Kubernetes
    pub memory_request_bytes: u64, "memory_request|unit_to_bytes|int",

    /// Memory limit set on Kubernetes
    pub memory_limit_bytes: u64, "memory_limit|unit_to_bytes|int",

    pub solr_server: Option<String>, "solr_server",

    /// Version of Nice.
    #[serde(
        deserialize_with = "utils::deser::version",
        serialize_with = "utils::ser::version",
    )]
    pub version: Version, "version",
}

/// Settings for a mail domain (outgoing mail).
#[derive(Debug)]
pub struct MailConfig<'a> {
    pub name: &'a str,
    pub dkim_check_enabled: bool,
    pub spf_check_enabled: bool,
    pub dkim_selector_name: &'a str,
    pub dkim_value: &'a str,
}

/// Iterator over mail domain settings.
///
/// Obtain via [`InstallConfig::mail_domains()`].
pub struct MailConfigIter<'a> {
    iter: std::collections::hash_map::Iter<'a, String, Option<MailConfigInternal>>,
    config: &'a InstallConfig,
}

impl InstallConfig {
    #[must_use]
    pub fn mail_domains(&self) -> MailConfigIter<'_> {
        MailConfigIter {
            iter: self.mail_domains.iter(),
            config: self,
        }
    }

    /// Prettified version of Nice
    ///
    /// ```
    /// # #[tokio::main]
    /// # async fn main() {
    /// # #[cfg(feature = "test-with-secrets")] {
    /// use tco::ansible;
    ///
    /// let repo = ansible::git::repo().await.unwrap();
    /// let config = ansible::config::load(&repo).await.unwrap();
    ///
    /// // master is hardcoded to 1000
    /// let master = config.installations.get("master").unwrap();
    /// assert_eq!(format!("{}", master.version_pretty()), "v1000.0");
    ///
    /// let tocco = config.installations.get("tocco").unwrap();
    /// assert!(tocco.version_pretty().starts_with("v3."));
    /// # }
    /// # }
    /// ```
    #[must_use]
    pub fn version_pretty(&self) -> String {
        utils::version_pretty(&self.version)
    }
}

impl<'a> Iterator for MailConfigIter<'a> {
    type Item = (&'a str, MailConfig<'a>);

    fn next(&mut self) -> Option<(&'a str, MailConfig<'a>)> {
        match self.iter.next() {
            Some((domain, Some(settings))) => {
                let settings = MailConfig {
                    name: domain,
                    dkim_check_enabled: !settings.disable_dkim_check,
                    spf_check_enabled: !settings.disable_spf_check,
                    dkim_selector_name: settings
                        .dkim_selector_name
                        .as_deref()
                        .unwrap_or(&self.config.default_dkim_selector_name),
                    dkim_value: settings
                        .dkim_value
                        .as_deref()
                        .unwrap_or(&self.config.default_dkim_value),
                };
                Some((domain, settings))
            }
            Some((domain, None)) => {
                let settings = MailConfig {
                    name: domain,
                    dkim_check_enabled: true,
                    spf_check_enabled: true,
                    dkim_selector_name: &self.config.default_dkim_selector_name,
                    dkim_value: &self.config.default_dkim_value,
                };
                Some((domain, settings))
            }
            None => None,
        }
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
struct MailConfigInternal {
    #[serde(default)]
    disable_dkim_check: bool,

    #[serde(default)]
    disable_spf_check: bool,

    dkim_selector_name: Option<String>,

    dkim_value: Option<String>,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Config {
    /// Git commit ID matching the cache state.
    ///
    /// Used to invalidate cache.
    commit_id: Vec<u8>,

    pub installations: HashMap<String, InstallConfig>,
}

/// An Elasticsearch cluster
pub struct EsCluster<'a> {
    nodes: Vec<&'a str>,
    password: &'a str,
}

impl EsCluster<'_> {
    /// Lexicographically first node of the cluster
    #[must_use]
    pub fn first_node(&self) -> &str {
        self.nodes.first().expect("empty list of nodes")
    }

    /// List of all nodes of a cluster
    #[must_use]
    pub fn nodes(&self) -> &[&str] {
        &self.nodes
    }

    #[must_use]
    pub fn user_name(&self) -> &'static str {
        "admin"
    }

    /// Admin password for cluster
    #[must_use]
    pub fn password(&self) -> &str {
        self.password
    }
}

impl fmt::Debug for EsCluster<'_> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("EsCluster")
            .field("nodes", &self.nodes)
            .field("password", &"************")
            .finish()
    }
}

impl Config {
    /// Get list of all Elasticsearch clusters
    #[must_use]
    pub fn elasticsearch_clusters(&self) -> Vec<EsCluster> {
        let mut clusters: Vec<EsCluster> = Vec::new();
        for installation in self.installations.values() {
            let cluster = clusters.iter_mut().find(|cluster| {
                installation
                    .elasticsearch_servers
                    .iter()
                    .any(|server| cluster.nodes.contains(&&server[..]))
            });

            if let Some(cluster) = cluster {
                // Seen cluster, merge additional nodes if needed

                for server in &installation.elasticsearch_servers {
                    if !cluster.nodes.contains(&&server[..]) {
                        cluster.nodes.push(&server[..]);
                    }
                }
            } else {
                // Unseen cluster, add it

                let nodes: Vec<_> = installation
                    .elasticsearch_servers
                    .iter()
                    .map(|i| &i[..])
                    .collect();
                assert!(!nodes.is_empty());
                let password = &installation.elasticsearch_admin_password;
                clusters.push(EsCluster { nodes, password });
            }
        }
        for cluster in &mut clusters {
            cluster.nodes.sort_unstable();
        }
        clusters.sort_by(|a, b| a.first_node().cmp(b.first_node()));
        clusters
    }
}

/// Load Ansible configuration
///
/// Returns `cached_config` if still up-to-date. Otherwise, `ansible-playbook`
/// is ran to obtain the current configuration.
///
/// See also [module-level](`self`) documentation.
pub async fn load(repo: &Repository) -> Result<Config> {
    #[allow(deprecated)]
    let ansible_password_path = dirs::home_dir()
        .expect("user has no home")
        .join(".ansible-tocco-password");
    if !fs::try_exists(&ansible_password_path).await? {
        return Err(Error::Custom(format!(
            "file {} not found: Ansible not fully configured",
            ansible_password_path.display(),
        )));
    }

    let repo = repo.try_clone()?;
    task::spawn_blocking(move || {
        let commit_id = repo
            .as_inner()
            .resolve_reference_from_short_name(REMOTE_BRANCH)?
            .target()
            .expect("Git reference is symbolic");
        if let Some(cache) = cache::load::<Config>(CACHE_NAME, &CACHE_SCHEMA_HASH)? {
            if commit_id.as_bytes() == cache.commit_id {
                debug!("Cache hash matches, reusing cache.");
                return Ok(cache);
            }
        }
        info!("Current Ansible config not cached, running Ansible to generate …");
        let config = futures::executor::block_on(config_from_ansible(&repo, commit_id))?;
        cache::save(CACHE_NAME, &CACHE_SCHEMA_HASH, &config)?;
        Ok(config)
    })
    .await
    .unwrap()
}

async fn write_playbook(path: &Path) -> Result<()> {
    debug!("Writing Ansible playbook to {path:?}.");
    let file = fs::File::create(path).await?;
    let mut file = tokio::io::BufWriter::new(file);
    let content = br#"
- hosts: tocco_installations
  gather_facts: false
  tasks:
  - debug:
      msg: |
        {
"#;
    file.write_all(content).await?;
    for (name, expr) in INSTALL_CONFIG_FIELDS.iter().zip(ANSIBLE_JINJA_EXPR) {
        let name_json = serde_json::to_string(&name)
            .expect("cannot convert string to JSON")
            .into_bytes();
        file.write_all(b"          ").await?;
        file.write_all(&name_json).await?;
        file.write_all(b": {{ (").await?;
        file.write_all(expr.as_bytes()).await?;
        file.write_all(b") | default(none) | to_json }},\n").await?;
    }
    file.write_all(b"        }\n").await?;
    file.flush().await?;
    Ok(())
}

async fn config_from_ansible(repo: &Repository, commit_id: git2::Oid) -> Result<Config> {
    // Checking out `commit_id` here to ensure ID mentioned in
    // cache matches `worktree`. Git repository / branch could
    // have been update since.
    let worktree = repo.create_worktree_dir(&commit_id).await?;
    let ansible_dir = worktree.path().join("tocco");
    let playbook_path = ansible_dir.join("tmp.yml");
    write_playbook(&playbook_path).await?;

    let command = "ansible-playbook";
    let mut proc = tokio::process::Command::new(command);
    proc.arg("tmp.yml")
        .current_dir(&ansible_dir)
        .env("ANSIBLE_STDOUT_CALLBACK", "json")
        .env("ANSIBLE_DISPLAY_FAILED_STDERR", "yes")
        .env("PYTHONWARNINGS", "ignore")
        .kill_on_drop(true)
        .stdout(Stdio::piped());
    debug!("Executing command: {}", DisplayCommand(proc.as_std()));
    let proc = match proc.spawn() {
        Ok(proc) => proc,
        Err(e) if e.kind() == io::ErrorKind::NotFound => {
            return Err(Error::Custom(format!("failure executing {command:?}: {e}")));
        }
        Err(e) => return Err(e.into()),
    };
    let output = proc.wait_with_output().await?;
    if !output.status.success() {
        return Err(Error::Custom(format!(
            "failure executing {command:?}: {}",
            output.status
        )));
    }
    let installations = tokio::task::spawn_blocking(move || {
        let output = std::str::from_utf8(&output.stdout)?;
        let data: Root = serde_json::from_str(output)?;
        Ok::<_, Error>(parse_config(data))
    })
    .await
    .unwrap()?;
    Ok(Config {
        commit_id: commit_id.as_bytes().to_vec(),
        installations,
    })
}

#[derive(Debug, Deserialize)]
struct Root {
    plays: [Play; 1],
}

#[derive(Debug, Deserialize)]
struct Play {
    tasks: [Task; 1],
}

#[derive(Debug, Deserialize)]
struct Task {
    hosts: HashMap<String, Message>,
}

#[derive(Debug, Deserialize)]
struct Message {
    msg: InstallConfig,
}

fn parse_config(data: Root) -> HashMap<String, InstallConfig> {
    let [play] = data.plays;
    let [task] = play.tasks;
    task.hosts
        .into_iter()
        .map(|(host_name, host_output)| (host_name, host_output.msg))
        .collect()
}

#[cfg(test)]
mod test {
    use std::path::Path;

    // `cache_schema_hash()` assumes this file is called "config.rs" and
    // only a literal (not a not `const 'static &str`) can be passed to `include_bytes!()`.
    #[test]
    fn this_file() {
        let path = &Path::new(file!());
        assert_eq!(path.file_name().unwrap(), "config.rs");
    }
}

#[cfg(test)]
#[cfg(feature = "test-with-secrets")]
mod tests {
    #[cfg(feature = "nightly")]
    use tokio::runtime::Runtime;

    #[tokio::test]
    async fn env() {
        let config = test_utils::memory_cached_ansible_config().await;
        let prod = config.installations.get("tocco").unwrap().environment;
        let test = config.installations.get("toccotest").unwrap().environment;

        assert!(prod.is_prod());
        assert!(!prod.is_test());

        assert!(!test.is_prod());
        assert!(test.is_test());

        assert_eq!(format!("{prod}"), "prod");
        assert_eq!(format!("{test}"), "test");
    }

    #[bench]
    #[cfg(feature = "nightly")]
    fn cached_load(b: &mut crate::test::Bencher) {
        use crate::ansible;
        use test_utils::{LockMode, lock_caches_sync};

        let _guard = lock_caches_sync(LockMode::Shared);
        let rt = Runtime::new().unwrap();
        let repo = rt.block_on(async {
            let repo = ansible::git::repo().await.unwrap();
            ansible::config::load(&repo).await.unwrap();
            repo
        });
        b.iter(|| {
            rt.block_on(async {
                ansible::config::load(&repo).await.unwrap();
            });
        });
    }

    #[tokio::test]
    async fn elasticsearch_clusters() {
        let config = test_utils::memory_cached_ansible_config().await;
        let clusters = config.elasticsearch_clusters();
        let nodes: Vec<_> = clusters.iter().map(|c| c.nodes()).collect();

        assert_eq!(
            nodes,
            &[
                &[
                    "es1.prod.tocco.cust.vshn.net",
                    "es2.prod.tocco.cust.vshn.net",
                    "es3.prod.tocco.cust.vshn.net"
                ][..],
                &["es1.stage.tocco.cust.vshn.net"][..]
            ]
        );
    }

    #[tokio::test]
    async fn elasticsearch_clusters_missing_node() {
        let mut config = test_utils::memory_cached_ansible_config().await.clone();

        config
            .installations
            .values_mut()
            .find(|i| i.environment.is_prod())
            .unwrap()
            .elasticsearch_servers
            .remove(0);

        let clusters = config.elasticsearch_clusters();
        let nodes: Vec<_> = clusters.iter().map(|c| c.nodes()).collect();

        assert_eq!(
            nodes,
            &[
                &[
                    "es1.prod.tocco.cust.vshn.net",
                    "es2.prod.tocco.cust.vshn.net",
                    "es3.prod.tocco.cust.vshn.net"
                ][..],
                &["es1.stage.tocco.cust.vshn.net"][..]
            ]
        );
    }

    #[tokio::test]
    async fn elasticsearch_clusters_additional_node() {
        let mut config = test_utils::memory_cached_ansible_config().await.clone();

        let second_prod = config
            .installations
            .values_mut()
            .filter(|i| i.environment.is_prod())
            .nth(1)
            .unwrap();
        second_prod.elasticsearch_servers.remove(0);
        second_prod.elasticsearch_servers.truncate(1);
        second_prod
            .elasticsearch_servers
            .push("es666.prod.tocco.cust.vshn.net".to_string());

        let clusters = config.elasticsearch_clusters();

        let nodes: Vec<_> = clusters.iter().map(|c| c.nodes()).collect();

        assert_eq!(
            nodes,
            &[
                &[
                    "es1.prod.tocco.cust.vshn.net",
                    "es2.prod.tocco.cust.vshn.net",
                    "es3.prod.tocco.cust.vshn.net",
                    "es666.prod.tocco.cust.vshn.net",
                ][..],
                &["es1.stage.tocco.cust.vshn.net"][..]
            ]
        );
        assert_eq!(clusters[0].first_node(), "es1.prod.tocco.cust.vshn.net");
    }
}
