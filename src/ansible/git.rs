//! Manage [ansible][] Git repository.
//!
//! See also documentation of [git](`crate::git`).
//!
//! [ansible]: https://gitlab.com/toccoag/ansible

use crate::Result;
use crate::git::{self, RepoBuilder, Repository};
use std::path::PathBuf;
use std::time::Duration;

/// Name of cached Git repository
const CACHE_NAME: &str = "ansible";

/// Repository URL
pub const URL: &str = "ssh://git@gitlab.com/toccoag/ansible.git";

/// Path to *ansible* repository.
#[must_use]
pub fn repo_path() -> PathBuf {
    git::repo_cache_path(CACHE_NAME)
}

/// Obtain access to internally cached [ansible][] Git repository.
///
/// Repository is created as needed.
///
/// See also [`crate::git#important-repositories`]
///
/// [ansible]: https://gitlab.com/toccoag/ansible
pub async fn repo() -> Result<Repository> {
    let path = repo_path();
    RepoBuilder::new(path, URL).create_or_open().await
}

/// Obtain access to up-to-date, internally cached [ansible][] Git repository.
///
/// Repository is created as needed and updated (`git fetch`) iff
/// `interval` has expired since the last update. An interval of
/// `None` indicates that the repository should not be updated.
///
/// This a wrapper around [`repo()`] offering a convenient way to
/// update the repository directly.
///
/// See also [`crate::git#important-repositories`]
///
/// [ansible]: https://gitlab.com/toccoag/ansible
pub async fn maybe_updated_repo(interval: Option<Duration>) -> Result<Repository> {
    let mut repo = repo().await?;
    repo.update_every(&["master"], interval).await?;
    Ok(repo)
}

#[cfg(test)]
#[cfg(feature = "test-with-repo-access")]
mod tests {
    use super::*;
    use test_utils::{LockMode, lock_caches};

    #[tokio::test]
    async fn concurrency_safe_cloning() {
        let guard = lock_caches(LockMode::Exclusive).await;
        guard.drop_caches().await;
        let a = repo();
        let b = repo();
        let c = repo();
        tokio::try_join!(a, b, c).unwrap();
    }
}
