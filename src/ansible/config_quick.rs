//! A quick(er) but limited way to obtain Ansible configs
//!
//! # Limitations
//!
//! This is much faster than [`config`](`super::config`) but it will not
//! evaluate Jinja2 expressions (`{{ expr }}`) and it cannot obtain variables
//! from Ansible Vault.
//!
//! Some Jinja2 expressions are replaced with their corresponding value. Most
//! notably, `"{{ customer_name }}"` and `"{{ installation_name }}"`. This is done
//! via simple string replacement and is not easily applicable to other expressions.
//!
//! # Caching
//!
//! Caching uses same underlying mechanism used by `config`. See
//! [Caching](`crate::ansible::config#caching`).

use crate::Result;
use crate::cache;
use crate::git::Repository;
use crate::nice;
use crate::utils;
use crate::utils::log::{DisplayCommand, soft_unreachable};
use git2::Commit;
use semver::Version;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::io::Write;
use std::os::unix::fs::OpenOptionsExt;
use std::path::Path;
use std::process::Command;
use std::process::Stdio;
use std::result::Result as StdResult;
use tokio::task;

pub(crate) const CACHE_NAME: &str = "ansible_quick_config";
pub(crate) const CACHE_SCHEMA_HASH: cache::SchemaHash =
    cache::cache_schema_hash(include_bytes!("config_quick.rs"), 0);

/// Configuration as defined in Ansible's config.yml,
/// and global.yml.
#[derive(Debug, Deserialize, Serialize)]
pub struct InstallConfig {
    /// Git branch in *nice2* repository deployed.
    pub branch: String,

    /// Name of the customer to which this installation belongs
    pub customer_name: String,

    /// Name of the customer in the *nice2* repository.
    ///
    /// Customer-specific code is located at `"customer/{customer_name_in_repo}/"`
    /// in *nice2* repository.
    pub customer_name_in_repo: String,
    pub db_name: String,
    pub db_server: String,
    pub db_user: String,
    pub history_db_name: String,
    pub history_db_server: String,
    pub history_db_user: String,
    pub installation_name: String,

    /// Whether an installation is prod or test
    ///
    /// Corresponds to `run_env_binary` in Ansible.
    #[serde(rename = "run_env")]
    environment: String,

    pub sibling_installations: Vec<String>,
    pub solr_server: Option<String>,

    version: String,
}

impl InstallConfig {
    /// Whether an installation is prod or test
    ///
    /// Corresponds to `run_env_binary` in Ansible.
    #[must_use]
    pub fn environment(&self) -> nice::Env {
        match &self.environment[..] {
            "pilot" | "production" => nice::Env::Prod,
            "test" => nice::Env::Test,
            value => unreachable!("invalid environment: {value:?}"),
        }
    }

    /// Version of Nice
    ///
    /// ```
    /// # #[tokio::main]
    /// # async fn main() {
    /// # #[cfg(feature = "test-with-repo-access")] {
    /// use tco::ansible;
    ///
    /// let repo = ansible::git::repo().await.unwrap();
    /// let config = ansible::config_quick::load(&repo).await.unwrap();
    ///
    /// // master is hardcoded to 1000
    /// let master = config.installations.get("master").unwrap();
    /// assert_eq!(format!("{}", master.version()), "1000.0.0");
    ///
    /// let tocco = config.installations.get("tocco").unwrap();
    /// assert_eq!(tocco.version().major, 3);
    /// # }
    /// # }
    /// ```
    #[must_use]
    pub fn version(&self) -> Version {
        parse_version(&self.version, &self.branch)
            .inspect_err(|e| {
                soft_unreachable!(
                    "Failed to parse version of installation {}: {e}",
                    self.installation_name,
                );
            })
            .unwrap_or(Version::new(0, 0, 0))
    }

    /// Prettified version of Nice
    ///
    /// ```
    /// # #[tokio::main]
    /// # async fn main() {
    /// # #[cfg(feature = "test-with-repo-access")] {
    /// use tco::ansible;
    ///
    /// let repo = ansible::git::repo().await.unwrap();
    /// let config = ansible::config_quick::load(&repo).await.unwrap();
    ///
    /// // master is hardcoded to 1000
    /// let master = config.installations.get("master").unwrap();
    /// assert_eq!(format!("{}", master.version_pretty()), "v1000.0");
    ///
    /// let tocco = config.installations.get("tocco").unwrap();
    /// assert!(tocco.version_pretty().starts_with("v3."));
    /// # }
    /// # }
    /// ```
    #[must_use]
    pub fn version_pretty(&self) -> String {
        let version = self.version();
        utils::version_pretty(&version)
    }
}

fn extract_config_files(dir: &Path, repo: &Repository, commit: &Commit) -> Result<()> {
    let paths = [
        ("tocco/config.yml", "config.yml"),
        ("tocco/global.yml", "global.yml"),
        ("tocco/inventory.py", "inventory.py"),
    ];
    let tree = commit.tree().unwrap();

    for (source, target) in paths {
        let source = Path::new(&source);
        let source = tree.get_path(source).unwrap();
        let file_mode = source.filemode();
        let source = source.to_object(repo.as_inner()).unwrap();

        let target = dir.join(target);
        #[allow(clippy::cast_sign_loss)]
        let mut target = std::fs::OpenOptions::new()
            .create_new(true)
            .write(true)
            .mode(file_mode as u32)
            .open(target)?;

        target.write_all(source.as_blob().unwrap().content())?;
    }
    Ok(())
}

fn jinja2_replace(data: &mut HashMap<String, InstallConfig>) {
    for config in data.values_mut() {
        let customer_name = config.customer_name.clone();
        let installation_name = config.installation_name.clone();
        assert!(!installation_name.contains("{{"));
        let installation_name_underscored = config.installation_name.replace('-', "_");
        let db_name = config.db_name.replace(
            "{{ installation_name|replace('-', '_') }}",
            &installation_name_underscored,
        );
        assert!(!db_name.contains("{{"));
        let db_user = config.db_user.replace(
            "{{ installation_name|replace('-', '_') }}",
            &installation_name_underscored,
        );
        assert!(!db_user.contains("{{"));
        let fallback_environment = if config.installation_name == config.customer_name {
            "production"
        } else {
            "test"
        };
        let replace = |value: &String| {
            let value = value.replace("{{ customer_name }}", &customer_name);
            let value = value.replace("{{ installation_name }}", &installation_name);
            let value = value.replace(
                "{{ installation_name|replace('-', '_') }}",
                &installation_name_underscored,
            );
            let value = value.replace("{{ db_name }}", &db_name);
            let value = value.replace("{{ db_server|default('') }}", &config.db_server);
            let value = value.replace(
                "{%- if customer_name == installation_name -%}\n{%-   if (customer_name + 'test') \
                 not in sibling_installations -%}\n{#- \"prod\" is considered pilot until there \
                 is a corresponding test system -#}\npilot\n{%-   else -%}\nproduction\n{%-   \
                 endif -%}\n{%- else -%}\ntest\n{%- endif -%}",
                fallback_environment,
            );
            let value = value.replace("{{ db_user }}", &db_user);
            assert!(
                !value.contains("{{"),
                "unsupported Jinja2 expression in {value:?}"
            );
            value
        };

        // Working with the whole InstallConfig struct here to enusre
        // we get a compiler error when a field is added without updating
        // the code here.
        *config = InstallConfig {
            branch: replace(&config.branch),
            customer_name: replace(&config.customer_name),
            customer_name_in_repo: replace(&config.customer_name_in_repo),
            db_server: replace(&config.db_server),
            history_db_server: replace(&config.history_db_server),
            history_db_name: replace(&config.history_db_name),
            history_db_user: replace(&config.history_db_user),
            environment: replace(&config.environment),
            sibling_installations: config.sibling_installations.clone(),
            solr_server: config.solr_server.as_ref().map(|s| replace(s)),
            version: config.version.clone(),
            db_name,
            db_user,
            installation_name,
        };
    }
}

/// Load Ansible configuration
///
/// ```
/// # #[tokio::main]
/// # async fn main() {
/// # #[cfg(feature = "test-with-repo-access")] {
/// use tco::ansible;
///
/// let repo = ansible::git::repo().await.unwrap();
/// let config = ansible::config_quick::load(&repo).await.unwrap();
/// let master = config.installations.get("master").unwrap();
///
/// assert_eq!(master.customer_name, "test");
/// assert_eq!(master.customer_name_in_repo, "test");
/// assert_eq!(master.db_server, "db5.stage.tocco.cust.vshn.net");
/// assert_eq!(master.db_name, "nice_master");
/// assert_eq!(master.installation_name, "master");
/// # }
/// # }
/// ```
///
/// See also [module-level](`self`) documentation.
pub async fn load(repo: &Repository) -> Result<Config> {
    let repo = repo.try_clone()?;
    task::spawn_blocking(move || {
        let commit = repo
            .as_inner()
            .revparse_single("master")
            .unwrap()
            .into_commit()
            .unwrap();
        if let Some(cache) = cache::load::<Config>(CACHE_NAME, &CACHE_SCHEMA_HASH)? {
            if cache.commit_id == commit.id().as_bytes() {
                debug!("Ansible cache up-to-date, using it.");
                return Ok(cache);
            }
            debug!("Ansible cache outdated, reading config.");
        }
        let dir = utils::temp_dir()?;
        extract_config_files(dir.path(), &repo, &commit)?;

        // Avoid ETXTBSY by calling the interpreter and passing
        // the script as argument.
        //
        // Because this code may be called concurrently, by different threads,
        // it's possible for this process to fork() before "./inventory.py" is
        // closed for writing in the fork, leading to an ETXTBSY on execve().
        //
        // See also https://github.com/rust-lang/rust/issues/114554#issuecomment-2045980572
        let mut child = Command::new("python3");
        child
            .current_dir(&dir)
            .arg("./inventory.py")
            .arg("--list-compact")
            .stdout(Stdio::piped());
        debug!("Executing command: {}", DisplayCommand(&child));
        let child = child.spawn()?;
        let output = child.wait_with_output()?;
        assert!(output.status.success());
        let stdout = std::str::from_utf8(&output.stdout)?;
        let mut installations = serde_json::from_str(stdout)?;
        assert!(output.status.success());
        jinja2_replace(&mut installations);
        let config = Config {
            commit_id: commit.id().as_bytes().to_vec(),
            installations,
        };
        cache::save(CACHE_NAME, &CACHE_SCHEMA_HASH, &config)?;
        Ok(config)
    })
    .await
    .unwrap()
}

#[derive(Deserialize, Serialize)]
pub struct Config {
    commit_id: Vec<u8>,
    pub installations: HashMap<String, InstallConfig>,
}

fn parse_version(version: &str, branch: &str) -> StdResult<Version, String> {
    if !version.contains("{{") {
        lenient_semver::parse(version)
            .map_err(|e| format!("failed to parse {version:?} as version: {e}"))
    } else if let Some(version) = branch.strip_prefix("releases/") {
        lenient_semver::parse(version)
            .map_err(|e| format!("failed to extract version from release branch {branch:?}: {e}"))
    } else if branch == "master" {
        Ok(Version::new(1000, 0, 0))
    } else {
        Err(format!(
            r#"cannot extract version from version ({version:?}) or branch ({branch:?})"#
        ))
    }
}

#[cfg(test)]
#[cfg(feature = "test-with-repo-access")]
mod tests {
    use super::*;

    use crate::ansible;
    use std::fs;
    use std::io::{BufRead as _, BufReader};
    #[cfg(feature = "nightly")]
    use test_utils::lock_caches_sync;
    use test_utils::{LockMode, lock_caches};
    #[cfg(feature = "nightly")]
    use tokio::runtime::Runtime;

    #[tokio::test]
    async fn env() {
        let _guard = lock_caches(LockMode::Shared).await;
        let repo = ansible::git::repo().await.unwrap();
        let config = ansible::config_quick::load(&repo).await.unwrap();
        let prod = config.installations.get("tocco").unwrap().environment();
        let test = config.installations.get("toccotest").unwrap().environment();

        assert!(prod.is_prod());
        assert!(!prod.is_test());

        assert!(!test.is_prod());
        assert!(test.is_test());

        assert_eq!(format!("{prod}"), "prod");
        assert_eq!(format!("{test}"), "test");
    }

    // To work around ETXTBSY, the interpreter is called directly rather
    // than relying on the shebang. Thus, we need to check the shebang
    // didn't change.
    //
    // See comment about ETXTBSY above.
    #[tokio::test]
    async fn inventory_py_interpreter() {
        let dir = utils::temp_dir().unwrap();
        let _guard = lock_caches(LockMode::Shared).await;
        let repo = ansible::git::repo().await.unwrap();
        let commit = repo
            .as_inner()
            .revparse_single("master")
            .unwrap()
            .into_commit()
            .unwrap();
        extract_config_files(dir.path(), &repo, &commit).unwrap();

        let expected_shebang = "#!/usr/bin/env python3";
        let file = fs::File::open(dir.path().join("inventory.py")).unwrap();
        let file = BufReader::new(file);
        let found_shebang = file.lines().next().unwrap().unwrap();

        assert_eq!(expected_shebang, found_shebang);
    }

    #[tokio::test]
    async fn version() {
        let _guard = lock_caches(LockMode::Shared).await;
        let repo = ansible::git::repo().await.unwrap();
        let config = ansible::config_quick::load(&repo).await.unwrap();
        let master = config.installations.get("tocco").unwrap();
        assert!(master.version() >= Version::new(3, 10, 0));
    }

    #[test]
    fn parse_version() {
        assert_eq!(
            super::parse_version("1.2", "<unused>").unwrap(),
            Version::new(1, 2, 0),
        );
        assert_eq!(
            super::parse_version("{{ unused }}", "releases/3.10").unwrap(),
            Version::new(3, 10, 0),
        );
        assert_eq!(
            super::parse_version("{{ unused }}", "master").unwrap(),
            Version::new(1000, 0, 0),
        );

        assert!(
            super::parse_version("invalid.1.2", "<unused>")
                .unwrap_err()
                .starts_with(r#"failed to parse "invalid.1.2" as version: "#)
        );
        assert!(
            super::parse_version("{{ unused }}", "releases/not-a-version")
                .unwrap_err()
                .starts_with(
                    r#"failed to extract version from release branch "releases/not-a-version": "#
                )
        );
        assert!(super::parse_version("{{ unused }}", "<invalid>")
            .unwrap_err()
            .starts_with(
                r#"cannot extract version from version ("{{ unused }}") or branch ("<invalid>")"#
            ));
    }

    #[bench]
    #[cfg(feature = "nightly")]
    fn uncached_load(b: &mut crate::test::Bencher) {
        use super::*;

        let _guard = lock_caches_sync(LockMode::Exclusive);
        let rt = Runtime::new().unwrap();
        let repo = rt.block_on(async {
            let repo = ansible::git::repo().await.unwrap();
            // Make sure we have a cache to be removed on all runs.
            ansible::config_quick::load(&repo).await.unwrap();
            repo
        });
        let cache_path = cache::cache_file_with_name(CACHE_NAME);
        b.iter(|| {
            rt.block_on(async {
                tokio::fs::remove_file(&cache_path).await.unwrap();
                ansible::config_quick::load(&repo).await.unwrap();
            });
        });
    }

    #[bench]
    #[cfg(feature = "nightly")]
    fn cached_load(b: &mut crate::test::Bencher) {
        let _guard = lock_caches_sync(LockMode::Shared);
        let rt = Runtime::new().unwrap();
        let repo = rt.block_on(async {
            let repo = ansible::git::repo().await.unwrap();
            ansible::config_quick::load(&repo).await.unwrap();
            repo
        });
        b.iter(|| {
            rt.block_on(async {
                ansible::config_quick::load(&repo).await.unwrap();
            });
        });
    }
}
