//! Fetch information from Nice's client setting page
//!
//! Example page: <https://master.tocco.ch/nice2/rest/client/settings>.
//!
//! See [`get()`]

use crate::{Result, http_client, nice, utils};
use chrono::{DateTime, offset};
use semver::Version;
use serde::Deserialize;

/// Settings from /nice2/rest/client/settings
#[derive(Debug, Deserialize)]
pub struct Settings {
    /// Whether this is production or test.
    #[serde(rename = "runEnv")]
    pub environment: nice::Env,

    /// Shortened Git commit ID
    #[serde(rename = "niceRevision")]
    pub revision: String,

    /// Version of Nice
    #[serde(rename = "niceVersion")]
    #[serde(deserialize_with = "utils::deser::version")]
    pub version: Version,

    /// Time Nice was built.
    ///
    /// Note that Docker images are copied during
    /// production deployments. So, this correspond
    /// to the time this was deployed to test.
    #[serde(rename = "buildTimestamp")]
    #[serde(deserialize_with = "nice::deser::deserialize_millisec_timestamp")]
    pub build_timestamp: DateTime<offset::Local>,
}

impl Settings {
    /// Prettified version of Nice
    ///
    /// Version in this format: `"v3.12"`. Patch version is
    /// omitted even if present.
    ///
    /// ```
    /// # #[tokio::main]
    /// # async fn main() {
    /// use tco::nice::client_settings;
    ///
    /// let status = client_settings::get("https://master.tocco.ch")
    ///     .await
    ///     .unwrap();
    /// assert!(status.version_pretty().starts_with("v3."));
    /// # }
    /// ```
    #[must_use]
    pub fn version_pretty(&self) -> String {
        utils::version_pretty(&self.version)
    }
}

/// Fetch client settings from /nice2/rest/client/settings.
///
/// ```
/// # #[tokio::main]
/// # async fn main() {
/// use tco::nice::client_settings::get;
/// use tco::nice::Env;
///
/// let url = "https://master.tocco.ch";
/// let settings = get(url).await.unwrap();
/// assert_eq!(settings.revision.len(), 11);
/// assert!(settings
///     .revision
///     .chars()
///     .all(|c| matches!(c, '0'..='9'| 'a'..='f')));
/// assert_eq!(settings.environment, Env::Test);
/// # }
/// ```
pub async fn get(base_url: &str) -> Result<Settings> {
    let url = format!("{base_url}/nice2/rest/client/settings");
    Ok(http_client()
        .get(url)
        .send()
        .await?
        .error_for_status()?
        .json()
        .await?)
}

#[cfg(test)]
mod tests {
    use super::*;

    use semver::Version;

    #[tokio::test]
    async fn get() {
        let settings = super::get("https://master.tocco.ch").await.unwrap();
        assert_eq!(settings.environment, nice::Env::Test);
        assert!(settings.version >= Version::new(3, 10, 0));
        assert!(settings.revision.len() >= 11);
        assert!(
            settings
                .revision
                .chars()
                .all(|c| matches!(c, '0'..='9' | 'a'..='f'))
        );
        assert!(*settings.build_timestamp.to_string() > *"2024-04-21");
    }
}
