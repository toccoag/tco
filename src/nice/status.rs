//! Fetch information from Nice's status page
//!
//! Example page: <https://master.tocco.ch/status-tocco>.
//!
//! See [`get()`]

use crate::{Error, Result, http_client, utils};
use chrono::{DateTime, offset};
use regex::Regex;
use semver::Version;
use std::collections::HashMap;
use std::sync::LazyLock;

/// Information from /status-tocco
#[derive(Debug)]
pub struct Status {
    /// Time Nice was built.
    ///
    /// Note that Docker images are copied during
    /// production deployments. So, this correspond
    /// to the time this was deployed to test.
    pub build_time: DateTime<offset::Local>,

    /// Versions of client packages.
    ///
    /// Key is the package name (e.g. `"tocco-admin"`)
    /// and value the version (e.g. `"1.0.162-hotfix35.36"`.
    pub client_packages: HashMap<String, String>,

    /// Version of Nice
    ///
    /// See also [`Self::version_pretty()`].
    pub version: Version,

    /// Shortened Git commit ID
    pub revision: String,

    /// Host name (=pod name on OpenShift)
    pub host: String,

    /// Maximum permissible Java heap memory.
    pub max_mem: u64,

    /// Total used Java heap memory.
    ///
    /// Memory used by application or not collected yet.
    pub used_mem: u64,

    /// Total committed Java heap memory.
    ///
    /// Committed memory is memory allocated by the OS.
    pub total_mem: u64,

    /// Total committed but unused Java heap memory.
    ///
    /// Committed memory is memory allocated by the OS.
    pub free_mem: u64,
}

impl Status {
    /// Prettified version of Nice
    ///
    /// Version in this format: `"v3.12"`. Patch version is
    /// omitted even if present.
    ///
    /// ```
    /// # #[tokio::main]
    /// # async fn main() {
    /// use tco::nice::status;
    ///
    /// let status = status::get("https://master.tocco.ch").await.unwrap();
    /// assert!(status.version_pretty().starts_with("v3."));
    /// # }
    /// ```
    #[must_use]
    pub fn version_pretty(&self) -> String {
        utils::version_pretty(&self.version)
    }
}

/// Fetch status page from /status-tocco
///
/// ```
/// # #[tokio::main]
/// # async fn main() {
/// let url = "https://master.tocco.ch";
/// let status = tco::nice::status::get(url).await.unwrap();
/// assert!(status.host.starts_with("nice-"));
/// assert_eq!(status.revision.len(), 11);
/// assert!(status
///     .revision
///     .chars()
///     .all(|c| matches!(c, '0'..='9'| 'a'..='f')));
/// assert!(status.client_packages.contains_key("tocco-admin"));
/// # }
/// ```
pub async fn get(base_url: &str) -> Result<Status> {
    static REGEX: LazyLock<Regex> =
        LazyLock::new(|| Regex::new("(?:<br />|<p>)([^:<]+): ([^<]*)").unwrap());

    // db-check=false disables the write check for the DB and ensures
    // we still get a response even if the DB server is offline or
    // slow to respond.
    let url = format!("{base_url}/status-tocco?db-check=false");
    let body = http_client()
        .get(url)
        .send()
        .await?
        .error_for_status()?
        .text()
        .await?;

    if body == "maintenance mode\n" {
        // Maintenance page enabled (https://gitlab.com/toccoag/maintenance-page)
        return Err(Error::Custom("in maintenance mode".to_string()));
    }

    let (generic, client) =
        if let Some((generic, client)) = body.split_once("<p>Client package versions:") {
            (generic, client)
        } else {
            (&body[..], "")
        };

    let mut map = HashMap::new();
    for r#match in REGEX.captures_iter(generic) {
        let key = r#match.get(1).unwrap().as_str().to_ascii_lowercase();
        let value = r#match.get(2).unwrap().as_str().to_string();
        map.insert(key, value);
    }

    let mut client_packages = HashMap::new();
    for r#match in REGEX.captures_iter(client) {
        let key = r#match.get(1).unwrap().as_str().to_string();
        let value = r#match.get(2).unwrap().as_str().to_string();
        client_packages.insert(key, value);
    }

    Ok(Status {
        build_time: {
            let ts_pattern = "%d.%m.%Y %H:%M:%S%z";
            map.remove("build time")
                .ok_or_else(|| Error::Custom("missing build timestamp".to_string()))
                .and_then(|ts| {
                    Ok(
                        DateTime::<offset::FixedOffset>::parse_from_str(&ts, ts_pattern)
                            .map_err(|e| {
                                Error::Custom(format!("Failed to parse build timestamp: {e}"))
                            })?
                            .into(),
                    )
                })?
        },
        client_packages,
        version: {
            let version = map
                .remove("version")
                .ok_or_else(|| Error::Custom("version missing in response".to_string()))?;
            lenient_semver::parse(&version)
                .map_err(|e| Error::Custom(format!("failed to parse version: {e}")))?
        },
        revision: map
            .remove("revision")
            .ok_or_else(|| Error::Custom("revision missing in response".to_string()))?,
        host: map
            .remove("host")
            .ok_or_else(|| Error::Custom("host missing in response".to_string()))?,
        max_mem: map
            .remove("max memory")
            .ok_or_else(|| Error::Custom("max memory in response".to_string()))?
            .parse()
            .map_err(|_| Error::Custom("failed to parse max memory".to_string()))?,
        used_mem: map
            .remove("used memory")
            .ok_or_else(|| Error::Custom("used memory missing response".to_string()))?
            .parse()
            .map_err(|_| Error::Custom("failed to parse used memory".to_string()))?,
        total_mem: map
            .remove("total memory")
            .ok_or_else(|| Error::Custom("total memory missing response".to_string()))?
            .parse()
            .map_err(|_| Error::Custom("failed to parse total memory".to_string()))?,
        free_mem: map
            .remove("free memory")
            .ok_or_else(|| Error::Custom("free memory messing in response".to_string()))?
            .parse()
            .map_err(|_| Error::Custom("failed to parse free memory".to_string()))?,
    })
}
