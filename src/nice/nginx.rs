use crate::utils::deser;
use chrono::{DateTime, offset};
use owo_colors::AnsiColors;
use reqwest::StatusCode;
use serde::Deserialize;
use std::fmt;
use std::net::IpAddr;

/// Log level
#[derive(Clone, Copy, Debug, Eq, PartialEq, Ord, PartialOrd, Deserialize, clap::ValueEnum)]
#[serde(rename_all = "UPPERCASE")]
pub enum Method {
    Delete,
    Get,
    Head,
    Post,
    Patch,
    Put,
    Options,
}

impl fmt::Display for Method {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let s = match self {
            Method::Delete => "DELETE",
            Method::Get => "GET",
            Method::Head => "HEAD",
            Method::Post => "POST",
            Method::Patch => "PATCH",
            Method::Put => "PUT",
            Method::Options => "OPTIONS",
        };
        s.fmt(f)
    }
}

/// A log message for Nginx logs
#[derive(Debug, Deserialize)]
pub struct NginxMessage {
    #[serde(rename = "@timestamp")]
    pub timestamp: DateTime<offset::Local>,
    pub message: String,
    pub client_addr: IpAddr,
    pub status: u16,
    pub body_bytes_sent: u64,
    pub request_time: f32,
    #[serde(deserialize_with = "deser::non_empty_string")]
    pub referrer: Option<String>,
    #[serde(deserialize_with = "deser::non_empty_string")]
    pub user_agent: Option<String>,
    pub protocol: String,
    pub method: Method,
    pub scheme: String,
    pub host: String,
    pub uri: String,
    pub params: String,
    pub url: String,
    #[serde(deserialize_with = "deser::non_empty_string")]
    pub origin: Option<String>,
    #[serde(deserialize_with = "deser::non_empty_string")]
    pub business_unit: Option<String>,
    /// HTTP request ID
    #[serde(deserialize_with = "deser::non_empty_string")]
    pub request_id: Option<String>,
    #[serde(deserialize_with = "deser::u64_with_hyphen_as_none")]
    pub session_id: Option<u64>,
    #[serde(deserialize_with = "deser::non_empty_string")]
    pub user_name: Option<String>,
    #[serde(deserialize_with = "deser::u64_with_hyphen_as_none")]
    pub user_pk: Option<u64>,
    #[serde(deserialize_with = "deser::u64_with_hyphen_as_none")]
    pub api_key_pk: Option<u64>,
    #[serde(deserialize_with = "deser::non_empty_string")]
    pub language: Option<String>,
    #[serde(deserialize_with = "deser::non_empty_string")]
    pub client: Option<String>,
    #[serde(deserialize_with = "deser::non_empty_string")]
    pub timezone: Option<String>,
    #[serde(deserialize_with = "deser::non_empty_string")]
    pub sec_fetch_dest: Option<String>,
    #[serde(deserialize_with = "deser::non_empty_string")]
    pub sec_fetch_mode: Option<String>,
    #[serde(deserialize_with = "deser::non_empty_string")]
    pub sec_fetch_site: Option<String>,
    #[serde(deserialize_with = "deser::non_empty_string")]
    pub cache: Option<String>,
}

impl NginxMessage {
    /// Human readble status code string
    #[must_use]
    pub fn status_pretty(&self) -> String {
        StatusCode::from_u16(self.status)
            .ok()
            .and_then(|status| status.canonical_reason())
            .map(|reason| format!("{} - {reason}", self.status))
            .unwrap_or_else(|| format!("{} - (<unknown status code>)", self.status))
    }
}

/// Match color of sidebar to status code
#[must_use]
pub fn match_color_to_statuscode(state: u16) -> AnsiColors {
    match state {
        101 => AnsiColors::Default,
        100..=199 => AnsiColors::Blue,
        200..=299 => AnsiColors::Green,
        300..=399 => AnsiColors::BrightBlue,
        401 | 404 => AnsiColors::Yellow,
        400..=499 => AnsiColors::BrightYellow,
        500..=599 => AnsiColors::Red,
        _ => AnsiColors::Default,
    }
}

/// Parse a single JSON log message from Nginx
#[must_use]
pub fn parse_nginx_log_line(line: &str) -> Option<NginxMessage> {
    serde_json::from_str(line)
        .inspect_err(|e| debug!("Failed to parse line in Nginx logs as JSON: {e}"))
        .ok()
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::net::Ipv4Addr;

    #[test]
    fn parse_json_nginx_log_line_1() {
        let line = r#"
        {
            "@timestamp": "2024-06-03T14:06:59+00:00",
            "message": "OPTIONS /nice2/rest/widgets/UserGradesSearch/inputData/count?locale=de-CH HTTP/1.1",
            "client_addr": "193.8.106.45",
            "status": 200,
            "body_bytes_sent": 787,
            "request_time": 0.012,
            "referrer": "https://tocco.hkv.ch/de/meine-bildung/noten/",
            "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0",
            "protocol": "HTTP/1.1",
            "method": "OPTIONS",
            "scheme": "https",
            "host": "tocco.tocco.hkv.ch",
            "uri": "/nice2/rest/widgets/UserGradesSearch/inputData/count",
            "params": "locale=de-CH",
            "url": "https://tocco.tocco.hkv.ch/nice2/rest/widgets/UserGradesSearch/inputData/count?locale=de-CH",
            "origin": "https://tocco.hkv.ch",
            "business_unit": "",
            "request_id": "",
            "session_id": "",
            "user_name": "",
            "user_pk": "-",
            "api_key_pk": "-",
            "language": "",
            "client": "",
            "timezone": "",
            "sec_fetch_dest": "empty",
            "sec_fetch_mode": "cors",
            "sec_fetch_site": "same-site",
            "cache": ""
        }
        "#;
        let message: NginxMessage = serde_json::from_str(line).unwrap();
        let localhost_v4 = IpAddr::V4(Ipv4Addr::new(193, 8, 106, 45));
        assert_eq!(message.client_addr, localhost_v4);
        assert_eq!(message.method, Method::Options);
        assert_eq!(message.status, 200);
        assert!(message.user_name.is_none());
        assert!(message.user_pk.is_none());
        assert!(message.api_key_pk.is_none());
    }

    #[test]
    fn parse_json_nginx_log_line_2() {
        let line = r#"{"@timestamp": "2024-06-03T14:09:59+00:00", "message": "GET /nice2/rest/widget/configs/benutzermenu HTTP/1.1", "client_addr": "193.47.104.34", "status": 200, "body_bytes_sent": 223, "request_time": 0.034, "referrer": "https://tocco.hkv.ch/", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/125.0.0.0 Safari/537.36 Edg/125.0.0.0", "protocol": "HTTP/1.1", "method": "GET", "scheme": "https", "host": "tocco.tocco.hkv.ch", "uri": "/nice2/rest/widget/configs/benutzermenu", "params": "", "url": "https://tocco.tocco.hkv.ch/nice2/rest/widget/configs/benutzermenu", "origin": "https://tocco.hkv.ch", "business_unit": "", "request_id": "", "session_id": "", "user_name": "", "user_pk": "", "api_key_pk": "", "language": "", "client": "", "timezone": "", "sec_fetch_dest": "empty", "sec_fetch_mode": "cors", "sec_fetch_site": "same-site", "cache": "MISS"}
        "#;
        let message: NginxMessage = serde_json::from_str(line).unwrap();
        let expected_ip = IpAddr::V4(Ipv4Addr::new(193, 47, 104, 34));
        assert_eq!(message.client_addr, expected_ip);
        assert_eq!(message.method, Method::Get);
        assert_eq!(message.status, 200);
    }

    #[test]
    fn parse_json_nginx_log_line_with_user_details() {
        let line = r#"
        {
            "@timestamp": "2024-12-16T15:31:57+00:00",
            "message": "GET /nice2/rest/client/settings?locale=de HTTP/1.1",
            "client_addr": "2a0b:f4c2::5",
            "status": 200,
            "body_bytes_sent": 203,
            "request_time": 0.015,
            "referrer": "https://www.tocco.ch/intranet/Personelles/Benefits",
            "user_agent": "Mozilla/5.0 (X11; Linux x86_64; rv:128.0) Gecko/20100101 Firefox/128.0",
            "protocol": "HTTP/1.1",
            "method": "GET",
            "scheme": "https",
            "host": "www.tocco.ch",
            "uri": "/nice2/rest/client/settings",
            "params": "locale=de",
            "url": "https://www.tocco.ch/nice2/rest/client/settings?locale=de",
            "origin": "",
            "business_unit": "",
            "request_id": "gidupb",
            "session_id": "539029",
            "user_name": "pgerber",
            "user_pk": "2996",
            "api_key_pk": "1234",
            "language": "",
            "client": "widget",
            "timezone": "",
            "sec_fetch_dest": "empty",
            "sec_fetch_mode": "cors",
            "sec_fetch_site": "same-origin",
            "cache": "MISS"
        }
        "#;
        let message: NginxMessage = serde_json::from_str(line).unwrap();
        assert_eq!(message.user_name.as_deref(), Some("pgerber"));
        assert_eq!(message.user_pk, Some(2996));
        assert_eq!(message.api_key_pk, Some(1234));
    }
}
