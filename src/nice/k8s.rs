use crate::utils;
/// Deal with Nice running on Kubernetes
use chrono::{DateTime, offset};
use owo_colors::{AnsiColors, Style};
use serde::Deserialize;
use std::borrow::Cow;
use std::collections::HashMap;
use std::fmt;

// Log level
#[derive(Clone, Copy, Debug, Eq, PartialEq, Ord, PartialOrd, Deserialize, clap::ValueEnum)]
#[serde(rename_all = "UPPERCASE")]
pub enum Level {
    Fatal,
    #[value(alias = "err", alias = "e")]
    Error,
    #[value(alias = "warning", alias = "w")]
    Warn,
    #[value(alias = "i")]
    Info,
    #[value(alias = "d")]
    Debug,
    #[value(alias = "t")]
    Trace,
}

impl Level {
    #[must_use]
    pub fn term_styled(self) -> Box<dyn fmt::Display> {
        use owo_colors::OwoColorize as _;

        match self {
            Self::Fatal => {
                let style = Style::new().bold().on_red();
                Box::new("fatal".style(style))
            }
            Self::Error => {
                let style = Style::new().bold().red();
                Box::new("error".style(style))
            }
            Self::Warn => {
                let style = Style::new().bold().yellow();
                Box::new("warn".style(style))
            }
            Self::Info => Box::new("info".blue()),
            Self::Debug => Box::new("debug".green()),
            Self::Trace => Box::new("trace".bright_green()),
        }
    }

    #[must_use]
    pub fn color(self) -> AnsiColors {
        match self {
            Self::Fatal | Self::Error => AnsiColors::Red,
            Self::Warn => AnsiColors::Yellow,
            Self::Info => AnsiColors::Blue,
            Self::Debug => AnsiColors::Green,
            Self::Trace => AnsiColors::BrightGreen,
        }
    }
}

/// A log message
#[derive(Debug, Deserialize)]
pub struct Message {
    /// Meta information
    ///
    /// May contain user-supplied, untrusted text. Do not use blindly.
    #[serde(default, rename = "meta")]
    untrusted_meta: HashMap<String, String>,

    /// Stack trace
    ///
    /// May contain user-supplied, untrusted text. Do not use blindly.
    #[serde(rename = "stack_trace")]
    untrusted_stack_trace: Option<String>,

    /// Log message
    ///
    /// May contain user-supplied, untrusted text. Do not use blindly.
    #[serde(
        rename = "message",
        // In rare cases, a log message contains only a stack trace and no message.
        default,
    )]
    untrusted_message: String,

    pub thread_name: String,
    #[serde(rename = "@timestamp")]
    pub timestamp: DateTime<offset::Local>,
    pub version: String,
    pub revision: String,
    pub logger_name: String,
    pub level: Level,
}

impl Message {
    /// Log message sanitized for printing on terminal.
    ///
    /// Log message with most control characters removed. '\n', '\t',
    /// and '\r' are preserved.
    #[must_use]
    pub fn term_sanitized_message(&self) -> Cow<str> {
        utils::term_sanitize_string(&self.untrusted_message)
    }

    /// Stack trace sanitized for printing on terminal.
    ///
    /// Log message with most control characters removed. '\n', '\t',
    /// and '\r' are preserved.
    #[must_use]
    pub fn term_sanitized_stack_trace(&self) -> Option<Cow<str>> {
        self.untrusted_stack_trace
            .as_ref()
            .map(|trace| utils::term_sanitize_string(trace))
    }

    /// Stack trace with "at" lines removed
    ///
    /// Removes lines like ""at nice.core.….SomeClass(SomeClass.java:115)" and
    /// empty lines.
    #[must_use]
    pub fn term_sanitized_simplified_stack_trace(&self) -> Option<String> {
        self.untrusted_stack_trace.as_ref().map(|trace| {
            let simplified = simplify_trace(trace);
            utils::term_sanitize_string(&simplified).into_owned()
        })
    }

    /// Meta information sanitized for printing on terminal
    ///
    /// `Vec` containing `(key, value)` pairs. Data is returned
    /// in an arbitrary order.
    #[must_use]
    pub fn term_sanitized_meta(&self) -> Vec<(&str, Cow<str>)> {
        let user_name_field = "user_name";
        let client_ip_field = "clientip";
        let excluded_fields = &[user_name_field, client_ip_field];

        let mut result: Vec<_> = self
            .untrusted_meta
            .iter()
            .filter(|(k, _)| !excluded_fields.contains(&&k[..]))
            .map(|(k, v)| {
                let v = utils::term_sanitize_string(v);
                (&k[..], v)
            })
            .collect();

        // Be more caution with user name to avoid spoofing (e.g.
        // by using homographs). Use encoding that makes this
        // easy to detect.
        if let Some(user_name) = self.untrusted_meta.get(user_name_field) {
            let user_name = if user_name
                .chars()
                .all(|c| matches!(c, 'a'..='z'|'0'..='9'|'@'|'.'|'-'|'_'))
            {
                Cow::Borrowed(user_name.as_str())
            } else {
                Cow::Owned(format!(
                    "{:?} (unicode-escaped: \"{}\")",
                    user_name,
                    user_name.escape_default()
                ))
            };
            result.push((user_name_field, user_name));
        }

        if let Some(client_ip) = self.untrusted_meta.get(client_ip_field) {
            let client_ip = client_ip
                .parse()
                .map(|ip| Cow::Owned(utils::prettify_ip(ip)))
                .unwrap_or(Cow::Borrowed(&client_ip[..]));
            result.push((client_ip_field, client_ip));
        }

        result
    }
}

/// Parse a single JSON log message from Nice
#[must_use]
pub fn parse_nice_log_line(line: &str) -> Option<Message> {
    serde_json::from_str(line)
        .map(|mut msg: Message| {
            // These are sometimes set to "-" when they should be unset.
            remove_hyphen_valued(&mut msg, "session");
            remove_hyphen_valued(&mut msg, "user_pk");

            // trim superfluous newline
            if let Some(ref mut trace) = msg.untrusted_stack_trace {
                trace.truncate(trace.trim_end().len());
            }

            msg
        })
        .inspect_err(|e| debug!("Failed to parse line in Nice app logs as JSON: {e}."))
        .ok()
}

fn remove_hyphen_valued(msg: &mut Message, meta_name: &str) {
    match msg.untrusted_meta.get(meta_name) {
        Some(value) if value == "-" => {
            msg.untrusted_meta.remove(meta_name);
        }
        _ => {}
    }
}

#[must_use]
pub fn simplify_trace(trace: &str) -> String {
    let mut msg: String = trace
        .split_inclusive('\n')
        .filter(|line| !line.starts_with("\tat "))
        .filter(|line| !line.starts_with("\t... ") && !line.ends_with(" common frames omitted"))
        .collect();
    if msg.ends_with('\n') {
        msg.pop();
    }
    msg
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_json_nice_version1_logline1() {
        let line = r#"{"level":"WARN","logger_name":"org.directwebremoting.servlet.NotFoundHandler","user_pk":"5316","user_name":"mbaumann@tocco.ch","session":"516930","clientip":"178.198.176.103","message":"<Test_Message>","thread_name":"http-nio-8080-exec-8","@timestamp":"2023-11-16T09:18:40.032Z","version":"NICE2_VERSION_IS_UNDEFINED","revision":"NICE2_REVISION_IS_UNDEFINED","tags":["nice_log"],"level_name":"WARN"}
        "#;
        let line = parse_nice_log_line(line).unwrap();
        assert_eq!(&line.level, &Level::Warn);
        assert_eq!(&line.term_sanitized_message().to_string(), "<Test_Message>");
    }

    #[test]
    fn parse_json_nice_version1_logline2() {
        let line = r#"{"level":"ERROR","logger_name":"ch.tocco.nice2.netui.impl.dwr.ExceptionBarrierImpl","user_pk":"4860","request_id":"hnFik1","user_name":"aalmeida@tocco.ch","session":"516925","clientip":"145.14.209.96","message":"An exception occurred on the server.","stack_hash":"9b456a3c","stack_trace":"<Test_Trace>","thread_name":"http-nio-8080-exec-4","@timestamp":"2023-11-16T11:35:38.59Z","version":"NICE2_VERSION_IS_UNDEFINED","revision":"NICE2_REVISION_IS_UNDEFINED","tags":["nice_log"],"level_name":"ERROR"}"#;
        let line = parse_nice_log_line(line).unwrap();
        assert_eq!(&line.level, &Level::Error);

        let trace = line.term_sanitized_stack_trace().unwrap();
        assert_eq!(&trace, &"<Test_Trace>");
    }

    #[test]
    fn parse_json_logline_nice_version2_logline1() {
        let line = r#"{"level":"ERROR","logger_name":"ch.tocco.nice2.web.core.impl.servlet.script.JavaScriptLogServlet","meta":{"user_pk":"10779","user_name":"swuersten@tocco.ch","session":"26171","clientip":"83.150.12.146"},"message":"Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:120.0) Gecko/20100101 Firefox/120.0: IP:83.150.12.146; ERROR MESSAGE: msg is not defined \nSTACK: LogFile@webpack-internal:///./packages/actions/log/src/components/Log.js:39:7\nrenderWithHooks@http://localhost:3000/static/node_modules/react-dom/umd/react-dom.development.js:16315:29\nmountIndeterminateComponent@http://localhost:3000/static/node_modules/react-dom/umd/react-dom.development.js:20084:15\nbeginWork@http://localhost:3000/static/node_modules/react-dom/umd/react-dom.development.js:21597:18\nbeginWork$1@http://localhost:3000/static/node_modules/react-dom/umd/react-dom.development.js:27436:16\nperformUnitOfWork@http://localhost:3000/static/node_modules/react-dom/umd/react-dom.development.js:26570:14\nworkLoopSync@http://localhost:3000/static/node_modules/react-dom/umd/react-dom.development.js:26476:24\nrenderRootSync@http://localhost:3000/static/node_modules/react-dom/umd/react-dom.development.js:26444:9\nrecoverFromConcurrentError@http://localhost:3000/static/node_modules/react-dom/umd/react-dom.development.js:25860:36\nperformSyncWorkOnRoot@http://localhost:3000/static/node_modules/","thread_name":"http-nio-8080-exec-3","@timestamp":"2023-11-23T13:50:40.262Z","version":"3.9","revision":"bafe8f51af0","tags":["nice_log"]
    }"#;
        let line = parse_nice_log_line(line).unwrap();
        assert_eq!(&line.level, &Level::Error);
        assert_eq!(&line.untrusted_meta["user_pk"], "10779");
        assert_eq!(&line.untrusted_meta["user_name"], "swuersten@tocco.ch");
    }

    #[test]
    fn simplify_trace() {
        let full_trace = include_str!("sample_trace.txt").trim_end();
        let simple_trace = include_str!("sample_trace_simplified.txt").trim_end();
        assert_eq!(super::simplify_trace(full_trace), simple_trace);
    }

    #[test]
    fn terminal_sanitized() {
        let line = r#"{
            "level":"ERROR",
            "logger_name":"unused",
            "meta": {
                "user_name": "swuersten@tocco.ch\n\u0000",
                "code": "-\u001b-"
            },
            "message":"\u001b[5A",
            "stack_trace": "\u0005",
            "thread_name":"unused",
            "@timestamp":"2023-11-23T13:50:40.262Z",
            "version":"3.9",
            "revision":"bafe8f51af0"
        }"#;
        let line = parse_nice_log_line(line).unwrap();

        assert_eq!(&line.term_sanitized_message(), &"�[5A");
        assert_eq!(&line.term_sanitized_stack_trace().unwrap(), &"�");
        assert_eq!(&line.term_sanitized_simplified_stack_trace().unwrap(), &"�");

        let meta = line.term_sanitized_meta();
        let find = |name| {
            meta.iter()
                .find(|(k, _)| k == &name)
                .map(|(_, v)| v)
                .unwrap()
        };

        let user_name: &str = find("user_name");
        assert_eq!(
            &user_name,
            &"\"swuersten@tocco.ch\\n\\0\" (unicode-escaped: \"swuersten@tocco.ch\\n\\u{0}\")"
        );

        let code: &str = find("code");
        assert_eq!(&code, &"-�-");
    }
}
