//! Manage [nice2][] Git repository
//!
//! See also documentation of [git](`crate::git`).
//!
//! [nice2]: https://gitlab.com/toccoag/nice2

use crate::Result;
use crate::git::{self, RepoBuilder, Repository, UpdateOnce, git_time_to_chrono};
use git2::Oid;
use std::path::PathBuf;
use std::sync::LazyLock;
use std::time::Duration;

/// Oldest version in active use.
///
/// Corresponds to the version to which [`oldest_commit_deployed()`] belongs.
#[macro_export]
macro_rules! oldest_nice_version_deployed {
    () => {
        "3.1"
    };
}
pub use oldest_nice_version_deployed;

/// Oldest commit in active use.
///
/// Used internally to optimize searches (via [`git2::Repository::revwalk()`]). It's
/// rarely necessary to search past this commit. Note that `revwalk()` needs
/// to be used in combination with [`git2::Sort::TOPOLOGICAL`] to ensure search
/// is aborted in the right place.
#[must_use]
pub fn oldest_commit_deployed() -> &'static Oid {
    static VALUE: LazyLock<Oid> = LazyLock::new(|| {
        Oid::from_str("a1a435f25e040046bd12f1685aee7eb88fa7f4a6").expect("invalid object ID")
    });
    &VALUE
}

/// Name of cached Git repository
const CACHE_NAME: &str = "nice2";

pub const URL: &str = "ssh://git@gitlab.com/toccoag/nice2.git";

/// Path to *nice2* repository.
#[must_use]
pub fn repo_path() -> PathBuf {
    git::repo_cache_path(CACHE_NAME)
}

/// Obtain access to internally cached [nice2][] Git repository.
///
/// Repository is created as needed.
///
/// See also [`crate::git#important-repositories`]
///
/// [nice2]: https://gitlab.com/toccoag/nice2
pub async fn repo() -> Result<Repository> {
    let path = repo_path();
    RepoBuilder::new(path, URL).create_or_open().await
}

/// Obtain access to up-to-date, internally cached [nice2][] Git repository.
///
/// Repository is created as needed and updated (`git fetch`) iff
/// `interval` has expired since the last update. An interval of
/// `None` indicates that the repository should not be updated.
///
/// This a wrapper around [`repo()`] offering a convenient way to
/// update the repository directly.
///
/// See also [`crate::git#important-repositories`]
///
/// [nice2]: https://gitlab.com/toccoag/nice2
pub async fn maybe_updated_repo(interval: Option<Duration>) -> Result<Repository> {
    let mut repo = repo().await?;
    repo.update_every(&[], interval).await?;
    Ok(repo)
}

/// Return commit time of commit with given ID.
///
/// Updating the Git repository is slow. To avoid unnecessary
/// updates, `commit_id` is looked up in the repository immediately.
/// Only if the commit ID can't be found, is the repository
/// updated and the look up repeated.
///
/// [`Repository::update_once()`] is used to ensure
/// an update is attempted at most once.
///
/// `None` is returned when commit does not exist.
pub async fn commit_time(
    commit_id: &str,
    once: &UpdateOnce,
) -> Result<Option<chrono::DateTime<chrono::Local>>> {
    // `Repository` is not `Sync`. Hence, open a fresh one
    // in every thread.
    let mut repo = repo().await?;

    let already_updated_once_before_lookup = once.update_attempted();

    // Try finding commit …
    if let Some(commit) = look_up_commit(&repo, commit_id)? {
        return Ok(Some(git_time_to_chrono(commit.time())));
    }

    // … and if commit doesn't exist, update repository ….
    repo.update_once(&[], once).await?;
    if already_updated_once_before_lookup {
        // Only if repository was updated before our previous lookup
        // of `commit_id` can we be sure it saw the updated repository.

        return Ok(None);
    }

    // … and try again on updated repository
    let commit = look_up_commit(&repo, commit_id)?;
    let time = commit.map(|c| git_time_to_chrono(c.time()));
    Ok(time)
}

fn look_up_commit<'a>(repo: &'a Repository, commit_id: &str) -> Result<Option<git2::Commit<'a>>> {
    let object = match repo.as_inner().revparse_single(commit_id) {
        Ok(object) => object,
        Err(e) if e.class() == git2::ErrorClass::Reference => {
            // no such object
            return Ok(None);
        }
        Err(e) => {
            return Err(e.into());
        }
    };
    Ok(object.into_commit().ok())
}

#[cfg(test)]
#[cfg(feature = "test-with-repo-access")]
mod tests {
    use super::*;
    use test_utils::{LockMode, lock_caches};

    #[tokio::test]
    async fn time_when_no_such_commit() {
        let _guard = lock_caches(LockMode::Shared).await;
        assert!(
            commit_time(
                "03cfd743661f07975fa2f1220c5194cbaff48451",
                &UpdateOnce::new()
            )
            .await
            .unwrap()
            .is_none()
        );
    }

    #[tokio::test]
    async fn time_of_commit() {
        let _guard = lock_caches(LockMode::Shared).await;
        let time = commit_time(
            "6e1c47217fd177673880305aac6981ff50b9705b",
            &UpdateOnce::new(),
        )
        .await
        .unwrap()
        .unwrap();
        let time: chrono::DateTime<chrono::Utc> = time.into();
        assert_eq!(time.to_rfc3339(), "2023-02-08T13:55:37+00:00");
    }
}
