//! Deserialization of response values from Nice

use chrono::{DateTime, offset};
use serde::{Deserializer, de};
use std::fmt;

/// Deserialize date/time given in milliseconds since
/// [`UNIX_EPOCH`][].
///
/// [`UNIX_EPOCH`]: `std::time::UNIX_EPOCH`
pub fn deserialize_millisec_timestamp<'de, D>(de: D) -> Result<DateTime<offset::Local>, D::Error>
where
    D: Deserializer<'de>,
{
    struct TsVisitor;

    impl de::Visitor<'_> for TsVisitor {
        type Value = DateTime<offset::Local>;

        fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
            formatter.write_str("millisecond timestamp")
        }

        fn visit_u64<E>(self, v: u64) -> Result<Self::Value, E>
        where
            E: de::Error,
        {
            let v: i64 = v
                .try_into()
                .map_err(|_| de::Error::invalid_value(de::Unexpected::Unsigned(v), &self))?;
            self.visit_i64(v)
        }

        fn visit_i64<E>(self, v: i64) -> Result<Self::Value, E>
        where
            E: de::Error,
        {
            Ok(DateTime::from_timestamp_millis(v)
                .ok_or_else(|| de::Error::custom("timestamp out of range"))?
                .into())
        }
    }

    de.deserialize_any(TsVisitor)
}

#[cfg(test)]
mod tests {
    use super::*;
    use chrono::Local;
    use serde::Deserialize;

    #[derive(Deserialize)]
    struct Data(#[serde(deserialize_with = "deserialize_millisec_timestamp")] DateTime<Local>);

    #[test]
    #[allow(clippy::similar_names)]
    fn deser() {
        let test_data = [
            ("0", "1970-01-01T00:00:00Z"),
            ("1000", "1970-01-01T00:00:01Z"),
            ("1585885335001", "2020-04-03 03:42:15.001Z"),
        ];

        for (json, expected) in test_data {
            let data: Data = serde_json::from_str(json).unwrap();
            let date: DateTime<Local> = expected.parse().unwrap();
            assert_eq!(data.0, date);
        }
    }
}
