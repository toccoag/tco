/// Identify commits pending deployment
use crate::ansible::config_quick;
use crate::git::{Repository, git_time_to_chrono};
use crate::nice;
use crate::{Error, Result};
use git2::Oid;
use regex::Regex;
use std::sync::LazyLock;
use tokio::sync::Semaphore;
use tokio::task;

pub static HTTP_SEMAPHORE: Semaphore = Semaphore::const_new(25);

/// Deployment status of an installation
#[derive(Debug)]
pub struct InstallationStatus {
    pub installation_name: String,
    pub build_time: chrono::DateTime<chrono::Local>,
    pub deploy_source: UpdateSource,
    pub deployed: Commit,
    pub deployable: Commit,
    pub commits: Vec<Commit>,
    pub version: String,
}

/// A Git commit
#[derive(Debug)]
pub struct Commit {
    pub author: String,
    pub id: Oid,
    pub message: String,
    pub time: chrono::DateTime<chrono::Local>,
}

impl Commit {
    /// Jira tickets mentioned in commit message
    pub fn tickets(&self) -> Vec<&str> {
        static REGEX: LazyLock<Regex> =
            LazyLock::new(|| Regex::new(r"\<[A-Z][A-Z0-9]{1,18}-\d{1,5}\>").unwrap());
        REGEX
            .captures_iter(&self.message)
            .map(|c| c.get(0).unwrap().as_str())
            .collect()
    }
}

impl From<git2::Commit<'_>> for Commit {
    fn from(commit: git2::Commit) -> Self {
        Commit {
            author: commit.author().name().unwrap().to_owned(),
            id: commit.id(),
            message: commit.message().unwrap_or_default().to_owned(),
            time: git_time_to_chrono(commit.time()),
        }
    }
}

/// From where changes are applied on update.
#[derive(Debug)]
pub enum UpdateSource {
    /// When installation is updated, application is built from this Git branch.
    Branch(String),

    /// When installation is updated, this installation is copied.
    Installation(String),
}

/// Get list all customer-commits pending deployment
///
/// # Panics
///
/// Panics if no installation with `installation_name` exists in `config`.
pub async fn pending_for_installation(
    config: &config_quick::Config,
    nice_repo: &Repository,
    installation_name: &str,
) -> Result<InstallationStatus> {
    let settings = {
        let _semaphore = HTTP_SEMAPHORE.acquire().await.unwrap();
        nice::client_settings::get(&format!("https://{installation_name}.tocco.ch")).await?
    };
    let deployed = nice_repo
        .as_inner()
        .revparse_single(&settings.revision)?
        .into_commit()
        .unwrap();
    let inst_config = config.installations.get(installation_name).unwrap();
    let name_of_test = format!("{installation_name}test");
    let (deployable, source) = if config.installations.contains_key(&name_of_test) {
        let test_settings = {
            let _semaphore = HTTP_SEMAPHORE.acquire().await.unwrap();
            nice::client_settings::get(&format!("https://{name_of_test}.tocco.ch")).await?
        };
        let commit = nice_repo
            .as_inner()
            .revparse_single(&test_settings.revision)?
            .into_commit()
            .unwrap();
        (commit, UpdateSource::Installation(name_of_test))
    } else {
        let commit = nice_repo
            .as_inner()
            .revparse_single(&inst_config.branch)?
            .into_commit()
            .unwrap();
        (commit, UpdateSource::Branch(inst_config.branch.clone()))
    };
    let commits = {
        let nice_repo = nice_repo.try_clone()?;
        let customer_name_in_repo = inst_config.customer_name_in_repo.clone();
        let deployable_id = deployable.id();
        let deployed_id = deployed.id();
        task::spawn_blocking(move || {
            pending_in_range(
                &nice_repo,
                &customer_name_in_repo,
                deployable_id,
                deployed_id,
            )
        })
        .await
        .expect("worker thread panicked")?
    };

    let deployed_commit = Commit::from(deployed);
    Ok(InstallationStatus {
        installation_name: installation_name.to_owned(),
        build_time: settings.build_timestamp,
        deployable: Commit::from(deployable),
        deployed: deployed_commit,
        deploy_source: source,
        commits,
        version: settings.version_pretty(),
    })
}

/// Find customer-specific changes in range `deployable`..`deployed`
pub fn pending_in_range(
    nice_repo: &Repository,
    customer_in_repo: &str,
    deployable: Oid,
    deployed: Oid,
) -> Result<Vec<Commit>> {
    // Using "{deployed}~" (note the ~) to ensure the deployed commit is
    // included in the walk. If it isn't, somehow we are walking through a
    // tree that does not contain the `deployed` commit. Can happen when
    // a branch change wasn't committed to Ansible yet.
    let range = format!("{deployed}~..{deployable}");

    debug!("Searching for commits for customer {customer_in_repo} in range {range}.");
    let topology = git2::Sort::TOPOLOGICAL;
    let mut walker = nice_repo.as_inner().revwalk()?;
    walker.set_sorting(topology)?;
    walker.push_range(&range)?;

    let mut commits = Vec::new();
    let mut deployed_seen = false;
    for id in walker {
        let id = id?;
        if id == deployed {
            deployed_seen = true;

            // Filter out `deployed` commit and it's children. Some of its
            // children may appear here if `deployed` is a merge commit. This
            // break is correct only because the sort order guarantees parents
            // come before children.
            debug_assert_eq!(topology, git2::Sort::TOPOLOGICAL);
            break;
        }
        let commit = nice_repo
            .as_inner()
            .find_commit(id)
            .expect("commit yielded by walker missing in repo");
        if commit.parent_count() > 1 {
            trace!("Ignoring merge commit {id}.");
            continue;
        }
        let parent_commit = commit.parent(0)?;
        let diff = nice_repo.as_inner().diff_tree_to_tree(
            Some(&parent_commit.tree()?),
            Some(&commit.tree()?),
            Some(git2::DiffOptions::new().pathspec(format!("customer/{customer_in_repo}/*"))),
        )?;
        if diff.deltas().next().is_some() {
            commits.push(Commit::from(commit));
        }
    }
    if deployed_seen {
        Ok(commits)
    } else {
        Err(Error::Custom(format!(
            "deployed commit {deployed} not found in branch. Is the branch/version information in \
             Ansible up-to-date?"
        )))
    }
}

#[cfg(test)]
#[cfg(feature = "test-with-repo-access")]
mod tests {
    use super::*;

    use crate::ansible;
    use test_utils::{LockMode, lock_caches};

    async fn setup() -> (config_quick::Config, Repository) {
        let ansible_repo = ansible::git::repo().await.unwrap();
        let nice_repo = nice::git::repo().await.unwrap();
        let config = ansible::config_quick::load(&ansible_repo).await.unwrap();
        (config, nice_repo)
    }

    #[tokio::test]
    async fn tests() {
        let _guard = lock_caches(LockMode::Shared).await;
        let _ = env_logger::builder().is_test(true).try_init();
        tokio::join!(
            pending_for_installation_with_test_prod_setup(),
            pending_for_installation_for_test(),
            pending_for_installation_for_test_with_irregular_name(),
        );
    }

    // Called from tests() to avoid "runtime dropped the dispatch task" error
    async fn pending_for_installation_with_test_prod_setup() {
        let (config, nice_repo) = setup().await;
        let status = pending_for_installation(&config, &nice_repo, "tocco")
            .await
            .unwrap();

        assert_eq!(status.installation_name, "tocco");
        match &status.deploy_source {
            UpdateSource::Installation(name) => assert_eq!(name, "toccotest"),
            UpdateSource::Branch(_) => panic!(),
        }
    }

    // Called from tests() to avoid "runtime dropped the dispatch task" error
    async fn pending_for_installation_for_test() {
        let (config, nice_repo) = setup().await;
        let status = pending_for_installation(&config, &nice_repo, "toccotest")
            .await
            .unwrap();

        assert_eq!(status.installation_name, "toccotest");
        match &status.deploy_source {
            UpdateSource::Branch(name) => assert!(name.starts_with("releases/")),
            UpdateSource::Installation(_) => panic!(),
        }
    }

    // Called from tests() to avoid "runtime dropped the dispatch task" error
    async fn pending_for_installation_for_test_with_irregular_name() {
        let (config, nice_repo) = setup().await;
        let status = pending_for_installation(&config, &nice_repo, "master")
            .await
            .unwrap();

        assert_eq!(status.installation_name, "master");
        match &status.deploy_source {
            UpdateSource::Branch(name) => assert_eq!(name, "master"),
            UpdateSource::Installation(_) => panic!(),
        }
    }

    #[tokio::test]
    async fn test_pending_in_range() {
        let _ = env_logger::builder().is_test(true).try_init();
        let _guard = lock_caches(LockMode::Shared).await;
        let nice_repo = nice::git::repo().await.unwrap();
        let deployed = Oid::from_str("283af663568060c4dd19a64f81ba0aa20379ba2a").unwrap();
        let deployable = Oid::from_str("db6d237b87b4b73e1340d72dbce0e641338c6318").unwrap();
        let [ref commit1, ref commit2] =
            pending_in_range(&nice_repo, "pszh", deployable, deployed).unwrap()[..]
        else {
            panic!()
        };

        assert_eq!(
            commit1.id,
            Oid::from_str("309594e9f25ec4efbd45350c3fa13a6e14d0d3fd").unwrap()
        );
        assert!(commit1.author.starts_with("Jeffrey"));
        assert_eq!(commit1.tickets(), &["BS-9939"]);
        assert!(
            commit1
                .message
                .starts_with("add PszhSetRegistrationCommentResource for customer pszh")
        );
        assert_eq!(commit1.time.naive_utc().to_string(), "2023-11-29 18:26:11");

        assert_eq!(
            commit2.id,
            Oid::from_str("f99750497a81908caec9e4d9d654f480e282bd9b").unwrap()
        );
        assert!(commit2.author.starts_with("Yilmaz"));
        assert_eq!(commit2.tickets(), &["BS-10132"]);
        assert!(
            commit2
                .message
                .starts_with("adjust presence_list_for_sports_group (PSZH)")
        );
        assert_eq!(commit2.time.naive_utc().to_string(), "2023-09-09 08:18:11");
    }

    #[allow(clippy::similar_names)]
    #[tokio::test]
    async fn test_boundries_of_range() {
        let _ = env_logger::builder().is_test(true).try_init();
        let _guard = lock_caches(LockMode::Shared).await;
        let nice_repo = nice::git::repo().await.unwrap();

        // Adjacent commits (parent atop direct child).
        //
        // `commit2` modifies customer pszh and no others do.
        let commit1 = Oid::from_str("406c60e4df71673134db379658b4028003536280").unwrap();
        let commit2 = Oid::from_str("f57c348558120fc084ed850b49d6f31c82430420").unwrap();
        let commit3 = Oid::from_str("17bc38b9ca7a91baccf4779eabbe39f323a39823").unwrap();

        {
            let data = [
                (commit1, commit1),
                (commit2, commit2),
                (commit3, commit3),
                (commit2, commit1),
            ];
            for (deployed, deployable) in data {
                let commits = pending_in_range(&nice_repo, "pszh", deployable, deployed).unwrap();
                // Nothing to deploy
                assert!(commits.is_empty());
            }
        }

        {
            let deployed = commit3;
            let deployable = commit2;
            let [ref commit] =
                pending_in_range(&nice_repo, "pszh", deployable, deployed).unwrap()[..]
            else {
                panic!()
            };
            // `commit2` in need of deployment
            assert_eq!(commit.id, commit2);
        }
    }

    #[tokio::test]
    async fn regression_test_deployed_commit_is_merge_commit() {
        let _ = env_logger::builder().is_test(true).try_init();
        let _guard = lock_caches(LockMode::Shared).await;
        let nice_repo = nice::git::repo().await.unwrap();

        let deployed = Oid::from_str("09ce3de2338b326697a1abd260c895535b7736bd").unwrap();
        let deployable = Oid::from_str("45707f703883f61191365d2eb60dee32f3243c12").unwrap();
        let commits = pending_in_range(&nice_repo, "svz", deployable, deployed).unwrap();
        // Nothing to deploy
        assert!(commits.is_empty());
    }

    #[tokio::test]
    async fn test_invalid_branch() {
        let _ = env_logger::builder().is_test(true).try_init();
        let _guard = lock_caches(LockMode::Shared).await;
        let nice_repo = nice::git::repo().await.unwrap();

        // newer
        let deployed = Oid::from_str("94550ee6a17b41e810cf932165895e1069acbcd8").unwrap();

        // older
        let deployable = Oid::from_str("1789f1bdeccbb79d4092ad7d3abfed91475f4153").unwrap();

        let err = pending_in_range(&nice_repo, "pszh", deployable, deployed).unwrap_err();
        // Invalid, `deployed` not part of tree of `deployable`.
        assert_eq!(
            err.to_string(),
            "deployed commit 94550ee6a17b41e810cf932165895e1069acbcd8 not found in branch. Is the \
             branch/version information in Ansible up-to-date?"
        );
    }
}
