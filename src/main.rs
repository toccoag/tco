#![allow(clippy::cast_precision_loss)]
#![allow(clippy::cast_possible_truncation)]
#![allow(clippy::cast_sign_loss)]
#![allow(clippy::doc_markdown)]
#![allow(clippy::print_stderr)]
#![allow(clippy::print_stdout)]

mod subcommand;

#[macro_use]
extern crate log;

use clap::{Parser, Subcommand};
use std::env;
use std::process::ExitCode;
use std::time::Duration;
use subcommand::{
    client_deploy_status, config, customer_log, customer_prod_undeployed, customer_undeployed,
    db_backup, db_connect, db_copy, db_forward, db_info, db_queries, db_rename, db_ssh, dbs,
    deploy_status, elasticsearch, git_log_common, git_worktree, info, internals,
    list_installations, log as git_log, logs, mail, memory, pending_deployment, prod_undeployed,
    s3, sessions, status, undeployed,
};
use tco::Error;
use tokio::sync::Semaphore;

pub static HTTP_SEMAPHORE: Semaphore = Semaphore::const_new(0);

macro_rules! title_line {
    ($text:expr $(,)?) => {
        concat!("\x1b[1;4m", $text, "\x1b[22;24m\n")
    };
}

macro_rules! item_line {
    ($text:expr $(,)?) => {
        concat!("  \x1b[1m", $text, "\x1b[22m\n")
    };
}

macro_rules! line {
    () => {
        "\n"
    };
    ($text:expr $(,)?) => {
        concat!("          ", $text, "\n")
    };
}

macro_rules! after_help {
    () => {
        concat!(
            title_line!("Global Environment Variables and Config Files:"),
            line!(),
            item_line!("GIT_* / ~/.gitconfig"),
            line!("Various commands call `git` to show commits. Various environment"),
            line!("variables and `git config --global` are available to influence `git`."),
            line!(),
            line!("See git-config(1)."),
            line!(),
            item_line!("HTTP_PROXY"),
            item_line!("HTTPS_PROXY"),
            line!("Tunnel network traffic through proxy. Supported for most HTTP/HTTPS requests."),
            line!(),
            line!("Traffic can redirected through another machine by using SSH's built-in"),
            line!("SOCKS proxy functionality. First, open tunnel:"),
            line!(),
            line!("    ssh -D 3333 a.host.example.net -N &"),
            line!(),
            line!("Then set env. var.:"),
            line!(),
            line!(
                "    HTTP_RPOXY=socks5://localhost:3333 HTTPS_RPOXY=socks5://localhost:3333 tco \
                 ..."
            ),
            line!(),
            item_line!("RUST_LOG"),
            line!("Enable logging at given level. RUST_LOG=debug to enable debug"),
            line!("logging globally, for tco and libraries. TCO_LOG=tco=trace to"),
            line!("enable trace logging for tco only."),
            line!(),
            line!("See https://docs.rs/env_logger/latest/env_logger/#enabling-logging"),
            line!(),
            item_line!("TCO_SSH_VERBOSITY"),
            line!("Debug level for SSH. Expects a value between 0 (disabled) and 3 (max."),
            line!("debug level). Value corresponds to the number of \"-v\" args passed"),
            line!("to ssh."),
            line!(),
            item_line!("~/.ssh/config"),
            line!("`ssh` is called to connect to DB servers and otherwise interact"),
            line!("with servers. ~/.ssh/config can be used to influence ssh."),
        )
    };
}

#[derive(Parser)]
#[command(after_long_help = after_help!(), max_term_width = 100)]
pub struct Args {
    #[command(subcommand)]
    command: Commands,

    /// Update interval for locally cached Git repositories.
    ///
    /// Update repositories that have not been updated for given seconds
    /// before use. Use -1 to disable updates completely.
    #[arg(
        long,
        short,
        default_value_t = 900,
        value_parser(clap::value_parser!(i64).range(-1..)),
        allow_hyphen_values = true,
    )]
    update_repos_secs: i64,

    /// Set log level for this crate
    ///
    /// If --global-log-level is set to a higher log level than this, it
    /// takes precedence.
    ///
    /// Ignored if RUST_LOG env. var. is set.
    #[arg(value_enum, long, short, default_value_t = LogLevel::Info)]
    log_level: LogLevel,

    /// Set global log level
    ///
    /// Log level for this crate and all it's dependencies. A higher
    /// log level for this crate may be set via --log-level.
    ///
    /// RUST_LOG env. var. may override this setting.
    #[arg(value_enum, long, short = 'L', default_value_t = LogLevel::Info)]
    global_log_level: LogLevel,

    /// Max. concurrent HTTP requests
    ///
    /// Limit number of simultaneous HTTP requests. Subcommands executing
    /// large number of simultaneous HTTP requests will try not to exceed
    /// this number of in-flight requests.
    #[arg(
        long,
        default_value_t = 50,
        value_parser(clap::value_parser!(u32).range(1..=10_000)),
    )]
    http_concurrency: u32,

    #[arg(long, short, default_value_t = clap::ColorChoice::Auto)]
    color: clap::ColorChoice,
}

struct LogLevels {
    tco: Option<log::LevelFilter>,
    global: log::LevelFilter,
}

impl Args {
    fn level_filters(&self) -> LogLevels {
        let tco = self.log_level.level_filter();
        let global = self.global_log_level.level_filter();

        LogLevels {
            tco: (tco > global).then_some(tco),
            global,
        }
    }

    fn update_repos_interval(&self) -> Option<Duration> {
        if self.update_repos_secs >= 0 {
            Some(Duration::from_secs(self.update_repos_secs as u64))
        } else {
            None
        }
    }
}

// TODO Group by categories once supported by clap:
//     https://github.com/clap-rs/clap/issues/1553
#[derive(Subcommand)]
enum Commands {
    /// Show raw Ansible config (raw config.yml)
    ///
    /// If INSTALLATION is given, the config of the installation
    /// itself and the corresponding customer are printed.
    ///
    /// If INSTALLATION is omitted, the whole config.yml
    /// is printed.
    #[command(visible_alias = "cfg", visible_alias = "conf")]
    Config(config::Args),

    /// Show all customer-specfic commits deployed.
    ///
    /// Shows all non-merge commits in cusotmer/${name} which
    /// have been deployed.
    #[command(visible_alias = "cl", visible_alias = "customer-log")]
    GitCustomerLog(git_log_common::Args),

    /// Show all commits deployed.
    ///
    /// Shows all non-merge commits deployed excluding those
    /// specific to other customers.
    #[command(visible_alias = "l", visible_alias = "log")]
    GitLog(git_log_common::Args),

    /// Customer-specific commits not yet deployed.
    ///
    /// Shows all non-merge commits in customer/${name} in the corresponding
    /// release branch not yet deployed.
    #[command(visible_alias = "cu", visible_alias = "customer-undeployed")]
    GitCustomerUndeployed(git_log_common::Args),

    /// Commits not yet deployed.
    ///
    /// Shows all non-merge commits in the corresponding release branch not
    /// yet deployed with the exception of those only affecting other
    /// customers.
    #[command(visible_alias = "u", visible_alias = "undeployed")]
    GitUndeployed(git_log_common::Args),

    /// Customer-specific commits not yet deployed to prod but already on
    /// test.
    ///
    /// Specify the production system as `installation`.
    ///
    /// Shows all non-merge commits in customer/${name} on test but
    /// but not yet on prod.
    #[command(visible_alias = "cpu", visible_alias = "customer-prod-undeployed")]
    GitCustomerProdUndeployed(git_log_common::Args),

    /// Commits not yet deployed to prod but already on prod.
    ///
    /// Specify the production system as `installation`.
    ///
    /// Shows all non-merge commits on test not yet deployed to prod with the
    /// exception of those only affecting other customers.
    #[command(visible_alias = "pu", visible_alias = "prod-undeployed")]
    GitProdUndeployed(git_log_common::Args),

    /// Show where specified commit is deployed (nice2 repo).
    #[command(
        visible_alias = "ds",
        long_about = concat!(
            "Show where specified commit is deployed (nice2 repo).\n\n",
            "For performance reasons, commits before version ",
            tco::nice::git::oldest_nice_version_deployed!(), " are ignored."
        ),
    )]
    DeployStatus(deploy_status::Args),

    /// Show where specified commit is deployed (tocco-client repo).
    #[command(
        visible_alias = "cds",
        long_about = concat!(
            "Show where specified commit is deployed (tocco-client repo).\n\n",
            "For performance reasons, commits before version ",
            tco::client::git::oldest_client_version_deployed!(), " are ignored."
        ),
    )]
    ClientDeployStatus(client_deploy_status::Args),

    ///
    /// Show all commits modifying `customer/{customer_name}` that will be
    /// rolled out during a deployment.
    ///
    /// Also shows when a customer was last deployed. More precisely,
    /// when the currently deployed application was built.
    ///
    /// Caveats:
    ///
    /// Any commit touching `customer/{customer_name}` in the nice2
    /// repository is listed. This sometimes includes changes
    /// not specfic to the customer.
    ///
    /// Exit codes:
    ///
    /// 0 -> No pending commits found.
    /// 2 -> Pending commits found.
    /// other -> An error occurred.
    #[command(visible_alias = "pd", verbatim_doc_comment)]
    PendingDeployment(pending_deployment::Args),

    /// Print logs from Kubernetes
    ///
    /// Be sure to be logged in:
    ///
    ///     $ oc login -w https://api.c-tocco-ocp4.tocco.ch:6443
    ///
    /// Pipe colored output to less:
    ///
    ///     $ lesstty tco logs
    ///
    ///     (apt-get install colorized-logs)
    ///
    /// Alternatively, pipe to less directly:
    ///
    ///     $ tco --color always logs | less -RF
    #[command(verbatim_doc_comment)]
    Logs(logs::Args),

    /// Interact with DB backups (experimental)
    ///
    /// A client for tocco-backup-transfer, see
    /// https://gitlab.com/toccoag/tocco-backup-transfer.
    #[command(visible_alias = "dbb")]
    DbBackup(db_backup::Args),

    /// Open `psql` terminal connected to an installation's DB
    #[command(visible_alias = "db")]
    DbConnect(db_connect::Args),

    /// Copy a database on a server or between servers
    ///
    /// Examples:
    ///
    /// Copy installation tocco to localhost:
    ///
    ///     $ tco db-copy tocco
    ///
    /// Copy installation tocco to localhost and call target DB nice_tocco_bugfix:
    ///
    ///     $ tco db-copy tocco /nice_tocco_bugfix
    ///
    /// Copy from prod to test DB:
    ///
    ///     $ tco db-copy tocco toccotest
    ///
    /// Copy nice_tocco from db3.prod to db5.prod setting nice_tocco as owner:
    ///
    ///     $ tco db-copy db3.prod/nice_tocco nice_tocco@db5.prod
    ///
    /// Copy nice_tocco from db3.prod to db5.prod using owner of source for target:
    ///
    ///     $ tco db-copy --owner-mode copy db3.prod/nice_tocco db5.prod
    ///
    /// Copy tocco DB to server where toccotest resides and call new DB nice_toccotest_new:
    ///
    ///     $ tco db-copy tocco toccotest/nice_toccotest_new
    #[command(visible_alias = "cp", verbatim_doc_comment)]
    DbCopy(db_copy::Args),

    /// Forward Postgres Unix socket to local machine
    ///
    /// Forwards Unix socket belonging to DB server associated
    /// with given INSTALLATION and then executes given command
    /// setting various environment variables to help connect
    /// to the forwarded socket.
    ///
    /// Environment variables available to COMMAND:
    ///
    /// • PGDATABASE - database name
    /// • PGHOST - forwarded unix socket
    /// • PGPORT - forwarded unix socket
    /// • PGUSER - your user name (based on SSH user)
    ///
    /// This variables are understood by libpq and used
    /// as default connection parameters.
    ///
    /// See https://www.postgresql.org/docs/current/libpq-envars.html
    ///
    /// Example Python script:
    ///
    ///     #!/usr/bin/python3
    ///     import psycopg2
    ///
    ///     # connect() will use information from PG* env. vars.
    ///     # unless set explicitly (e.g. connect(database="postgres")).
    ///     with psycopg2.connect() as conn, conn.cursor() as cur:
    ///         cur.execute('SELECT current_database()')
    ///         print(cur.fetchone()[0])
    #[command(verbatim_doc_comment)]
    DbForward(db_forward::Args),

    /// Print information about installation's database
    #[command(visible_alias = "dbi")]
    DbInfo(db_info::Args),

    /// Rename main and history DBs
    ///
    /// Indented for renaming tests systems. For instance, to rename
    /// "exampletest" to "exampletestold". Both main and history DBs
    /// are renamed and the owner is adjusted as needed.
    ///
    /// Example:
    ///
    /// tco db-rename exampletest exampletestold
    ///
    /// Only "exampletest" has to exist in Ansible (i.e. be pushed
    /// to master branch), setting for corresponding "exampletestold"
    /// and "exampletestnew" are infered.
    #[command()]
    DbRename(db_rename::Args),

    /// Open SSH terminal on installation's DB server
    #[command()]
    DbSsh(db_ssh::Args),

    /// Show running DB queries
    ///
    /// More precisely, list all connections in an active transaction. This
    /// may include connections idle in transaction.
    ///
    /// See also:
    ///
    /// • Output is based on the pg_stat_activity view:
    ///
    ///   https://www.postgresql.org/docs/current/monitoring-stats.html#MONITORING-PG-STAT-ACTIVITY-VIEW
    ///
    /// • Field "wait" corresponds to the Wait Event Type:
    ///
    ///   https://www.postgresql.org/docs/current/monitoring-stats.html#WAIT-EVENT-ACTIVITY-TABLE
    #[command(verbatim_doc_comment)]
    DbQueries(db_queries::Args),

    /// Commands for working with all DBs
    Dbs(dbs::Args),

    #[doc = elasticsearch::help_doc_elastic_vpn_hint!()]
    #[command(
        about = "Interact with Elastichsearch",
        long_about = concat!(
            "Interact with Elastichsearch\n",
            "\n",
            elasticsearch::help_doc_elastic_vpn_hint!(),
        ),
        visible_alias = "es",
        verbatim_doc_comment
    )]
    Elasticsearch(elasticsearch::Args),

    /// Manage Git working tree
    ///
    /// Internally, a persistent copy of the selected repository
    /// is created automatically and working trees are created
    /// referencing this, shared repository.
    GitWorktree(git_worktree::Args),

    /// Show Ansible settings / variables.
    ///
    /// Specify `var` to for use in script. When extracting
    /// multiple variables, consider using `tco --no-update-repos …`
    /// for on all but the first call to avoid delays as result of
    /// automatic Git repo updates.
    Info(info::Args),

    /// Internal commands for debugging
    Internals(internals::Args),

    /// Mail related subcommands
    Mail(mail::Args),

    /// Show memory settings for all installations
    #[command(visible_alias = "mem")]
    Memory(memory::Args),

    /// S3-related subcommands
    S3(s3::Args),

    /// List all installations
    #[command(visible_alias = "li")]
    ListInstallations(list_installations::Args),

    /// Show logged in users and latest logins
    ///
    /// To see latest logins across all installations,
    /// use `tco dbs latest-login`.
    ///
    /// column KEY: "x" if session belongs to an API key
    #[command(visible_alias = "ses")]
    Sessions(sessions::Args),

    /// Show installation status
    Status(status::Args),

    /// Print version information
    Version,
}

#[derive(Clone, clap::ValueEnum, Eq, Ord, PartialEq, PartialOrd)]
enum LogLevel {
    Error,
    Warn,
    Info,
    Debug,
    Trace,
}

impl LogLevel {
    fn level_filter(&self) -> log::LevelFilter {
        match self {
            LogLevel::Trace => log::LevelFilter::Trace,
            LogLevel::Debug => log::LevelFilter::Debug,
            LogLevel::Info => log::LevelFilter::Info,
            LogLevel::Warn => log::LevelFilter::Warn,
            LogLevel::Error => log::LevelFilter::Error,
        }
    }
}

#[tokio::main]
async fn main() -> ExitCode {
    let args = Args::parse();

    match args.color {
        clap::ColorChoice::Always => colorchoice::ColorChoice::Always,
        clap::ColorChoice::Auto => colorchoice::ColorChoice::Auto,
        clap::ColorChoice::Never => colorchoice::ColorChoice::Never,
    }
    .write_global();

    HTTP_SEMAPHORE.add_permits(args.http_concurrency as usize);

    let mut builder = env_logger::Builder::new();
    let levels = args.level_filters();
    builder.filter_level(levels.global);
    match levels.tco {
        Some(level) if env::var_os("RUST_LOG").is_none() => {
            builder.filter_module(module_path!(), level);
        }
        _ => (),
    }
    builder.parse_default_env();
    if args.log_level <= LogLevel::Info && env::var_os("RUST_LOG").is_none() {
        builder.format_timestamp(None);
        builder.format_target(false);
    }
    builder.init();

    if let Err(e) = migrate_cache_to_cache_v2().await {
        error!("Cache v2 migration failed: {e}");
        return ExitCode::FAILURE;
    }

    let result = match &args.command {
        Commands::ClientDeployStatus(subcmd_args) => {
            client_deploy_status::main(&args, subcmd_args).await
        }
        Commands::DeployStatus(subcmd_args) => deploy_status::main(&args, subcmd_args).await,
        Commands::Config(subcmd_args) => config::main(&args, subcmd_args).await,
        Commands::DbBackup(subcmd_args) => db_backup::main(&args, subcmd_args).await,
        Commands::DbConnect(subcmd_args) => db_connect::main(&args, subcmd_args).await,
        Commands::DbCopy(subcmd_args) => db_copy::main(&args, subcmd_args).await,
        Commands::DbForward(subcmd_args) => db_forward::main(&args, subcmd_args).await,
        Commands::DbInfo(subcmd_args) => db_info::main(&args, subcmd_args).await,
        Commands::DbRename(subcmd_args) => db_rename::main(&args, subcmd_args).await,
        Commands::DbSsh(subcmd_args) => db_ssh::main(&args, subcmd_args).await,
        Commands::DbQueries(subcmd_args) => db_queries::main(&args, subcmd_args).await,
        Commands::Dbs(subcmd_args) => dbs::main(&args, subcmd_args).await,
        Commands::Elasticsearch(subcmd_args) => elasticsearch::main(&args, subcmd_args).await,
        Commands::GitCustomerLog(subcmd_args) => customer_log::main(&args, subcmd_args).await,
        Commands::GitCustomerProdUndeployed(subcmd_args) => {
            customer_prod_undeployed::main(&args, subcmd_args).await
        }
        Commands::GitCustomerUndeployed(subcmd_args) => {
            customer_undeployed::main(&args, subcmd_args).await
        }
        Commands::GitProdUndeployed(subcmd_args) => prod_undeployed::main(&args, subcmd_args).await,
        Commands::GitWorktree(subcmd_args) => git_worktree::main(&args, subcmd_args).await,
        Commands::GitUndeployed(subcmd_args) => undeployed::main(&args, subcmd_args).await,
        Commands::Info(subcmd_args) => info::main(&args, subcmd_args).await,
        Commands::Internals(subcmd_args) => internals::main(&args, subcmd_args).await,
        Commands::Mail(subcmd_args) => mail::main(&args, subcmd_args).await,
        Commands::Memory(subcmd_args) => memory::main(&args, subcmd_args).await,
        Commands::ListInstallations(subcmd_args) => {
            list_installations::main(&args, subcmd_args).await
        }
        Commands::GitLog(subcmd_args) => git_log::main(&args, subcmd_args).await,
        Commands::PendingDeployment(subcmd_args) => {
            pending_deployment::main(&args, subcmd_args).await
        }
        Commands::Logs(subcmd_args) => logs::main(&args, subcmd_args).await,
        Commands::S3(subcmd_args) => s3::main(&args, subcmd_args).await,
        Commands::Sessions(subcmd_args) => sessions::main(&args, subcmd_args).await,
        Commands::Status(subcmd_args) => status::main(&args, subcmd_args).await,
        Commands::Version => {
            print_version();
            Ok(())
        }
    };
    match result {
        Ok(()) => ExitCode::SUCCESS,
        Err(Error::CustomExitStatus(code)) => ExitCode::from(code as u8),
        Err(e) => {
            eprintln!("error: {e}.");
            ExitCode::FAILURE
        }
    }
}

fn print_version() {
    print!("{} {}", clap::crate_name!(), clap::crate_version!());
    // `CI_COMMIT_SHA` is set by GitLab CI.
    if let Some(hash) = option_env!("CI_COMMIT_SHA").or(option_env!("GITHUB_SHA")) {
        print!(" ({})", &hash[0..11]);
    }
    println!();
    println!();
    // `CI_COMMIT_TIMESTAMP` is set by GitLab CI.
    if let Some(commit_time) = option_env!("CI_COMMIT_TIMESTAMP") {
        let ts: chrono::DateTime<chrono::Local> = commit_time.parse().expect("invalid timestamp");
        println!("commit time: {ts}");
    }
    println!("platform:    {} / {}", env::consts::OS, env::consts::ARCH);
}

// TODO: remove when everyone has updated.
async fn migrate_cache_to_cache_v2() -> Result<(), Error> {
    use std::fs;
    use tco::utils;

    let cache_dir = tco::cache::cache_dir();
    let migration_completed_path = cache_dir.join("v2");
    if migration_completed_path.try_exists()? {
        return Ok(());
    }
    debug!("No cache v2 found, removing v1 if it exists.");

    // Path used before d49c9d5e69248. Remove it.
    #[allow(deprecated)]
    let legacy_cache = std::env::home_dir()
        .expect("user has no home")
        .join("tocco-tco");
    debug!("Removing legacy cache directory {legacy_cache:?}.");
    utils::maybe_remove_dir_all(&legacy_cache).await?;

    // Drop cache to ensure mirror-mode on repos gets enabled. See 1f97cad73d46a83.
    debug!("Dropping cache v1.");
    utils::drop_all_caches().await?;

    fs::create_dir_all(&migration_completed_path)?;
    debug!("Cache v2 migration completed.");
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn argument_parser() {
        use clap::CommandFactory;
        // Catches most runtime errors.
        Args::command().debug_assert();
    }
}
