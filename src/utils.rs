/// Utilities
use crate::cache;
use chrono::TimeDelta;
use cidr::IpCidr;
use nix::fcntl::{Flock, FlockArg};
use semver::Version;
use std::borrow::Cow;
use std::env;
use std::fs::File as StdFile;
use std::net::IpAddr;
use std::path::Path;
use std::sync::LazyLock;
use tokio::{fs, io, task};

pub(crate) mod deser;
pub mod log;
pub(crate) mod ser;

const OFFICE_CIDR_V4: &str = "83.150.12.144/28";
const OFFICE_CIDR_V6: &str = "2001:8e3:5396:c90::/60";

pub async fn drop_all_caches() -> io::Result<()> {
    maybe_remove_dir_all(cache::cache_dir()).await
}

/// Remove directory recusively if it exists.
pub async fn maybe_remove_dir_all<P>(path: P) -> io::Result<()>
where
    P: AsRef<Path>,
{
    match fs::remove_dir_all(path).await {
        Ok(()) => Ok(()),
        Err(e) if e.kind() == std::io::ErrorKind::NotFound => Ok(()),
        Err(e) => Err(e),
    }
}

/// Print duration, with hour precision, in a human-readable format
///
/// Duration is truncated to the hour. Returned string is design to
/// align properly when printing multiple durations in a column.
///
/// ```
/// use chrono::TimeDelta;
/// use tco::utils::human_duration_hours;
///
/// assert_eq!("  0 d  0 h", human_duration_hours(TimeDelta::zero()));
///
/// let duration = TimeDelta::try_days(11).unwrap()
///     + TimeDelta::try_hours(12).unwrap()
///     + TimeDelta::try_minutes(13).unwrap()
///     + TimeDelta::try_seconds(59).unwrap();
/// assert_eq!(" 11 d 12 h", human_duration_hours(duration));
/// assert_eq!("-11 d 12 h", human_duration_hours(-duration));
/// ```
#[must_use]
pub fn human_duration_hours(age: TimeDelta) -> String {
    let days = age.num_days();
    let hours = age.num_hours().abs() % 24;
    format!("{days:3} d {hours:2} h")
}

/// Print duration, with minute precision, in a human-readable format
///
/// Duration is truncated to the minute. Returned string is design to
/// align properly when printing multiple durations in a column.
///
/// ```
/// use chrono::TimeDelta;
/// use tco::utils::human_duration_minutes;
///
/// assert_eq!(
///     "  0 d  0 h  0 min",
///     human_duration_minutes(TimeDelta::zero())
/// );
///
/// let duration = TimeDelta::try_days(11).unwrap()
///     + TimeDelta::try_hours(12).unwrap()
///     + TimeDelta::try_minutes(13).unwrap()
///     + TimeDelta::try_seconds(59).unwrap();
/// assert_eq!(" 11 d 12 h 13 min", human_duration_minutes(duration));
/// assert_eq!("-11 d 12 h 13 min", human_duration_minutes(-duration));
/// ```
#[must_use]
pub fn human_duration_minutes(age: TimeDelta) -> String {
    let days = age.num_days();
    let hours = age.num_hours().abs() % 24;
    let minutes = age.num_minutes().abs() % 60;
    format!("{days:3} d {hours:2} h {minutes:2} min")
}

/// Convert IP to human readable string
///
/// A hardcoded list is used to convert IP to its human-readable
/// form:
///
/// ```
/// use tco::utils::prettify_ip;
///
/// assert_eq!(
///     prettify_ip("83.150.12.146".parse().unwrap()),
///     "Tocco office"
/// );
/// assert_eq!(
///     prettify_ip("2001:8e3:5396:c92::fe:153".parse().unwrap()),
///     "Tocco office"
/// );
/// ```
///
/// If there is no human-readable form, the IP is converted
/// to a string:
///
/// ```
/// use tco::utils::prettify_ip;
///
/// let ip = "1.2.3.4";
/// assert_eq!(prettify_ip(ip.parse().unwrap()), ip);
/// ```
#[must_use]
pub fn prettify_ip(ip: IpAddr) -> String {
    // First match wins
    static MAPPING: LazyLock<[(IpCidr, &'static str); 11]> = LazyLock::new(|| {
        [
            // IPv6
            ("2001:8e3:5396:c92::136/128", "Wireguard office"),
            ("2001:1600:13:100:f816:3eff:fe07:411c", "wp1.prod.tocco.ch"),
            ("2001:1600:13:102:f816:3eff:fe65:8eb1", "wp1.stage.tocco.ch"),
            (OFFICE_CIDR_V6, "Tocco office"),
            ("::1/128", "localhost"),
            // IPv4
            ("5.102.151.114/32", "OpenShift"),
            ("83.150.12.155/32", "Wireguard office"),
            ("84.16.72.78", "wp1.stage.tocco.ch"),
            (OFFICE_CIDR_V4, "Tocco office"),
            ("195.15.210.114", "wp1.prod.tocco.ch"),
            ("127.0.0.0/8", "localhost"),
        ]
        .map(|(ip, text)| (ip.parse().unwrap(), text))
    });

    MAPPING
        .iter()
        .find(|(cidr, _)| cidr.contains(&ip))
        .map(|(_, text)| (*text).to_string())
        .unwrap_or_else(|| ip.to_string())
}

#[must_use]
pub fn is_office_ip(ip: IpAddr) -> bool {
    static MAPPING: LazyLock<[IpCidr; 2]> =
        LazyLock::new(|| [OFFICE_CIDR_V6, OFFICE_CIDR_V4].map(|ip| ip.parse().unwrap()));

    MAPPING.iter().any(|cidr| cidr.contains(&ip))
}

/// Current width of terminal
///
/// `default` if width cannot be determined.
#[must_use]
pub fn terminal_width(default: u16) -> usize {
    use terminal_size::{Width, terminal_size};

    terminal_size()
        .map(|(Width(n), _)| n)
        .unwrap_or(default)
        .into()
}

/// String with **most** terminal control characters removed
///
/// Replaces control characters with [`char::REPLACEMENT_CHARACTER`].
/// The control characters `'\t'`, `'\n'` and `'\r'` are not replaced.
///
/// ```
/// use std::borrow::Cow;
/// use tco::utils::term_sanitize_string;
///
/// {
///     let text = "A\tlegitimate\nstring.";
///     assert_eq!(term_sanitize_string(text), Cow::Borrowed(text));
/// }
/// assert_eq!(
///     term_sanitize_string("\u{1b}[31;1mRed\u{1b}[0m\tcolor is\0not allowed."),
///     Cow::<str>::Owned("�[31;1mRed�[0m\tcolor is�not allowed.".to_string()),
/// );
/// ```
///
/// See also [`term_sanitize_string_strict()`].
#[must_use]
pub fn term_sanitize_string(text: &str) -> Cow<'_, str> {
    let is_forbidden_control_char = |c| match c {
        '\t' | '\n' | '\r' => false,
        '\u{0}'..='\u{1f}' | '\u{80}'..='\u{9f}' | '\u{7f}' => true,
        _ => false,
    };
    term_sanitize_string_internal(text, is_forbidden_control_char)
}

/// String with **all** terminal control characters removed
///
/// Replaces control characters with [`char::REPLACEMENT_CHARACTER`].
/// This includes control characters `'\t'`, `'\n'` and `'\r'` .
///
/// ```
/// use std::borrow::Cow;
/// use tco::utils::term_sanitize_string_strict;
///
/// {
///     let text = "A legitimate string.";
///     assert_eq!(term_sanitize_string_strict(text), Cow::Borrowed(text));
/// }
/// assert_eq!(
///     term_sanitize_string_strict("\u{1b}[31;1mRed\u{1b}[0m\tcolor is\0not allowed."),
///     Cow::<str>::Owned("�[31;1mRed�[0m�color is�not allowed.".to_string()),
/// );
/// ```
///
/// See also [`term_sanitize_string()`].
#[must_use]
pub fn term_sanitize_string_strict(text: &str) -> Cow<'_, str> {
    let is_forbidden_control_char =
        |c| matches!(c, '\u{0}'..='\u{1f}' | '\u{80}'..='\u{9f}' | '\u{7f}');
    term_sanitize_string_internal(text, is_forbidden_control_char)
}

#[must_use]
fn term_sanitize_string_internal<F>(text: &str, is_forbidden: F) -> Cow<'_, str>
where
    F: Fn(char) -> bool,
{
    if !text.chars().any(|c| is_forbidden(c)) {
        return Cow::Borrowed(text);
    }
    let text: String = text
        .chars()
        .map(|c| {
            if is_forbidden(c) {
                trace!("Replacing character {c:?} with UTF-8 replacement character.");
                char::REPLACEMENT_CHARACTER
            } else {
                c
            }
        })
        .collect();
    Cow::Owned(text)
}

/// Get verbosity argument for ssh based on `TCO_SSH_VERBOSITY` env. var.
pub(crate) fn ssh_verbosity_arg() -> Option<&'static str> {
    static VALUE: LazyLock<Option<&str>> =
        LazyLock::new(|| match env::var("TCO_SSH_VERBOSITY").ok()?.parse().ok()? {
            1_u8 => Some("-v"),
            2 => Some("-vv"),
            3 => Some("-vvv"),
            _ => None,
        });
    *VALUE
}

#[must_use]
pub(crate) fn version_pretty(v: &Version) -> String {
    format!("v{}.{}", &v.major, &v.minor)
}

pub(crate) async fn flock_lock<P>(lock_file: P) -> Result<Flock<StdFile>, io::Error>
where
    P: AsRef<Path>,
{
    let lock_file = lock_file.as_ref().to_owned();
    task::spawn_blocking(|| {
        let file = StdFile::create(lock_file)?;
        Ok(Flock::lock(file, FlockArg::LockExclusive).map_err(|(_, errno)| errno)?)
    })
    .await
    .expect("worker thread panicked")
}

/// Create temporary directory with secure permissions
///
/// `Drop` will remove the directory.
pub(crate) fn temp_dir() -> io::Result<tempfile::TempDir> {
    use std::fs::Permissions;
    use std::os::unix::fs::PermissionsExt;

    let mut dir = tempfile::Builder::new();
    dir.prefix("tco-")
        .permissions(Permissions::from_mode(0o700));

    // We want this to be the only place where `.tempdir()`
    // is used directly to make it easier to ensure permissions
    // are set securely.
    #[allow(clippy::disallowed_methods)]
    dir.tempdir()
}
