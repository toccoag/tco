pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("{0}")]
    Custom(String),

    /// Error returned by subcommands to indicate
    /// that the process should exit with given
    /// code.
    ///
    /// No error message is printed.
    #[error("Exiting with error code {0}")]
    CustomExitStatus(i32),

    #[cfg(feature = "nightly")]
    #[error("command failed: {0}")]
    ExitStatusError(#[from] std::process::ExitStatusError),

    #[error("git error: {0}")]
    Git(#[from] git2::Error),

    #[error("i/o error: {0}")]
    Io(#[from] std::io::Error),

    #[error("k8s error: {0}")]
    Kube(#[from] kube::Error),

    #[error("k8s error: {0}")]
    KubeConfig(#[from] kube::config::InferConfigError),

    #[error("message pack decoding error: {0}")]
    RmpDecode(#[from] rmp_serde::decode::Error),

    #[error("message pack encoding error: {0}")]
    RmpEncode(#[from] rmp_serde::encode::Error),

    #[error("non-UTF-8 string: {0}")]
    FromUtf8Error(#[from] std::str::Utf8Error),

    #[error("JSON en/decode error: {0}")]
    Json(#[from] serde_json::Error),

    #[error("{0}")]
    Nix(#[from] nix::Error),

    #[error("reqwest: {0:?}")]
    Reqwest(#[from] reqwest::Error),

    #[error("postgres: {0}")]
    Postgres(#[from] tokio_postgres::Error),
}
