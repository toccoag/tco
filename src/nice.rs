//! Interaction with *Nice2* (AKA *Tocco*)
pub mod client_settings;
pub mod deser;
pub mod git;
pub mod k8s;
pub mod nginx;
pub mod pending_deployment;
pub mod status;

use serde::{Deserialize, Deserializer, Serialize, de};
use std::fmt;
use std::result::Result as StdResult;

/// Whether an installation is prod or test.
#[derive(Clone, Copy, Debug, Eq, PartialEq, Ord, PartialOrd, Serialize)]
pub enum Env {
    Prod,
    Test,
}

impl Env {
    /// Wether this is a production system.
    #[must_use]
    pub fn is_prod(self) -> bool {
        matches!(self, Env::Prod)
    }

    /// Wether this is a test system.
    #[must_use]
    pub fn is_test(self) -> bool {
        matches!(self, Env::Test)
    }
}

impl fmt::Display for Env {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let text = match self {
            Env::Prod => "prod",
            Env::Test => "test",
        };
        fmt::Display::fmt(text, f)
    }
}

impl<'de> Deserialize<'de> for Env {
    fn deserialize<D>(de: D) -> StdResult<Self, <D as Deserializer<'de>>::Error>
    where
        D: Deserializer<'de>,
    {
        struct EnvVisitor;

        impl de::Visitor<'_> for EnvVisitor {
            type Value = Env;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                formatter.write_str(
                    "environment string \"production\", \"prod\" or \"test\" (case insensitive)",
                )
            }

            fn visit_str<E>(self, v: &str) -> StdResult<Self::Value, E>
            where
                E: de::Error,
            {
                if "production".eq_ignore_ascii_case(v) || "prod".eq_ignore_ascii_case(v) {
                    Ok(Env::Prod)
                } else if "test".eq_ignore_ascii_case(v) {
                    Ok(Env::Test)
                } else {
                    Err(de::Error::invalid_value(de::Unexpected::Str(v), &self))
                }
            }
        }

        de.deserialize_any(EnvVisitor)
    }
}
