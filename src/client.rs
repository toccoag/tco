//! Interaction with the Tocco Client
//!
//! See [tocco-client](https://gitlab.com/toccoag/tocco-client)
pub mod git;
pub mod releases;
