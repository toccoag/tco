/// Serialization / Deserialization helpers
use git2::Oid;
use serde::{Deserialize, Deserializer, Serialize, Serializer, de};
use std::fmt;
use std::ops::Deref;

/// Serializable version of `git2::Oid`
#[derive(Clone, Copy, Hash, PartialEq, Eq)]
pub struct CommitId(pub(crate) git2::Oid);

impl CommitId {
    /// Get Git Object ID
    #[must_use]
    pub fn id(&self) -> &Oid {
        &self.0
    }
}

impl fmt::Display for CommitId {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Display::fmt(&self.0, f)
    }
}

impl fmt::Debug for CommitId {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Debug::fmt(&self.0, f)
    }
}

impl Deref for CommitId {
    type Target = Oid;

    fn deref(&self) -> &<Self as Deref>::Target {
        &self.0
    }
}

impl Serialize for CommitId {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.collect_seq(self.0.as_bytes())
    }
}

impl<'de> Deserialize<'de> for CommitId {
    fn deserialize<D>(deserializer: D) -> Result<CommitId, D::Error>
    where
        D: Deserializer<'de>,
    {
        let id: Vec<u8> = Deserialize::deserialize(deserializer)?;
        let id = Oid::from_bytes(&id)
            .map_err(|err| de::Error::custom(format!("invalid Git Object ID: {err}")))?;
        Ok(CommitId(id))
    }
}
