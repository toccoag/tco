//! Git working tree
//!
//! Working trees allow checking out all files belonging to a certain
//! branch, tag or commit.
//!
//! # Concurrency
//!
//! Both [`Worktree`] and [`WorktreeDir`] provide access to a freshly checked
//! out working tree in a temporary directory. One important reason for doing
//! this is to allow for concurrency. With a single working tree, access would
//! have to be synchronized.
//!
//! # `Worktree` vs `WorktreeDir`
//!
//! ## `Worktree` - An opened working tree:
//!
//! [`Worktree`] is a wrapper around [`git2::Worktree`] which, in turn, is
//! a wrapper around libgit2 written in C. `git2::Worktree` is `!Send`.
//! That is, it cannot be sent to another thread.
//!
//! `Worktree` keeps the working tree opened internally and is useful if
//! you want to manipulate it (via [`Worktree::as_inner()`]).
//!
//! ## `WorktreeDir` - A closed working tree:
//!
//! [`WorktreeDir`] provides access to the checked out working tree but no longer
//! holds an internal `git2::Worktree` and, thus, the working tree can no longer be
//! manipulated without opening it again first. However, `WorktreeDir` is
//! thread-safe.
//!
//! # Working Tree Clean Up
//!
//! Both, `Worktree` and `WorktreeDir`, will remove the temporary directory
//! containing the working tree on `Drop`.
//!
//! # Examples
//!
//! There is two ways to obtain a `WorktreeDir`, [`Repository::create_worktree_dir()`]
//! and [`Worktree::into_worktree_dir()`].
//!
//! **Obtain access to working tree directory:**
//!
//! ```
//! # #[tokio::main]
//! # async fn main() {
//! # #[cfg(feature = "test-with-repo-access")] {
//! let repository = tco::ansible::git::repo().await.unwrap();
//! let working_tree = repository.create_worktree_dir(&"master").await.unwrap();
//! let config_path = working_tree.path().join("tocco/config.yml");
//!
//! assert!(config_path.exists());
//!
//! // Remove working tree and temporary directory.
//! drop(working_tree);
//!
//! assert!(!config_path.exists());
//! # }
//! # }
//! ```
//!
//! **`WorktreeDir` can be used in multi-threading context:**
//!
//! ```
//! # #[tokio::main]
//! # async fn main() {
//! # #[cfg(feature = "test-with-repo-access")] {
//! # use std::sync::Arc;
//! let repository = tco::ansible::git::repo().await.unwrap();
//! let working_tree = repository.create_worktree_dir(&"master").await.unwrap();
//! let working_tree = Arc::new(working_tree);
//! let ref1 = Arc::clone(&working_tree);
//! let ref2 = Arc::clone(&working_tree);
//! let config_path = working_tree.path().join("tocco/config.yml");
//!
//! assert!(config_path.exists());
//!
//! drop(working_tree); // minus one reference
//! assert!(config_path.exists());
//!
//! // WorktreeDir can be moved to other threads freely.
//! tokio::task::spawn(async move { drop(ref1) }).await.unwrap();
//! assert!(config_path.exists());
//!
//! drop(ref2); // minus last reference
//! assert!(!config_path.exists());
//! # }
//! # }
//! ```
//!
//! **Manipulating a `WorkingtreeDir`:**
//!
//! ```
//! # #[tokio::main]
//! # async fn main() {
//! # #[cfg(feature = "test-with-repo-access")] {
//! let repository = tco::ansible::git::repo().await.unwrap();
//! let tree = repository.create_worktree(&"master").unwrap();
//!
//! // Open working tree as repository.
//! let _repo = git2::Repository::open_from_worktree(&tree.as_inner()).unwrap();
//!
//! // … manipulate tree / repository …
//!
//! // Obtain thread-safe WorktreeDir.
//! let tree = tree.into_worktree_dir();
//!
//! // … work with files in working tree …
//!
//! // Open WorktreeDir as Worktree again (e.g. to commit changes).
//! let _repo = tree.into_worktree();
//! # }
//! # }
//! ```

use crate::Result;
use crate::git::Repository;
use crate::git::ToOid;
use crate::utils;
use crate::utils::log::soft_unreachable;
use rand::distr::{Alphanumeric, SampleString};
use std::ops::Deref;
use std::path::{Path, PathBuf};
use tempfile::TempDir;

/// Git working tree
///
/// Underlying temporary directory and temporary branch is removed on [`Drop`].
///
/// Obtain via [`Repository::create_worktree()`]
pub struct Worktree {
    inner: PruneableWorktree,
    tmp_dir: TempDir,
    path: PathBuf,
}

/// Worktree automatically pruned when dropped
///
/// When `prune` is true, the worktree is pruned. That
/// is, the tree is unregistered from the repository, and
/// the associated branch and working tree are removed.
struct PruneableWorktree {
    branch: String,
    tree: git2::Worktree,
    prune: bool,
}

impl Deref for PruneableWorktree {
    type Target = git2::Worktree;

    fn deref(&self) -> &git2::Worktree {
        &self.tree
    }
}

impl Drop for PruneableWorktree {
    fn drop(&mut self) {
        if self.prune {
            let drop_branch = || {
                git2::Repository::open_from_worktree(self)?
                    .find_reference(&self.branch)?
                    .delete()
            };
            if let Err(e) = drop_branch() {
                soft_unreachable!("Failed to remove branch used by worktree: {e}");
            }

            let mut opts = git2::WorktreePruneOptions::new();
            opts.valid(true).working_tree(true);
            if let Err(e) = self.prune(Some(&mut opts)) {
                soft_unreachable!("Failed to prune Git working tree: {e}");
            }
        }
    }
}

/// An Opened working tree
impl Worktree {
    #[must_use]
    pub fn path(&self) -> &Path {
        &self.path
    }

    /// Return inner repository.
    #[must_use]
    pub fn as_inner(&self) -> &git2::Worktree {
        &self.inner
    }

    /// Close working tree and return directory containing it.
    ///
    /// See [module-level][self] documentation.
    #[must_use]
    pub fn into_worktree_dir(mut self) -> WorktreeDir {
        // taking over responsibility to prune working tree
        self.inner.prune = false;

        WorktreeDir {
            branch: self.inner.branch.clone(),
            tmp_dir: Some(self.tmp_dir),
            path: Some(self.path),
        }
    }
}

/// An unopened Git worktree
///
/// Underlying temporary directory and temporary branch is removed on [`Drop`].
///
/// Obtain via [`Repository::create_worktree_dir()`] or
/// [`Worktree::into_worktree_dir()`].
pub struct WorktreeDir {
    branch: String,
    tmp_dir: Option<TempDir>,
    path: Option<PathBuf>,
}

impl WorktreeDir {
    #[must_use]
    pub fn path(&self) -> &Path {
        self.path.as_ref().expect("opened as Worktree")
    }

    /// Open as [`Worktree`]
    pub fn into_worktree(mut self) -> Result<Worktree> {
        let tree = self.open_worktree()?;
        Ok(Worktree {
            inner: PruneableWorktree {
                branch: self.branch.clone(),
                tree,
                prune: true,
            },
            tmp_dir: self.tmp_dir.take().expect("opened as Worktree"),
            path: self.path.take().expect("opened as Worktree"),
        })
    }

    fn open_worktree(&self) -> Result<git2::Worktree> {
        let repo = git2::Repository::open(self.path())?;
        let tree = git2::Worktree::open_from_repository(&repo)?;
        Ok(tree)
    }
}

impl Drop for WorktreeDir {
    fn drop(&mut self) {
        if self.path.is_some() {
            match self.open_worktree() {
                Ok(tree) => drop(PruneableWorktree {
                    branch: self.branch.clone(),
                    tree,
                    prune: true,
                }),
                Err(e) => {
                    soft_unreachable!("Failed to open worktree for pruning: {e}.");
                }
            }
        }
    }
}

pub(crate) fn create_worktree<O>(repo: &Repository, refspec: &O) -> Result<Worktree>
where
    O: ToOid,
{
    let repo = repo.as_inner();
    let oid = refspec.to_oid(repo)?;
    let tmp_dir = utils::temp_dir()?;
    let name = Alphanumeric.sample_string(&mut rand::rng(), 12);
    let path = tmp_dir.path().join("repo");
    let branch = format!("refs/heads/tocco-tco/{name}");
    let local_reference = repo.reference(&branch, oid, false, "")?;
    let mut opts = git2::WorktreeAddOptions::new();
    opts.reference(Some(&local_reference));
    debug!(
        "Creating worktree for {:?} named {name:?} at {path:?} checking out {refspec:?} with ID \
         {oid}",
        repo.path()
    );
    let worktree = repo.worktree(&name, &path, Some(&opts))?;
    Ok(Worktree {
        inner: PruneableWorktree {
            branch,
            tree: worktree,
            prune: true,
        },
        tmp_dir,
        path,
    })
}

#[cfg(test)]
#[cfg(feature = "test-with-repo-access")]
mod tests {
    use crate::{Error, ansible};
    use futures::future;
    #[cfg(feature = "nightly")]
    use test_utils::lock_caches_sync;
    use test_utils::{LockMode, lock_caches};
    #[cfg(feature = "nightly")]
    use tokio::runtime::Runtime;

    #[tokio::test]
    #[ignore = "sporadically fails (race condition)"]
    async fn multiple_worktrees_same_branch() {
        let _guard = lock_caches(LockMode::Shared).await;
        let repo = ansible::git::repo().await.unwrap();
        let tasks: Vec<_> = (0..100)
            .map(|_| async {
                let tree = repo.create_worktree_dir(&"master").await?;
                tokio::task::spawn_blocking(|| drop(tree)).await.unwrap();
                Ok::<_, Error>(())
            })
            .collect();
        future::try_join_all(tasks).await.unwrap();
    }

    #[tokio::test]
    async fn drop_worktree() {
        let _guard = lock_caches(LockMode::Exclusive).await;
        let repo = ansible::git::repo().await.unwrap();
        let worktree = repo.create_worktree(&"master").unwrap();
        let name = worktree.as_inner().name().unwrap().to_owned();
        let path = worktree.path().to_owned();

        assert!(repo.as_inner().find_worktree(&name).is_ok());
        assert!(path.try_exists().unwrap());

        drop(worktree);

        // returns generic error code which is hard to check
        assert!(repo.as_inner().find_worktree(&name).is_err());
        assert!(!path.try_exists().unwrap());
    }

    #[tokio::test]
    async fn drop_worktree_dir() {
        let _guard = lock_caches(LockMode::Exclusive).await;
        let repo = ansible::git::repo().await.unwrap();
        let worktree = repo.create_worktree(&"master").unwrap();
        let name = worktree.as_inner().name().unwrap().to_owned();
        let path = worktree.path().to_owned();
        let worktree_dir = worktree.into_worktree_dir();

        assert!(repo.as_inner().find_worktree(&name).is_ok());
        assert!(path.try_exists().unwrap());

        drop(worktree_dir);

        // returns generic error code which is hard to check
        assert!(repo.as_inner().find_worktree(&name).is_err());
        assert!(!path.try_exists().unwrap());
    }

    #[bench]
    #[cfg(feature = "nightly")]
    fn worktree_creation_and_drop(b: &mut crate::test::Bencher) {
        let _guard = lock_caches_sync(LockMode::Shared);
        let rt = Runtime::new().unwrap();
        let repo = rt.block_on(async { ansible::git::repo().await.unwrap() });
        b.iter(|| {
            rt.block_on(async {
                repo.create_worktree(&"master").unwrap();
            });
        });
    }
}
