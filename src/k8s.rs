//! Kubernetes
use crate::Result;
use crate::utils;
use futures::AsyncBufRead;
use k8s_openapi::api::core::v1::Pod;
use kube::Client;
use kube::api::{Api, ListParams, LogParams};
use kube::core::ErrorResponse;
use std::time::Duration;

/// Create a Kubernetes client
pub async fn pod_api_client(installation: Option<&str>) -> Result<Api<Pod>> {
    pod_api_client_internal(installation, false).await
}

/// Create a Kubernetes client with disabled read timeout
///
/// Useful when streaming data where reading may stall
/// for longer than the default timeout of 290s.
pub async fn pod_api_client_no_timeout(installation: Option<&str>) -> Result<Api<Pod>> {
    pod_api_client_internal(installation, true).await
}

pub async fn pod_api_client_internal(
    installation: Option<&str>,
    disable_timeout: bool,
) -> Result<Api<Pod>> {
    let mut config = kube::Config::infer().await?;
    if disable_timeout {
        config.read_timeout = None;
    }
    let client = Client::try_from(config)?;
    if let Some(installation) = installation {
        let ns = format!("nice-{installation}");
        Ok(Api::namespaced(client, &ns))
    } else {
        Ok(Api::default_namespaced(client))
    }
}

/// Checks connection to k8s server
///
/// Tries to detect if user needs to login, printing a hint
/// via `error!()`. A crude request is made to the server to
/// detect if everything is working. If there is an error,
/// the underlying error is returned.
pub async fn check_connection() -> Result<()> {
    let config = kube::Config::infer()
        .await
        .inspect_err(|_| error!("Failed to load config. Try logging in: oc login -w https://api.c-tocco-ocp4.tocco.ch:6443"))?;
    let client = Client::try_from(config)?;

    // TODO use this once supported on OpenShift (Kubernetes v1.29):
    //
    //     let _: Api<k8s_openapi::api::authorization::v1::SelfSubjectAccessReview> = Api::all(client);
    //
    // The current code assumes namespace "nice-master" exists.
    let pods: Api<Pod> = Api::namespaced(client, "nice-master");
    let lp = ListParams::default().limit(0);
    let _abort_handle = utils::log::schedule_vpn_needed_info_message(
        Duration::from_secs(5),
        "Kubernetes failed to respond within 5 seconds: is VPN enabled?",
    );
    pods.list(&lp).await.inspect_err(|e| {
        match &e {
            kube::Error::Api(ErrorResponse { status, reason, .. })
                if status == "Failure" && reason == "Unauthorized" =>
            {
                error!("You're not authorized. Try logging in: oc login -w https://api.c-tocco-ocp4.tocco.ch:6443");
            }
            _ => (),
        }
    })?;
    Ok(())
}

/// Return an arbitrary, running Nice pod
///
/// `None` if none is running.
pub async fn any_pod_for_installation(api: &Api<Pod>) -> Result<Option<Pod>> {
    let lp = ListParams::default()
        .labels("run=nice")
        .fields("status.phase=Running")
        .limit(1);
    let pod_list = api.list(&lp).await?;
    Ok(pod_list.into_iter().next())
}

/// Return logs of given pod and container
pub async fn logs(
    pod_api: &Api<Pod>,
    pod_name: &str,
    container_name: &str,
    follow: bool,
    previous: bool,
    since_seconds: Option<i64>,
) -> Result<impl AsyncBufRead + use<>> {
    Ok(pod_api
        .log_stream(
            pod_name,
            &LogParams {
                container: Some(container_name.to_owned()),
                follow,
                previous,
                since_seconds,
                ..LogParams::default()
            },
        )
        .await?)
}
