//! Work with a Postgres DBs
//!
//! See [`ssh`].

use async_trait::async_trait;
use std::result::Result as StdResult;
use tokio_postgres as postgres;

pub mod batch;
pub mod dump;
pub mod local;
pub mod query;
pub mod ssh;

/// Connect to SQL server
#[async_trait]
pub trait SqlConnect {
    /// Connect to DB server
    ///
    /// See [`ssh`] for examples.
    async fn sql_connect(&self, db_name: &str) -> StdResult<postgres::Client, postgres::Error>;
}

/// Quote an SQL identifier for use in SQL
///
/// ```
/// use tco::sql::quote_ident;
///
/// assert_eq!(quote_ident("user"), r#""user""#);
/// assert_eq!(quote_ident("a\"b"), r#""a""b""#);
/// ```
///
/// # Panics
///
/// Panics if name contains `'\0'`.
///
/// ```should_panic
/// use tco::sql::quote_ident;
///
/// quote_ident("a\0name");
/// ```
#[must_use]
pub fn quote_ident(name: &str) -> String {
    assert!(!name.contains('\0'), "NUL character in SQL identifier");
    format!("\"{}\"", name.replace('"', "\"\""))
}

/// Quote `String` as SQL literal for use in SQL
///
/// Only use this if the value can't be passed as parameter to
/// *tokio-postgres* (i.e. via [`postgres::Client::execute`].
///
/// ```
/// use tco::sql::quote_literal;
///
/// assert_eq!(quote_literal("some text"), "'some text'");
/// assert_eq!(quote_literal("a'b"), "'a''b'");
/// ```
///
/// # Panics
///
/// Panics if name contains `'\0'`.
///
/// ```should_panic
/// use tco::sql::quote_literal;
///
/// quote_literal("a\0name");
/// ```
#[must_use]
pub fn quote_literal(name: &str) -> String {
    assert!(!name.contains('\0'), "NUL character in SQL identifier");
    format!("'{}'", name.replace('\'', "''"))
}
