use clap::{CommandFactory, Parser};
use std::result::Result as StdResult;
use tco::ansible;
use tco::error::{Error, Result};
use tco::sql;
use tco::sql::SqlConnect;
use tco::sql::query::ClientExt;
use tco::sql::ssh::SshProxyBuilder;
use tokio_postgres::Client;
use tokio_postgres::error::SqlState;

#[derive(Parser)]
#[command()]
pub struct Args {
    /// Source installation
    #[arg(value_name = "SOURCE_INSTALLATION", value_parser = parse_installation)]
    source: Installation,

    /// Target installation
    ///
    /// DBs associated with TARGET_INSTALLATION must be empty
    /// and unused or not exist at all.
    #[arg(value_name = "TARGET_INSTALLATION", value_parser = parse_installation)]
    target: Installation,
}

#[derive(Clone, Debug)]
struct Installation {
    full_name: String,

    /// "agogistest" in "agogistestold"
    base_name: String,

    /// "old" in "agogistestold", empty for "agogistest"
    suffix: String,
}

fn parse_installation(value: &str) -> StdResult<Installation, String> {
    let allowed_endings = ["test", "testold", "testnew"];
    if !allowed_endings.iter().any(|ending| value.ends_with(ending)) {
        return Err(format!(
            r#"{value:?} not a valid test system: name must end with one of {allowed_endings:?}"#,
        ));
    }

    let (prefix, suffix) = value.rsplit_once("test").expect("no \"test\" substring");
    Ok(Installation {
        full_name: value.to_owned(),
        base_name: format!("{prefix}test"),
        suffix: suffix.to_owned(),
    })
}

fn assert_args(args: &Args) {
    if args.source.full_name == args.target.full_name {
        Args::command()
            .error(
                clap::error::ErrorKind::ValueValidation,
                "SOURCE_INSTALLATION and TARGET_INSTALLATION must differ",
            )
            .exit();
    }
    if args.source.base_name != args.target.base_name {
        let msg = format!(
            "SOURCE_INSTALLATION and TARGET_INSTALLATION must belong to the same
            base test installation: {:?} != {:?}",
            args.source.base_name, args.target.base_name
        );
        Args::command()
            .error(clap::error::ErrorKind::ValueValidation, msg)
            .exit();
    }
}

struct RenameInfo {
    server: String,
    source_db: String,
    target_db: String,
    source_user: String,
    target_user: String,
}

impl RenameInfo {
    fn source_display(&self) -> String {
        format!("{}@{}/{}", self.source_user, self.server, self.source_db)
    }

    fn target_display(&self) -> String {
        format!("{}@{}/{}", self.target_user, self.server, self.target_db)
    }
}

async fn check_preconditions(proxy: &dyn SqlConnect, info: &RenameInfo) -> Result<()> {
    // source DB
    {
        let src_conn = proxy.sql_connect(&info.source_db).await?;
        if src_conn.is_in_use().await? {
            return Err(Error::Custom(format!(
                "source DB {:?} in use, use `tco db-queries -a {}/{}` to see connections",
                info.source_db, info.server, info.source_db,
            )));
        }
        let observed_source_owner = src_conn.db_owner().await?;
        if observed_source_owner != info.source_user {
            return Err(Error::Custom(format!(
                "unexpected owner of source db, found: {:?}, expected: {:?}",
                observed_source_owner, info.source_user,
            )));
        }
        if !src_conn.role_exists(&info.target_user).await? {
            return Err(Error::Custom(format!(
                "target role/user {:?} missing (run Ansible)",
                info.target_user,
            )));
        }
    }

    // target DB
    match proxy.sql_connect(&info.target_db).await {
        Ok(target_conn) => {
            if !(target_conn.is_empty().await? || target_conn.is_empty_history().await?) {
                return Err(Error::Custom(format!(
                    "target db {:?} exists and has content, refusing to replace",
                    info.target_db,
                )));
            }
            if target_conn.is_in_use().await? {
                return Err(Error::Custom(format!(
                    "target DB {:?} in use, use `tco db-queries -a {}/{}` to see connections",
                    info.target_db, info.server, info.target_db,
                )));
            }
            let observed_target_owner = target_conn.db_owner().await?;
            if observed_target_owner != info.target_user {
                return Err(Error::Custom(format!(
                    "unexpected owner of target db, found: {:?}, expected: {:?}",
                    observed_target_owner, info.target_user,
                )));
            }
        }
        Err(e) if e.code() == Some(&SqlState::UNDEFINED_DATABASE) => (),
        Err(e) => {
            return Err(e.into());
        }
    }

    Ok(())
}

async fn reassign_db(proxy: &dyn SqlConnect, info: &RenameInfo) -> Result<()> {
    let mut conn = proxy.sql_connect(&info.target_db).await?;
    // Use transaction to prevent half-reassigned state on failure.
    let tx = conn.transaction().await?;

    // `REASSIGNED OWNED BY "user_a" TO "user_b"` changes the owner
    // of all objects in the current database owned by "user_a" **and**
    // the **owner of all databases owned by "user_a"**.
    //
    // To prevent this, a list of all DBs owned by "user_a" is obtained
    // and the owner corrected back.
    let dbs = tx
        .query(
            "\
            SELECT d.datname
            FROM pg_catalog.pg_database AS d
            WHERE
                pg_catalog.pg_get_userbyid(d.datdba) = $1
                AND datname != current_database()
            ",
            &[&info.source_user],
        )
        .await?;
    let dbs: Vec<String> = dbs.into_iter().map(|row| row.get(0)).collect();

    tx.execute(
        &format!(
            "REASSIGN OWNED BY {} TO {}",
            &sql::quote_ident(&info.source_user),
            &sql::quote_ident(&info.target_user),
        ),
        &[],
    )
    .await?;

    for db in &dbs {
        // See comment above
        tx.execute(
            &format!(
                "ALTER DATABASE {} OWNER TO {}",
                sql::quote_ident(db),
                sql::quote_ident(&info.source_user),
            ),
            &[],
        )
        .await?;
    }

    tx.commit().await?;
    Ok(())
}

async fn rename_db(conn: &Client, source_db: &str, target_db: &str) -> Result<()> {
    conn.execute(
        &format!(
            "ALTER DATABASE {} RENAME TO {}",
            &sql::quote_ident(source_db),
            &sql::quote_ident(target_db),
        ),
        &[],
    )
    .await?;
    Ok(())
}

async fn drop_db(conn: &Client, db_name: &str) -> Result<()> {
    if conn.db_exists(db_name).await? {
        conn.execute(
            &format!("DROP DATABASE {}", &sql::quote_ident(db_name)),
            &[],
        )
        .await?;
    }
    Ok(())
}

async fn rename_and_reassign(proxy: &dyn SqlConnect, info: &RenameInfo) -> Result<()> {
    let admin_conn = proxy.sql_connect("postgres").await?;
    drop_db(&admin_conn, &info.target_db).await?;
    rename_db(&admin_conn, &info.source_db, &info.target_db).await?;
    reassign_db(proxy, info).await
}

pub async fn main(root_args: &crate::Args, args: &Args) -> Result<()> {
    assert_args(args);
    debug!("Raw rename source: {:?}", args.source);
    debug!("Raw rename target: {:?}", args.target);

    let repo = tco::ansible::git::maybe_updated_repo(root_args.update_repos_interval()).await?;
    let config = ansible::config_quick::load(&repo).await?;
    let Some(installation) = config.installations.get(&args.source.base_name) else {
        return Err(Error::Custom(format!(
            "base installation {:?} not found in config.yml",
            &args.source.base_name
        )));
    };

    let main_info = RenameInfo {
        server: installation.db_server.clone(),
        source_db: format!("{}{}", installation.db_name, args.source.suffix),
        target_db: format!("{}{}", installation.db_name, args.target.suffix),
        source_user: format!("{}{}", installation.db_name, args.source.suffix),
        target_user: format!("{}{}", installation.db_name, args.target.suffix),
    };
    let history_info = RenameInfo {
        server: installation.history_db_server.clone(),
        source_db: format!("{}{}_history", installation.db_name, args.source.suffix),
        target_db: format!("{}{}_history", installation.db_name, args.target.suffix),
        source_user: format!("{}{}", installation.db_name, args.source.suffix),
        target_user: format!("{}{}", installation.db_name, args.target.suffix),
    };

    let (main_proxy, history_proxy) = tokio::try_join!(
        SshProxyBuilder::new(&main_info.server).open(),
        SshProxyBuilder::new(&history_info.server).open()
    )?;

    println!("main DB:");
    println!("    source: {}", main_info.source_display());
    println!("    target: {}", main_info.target_display());
    println!("history DB:");
    println!("    source: {}", history_info.source_display());
    println!("    target: {}", history_info.target_display());
    println!();

    check_preconditions(&main_proxy, &main_info)
        .await
        .map_err(|e| Error::Custom(format!("main DB pre-check: {e}")))?;
    check_preconditions(&history_proxy, &history_info)
        .await
        .map_err(|e| Error::Custom(format!("history DB pre-check: {e}")))?;

    rename_and_reassign(&main_proxy, &main_info)
        .await
        .map_err(|e| Error::Custom(format!("main DB rename: {e}")))?;
    rename_and_reassign(&history_proxy, &history_info)
        .await
        .map_err(|e| Error::Custom(format!("history DB rename: {e}")))
}

#[cfg(test)]
#[cfg(feature = "test-with-local-postgres")]
mod tests {
    #![allow(clippy::await_holding_lock)]

    use super::*;
    use tco::sql::SqlConnect;
    use tco::sql::local::Local;
    use tokio::sync::{Mutex, MutexGuard};

    // Modifying roles concurrently leads to errors returned
    // by Postgres.
    static SYNC_TESTS: Mutex<()> = Mutex::const_new(());

    #[tokio::test(flavor = "multi_thread", worker_threads = 2)]
    async fn full_run() {
        let h = Helper::init("phaeze3aca").await;

        let additional_db_owner_before = h.admin_conn.db_owner_of(&h.additional_db).await.unwrap();

        check_preconditions(&h.proxy, &h.info).await.unwrap();
        rename_and_reassign(&h.proxy, &h.info).await.unwrap();

        // check source
        assert!(
            !h.admin_conn.db_exists(&h.info.source_db).await.unwrap(),
            "source not renamed"
        );

        // check target
        let conn = h.connect_target().await;
        assert_eq!(conn.db_owner().await.unwrap(), h.info.target_user);
        let table_owner: String = conn
            .query_one(
                "SELECT relowner::regrole::text FROM pg_class WHERE relname = 'a_table'",
                &[],
            )
            .await
            .unwrap()
            .get(0);
        assert_eq!(table_owner, h.info.target_user, "table not copied");

        let additional_db_owner_after = h.admin_conn.db_owner_of(&h.additional_db).await.unwrap();
        assert_eq!(
            additional_db_owner_before, additional_db_owner_after,
            "owner of additional DB was touched"
        );
    }

    #[tokio::test(flavor = "multi_thread", worker_threads = 2)]
    async fn target_not_empty() {
        let h = Helper::init("wepho7ob7o").await;
        h.connect_target()
            .await
            .execute("CREATE TABLE a_table ()", &[])
            .await
            .unwrap();
        let failed_precond = check_preconditions(&h.proxy, &h.info)
            .await
            .unwrap_err()
            .to_string();
        assert_eq!(
            failed_precond,
            r#"target db "tco_test_wepho7ob7o_target_db" exists and has content, refusing to replace"#,
        );
    }

    #[tokio::test(flavor = "multi_thread", worker_threads = 2)]
    async fn history_db_empty_and_not_empty() {
        let h = Helper::init("noo8xupieb").await;
        h.connect_target()
            .await
            .execute("CREATE TABLE nice_history (x INT)", &[])
            .await
            .unwrap();
        h.connect_target()
            .await
            .execute("CREATE TABLE nice_history_data (x INT)", &[])
            .await
            .unwrap();
        h.connect_target()
            .await
            .execute("CREATE TABLE schema_info (x INT)", &[])
            .await
            .unwrap();

        // empty history
        check_preconditions(&h.proxy, &h.info).await.unwrap();

        h.connect_target()
            .await
            .execute("INSERT INTO nice_history VALUES (1)", &[])
            .await
            .unwrap();

        // non-empty history
        let failed_precond = check_preconditions(&h.proxy, &h.info).await.unwrap_err();
        assert_eq!(
            failed_precond.to_string(),
            r#"target db "tco_test_noo8xupieb_target_db" exists and has content, refusing to replace"#
        );
    }

    struct Helper {
        _guard: MutexGuard<'static, ()>,
        proxy: Local,
        admin_conn: Client,
        info: RenameInfo,
        drop_statements: Vec<String>,
        additional_db: String,
    }

    impl Helper {
        async fn init(arbitrary: &str) -> Self {
            let _ = env_logger::builder().is_test(true).try_init();
            let guard = SYNC_TESTS.lock().await;
            let proxy = Local::new();
            let admin_conn = proxy.sql_connect("postgres").await.unwrap();

            let info = RenameInfo {
                server: "!!! unused !!!".to_string(),
                source_db: format!("tco_test_{arbitrary}_source_db"),
                target_db: format!("tco_test_{arbitrary}_target_db"),
                source_user: format!("tco_test_{arbitrary}_source_role"),
                target_user: format!("tco_test_{arbitrary}_target_role"),
            };
            let additional_db = format!("tco_test_{arbitrary}_additional_db");

            let drop_statements = vec![
                format!("DROP DATABASE IF EXISTS {}", info.source_db),
                format!("DROP DATABASE IF EXISTS {}", info.target_db),
                format!("DROP DATABASE IF EXISTS {}", additional_db),
                format!("DROP ROLE IF EXISTS {}", info.source_user),
                format!("DROP ROLE IF EXISTS {}", info.target_user),
            ];
            let create_statements = [
                format!("CREATE ROLE {}", info.source_user),
                format!("CREATE ROLE {}", info.target_user),
                format!(
                    "CREATE DATABASE {} WITH OWNER {}",
                    info.source_db, info.source_user
                ),
                format!(
                    "CREATE DATABASE {} WITH OWNER {}",
                    info.target_db, info.target_user
                ),
                format!(
                    "CREATE DATABASE {} WITH OWNER {}",
                    additional_db, info.source_user
                ),
            ];
            for sql in &drop_statements {
                admin_conn.execute(sql, &[]).await.unwrap();
            }
            for sql in &create_statements {
                admin_conn.execute(sql, &[]).await.unwrap();
            }

            let source_conn = proxy.sql_connect(&info.source_db).await.unwrap();
            source_conn
                .batch_execute(&format!(
                    "CREATE TABLE a_table (); ALTER TABLE a_table OWNER TO {};",
                    info.source_user
                ))
                .await
                .unwrap();

            Helper {
                _guard: guard,
                proxy,
                admin_conn,
                info,
                drop_statements,
                additional_db,
            }
        }

        async fn connect_target(&self) -> Client {
            self.proxy.sql_connect(&self.info.target_db).await.unwrap()
        }
    }

    impl Drop for Helper {
        fn drop(&mut self) {
            for sql in &self.drop_statements {
                futures::executor::block_on(self.admin_conn.execute(sql, &[])).unwrap();
            }
        }
    }
}
