use super::common::{self, DbLocation};
use chrono::{DateTime, Local, SubsecRound};
use clap::Parser;
use futures::FutureExt;
use std::fmt;
use tco::ansible;
use tco::ansible::config_quick::InstallConfig;
use tco::sql::SqlConnect;
use tco::sql::dump::DbMetadata;
use tco::sql::query::ClientExt;
use tco::sql::ssh::SshProxyBuilder;
use tco::utils;
use tco::{Error, Result};
use tokio_postgres::error::SqlState;

#[derive(Parser)]
#[command()]
pub struct Args {
    /// Installation / DB location
    ///
    /// Syntax:
    ///
    /// <INSTALLATION[/DB_NAME]|DB_SERVER/DB_NAME>
    #[arg(value_parser = common::parse_db_location_arg)]
    target: DbLocation,
}

struct Info<'a> {
    metadata: Option<DbMetadata>,
    size: Option<u64>,
    last_modified: ModificationTime,
    db_server: &'a str,
    db_name: &'a str,
}

enum ModificationTime {
    Time(DateTime<Local>),
    None,
    Timeout,
}

impl fmt::Display for ModificationTime {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            ModificationTime::Time(time) => {
                let delta = chrono::Local::now() - time;
                write!(
                    f,
                    "{} ({})",
                    time.naive_local().trunc_subsecs(0),
                    utils::human_duration_minutes(delta)
                )
            }
            ModificationTime::None => f.write_str("<n/a>"),
            ModificationTime::Timeout => f.write_str("<timeout>"),
        }
    }
}

async fn query_information<'a>(db_server: &'a str, db_name: &'a str) -> Result<Info<'a>> {
    let proxy = SshProxyBuilder::new(db_server).open().await?;
    let conn = match proxy.sql_connect(db_name).await {
        Ok(conn) => conn,
        Err(e) if e.code() == Some(&SqlState::UNDEFINED_DATABASE) => {
            return Ok(Info {
                metadata: None,
                size: None,
                last_modified: ModificationTime::None,
                db_server,
                db_name,
            });
        }
        Err(e) => return Err(e.into()),
    };

    // Fetching last modification time for a history DB can be slow. Set a timeout
    // to abort query that takes too long.
    //
    // It's not possible to simply cancel the `Future` because the Postgres Protocol
    // does not allow canceling a specific query, and it's not possible to know what
    // query is running as result of pipelining.
    conn.execute("SET SESSION statement_timeout = '2s'", &[])
        .await?;

    let (size, metadata, last_modified) = tokio::try_join!(
        conn.db_size(db_name),
        conn.db_metadata(db_name),
        conn.last_modified().then(|result| async {
            match result {
                Ok(Some(time)) => Ok(ModificationTime::Time(time)),
                Ok(None) => Ok(ModificationTime::None),
                // Postgres doesn't discriminate between different reasons for cancellation. Be
                // optimistic and assume it's because of our `statement_timeout`.
                Err(e) if e.code() == Some(&SqlState::QUERY_CANCELED) => {
                    Ok(ModificationTime::Timeout)
                }
                Err(e) => Err(e),
            }
        }),
    )?;

    Ok(Info {
        metadata,
        size,
        last_modified,
        db_server,
        db_name,
    })
}

async fn print_information_for_installation(installation: &InstallConfig) -> Result<()> {
    let (main, history) = tokio::try_join!(
        query_information(&installation.db_server, &installation.db_name),
        query_information(
            &installation.history_db_server,
            &installation.history_db_name,
        ),
    )?;
    println!("Database main:");
    print_information(&main, "    ");
    println!();
    println!("Database history:");
    print_information(&history, "    ");
    Ok(())
}

async fn print_information_for_db(db_server: &str, db_name: &str) -> Result<()> {
    let info = query_information(db_server, db_name).await?;
    if info.size.is_none() {
        return Err(Error::Custom(format!(
            "database {db_server}/{db_name} not found"
        )));
    }
    print_information(&info, "");
    Ok(())
}

fn print_information(info: &Info, prefix: &str) {
    println!("{prefix}server:        {}", info.db_server);
    println!("{prefix}name:          {}", info.db_name);
    println!("{prefix}last modified: {}", info.last_modified);
    if let Some(size) = info.size {
        println!(
            "{prefix}size:          {:.1} GiB",
            size as f32 / 1024.0 / 1024.0 / 1024.0
        );
    } else {
        println!("{prefix}size:          n/a");
    }
    if let Some(metadata) = &info.metadata {
        println!("{prefix}DB source / copied from:");
        println!("{prefix}    from:    {}", metadata.source);
        println!("{prefix}    at:      {}", metadata.creation_time);
        println!("{prefix}    by:      {}", metadata.creator);
        let tainted_str = if metadata.tainted {
            "true ⚠️ (partial DB copy)"
        } else {
            "false"
        };
        println!("{prefix}    tainted: {tainted_str}");
        if let Some(comment) = &metadata.comment {
            println!("{prefix}    comment: {comment}");
        }
    }
}

pub async fn main(root_args: &crate::Args, args: &Args) -> Result<()> {
    match &args.target {
        DbLocation::Installation { name, db } => {
            let repo =
                tco::ansible::git::maybe_updated_repo(root_args.update_repos_interval()).await?;
            let config = ansible::config_quick::load(&repo).await?;
            let Some(installation) = config.installations.get(name) else {
                return Err(Error::Custom(format!("installation {:?} not found", &name)));
            };
            match db {
                Some(db) => print_information_for_db(&installation.db_server, db).await?,
                None => print_information_for_installation(installation).await?,
            }
        }
        DbLocation::Host { name, db } => {
            print_information_for_db(name, db).await?;
        }
    }
    Ok(())
}
