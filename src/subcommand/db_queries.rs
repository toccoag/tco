use super::common::{self, DbOrServerLocation};
use clap::Parser;
use log::trace;
use regex::RegexBuilder;
use std::net::IpAddr;
use std::result::Result as StdResult;
use tco::ansible;
use tco::sql::SqlConnect;
use tco::sql::ssh::SshProxyBuilder;
use tco::utils;
use tco::{Error, Result};
use tokio_postgres as postgres;

#[derive(Parser)]
#[command()]
pub struct Args {
    /// DB or server
    ///
    /// Syntax:
    ///
    /// <INSTALLATION|DB_SERVER>[/DB_NAME]
    ///
    /// If DB_SERVER is specified without DB_NAME, running queries
    /// from all DBs are printed.
    #[arg(value_parser = common::parse_db_or_server_location_arg)]
    target: DbOrServerLocation,

    /// On what type of DB to operate.
    ///
    /// Only meaningful when INSTALLATION is given in which case
    /// this is used to decide to which DB of the installation
    /// to connect.
    #[arg(value_enum, long, short, default_value_t = TypeArg::Main)]
    r#type: TypeArg,

    /// Only show transactions exceeding SECONDS
    #[arg(short, long, value_name = "SECONDS", value_parser = positive_float)]
    min_duration: Option<f32>,

    /// Adjust sorting of output
    #[arg(value_enum, long, short, default_value_t = SortBy::TxTime)]
    sort_by: SortBy,

    /// Show all connections
    ///
    /// By default, the following is excluded:
    ///
    /// - Idle connections (i.e. not in a transaction).
    /// - Queries where the user doesn't have privileges to
    ///   see details (i.e. where the user cannot see
    ///   if the connection is idle).
    ///
    /// Useful to see who is connected to a DB.
    #[arg(long, short = 'a', verbatim_doc_comment)]
    show_all: bool,
}

#[derive(Clone, Copy, clap::ValueEnum)]
enum SortBy {
    /// Transaction start time (oldest first)
    TxTime,

    /// Query start time (oldest first)
    QueryTime,

    /// DB name and PID
    DbName,
}

impl SortBy {
    fn sql_order_by(self) -> &'static str {
        match self {
            SortBy::TxTime => "xact_start",
            SortBy::QueryTime => "query_start",
            SortBy::DbName => "datname, pid",
        }
    }
}

#[derive(Clone, Copy, clap::ValueEnum)]
enum TypeArg {
    /// Main DB
    Main,

    /// History DB
    History,
}

fn positive_float(value: &str) -> StdResult<f32, String> {
    let range = 0.0..1_000_000.0;
    match value.parse::<f32>() {
        Ok(v) if range.contains(&v) => Ok(v),
        Ok(_) => Err(format!("expected float in range {range:?}")),
        Err(e) => Err(e.to_string()),
    }
}

#[allow(clippy::too_many_lines)]
async fn show_queries(
    server: &str,
    db: Option<&str>,
    sort_by: SortBy,
    min_duration: Option<f32>,
    show_all: bool,
) -> Result<()> {
    let db_condition = db
        .map(|_| "AND datname = current_database()")
        .unwrap_or_default();
    let min_duration = min_duration
        .map(|v| format!("AND (now() - xact_start) >= interval '{v} s'"))
        .unwrap_or_default();
    let hide_idle = if show_all {
        ""
    } else {
        "AND state IS NOT NULL AND state <> 'idle'"
    };
    let order_by = sort_by.sql_order_by();
    let sql = format!(
        r#"
        SELECT
          datname AS db_name,
          pid,
          application_name AS app_name,
          extract(second FROM (now() - xact_start))::real AS tx_secs,
          extract(second FROM CASE
              WHEN state IN('idle in transaction', 'idle in transaction (aborted)')
              THEN state_change - query_start
              ELSE now() - query_start
          END)::real AS query_secs,
          state,
          query,
          (SELECT count(*) FROM pg_stat_activity AS "inner" WHERE "inner".leader_pid = pg_stat_activity.pid) AS worker,
          wait_event,
          wait_event_type,
          client_addr,
          client_port,
          usename AS user_name
        FROM
          pg_stat_activity
        WHERE
          datname IS NOT NULL
          AND leader_pid is NULL  -- omit parallel worker processes
          {hide_idle}
          AND pid <> pg_backend_pid()
          {db_condition}
          {min_duration}
        ORDER BY
          {order_by};"#
    );
    let proxy = SshProxyBuilder::new(server).open().await?;
    let conn = proxy.sql_connect(db.unwrap_or("postgres")).await?;
    if db.is_none() && !has_user_full_stat_privileges(&conn).await? {
        info!(
            "SUPERUSER privileges or pg_read_all_stats role missing: some queries may not be \
             shown, use --show-all to show all queries."
        );
    }
    let rows = conn.query(&sql, &[]).await?;

    if rows.is_empty() {
        if show_all {
            println!("<no active connections>");
        } else {
            println!("<no active transactions>");
        }
        return Ok(());
    }

    let wrap_opts = textwrap::Options::with_termwidth()
        .initial_indent("    query:      ")
        .subsequent_indent("                ")
        .break_words(false);

    let skip_empty_lines_at_start = RegexBuilder::new(r"^(?:\s*\n)*(.*)")
        .dot_matches_new_line(true)
        .build()
        .unwrap();

    for row in rows {
        let query: &str = row.get("query");
        let state: Option<&str> = row.get("state");
        trace!("Processing query in state {state:?}: {query}");
        let db_name: &str = row.get("db_name");
        let pid: i32 = row.get("pid");
        let app_name: &str = row.get("app_name");
        let tx_secs: Option<f32> = row.get("tx_secs");
        let query_secs: Option<f32> = row.get("query_secs");
        let worker: i64 = row.get("worker");
        let wait_event: Option<&str> = row.get("wait_event");
        let wait_event_type: Option<&str> = row.get("wait_event_type");
        let user_name: Option<&str> = row.get("user_name");

        let client_addr: Option<IpAddr> = row.get("client_addr");
        let client_port: Option<i32> = row.get("client_port");
        let client_text = client_addr
            .map(|a| utils::prettify_ip(a))
            .unwrap_or_else(|| {
                match client_port {
                    Some(-1) => "Unix Socket",
                    None => "internal",
                    Some(v) => {
                        warn!("Unexpected port value: {v}");
                        "unknown"
                    }
                }
                .to_string()
            });

        let query = textwrap::dedent(query);
        let query = skip_empty_lines_at_start
            .captures(&query)
            .unwrap()
            .get(1)
            .unwrap()
            .as_str();
        let query = query.trim_end();
        let query = textwrap::wrap(query, &wrap_opts);

        let query_state = match state {
            Some("active") => "running",
            Some(_) => "completed",
            None => "<insufficient privilege>",
        };

        let wait_event = match (wait_event, wait_event_type) {
            (Some(wait_event), Some(wait_event_type)) => {
                format!("{wait_event_type} / {wait_event}")
            }
            (None, None) => "-".to_string(),
            _ => {
                warn!("Only one of wait_event and wait_event_type is set.");
                "n/a".to_string()
            }
        };

        let tx_secs_text = match tx_secs {
            Some(tx_secs) => format!("{tx_secs:0.3} s"),
            None => match state {
                Some("idle in transaction (aborted)") => "aborted".to_string(),
                Some("active" | "idle") => "-".to_string(),
                None => "<insufficient privilege>".to_string(),
                _ => {
                    warn!("tx_secs is None unexpectedly (state == {state:?}).");
                    "-".to_string()
                }
            },
        };

        let query_time_text = match query_secs {
            Some(query_secs) => format!("{query_secs:0.3} s ({query_state})"),
            None => "-".to_string(),
        };

        println!("{db_name} - {pid}");
        println!(
            "    state:      {}",
            state.unwrap_or("<insufficient privilege>"),
        );
        println!("    app:        {app_name}");
        println!("    user:       {}", user_name.unwrap_or("-"));
        println!("    client:     {client_text}");
        println!("    TX time:    {tx_secs_text}");
        println!("    query time: {query_time_text}");
        println!("    worker:     {worker} process(es)");
        println!("    wait event: {wait_event}");
        for line in query {
            println!("{line}");
        }
        println!();
    }
    Ok(())
}

async fn has_user_full_stat_privileges(client: &postgres::Client) -> Result<bool> {
    Ok(client
        .query_one(
            // SUPERUSER is member of all roles
            "select pg_has_role(current_user, 'pg_read_all_stats', 'member')",
            &[],
        )
        .await?
        .get(0))
}

pub async fn main(root_args: &crate::Args, args: &Args) -> Result<()> {
    match &args.target {
        DbOrServerLocation::Installation { name, db } => {
            let repo =
                tco::ansible::git::maybe_updated_repo(root_args.update_repos_interval()).await?;
            let config = ansible::config_quick::load(&repo).await?;
            let Some(installation) = config.installations.get(name) else {
                return Err(Error::Custom(format!("installation {:?} not found", &name)));
            };
            let db_server = match args.r#type {
                TypeArg::Main => &installation.db_server,
                TypeArg::History => &installation.history_db_server,
            };
            let db_name = match args.r#type {
                TypeArg::Main => &installation.db_name,
                TypeArg::History => &installation.history_db_name,
            };
            show_queries(
                db_server,
                db.as_deref().or(Some(db_name)),
                args.sort_by,
                args.min_duration,
                args.show_all,
            )
            .await?;
        }
        DbOrServerLocation::Host { name, db } => {
            show_queries(
                name,
                db.as_deref(),
                args.sort_by,
                args.min_duration,
                args.show_all,
            )
            .await?;
        }
    }
    Ok(())
}

#[cfg(test)]
#[cfg(feature = "test-with-secrets")]
mod tests {
    use super::*;
    use postgres::error::SqlState;
    use std::time::Duration;
    use tokio::{task, time, try_join};
    use tokio_postgres as postgres;

    #[tokio::test]
    async fn does_not_err_or_panic() {
        let server = "db5.stage.tocco.cust.vshn.net";
        let db = "nice_master";

        let proxy = SshProxyBuilder::new(server).open().await.unwrap();
        let (client1, client2) = try_join!(proxy.sql_connect(db), proxy.sql_connect(db)).unwrap();

        // Queries to be printed by `show_queries().
        let long_running_query = task::spawn(long_running_query(client1));
        let in_aborted_tx = task::spawn(in_aborted_tx(client2));

        try_join!(
            // With at least `long_running_query` running.
            show_queries(server, None, SortBy::TxTime, None, false),
            show_queries(server, Some(db), SortBy::QueryTime, Some(0.5), false),
            show_queries(server, Some(db), SortBy::DbName, None, true),
            // With no active transaction, probably.
            show_queries(server, Some("template1"), SortBy::TxTime, Some(12.2), true),
        )
        .unwrap();

        long_running_query.abort_handle().abort();
        assert!(long_running_query.await.unwrap_err().is_cancelled());

        in_aborted_tx.abort_handle().abort();
        assert!(in_aborted_tx.await.unwrap_err().is_cancelled());
    }

    /// Keeps a query running for a long period of time (60 s).
    ///
    /// `Future` is expected to be aborted rather than completed.
    async fn long_running_query(client: postgres::Client) {
        client.execute("SELECT pg_sleep(60)", &[]).await.unwrap();
        panic!("sleep terminated early");
    }

    /// Opens a transactions, transitions it into a failed state
    /// and keeps it in that state indefinitely.
    ///
    /// `Future` is expected to be aborted rather than completed.
    async fn in_aborted_tx(mut client: postgres::Client) {
        let tx = client.transaction().await.unwrap();
        assert_eq!(
            tx.execute("SELECT does_not_exist_hopefully", &[])
                .await
                .unwrap_err()
                .code(),
            Some(&SqlState::UNDEFINED_COLUMN)
        );
        // keep TX open in aborted state
        time::sleep(Duration::MAX).await;
        unreachable!();
    }
}
