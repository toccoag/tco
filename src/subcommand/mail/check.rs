use clap::Parser;
use tco::ansible::config::{Config, InstallConfig};
use tco::{Error, Result};

#[derive(Parser)]
#[command()]
pub struct DomainCheckArgs {
    #[arg(long, short)]
    installation: Option<String>,
}

#[allow(clippy::unused_async)] // TODO remove again
pub async fn validate_installation(install_config: &InstallConfig) {
    for (_domain_name, mail_config) in install_config.mail_domains() {
        println!("{mail_config:#?}");
    }
}

pub async fn main(config: &Config, _args: &super::Args, sub_args: &DomainCheckArgs) -> Result<()> {
    match &sub_args.installation {
        Some(installation) => match config.installations.get(installation) {
            Some(installation) => {
                validate_installation(installation).await;
                Err(Error::Custom(
                    "SPF/DKIM check not yet implemented!".to_string(),
                ))
            }
            None => Err(Error::Custom(format!(
                "Installation {installation:?} not found."
            ))),
        },
        None => {
            for (inst_name, config) in &config.installations {
                println!("installation: {inst_name:?}");
                validate_installation(config).await;
                println!();
            }
            Err(Error::Custom(
                "SPF/DKIM check not yet implemented!".to_string(),
            ))
        }
    }
}
