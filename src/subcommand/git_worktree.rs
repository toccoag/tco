use clap::Parser;
use git2::{ErrorClass, ErrorCode};
use std::fs::OpenOptions;
use std::io::Write;
use std::os::unix::fs::OpenOptionsExt;
use std::path::PathBuf;
use tco::git;
use tco::{Error, Result};

#[derive(Parser)]
#[command()]
pub struct Args {
    #[command(subcommand)]
    subcommand: Subcommand,
}

#[derive(Parser)]
#[command()]
enum Subcommand {
    /// Create a Git worktree
    Create(CreateArgs),
}

#[derive(Parser)]
#[command()]
pub struct CreateArgs {
    /// Repository to use
    #[arg(value_enum)]
    repo: RepositorySelection,

    /// Target path for working tree
    #[arg()]
    path: PathBuf,

    /// User name used to clone repository
    ///
    /// Required by Gerrit. Is "jdoe" for Jane Doe.
    #[arg(long, short, required_if_eq("repo", "nice2"))]
    user: Option<String>,
}

#[derive(Clone, clap::ValueEnum)]
enum RepositorySelection {
    Ansible,
    Nice2,
    ToccoClient,
}

impl RepositorySelection {
    fn url(&self, user: Option<&str>) -> String {
        match self {
            RepositorySelection::Ansible => tco::ansible::git::URL.to_string(),
            RepositorySelection::Nice2 => {
                let user = user.expect("missing user name");
                format!("ssh://{user}@git.tocco.ch:29418/nice2")
            }
            RepositorySelection::ToccoClient => {
                "git@gitlab.com:toccoag/tocco-client.git".to_string()
            }
        }
    }

    fn dir_name(&self) -> &'static str {
        match self {
            RepositorySelection::Ansible => "ansible",
            RepositorySelection::Nice2 => "nice2",
            RepositorySelection::ToccoClient => "tocco-client",
        }
    }
}

pub async fn create_tree(root_args: &crate::Args, args: &CreateArgs) -> Result<()> {
    let mut repo_path = dirs::data_local_dir().expect("failed to locate user's data directory");
    repo_path.push("tocco-tco/repositories");
    repo_path.push(args.repo.dir_name());
    let url = args.repo.url(args.user.as_deref());

    println!("Selected {url:?} as source");
    println!("Creating / opening repository at {repo_path:?}");
    let builder = git::RepoBuilder::new(&repo_path, url.as_ref());
    let mut repo = builder.create_or_open().await?;

    if matches!(args.repo, RepositorySelection::Nice2) {
        println!("Creating/updating \"commit-msg\" Gerrit Git hook.");
        let mut file = OpenOptions::new()
            .mode(0o755)
            .create(true)
            .truncate(true)
            .write(true)
            .open(repo_path.join("hooks/commit-msg"))?;
        file.write_all(include_bytes!("gerrit_commit_msg_hook"))?;

        println!("Add \"gitlab\" remote.");
        match repo.as_inner().remote("gitlab", tco::nice::git::URL) {
            Ok(_) => {}
            Err(e) if e.class() == ErrorClass::Config && e.code() == ErrorCode::Exists => {
                debug!("\"gitlab\" remote already configured.");
            }
            Err(e) => return Err(e.into()),
        }

        println!("Set \"origin\" as default remote.");
        let mut config = repo.as_inner().config().unwrap();
        let snapshot = config.snapshot().unwrap();
        let name = "checkout.defaultRemote";
        match snapshot.get_str(name) {
            Ok(_) => {
                debug!("Default remote already set.");
            }
            Err(e) if e.class() == ErrorClass::Config && e.code() == ErrorCode::NotFound => {
                config.set_str(name, "origin").unwrap();
            }
            Err(e) => panic!("{}", e),
        }
    }

    if let Some(interval) = root_args.update_repos_interval() {
        println!("Updating repository …");
        repo.update_every(&[], Some(interval)).await?;
    }

    println!("Creating working tree at {:?}", args.path);
    let name = args
        .path
        .file_name()
        .ok_or_else(|| {
            Error::Custom(format!(
                "repository path {:?} not ending in a file name",
                args.path
            ))
        })?
        .to_str()
        .expect("non-UTF8 file name in path");
    repo.as_inner().worktree(name, &args.path, None)?;

    println!();
    println!("Working tree is ready and you can now use git to manage it:");
    println!();
    println!("    $ git worktree --help");

    Ok(())
}

pub async fn main(root_args: &crate::Args, args: &Args) -> Result<()> {
    match &args.subcommand {
        Subcommand::Create(create_args) => create_tree(root_args, create_args).await,
    }
}
