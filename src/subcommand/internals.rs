use clap::{CommandFactory, Parser, Subcommand};
use clap_complete::Shell;
use std::io;
use std::time::Duration;
use tco::ansible;
use tco::client;
use tco::{Error, Result};

#[derive(Parser)]
#[command()]
pub struct Args {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
enum Commands {
    /// Drop all internal caches
    DropAllCaches,

    /// Print `ansible::config::Config`
    DumpAnsibleConfig(DumpConfigArgs),

    /// Print `ansible::config_quick::Config`
    DumpAnsibleConfigQuick(DumpConfigArgs),

    /// Print client releases
    ///
    /// Print map matching releases from tocco-client repository
    /// to the corresponding commit ID.
    DumpClientReleases,

    /// Shell autocompletion
    ///
    /// Example, bash:
    ///
    ///     . <(tco internals completion bash)
    #[command(verbatim_doc_comment)]
    Completion(CompletionArgs),
}

#[derive(Parser)]
#[command()]
pub struct DumpConfigArgs {
    /// Print only config of given installation.
    installation: Option<String>,
}

#[derive(Parser)]
#[command()]
pub struct CompletionArgs {
    #[arg(value_enum)]
    shell: Shell,
}

async fn print_ansible_config(
    update_interval: Option<Duration>,
    installation: Option<&str>,
) -> Result<()> {
    let repo = ansible::git::maybe_updated_repo(update_interval).await?;
    let config = ansible::config::load(&repo).await?;
    if let Some(installation) = installation {
        if let Some(inst_config) = config.installations.get(installation) {
            println!("{inst_config:#?}");
            Ok(())
        } else {
            Err(Error::Custom(format!(
                "installation {installation} not found"
            )))
        }
    } else {
        println!("{:#?}", config.installations);
        Ok(())
    }
}

async fn print_ansible_config_quick(
    update_interval: Option<Duration>,
    installation: Option<&str>,
) -> Result<()> {
    let repo = ansible::git::maybe_updated_repo(update_interval).await?;
    let config = ansible::config_quick::load(&repo).await?;
    if let Some(installation) = installation {
        if let Some(inst_config) = config.installations.get(installation) {
            println!("{inst_config:#?}");
            Ok(())
        } else {
            Err(Error::Custom(format!(
                "installation {installation} not found"
            )))
        }
    } else {
        println!("{:#?}", config.installations);
        Ok(())
    }
}

async fn print_client_releases(update_interval: Option<Duration>) -> Result<()> {
    let repo = client::git::maybe_updated_repo(update_interval).await?;
    let releases = client::releases::releases(&repo)?;
    println!("{releases:#?}");
    Ok(())
}

pub async fn main(root_args: &crate::Args, args: &Args) -> Result<()> {
    match &args.command {
        Commands::DropAllCaches => tco::utils::drop_all_caches().await?,
        Commands::DumpAnsibleConfig(args) => {
            print_ansible_config(
                root_args.update_repos_interval(),
                args.installation.as_deref(),
            )
            .await?;
        }
        Commands::DumpAnsibleConfigQuick(args) => {
            print_ansible_config_quick(
                root_args.update_repos_interval(),
                args.installation.as_deref(),
            )
            .await?;
        }
        Commands::DumpClientReleases => {
            print_client_releases(root_args.update_repos_interval()).await?;
        }
        Commands::Completion(args) => {
            clap_complete::generate(
                args.shell,
                &mut crate::Args::command(),
                "tco",
                &mut io::stdout(),
            );
        }
    }
    Ok(())
}
