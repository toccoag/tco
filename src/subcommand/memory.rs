use crate::HTTP_SEMAPHORE;
use crate::subcommand::common::EnvArg;
use clap::Parser;
use std::collections::HashMap;
use tco::ansible;
use tco::ansible::config::InstallConfig;
use tco::nice;
use tco::nice::status::Status;
use tco::{Error, Result};
use tokio::task;

#[derive(Parser)]
#[command()]
pub struct Args {
    /// Limit output to given installation
    #[arg()]
    installation: Option<String>,

    /// Limit output to selected environment.
    #[arg(value_enum, long, short, default_value_t = EnvArg::All)]
    environment: EnvArg,

    /// Limit output to installations where configured and active
    /// heap memory differs.
    ///
    /// Installations where active memory cannot be fetched
    /// from their /status-tocco page are not printed.
    #[arg(long, short = 'H', conflicts_with = "csv")]
    heap_mismatch_only: bool,

    /// Adjust sorting of output
    #[arg(value_enum, long, short, default_value_t = SortBy::Installation, conflicts_with = "csv")]
    sort_by: SortBy,

    /// Print memory in CSV format
    #[arg(long)]
    csv: bool,
}

#[derive(Clone, Copy, clap::ValueEnum)]
enum SortBy {
    Installation,
    Memory,
    UsedMemory,
}

async fn fetch_status_pages(
    installations: &HashMap<String, InstallConfig>,
) -> HashMap<&str, Status> {
    let status_pages_tasks: Vec<_> = installations
        .keys()
        .map(|name| {
            let url = format!("https://{name}.tocco.ch");
            task::spawn(async move {
                let _semaphore = HTTP_SEMAPHORE.acquire().await.unwrap();
                nice::status::get(&url).await
            })
        })
        .collect();

    let mut status_pages = HashMap::new();
    for (name, page) in installations.keys().zip(status_pages_tasks) {
        match page.await.expect("thread panicked") {
            Ok(page) => {
                status_pages.insert(&name[..], page);
            }
            Err(e) => error!("Failed to fetch status page for installation {name}: {e}."),
        }
    }
    status_pages
}

async fn print_memory_settings_in_plain_text(
    installations: &HashMap<String, InstallConfig>,
    sort_by: SortBy,
    heap_mismatch_only: bool,
) {
    let status_pages = fetch_status_pages(installations).await;
    let mut installs: Vec<_> = installations.values().collect();
    installs.sort_unstable_by_key(|config| &config.installation_name);
    match sort_by {
        SortBy::Installation => {}
        SortBy::Memory => installs.sort_by_key(|config| config.set_memory_bytes),
        SortBy::UsedMemory => {
            installs.sort_by_cached_key(|config| {
                status_pages
                    .get(&config.installation_name[..])
                    .map(|status| (status.used_mem * 1_000_000) / status.max_mem)
                    .unwrap_or(0)
            });
        }
    }
    println!(concat!(
        "                   |<  <  <  <  SOURCE: ANSIBLE   >  >  >|",
        "< SOURCE: STATUS PAGE / JAVA HEAP >"
    ));
    println!(concat!(
        "INSTALLATION       |        CONFIGURED     REQUEST       |",
        " MAX           USED       % USED"
    ));
    println!(concat!(
        "                   | VERSION         HEAP          LIMIT |",
        "       TOTAL          FREE"
    ));
    let mut heap_mismatch = false;
    for config in installs {
        let status_page = status_pages.get(&config.installation_name[..]);
        let heap_mem_mbytes = config.heap_memory_bytes / 1024 / 1024;
        let asterisk = match status_page.as_ref() {
            Some(status_page) if status_page.max_mem.abs_diff(heap_mem_mbytes) > 5 => {
                heap_mismatch = true;
                '*'
            }
            Some(_) | None if heap_mismatch_only => continue,
            Some(_) | None => ' ',
        };

        print!(
            "{:19}   {:5}   {:4.1}   {:4.1}{asterisk}  {:4.1}   {:4.1}",
            config.installation_name,
            config.version_pretty(),
            config.set_memory_bytes as f32 / (1024_u64).pow(3) as f32,
            config.heap_memory_bytes as f32 / (1024_u64).pow(3) as f32,
            config.memory_request_bytes as f32 / (1024_u64).pow(3) as f32,
            config.memory_limit_bytes as f32 / (1024_u64).pow(3) as f32,
        );
        if let Some(status_page) = status_page.as_ref() {
            print!(
                "   {:4.1}{asterisk}  {:4.1}   {:4.1}   {:4.1}  {:3.0}%",
                status_page.max_mem as f32 / 1024.0,
                status_page.total_mem as f32 / 1024.0,
                status_page.used_mem as f32 / 1024.0,
                status_page.free_mem as f32 / 1024.0,
                status_page.used_mem as f32 / status_page.max_mem as f32 * 100.0,
            );
        }
        println!();
    }
    println!();
    println!("Unit is GiB");
    println!();
    println!("% USED is the percentage of MAX which is USED. USED memory includes");
    println!("memory ready for collection.");
    if heap_mismatch {
        println!();
        println!("* → Java heap memory differs from heap memory seen on /status-tocco.");
        println!("    by more than 5 MiB. Current memory setting has likely never been");
        println!("    applied.");
    }
}

async fn print_memory_settings_as_csv(installations: &HashMap<String, InstallConfig>) {
    let mut grouped_by_customer: HashMap<_, Vec<_>> = HashMap::new();
    for config in installations.values() {
        grouped_by_customer
            .entry(&config.customer_name)
            .or_default()
            .push(config);
    }

    let mut grouped_by_customer: Vec<_> = grouped_by_customer.into_iter().collect();
    grouped_by_customer
        .sort_unstable_by(|(customer_a, _), (customer_b, _)| customer_a.cmp(customer_b));

    let status_pages = fetch_status_pages(installations).await;

    println!("Generated {},,,,,", chrono::Local::now());
    println!("Numbers in GiB,,,,,");
    println!("¹ → values from Ansible,,,,,");
    println!("² → values from status page (heap memory),,,,,");
    println!(",,,,,");
    println!(concat!(
        "Customer,Installation,Version¹,Environment¹,Configured¹,Java Heap¹,",
        "Pod Requested¹,Pod Limit¹, Max. Memory²,Total Memory²,Used Memory²,",
        "Free Memory²",
    ));
    for (_customer, mut installations) in grouped_by_customer {
        installations.sort_unstable_by(|a, b| a.installation_name.cmp(&b.installation_name));
        for installation in &installations {
            let status_page = status_pages.get(&installation.installation_name[..]);

            print!(
                "{},{},{},{},{},{},{},{}",
                installation.customer_name,
                installation.installation_name,
                installation.version_pretty(),
                installation.environment,
                installation.set_memory_bytes as f32 / (1024_u64).pow(3) as f32,
                installation.heap_memory_bytes as f32 / (1024_u64).pow(3) as f32,
                installation.memory_request_bytes as f32 / (1024_u64).pow(3) as f32,
                installation.memory_limit_bytes as f32 / (1024_u64).pow(3) as f32,
            );
            if let Some(status_page) = status_page.as_ref() {
                println!(
                    ",{},{},{},{}",
                    status_page.max_mem as f32 / 1024.0,
                    status_page.total_mem as f32 / 1024.0,
                    status_page.used_mem as f32 / 1024.0,
                    status_page.free_mem as f32 / 1024.0,
                );
            } else {
                println!(",,,,");
            }
        }
    }
}

pub async fn main(root_args: &crate::Args, args: &Args) -> Result<()> {
    let repo = tco::ansible::git::maybe_updated_repo(root_args.update_repos_interval()).await?;
    let mut config = ansible::config::load(&repo).await?;
    let config = match &args.installation {
        Some(installation) => match config.installations.remove_entry(installation) {
            Some((name, config)) => {
                let mut map = HashMap::new();
                map.insert(name, config);
                map
            }
            None => {
                return Err(Error::Custom(format!(
                    "Installation {installation:?} not found."
                )));
            }
        },
        None => {
            config
                .installations
                .retain(|_k, v| args.environment.includes(v.environment));
            config.installations
        }
    };
    if args.csv {
        print_memory_settings_as_csv(&config).await;
    } else {
        print_memory_settings_in_plain_text(&config, args.sort_by, args.heap_mismatch_only).await;
    }
    Ok(())
}
