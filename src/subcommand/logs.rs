use chrono::SubsecRound as _;
use clap::Parser;
use core::net::IpAddr;
use futures::io::Lines;
use futures::{AsyncBufRead, AsyncBufReadExt, TryStreamExt};
use kube::api::ResourceExt;
use owo_colors::AnsiColors;
use serde_json::Value;
use std::borrow::Cow;
use std::ops::RangeInclusive;
use std::path::{Path, PathBuf};
use std::result::Result as StdResult;
use std::str::FromStr;
use tco::git;
use tco::k8s;
use tco::nice;
use tco::nice::k8s::Level;
use tco::nice::k8s::Message;
use tco::nice::nginx::Method;
use tco::nice::nginx::NginxMessage;
use tco::utils;
use tco::{Error, Result};
use tokio::fs::File;

const INDENT_UNCOLORED: &str = "    ";

#[allow(clippy::struct_excessive_bools)]
#[derive(Parser)]
#[command()]
pub struct Args {
    /// Installation for which to print logs
    ///
    /// If omitted, logs from current namespace / project are printed:
    ///
    ///     $ oc project nice-<INSTALLATION>
    ///     $ tco logs
    #[arg(verbatim_doc_comment)]
    installation: Option<String>,

    /// Read JSON-based logs from file
    ///
    /// Use *-raw variants to store logs in file:
    ///
    ///     tco logs -c [nice-raw|nginx-raw] > FILE
    ///
    /// Or, alternatively:
    ///
    ///     oc logs -c [nice|nginx] POD > FILE
    ///
    /// Then view it using --file:
    ///
    ///     tco logs --file FILE
    ///
    /// Use with `-c nginx` to parse Nginx logs.
    #[arg(
        short = 'P',
        long,
        conflicts_with = "installation",
        verbatim_doc_comment
    )]
    file: Option<PathBuf>,

    /// Read logs from stdin
    ///
    /// Like --file but content is read from stdin:
    ///
    ///    cat FILE | tco logs --stdin
    #[arg(
        long,
        conflicts_with = "installation",
        conflicts_with = "file",
        verbatim_doc_comment
    )]
    stdin: bool,

    /// Container for which to print logs
    ///
    /// See --file to see usage examples for *-raw options.
    #[arg(value_enum, long, short, default_value_t = Container::Nice)]
    container: Container,

    /// Show only logs with at least this severity
    ///
    /// Ignored unless `--container nice` was selected.
    ///
    /// Short names, `-l e`, `-l w`, etc. are accepted too.
    #[arg(value_enum, long, short, default_value_t = Level::Trace)]
    level: Level,

    /// Show full stack traces
    ///
    /// By default, "at nice.core.….SomeClass(SomeClass.java:115)" lines
    /// are omitted.
    ///
    /// Ignored unless `--container nice` was selected.
    #[arg(long, short = 'F')]
    full_trace: bool,

    /// Hide meta information
    ///
    /// Hides clientip, user_name, business_unit, etc. associated with
    /// a log message.
    #[arg(long)]
    hide_meta: bool,

    /// Limit output to log messages newer than N seconds
    #[arg(
        long = "since",
        short = 's',
        conflicts_with = "file",
        conflicts_with = "stdin",
        value_name = "SECONDS",
        // API doesn't accept 0 and returns no log messages, and
        // no error, for a very large value.
        value_parser = clap::value_parser!(i64).range(1..=31536000)
    )]
    since_seconds: Option<i64>,

    /// Keep printing new messages as they are generated
    #[arg(long, short, conflicts_with = "file", conflicts_with = "stdin")]
    follow: bool,

    /// Show logs from container before a restart
    ///
    /// Only works for restarted containers. Use `oc get pods` /
    /// `kubectl get pods` to see restarts.
    #[arg(short, long)]
    previous: bool,

    /// Display all fields in the nginx logs
    #[arg(short = 'v', long = "verbose", default_value_t = false)]
    verbose: bool,

    /// Filter nginx logs by status code
    ///
    /// May be given multiple times and ranges may be used:
    ///
    ///     -S 100,200-299
    ///
    /// Values must be in range 100..=599.
    #[arg(
        short = 'S',
        long = "status-codes",
        value_delimiter = ',',
        verbatim_doc_comment
    )]
    status_codes: Option<Vec<StatusCodeArg>>,

    /// Filter nginx logs by client IP addresses
    #[arg(short = 'I', long = "client-ips", value_delimiter = ',')]
    client_ips: Option<Vec<IpAddr>>,

    /// Filter nginx logs by HTTP methods (e.g., get, post)
    #[arg(short = 'M', long = "methods", value_delimiter = ',')]
    methods: Option<Vec<Method>>,
}

#[derive(Clone)]
struct StatusCodeArg(RangeInclusive<u16>);

impl FromStr for StatusCodeArg {
    type Err = String;
    fn from_str(s: &str) -> StdResult<Self, Self::Err> {
        let allowed = 100..=599;
        let parse_code = |value: &str| {
            value
                .parse::<u16>()
                .map_err(|_| ())
                .and_then(|start| {
                    if allowed.contains(&start) {
                        Ok(start)
                    } else {
                        Err(())
                    }
                })
                .map_err(|()| format!("{value:?} cannot be parsed as number in range {allowed:?}"))
        };

        let (start, end) = s.split_once('-').unwrap_or((s, s));
        let start = parse_code(start)?;
        let end = parse_code(end)?;
        Ok(StatusCodeArg(start..=end))
    }
}

#[derive(Clone, Copy, clap::ValueEnum)]
enum Container {
    /// human-readable application logs
    Nice,

    /// unmodified, raw JSON-based application logs
    NiceRaw,

    /// human-readable request logs
    Nginx,

    /// unmodified, raw JSON-based HTTP request logs
    NginxRaw,
}

impl Container {
    fn name(self) -> &'static str {
        match self {
            Container::Nice | Container::NiceRaw => "nice",
            Container::Nginx | Container::NginxRaw => "nginx",
        }
    }
}

async fn process_nice_logs<R>(
    logs: &mut Lines<R>,
    namespace: Option<&str>,
    pod: Option<&str>,
    args: &Args,
) -> Result<()>
where
    R: AsyncBufRead + Unpin,
{
    use anstream::println;
    use owo_colors::OwoColorize as _;

    let mut version = None;
    let mut revision = None;
    let width = utils::terminal_width(120);
    while let Some(raw_line) = logs.try_next().await? {
        let parsed_line = tco::nice::k8s::parse_nice_log_line(raw_line.as_ref());
        if let Some(parsed_line) = parsed_line {
            let indent = format!("{}   ", " ".on_color(parsed_line.level.color()));
            let wrap_opts = textwrap::Options::new(width)
                .initial_indent(&indent)
                .subsequent_indent(&indent)
                .break_words(false);
            if version.is_none() {
                version = Some(parsed_line.version.clone());
                revision = Some(parsed_line.revision.clone());
            }
            if args.level >= parsed_line.level {
                println!(
                    "{} {:5} - thread: {}, logger: {}",
                    parsed_line.timestamp.naive_local().trunc_subsecs(0),
                    parsed_line.level.term_styled(),
                    parsed_line.thread_name,
                    parsed_line.logger_name
                );
                println!();

                for line in
                    textwrap::wrap(parsed_line.term_sanitized_message().as_ref(), &wrap_opts)
                {
                    if line.is_empty() {
                        // print colored bar
                        println!("{indent}");
                    } else {
                        println!("{line}");
                    }
                }
                let trace = if args.full_trace {
                    parsed_line.term_sanitized_stack_trace()
                } else {
                    parsed_line
                        .term_sanitized_simplified_stack_trace()
                        .map(|t| Cow::Owned(t))
                };
                if let Some(trace) = trace {
                    println!("{indent}");
                    println!("{indent}{:13}", "Stack trace:".cyan());
                    println!("{indent}");
                    for line in textwrap::wrap(&trace, &wrap_opts) {
                        if line.is_empty() {
                            // print colored bar
                            println!("{indent}");
                        } else {
                            println!("{line}");
                        }
                    }
                    if !args.full_trace {
                        println!();
                        println!(
                            "{INDENT_UNCOLORED}{}",
                            "Use --full-trace to show full stack trace.".cyan()
                        );
                    }
                }
                println!();

                if !args.hide_meta {
                    print_nice_meta(&parsed_line);
                }
            }
        } else {
            println!("{}: {raw_line}", "Plain text".cyan());
        }
    }
    if let (Some(version), Some(revision)) = (version, revision) {
        let time = nice::git::commit_time(&revision, &git::UpdateOnce::new()).await?;
        let time = time.map(|time| format!(" ({time})")).unwrap_or_default();
        println!("---");
        println!(
            "{}: {}, {}: {}, {}: {version}, {}: {revision}{time}",
            "namespace".cyan(),
            namespace.unwrap_or("-"),
            "pod".cyan(),
            pod.unwrap_or("-"),
            "version".cyan(),
            "revision".cyan()
        );
    }
    Ok(())
}

fn print_nice_meta(message: &Message) {
    use anstream::{print, println};
    use owo_colors::OwoColorize as _;

    let mut meta: Vec<_> = message.term_sanitized_meta();
    meta.sort_by(|(key_a, _), (key_b, _)| key_a.cmp(key_b));
    if !meta.is_empty() {
        print!("{INDENT_UNCOLORED}");
        for (idx, (key, value)) in meta.iter().enumerate() {
            print!("{}: {value}", key.magenta());
            if idx < meta.len() - 1 {
                print!(", ");
            }
        }
        println!();
        println!();
    }
}

async fn process_nginx_logs<R>(
    logs: &mut Lines<R>,
    _namespace: Option<&str>,
    _pod: Option<&str>,
    args: &Args,
    verbose: bool,
) -> Result<()>
where
    R: AsyncBufRead + Unpin,
{
    use anstream::println;
    use futures::StreamExt;
    use owo_colors::OwoColorize;

    // Define thresholds for request time coloring
    let request_time_warning_threshold = 10.0; // 10 seconds for yellow
    let request_time_critical_threshold = 50.0; // 50 seconds for red

    while let Some(raw_line_result) = logs.next().await {
        let raw_line = match raw_line_result {
            Ok(line) => line,
            Err(e) => {
                warn!("Error reading line: {:?}", e);
                continue;
            }
        };

        // Attempt to parse the line
        let Some(parsed_line) = tco::nice::nginx::parse_nginx_log_line(raw_line.as_ref()) else {
            // Format and print lines that can't be parsed as JSON
            print_unparsable_line(raw_line.as_ref());
            continue;
        };

        // Apply filters
        if !nginx_filter_by_methods(&parsed_line, &args.methods) {
            continue; // Skip logs with methods not in the list
        }

        if !nginx_filter_by_client_ips(&parsed_line, &args.client_ips) {
            continue; // Skip logs with IP addresses not in the list
        }

        if !nginx_filter_by_status_codes(&parsed_line, &args.status_codes) {
            continue; // Skip logs with status codes not in the list
        }

        // Change status color
        let marker_color = tco::nice::nginx::match_color_to_statuscode(parsed_line.status);

        let indent = format!("{}   ", " ".on_color(marker_color));

        // Determine request time color as an AnsiColors
        let request_time_color = if parsed_line.request_time >= request_time_critical_threshold {
            AnsiColors::Red
        } else if parsed_line.request_time >= request_time_warning_threshold {
            AnsiColors::Yellow
        } else {
            AnsiColors::Default
        };

        println!(
            "{} - {} - {}",
            parsed_line.timestamp.naive_local(),
            parsed_line.method,
            parsed_line.uri
        );
        println!();

        print_nginx_log_details(&parsed_line, &indent, request_time_color, verbose);
    }
    Ok(())
}

/// Helper function to filter by request methods
fn nginx_filter_by_methods(parsed_line: &NginxMessage, methods: &Option<Vec<Method>>) -> bool {
    if let Some(methods) = methods {
        methods.contains(&parsed_line.method)
    } else {
        true
    }
}

/// Helper function to filter by client IP addresses
fn nginx_filter_by_client_ips(
    parsed_line: &NginxMessage,
    client_ips: &Option<Vec<IpAddr>>,
) -> bool {
    if let Some(client_ips) = client_ips {
        client_ips.contains(&parsed_line.client_addr)
    } else {
        true
    }
}

/// Helper function to filter by status codes
fn nginx_filter_by_status_codes(
    parsed_line: &NginxMessage,
    status_codes: &Option<Vec<StatusCodeArg>>,
) -> bool {
    if let Some(status_codes) = status_codes {
        status_codes
            .iter()
            .any(|range| range.0.contains(&parsed_line.status))
    } else {
        true
    }
}

/// Function to format unparsable nginx lines at least a bit
fn print_unparsable_line(raw_line: &str) {
    match serde_json::from_str::<Value>(raw_line) {
        Ok(json_value) => {
            if let Some(obj) = json_value.as_object() {
                warn!("Unparsable Nginx log entry found, printing raw data from JSON.");
                let mut obj_vec: Vec<_> = obj.iter().collect();
                obj_vec.sort_by(|a, b| a.0.cmp(b.0));
                println!("Raw, unparsable log entry:");
                for (key, value) in obj_vec {
                    println!("    {key}: {value}");
                }
                println!();
            } else {
                // If the root element is not an object, print the entire JSON value
                warn!("Unparsable JSON line in Nginx logs: {raw_line}");
            }
        }
        Err(e) => {
            if raw_line.starts_with('{') {
                // Some JSON we fail to parse

                warn!("Unparsable JSON-like line in Nginx logs: {e}: {raw_line}");
            } else {
                // Not JSON. Must be a log message from Nginx (warning, startup message, etc).

                println!("{raw_line}");
            }
        }
    }
}

fn print_nginx_log_details(
    parsed_line: &NginxMessage,
    indent: &str,
    request_time_color: AnsiColors,
    verbose: bool,
) {
    use anstream::{print, println};
    use owo_colors::OwoColorize;

    println!(
        "{indent}{:14}: {}",
        "Status".cyan(),
        parsed_line.status_pretty()
    );
    println!("{indent}{:14}: {}", "URL".cyan(), parsed_line.url);
    println!(
        "{indent}{:14}: {} bytes",
        "Body sent".cyan(),
        parsed_line.body_bytes_sent
    );
    println!(
        "{indent}{:14}: {} s",
        "Request time".cyan(),
        parsed_line.request_time.color(request_time_color)
    );
    if let Some(user_name) = &parsed_line.user_name {
        print!("{indent}{:14}: {user_name}", "User".cyan(),);
        if let Some(user_pk) = parsed_line.user_pk {
            print!(" (PK: {user_pk}");
            if let Some(api_key_pk) = parsed_line.api_key_pk {
                print!(", API key PK: {api_key_pk}");
            }
            print!(")");
        }
        println!();
    }
    if let Some(referrer) = &parsed_line.referrer {
        println!("{indent}{:14}: {referrer}", "Referrer".cyan());
    }
    if let Some(user_agent) = &parsed_line.user_agent {
        println!("{indent}{:14}: {user_agent}", "User agent".cyan());
    }

    if verbose {
        println!(
            "{indent}{:14}: {}",
            "IP".cyan(),
            utils::prettify_ip(parsed_line.client_addr)
        );
        if let Some(business_unit) = &parsed_line.business_unit {
            println!("{indent}{:14}: {business_unit}", "Business Unit".cyan());
        }
        if let Some(request_id) = &parsed_line.request_id {
            println!("{indent}{:14}: {request_id}", "Request ID".cyan());
        }
        if let Some(session_id) = &parsed_line.session_id {
            println!("{indent}{:14}: {session_id}", "Session ID".cyan());
        }
        if let Some(language) = &parsed_line.language {
            println!("{indent}{:14}: {language}", "Language".cyan());
        }
        if let Some(client) = &parsed_line.client {
            println!("{indent}{:14}: {client}", "Client".cyan());
        }
        if let Some(timezone) = &parsed_line.timezone {
            println!("{indent}{:14}: {timezone}", "Timezone".cyan());
        }
        if let Some(origin) = &parsed_line.origin {
            println!("{indent}{:14}: {origin}", "Origin".cyan());
        }
        if let Some(sec_fetch_dest) = &parsed_line.sec_fetch_dest {
            println!("{indent}{:14}: {sec_fetch_dest}", "Sec-Fetch-Dest".cyan());
        }
        if let Some(sec_fetch_mode) = &parsed_line.sec_fetch_mode {
            println!("{indent}{:14}: {sec_fetch_mode}", "Sec-Fetch-Mode".cyan());
        }
        if let Some(sec_fetch_site) = &parsed_line.sec_fetch_site {
            println!("{indent}{:14}: {sec_fetch_site}", "Sec-Fetch-Site".cyan());
        }
        if let Some(cache) = &parsed_line.cache {
            println!("{indent}{:14}: {}", "Cache".cyan(), cache);
        }
    }

    println!();
}

async fn print_raw<R>(logs: &mut Lines<R>) -> Result<()>
where
    R: AsyncBufRead + Unpin,
{
    while let Some(line) = logs.try_next().await? {
        println!("{line}");
    }
    Ok(())
}

async fn pod_lines(args: &Args) -> Result<(Lines<impl AsyncBufRead>, String, String)> {
    let pod_api = k8s::pod_api_client_no_timeout(args.installation.as_deref()).await?;
    let Some(pod) = k8s::any_pod_for_installation(&pod_api).await? else {
        return Err(Error::Custom("no running pod found".to_string()));
    };
    let pod_name = pod.name_any();
    debug!("Using logs from pod {pod_name}.");
    let lines = k8s::logs(
        &pod_api,
        &pod_name,
        args.container.name(),
        args.follow,
        args.previous,
        args.since_seconds,
    )
    .await?
    .lines();
    let namespace_name = pod.namespace().unwrap_or_else(|| "-".to_string());
    Ok((lines, namespace_name, pod_name))
}

async fn file_lines(file: &Path) -> Result<Lines<impl AsyncBufRead>> {
    use tokio_util::compat::TokioAsyncReadCompatExt;

    let reader = tokio::io::BufReader::new(File::open(file).await?).compat();
    Ok(reader.lines())
}

fn stdin_lines() -> Lines<impl AsyncBufRead> {
    use tokio_util::compat::TokioAsyncReadCompatExt;

    let reader = tokio::io::BufReader::new(tokio::io::stdin()).compat();
    reader.lines()
}

/// Source from where logs are read
enum Source<P, F, S> {
    Pod {
        lines: P,
        namespace: String,
        pod: String,
    },
    File(F),
    Stdin(S),
}

pub async fn main(_root_args: &crate::Args, args: &Args) -> Result<()> {
    let lines = match (&args.installation, &args.file, args.stdin) {
        (_installation, None, false) => {
            k8s::check_connection().await?;
            let (lines, namespace, pod) = pod_lines(args).await?;
            Source::Pod {
                lines,
                namespace,
                pod,
            }
        }
        (None, Some(file), false) => Source::File(file_lines(file).await?),
        (None, None, true) => Source::Stdin(stdin_lines()),
        _ => unreachable!("more than one of --file, --stdin and INSTALLATION set"),
    };

    match args.container {
        Container::Nice => match lines {
            Source::Pod {
                mut lines,
                namespace,
                pod,
            } => process_nice_logs(&mut lines, Some(&namespace), Some(&pod), args).await,
            Source::File(mut lines) => process_nice_logs(&mut lines, None, None, args).await,
            Source::Stdin(mut lines) => process_nice_logs(&mut lines, None, None, args).await,
        },
        Container::Nginx => match lines {
            Source::Pod {
                mut lines,
                namespace,
                pod,
            } => {
                process_nginx_logs(&mut lines, Some(&namespace), Some(&pod), args, args.verbose)
                    .await
            }
            Source::File(mut lines) => {
                process_nginx_logs(&mut lines, None, None, args, args.verbose).await
            }
            Source::Stdin(mut lines) => {
                process_nginx_logs(&mut lines, None, None, args, args.verbose).await
            }
        },
        Container::NiceRaw | Container::NginxRaw => match lines {
            Source::Pod { mut lines, .. } => print_raw(&mut lines).await,
            Source::File(mut lines) => print_raw(&mut lines).await,
            Source::Stdin(mut lines) => print_raw(&mut lines).await,
        },
    }
}
