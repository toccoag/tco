use clap::{Parser, Subcommand};
use futures::future;
use std::cmp::Reverse;
use std::collections::HashMap;
use std::result::Result as StdResult;
use tco::ansible;
use tco::ansible::config::EsCluster;
use tco::elasticsearch::index_stats::{self, IndexStats};
use tco::http_client;
use tco::{Error, Result};

macro_rules! help_doc_elastic_vpn_hint {
    () => {
        concat!(
            "Note about connection *timeouts*:\n",
            "\n",
            "Elasticsearch clusters use an IP whitelist. Try connecting to VPN first:\n",
            "\n",
            "    $ wg-quick up wg-tocco\n",
            "\n",
            "Alternatively, for superusers, try connecting from a whitelisted IP via SSH proxy:\n",
            "\n",
            "    $ ssh -D 3333 -N tocco-proxy@git.tocco.ch &\n",
            "    $ HTTPS_PROXY=\"socks5h://localhost:3333\" tco elasticsearch ...",
        )
    };
}
pub(crate) use help_doc_elastic_vpn_hint;

#[derive(Parser)]
#[command()]
pub struct Args {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
enum Commands {
    #[command(
        about = "Print cluster and node health / overview",
        long_about = concat!(
            "Print cluster and node health / overview\n",
            "\n",
            help_doc_elastic_vpn_hint!(),
        ),
        verbatim_doc_comment,
    )]
    Health(HealthArgs),

    /// List cluster / node details including credentials
    Info(InfoArgs),

    #[command(
        about = "Execute raw GET request against clusters.",
        long_about = concat!(
            "Execute raw GET request against clusters.\n",
            "\n",
            "Does a GET request against all clusters and prints\n",
            "the returned body on stdout.\n",
            "\n",
            "An attempt is made to detect JSON and format it.\n",
            "\n",
            help_doc_elastic_vpn_hint!(),
        ),
        verbatim_doc_comment,
    )]
    Raw(RawArgs),

    #[doc = help_doc_elastic_vpn_hint!()]
    #[command(
        about = "List indexes and their stats.",
        long_about = concat!(
            "List indexes and their stats.\n",
            "\n",
            help_doc_elastic_vpn_hint!(),
        ),
        verbatim_doc_comment,
    )]
    IndexStats(IndexStatsArgs),
}

#[derive(Parser)]
#[command()]
pub struct HealthArgs {
    #[command(flatten)]
    common: CommonArgs,
}

#[derive(Parser)]
#[command()]
pub struct InfoArgs {
    #[command(flatten)]
    common: CommonArgs,
}

#[derive(Parser)]
#[command()]
pub struct RawArgs {
    /// API against which to do a GET request.
    ///
    /// Mostly useful for /_cat/* API endpoints. "/_cat", the default endpoint,
    /// can be used to list available /_cat/* endpoints.
    ///
    /// Use the ?help to see available fields:
    ///
    ///     '/_cat/indices?help'
    ///
    /// Then select them via h= and sort using s=:
    ///
    ///     '/_cat/indices?h=index,search.query_time&s=search.query_time'
    ///
    /// Useful query parameters:
    ///
    ///     h=FIELD1[,FIELD2]...   select fields (comma-separated)
    ///     s=FIELD1[,FIELD2]...   sort by fields (comma-separated)
    ///     v:                     show headers
    ///     format=json            format output as JSON
    ///
    /// Can also be used for other endpoints like /{index}/_search, /{index}/_settings
    /// and /{index}/mappings:
    ///
    ///     • '/nice-tocco/_search?q="peter gerber"&size=1'
    ///     • '/nice-tocco/_settings'
    ///     • '/nice-tocco/_mappings'
    ///
    /// Show running tasks with execution time:
    ///
    ///     • '/_tasks?actions=indices:*&wait_for_completion&detailed'
    ///
    /// Quote argument in terminal as indicated above.
    #[arg(default_value = "/_cat", value_parser = parse_api_endpoint, verbatim_doc_comment)]
    api: String,

    /// Limit output to result as returned by API
    #[arg(long, short, requires = "node")]
    quiet: bool,

    #[command(flatten)]
    common: CommonArgs,
}

#[derive(Parser)]
#[command()]
pub struct IndexStatsArgs {
    /// Sort output
    #[arg(value_enum, short, long, default_value_t = SortBy::Name)]
    sort_by: SortBy,

    /// Print data as machine-readable CSV
    #[arg(long, short, conflicts_with = "sort_by")]
    csv: bool,

    #[command(flatten)]
    common: CommonArgs,
}

#[derive(clap::Args)]
pub struct CommonArgs {
    /// Send request to this node
    ///
    /// BY default, all clusters are queried by contacting an arbitrary
    /// node within each cluster.
    ///
    /// Use this, to limit a request to a specific cluster or when a node
    /// is down and, thus, cannot be contacted.
    ///
    /// Specify a FQDN like "es3.prod.tocco.cust.vshn.net".
    #[arg(short, long)]
    node: Option<String>,
}

#[derive(Clone, Copy, clap::ValueEnum)]
enum SortBy {
    /// Number of documents
    Docs,

    /// Index name
    Name,

    /// Number of replicas
    Replicas,

    /// Primary size
    Size,
}

fn parse_api_endpoint(value: &str) -> StdResult<String, String> {
    if value.starts_with('/') {
        Ok(value.to_string())
    } else {
        Err("value must start with '/'".to_string())
    }
}

async fn print_index_stats(args: &IndexStatsArgs, clusters: &[(String, EsCluster<'_>)]) {
    let cluster_stats: Vec<_> = clusters
        .iter()
        .map(|c| index_stats::load_all(c.1.user_name(), c.1.password(), &c.0))
        .collect();
    let cluster_stats = future::join_all(cluster_stats).await;
    let printer = if args.csv {
        print_csv_header();
        print_index_stats_for_cluster_as_csv
    } else {
        print_index_stats_for_cluster
    };
    for (cluster, stats) in clusters.iter().zip(&cluster_stats) {
        match stats {
            Ok(stats) => printer(&cluster.1, stats, args.sort_by),
            Err(e) => error!("Failed to fetch indexes: {e}."),
        }
    }
}

fn print_csv_header() {
    println!("Generated {},,,,,,", chrono::Local::now());
    println!("¹ -> Lexicographically first node in cluster,,,,,,");
    println!("² -> Size without replicas (i.e. size of a single copy),,,,,,");
    println!("³ -> Total size including replicas,,,,,,");
    println!(",,,,,,");
    println!("Cluster¹,Index Name,Replicas,Doc Count,Primary Size (GiB)²,Total Size (GiB)³");
}

fn print_index_stats_for_cluster(
    cluster: &EsCluster,
    stats: &HashMap<String, IndexStats>,
    sort_by: SortBy,
) {
    let mut indexes: Vec<_> = stats.values().collect();

    indexes.sort_unstable_by_key(|i| &i.name[..]);
    match sort_by {
        SortBy::Docs => indexes.sort_by_key(|i| Reverse(i.docs)),
        SortBy::Name => { /* already sorted by name*/ }
        SortBy::Replicas => indexes.sort_by_key(|i| Reverse(i.replicas)),
        SortBy::Size => indexes.sort_by_key(|i| Reverse(i.primary_size)),
    }

    print_cluster_title(cluster);
    println!(
        "NAME                       HEALTH  STATUS  REPLICAS       DOCS     PRIMARY     TOTAL"
    );
    for index in indexes {
        anstream::println!(
            "{:25}  {:6}  {:6}  {:8}  {:9}  {:6.1} GiB  {:6.1} GiB",
            index.name,
            index.health.term_styled(),
            index.status.term_styled(),
            index.replicas,
            index.docs,
            index.primary_size as f32 / 1024.0 / 1024.0 / 1024.0,
            index.total_size as f32 / 1024.0 / 1024.0 / 1024.0
        );
    }
    println!();
}

fn print_index_stats_for_cluster_as_csv(
    cluster: &EsCluster,
    stats: &HashMap<String, IndexStats>,
    _sort_by: SortBy,
) {
    let mut indexes: Vec<_> = stats.values().collect();
    indexes.sort_unstable_by_key(|i| &i.name[..]);
    for index in indexes {
        println!(
            "{},{},{},{},{},{}",
            cluster.first_node(),
            index.name,
            index.replicas,
            index.docs,
            index.primary_size as f32 / 1024.0 / 1024.0 / 1024.0,
            index.total_size as f32 / 1024.0 / 1024.0 / 1024.0
        );
    }
}

fn print_cluster_title(cluster: &EsCluster) {
    match cluster.nodes() {
        [] => unreachable!(),
        [node] => println!("Cluster of node {node}:\n"),
        [node1, node2] => println!("Cluster of nodes {node1} and {node2}:\n"),
        [node1, node2, ..] => println!("Cluster of nodes {node1}, {node2}, ...:\n"),
    }
}

async fn print_raw_query_for_clusters(args: &RawArgs, clusters: &[(String, EsCluster<'_>)]) {
    for cluster in clusters {
        if !args.quiet {
            print_cluster_title(&cluster.1);
        }
        let node = &cluster.0;
        let user_name = cluster.1.user_name();
        let password = cluster.1.password();
        if let Err(e) = print_raw_query_for_cluster(args, user_name, password, node).await {
            eprintln!("Request failed for {node}: {e}");
        }
        if !args.quiet {
            println!();
        }
    }
}

async fn print_raw_query_for_cluster(
    args: &RawArgs,
    user: &str,
    password: &str,
    node: &str,
) -> Result<()> {
    let endpoint = format!("https://{node}{}", args.api);
    let result = http_client()
        .get(endpoint)
        .basic_auth(user, Some(password))
        .send()
        .await?
        .error_for_status();

    let resp = match result {
        Ok(resp) => resp,
        Err(e) if e.is_timeout() => {
            warn!(
                "A connection to a Elasticsearch cluster timed out. This could mean that you are \
                 sending the request from an IP not whitelisted. Try sending the request via VPN. \
                 See also `tco elasticsearch --help`."
            );
            return Err(e.into());
        }
        Err(e) => return Err(e.into()),
    };

    if resp
        .headers()
        .get("content-type")
        .map(|value| value.as_bytes() == b"application/json")
        .unwrap_or(false)
    {
        let body: serde_json::Value = resp.json().await?;
        println!("{}", serde_json::to_string_pretty(&body)?);
    } else {
        let body = resp.text().await?;
        println!("{body}");
    }
    Ok(())
}

async fn print_cluster_health(clusters: &[(String, EsCluster<'_>)]) {
    for cluster in clusters {
        print_cluster_title(&cluster.1);
        let node = &cluster.0;
        let user_name = cluster.1.user_name();
        let password = cluster.1.password();
        if let Err(e) = print_cluster_health_for_cluster(user_name, password, node).await {
            println!("Request failed for {node}: {e}");
        }
        println!();
    }
}

async fn print_cluster_health_for_cluster(
    user_name: &str,
    password: &str,
    node: &str,
) -> Result<()> {
    let health_url = format!("https://{node}/_cat/health?v&s=cluster");
    let health_request = http_client()
        .get(health_url)
        .basic_auth(user_name, Some(password))
        .send();

    let nodes_url = format!("https://{node}/_cat/nodes?v&s=name");
    let nodes_request = http_client()
        .get(nodes_url)
        .basic_auth(user_name, Some(password))
        .send();

    let (health, nodes) = tokio::join!(health_request, nodes_request);

    println!("Cluster health\n");
    match health {
        Ok(health) => println!("{}", health.text().await?),
        Err(e) if e.is_timeout() => {
            warn!(
                "A connection to a Elasticsearch cluster timed out. This could mean that you are \
                 sending the request from an IP not whitelisted. Try sending the request from the \
                 office network. See also `tco elasticsearch --help`."
            );
            println!("Failed to fetch cluster health: {e}");
        }
        Err(e) => println!("Failed to fetch cluster health: {e}"),
    }
    println!();

    println!("Node health\n");
    match nodes {
        Ok(nodes) => println!("{}", nodes.text().await?),
        Err(e) => println!("Failed to fetch node health: {e}"),
    }

    Ok(())
}

fn print_info(clusters: &[(String, EsCluster<'_>)]) {
    for (idx, cluster) in clusters.iter().map(|i| &i.1).enumerate() {
        let user = cluster.user_name();
        let password = cluster.password();
        let first_node = cluster.first_node();
        println!("Cluster {idx}:");
        println!();
        println!("    Nodes:");
        for node in cluster.nodes() {
            println!("        {node}");
        }
        println!();
        println!("    Credentials:");
        println!("        User:     {user}");
        println!("        Password: {password}");
        println!();
        println!("    CURL examples:");
        println!("        curl --user '{user}:{password}' 'https://{first_node}/_tasks'");
        println!(
            "        curl --user '{user}:{password}' 'https://{first_node}/an-index/_search?q=\"peter%20gerber\"&size=5'"
        );
        println!(
            "        curl --user '{user}:{password}' --request DELETE 'https://{first_node}/an-index'"
        );
        println!();
    }
    println!("{}", help_doc_elastic_vpn_hint!());
}

pub async fn main(root_args: &crate::Args, args: &Args) -> Result<()> {
    let repo = ansible::git::maybe_updated_repo(root_args.update_repos_interval()).await?;
    let config = ansible::config::load(&repo).await?;
    let clusters = config.elasticsearch_clusters();
    let clusters = match &args.command {
        Commands::Health(HealthArgs { common, .. })
        | Commands::IndexStats(IndexStatsArgs { common, .. })
        | Commands::Info(InfoArgs { common, .. })
        | Commands::Raw(RawArgs { common, .. }) => {
            if let Some(node) = &common.node {
                let clusters: Vec<_> = clusters
                    .into_iter()
                    .filter(|c| c.nodes().contains(&&node[..]))
                    .map(|c| (node.clone(), c))
                    .collect();
                if clusters.is_empty() {
                    return Err(Error::Custom(format!(
                        "no cluster found containing node {node}"
                    )));
                }
                assert!(clusters.len() == 1, "node in multiple clusters");
                clusters
            } else {
                clusters
                    .into_iter()
                    .map(|c| (c.first_node().to_owned(), c))
                    .collect()
            }
        }
    };

    match &args.command {
        Commands::Health(_) => print_cluster_health(&clusters).await,
        Commands::IndexStats(args) => print_index_stats(args, &clusters).await,
        Commands::Info(_) => print_info(&clusters),
        Commands::Raw(args) => print_raw_query_for_clusters(args, &clusters).await,
    }

    Ok(())
}
