use crate::subcommand::git_log_common::{self, Args, Mode};
use tco::Result;

pub async fn main(root_args: &crate::Args, args: &Args) -> Result<()> {
    git_log_common::main(root_args, args, Mode::CustomerProdUndeployed).await
}
