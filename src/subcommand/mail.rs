use clap::{Parser, Subcommand};
use tco::Result;
use tco::ansible;

mod check;

#[derive(Parser)]
#[command()]
pub struct Args {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
enum Commands {
    /// Check SPF and DKIM records for mail domains
    DomainCheck(check::DomainCheckArgs),
}

pub async fn main(root_args: &crate::Args, args: &Args) -> Result<()> {
    let repo = tco::ansible::git::maybe_updated_repo(root_args.update_repos_interval())
        .await
        .unwrap();
    let config = ansible::config::load(&repo).await.unwrap();
    match args.command {
        Commands::DomainCheck(ref sub_args) => check::main(&config, args, sub_args).await,
    }
}
