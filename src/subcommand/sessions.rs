use chrono::{DateTime, Local, offset};
use clap::Parser;
use owo_colors::OwoColorize;
use owo_colors::Style;
use owo_colors::colored::Color;
use postgres::Row;
use std::borrow::Cow;
use std::net::IpAddr;
use tco::sql::SqlConnect;
use tco::sql::query::{ClientExt, UserLogin};
use tco::sql::ssh::SshProxyBuilder;
use tco::utils;
use tco::{Error, Result, ansible};
use tokio_postgres as postgres;

const SQL: &str = r#"
SELECT
    p.username AS "user",
    s.ip,
    s.timestamp AS "refresh_time",
    s.user_agent,
    CASE WHEN u.email <> '' THEN u.email ELSE NULL END as email,
    u.firstname,
    u.lastname,
    (SELECT label FROM nice_api_key AS ak WHERE ak.session_key = s.uid) AS api_key
FROM nice_session AS s LEFT OUTER JOIN nice_principal AS p ON s.fk_principal = p.pk
     LEFT OUTER JOIN nice_user AS u ON u.pk = p.fk_user
WHERE s.timestamp > now() - interval '30 minutes'
ORDER BY p.username, s.timestamp DESC;"#;

#[derive(Parser)]
#[command()]
pub struct Args {
    /// Installation name
    #[arg()]
    installation: String,

    /// Show additional information
    #[arg(long, short)]
    verbose: bool,
}

struct Session<'a> {
    email: Option<&'a str>,
    name: String,
    ip: IpAddr,
    ip_pretty: String,
    refresh_time: DateTime<offset::Local>,
    untrusted_api_key: Option<&'a str>,
    untrusted_user_agent: Option<&'a str>,
    user: &'a str,
}

fn parse_row(row: &Row) -> Session<'_> {
    let user: &str = row.get("user");

    let ip: &str = row.get("ip");
    let ip: IpAddr = ip.parse().expect("invalid ip");
    let ip_pretty = utils::prettify_ip(ip);

    let refresh_time: DateTime<offset::Local> = row.get("refresh_time");

    // This is user supplied and unsanitized. Do not print to terminal
    // directly, in unmodified form, as it may contain control characters.
    let untrusted_first_name: &str = row.get("firstname");
    let untrusted_last_name: &str = row.get("lastname");
    let name = format!(
        "{} {}",
        utils::term_sanitize_string_strict(untrusted_first_name),
        utils::term_sanitize_string_strict(untrusted_last_name),
    );

    // This is user supplied and unsanitized. Do not print to terminal
    // directly, in unmodified form, as it may contain control characters.
    let untrusted_api_key: Option<&str> = row.get("api_key");

    // This is user supplied and unsanitized. Do not print to terminal
    // directly, in unmodified form, as it may contain control characters.
    let untrusted_user_agent: Option<&str> = row.get("user_agent");

    let email: Option<&str> = row.get("email");

    Session {
        email,
        name,
        ip,
        ip_pretty,
        refresh_time,
        untrusted_api_key,
        untrusted_user_agent,
        user,
    }
}

async fn print_sessions(client: &mut postgres::Client, verbose: bool) -> Result<()> {
    let sessions = client.query(SQL, &[]).await?;
    let sessions: Vec<_> = sessions.iter().map(|row| parse_row(row)).collect();

    anstream::println!("{}", "Active sessions:".magenta().bold());
    println!();
    let ip_width = sessions
        .iter()
        .map(|s| s.ip_pretty.len())
        .max()
        .unwrap_or_default();

    if sessions.is_empty() {
        anstream::println!("{}", "<no sessions>".cyan());
    } else if !verbose {
        println!(
            "{:1$}  SESSION REFRESH  KEY  NAME                            USER NAME",
            "IP", ip_width,
        );
    }

    let now = Local::now();
    for session in &sessions {
        let style = |condition| {
            if condition {
                Style::new().bold().color(Color::Blue)
            } else {
                Style::new()
            }
        };
        let user_style = style(session.user.ends_with("@tocco.ch"));
        let ip_style = style(utils::is_office_ip(session.ip));
        let name_style = style(session.name == "Support Tocco AG");
        let refresh_age = now - session.refresh_time;
        let refresh_age = format!(
            "{:2} min {:2} s ago",
            refresh_age.num_minutes(),
            refresh_age.num_seconds() % 60
        );

        if verbose {
            anstream::println!("{}:", user_style.style(session.user));
            anstream::println!("    name:         {}", name_style.style(&session.name));
            if let Some(email) = session.email {
                println!("    email:        {email}");
            }
            anstream::println!("    IP:           {}", ip_style.style(&session.ip_pretty));
            println!("    last refresh: {refresh_age}");
            println!(
                "    API key:      {}",
                session
                    .untrusted_api_key
                    .map(|k| utils::term_sanitize_string_strict(k))
                    .unwrap_or(Cow::Borrowed("<none>"))
            );
            if let Some(untrusted_user_agent) = session.untrusted_user_agent {
                println!("    user agent:   {untrusted_user_agent:?}");
            }
            println!();
        } else {
            let is_api_key = session.untrusted_api_key.map(|_| 'x').unwrap_or(' ');
            anstream::println!(
                "{:ip_width$}  {refresh_age}  {is_api_key}    {:30}  {}",
                ip_style.style(&session.ip_pretty),
                name_style.style(&session.name),
                user_style.style(session.user),
                ip_width = ip_width,
            );
        }
    }

    if !sessions.is_empty() {
        if !verbose {
            println!();
        }
        let count = sessions.len();
        let non_api_key_count = sessions
            .iter()
            .filter(|session| session.untrusted_api_key.is_none())
            .count();
        println!(
            "Found {count} active session(s) of which {non_api_key_count} are non-API-key \
             session(s)."
        );
    }
    Ok(())
}

fn print_single_login(login: &UserLogin, name: &str, verbose: bool) {
    println!(
        "{name:10} {:>17} ago  {:30}  {}",
        utils::human_duration_minutes(Local::now() - login.time),
        login.name,
        login.login,
    );
    if verbose {
        if let Some(email) = &login.email {
            println!("                                  <{email}>");
        }
    }
}

async fn print_latest_login(client: &postgres::Client, verbose: bool) -> Result<()> {
    anstream::println!("{}", "Latest logins:".magenta().bold());
    println!();
    let latest_login = client.latest_login().await?;
    println!("             LOGIN TIME           NAME                            USER NAME");
    if let Some(login) = latest_login.global() {
        print_single_login(login, "Overall", verbose);
        println!();
    } else {
        println!("           <none recorded>");
    }
    if let Some(login) = latest_login.tocco() {
        print_single_login(login, "Tocco", verbose);
    }
    if let Some(login) = latest_login.non_tocco() {
        print_single_login(login, "non-Tocco", verbose);
    }
    println!();
    println!("Logins do not include uses of API keys.");
    Ok(())
}

pub async fn main(root_args: &crate::Args, args: &Args) -> Result<()> {
    let repo = tco::ansible::git::maybe_updated_repo(root_args.update_repos_interval()).await?;
    let config = ansible::config_quick::load(&repo).await.unwrap();

    let Some(installation) = config.installations.get(&args.installation) else {
        return Err(Error::Custom(format!(
            "installation {:?} not found",
            args.installation
        )));
    };
    let proxy = SshProxyBuilder::new(&installation.db_server).open().await?;
    let mut client = proxy.sql_connect(&installation.db_name).await?;
    print_sessions(&mut client, args.verbose).await?;
    println!();
    print_latest_login(&client, args.verbose).await?;
    Ok(())
}
