use crate::subcommand::common;
use crate::subcommand::deploy_status_common::{self, CommonArgs};
use clap::Parser;
use tco::client;
use tco::nice::status::Status;
use tco::{Error, Result};

#[derive(Parser)]
#[command()]
pub struct Args {
    /// Package for which to check deployment status.
    ///
    /// Specify any package listed on /status-tocco,
    /// with or without "tocco-" prefix.
    ///
    /// Installation where this package is not installed,
    /// are marked "not installed".
    #[arg(value_parser = parse_package_arg)]
    package: String,

    #[command(flatten)]
    common: CommonArgs,
}

#[allow(clippy::unnecessary_wraps)] // required by API
fn parse_package_arg(arg: &str) -> Result<String> {
    if arg.starts_with("tocco-") {
        Ok(arg.to_string())
    } else {
        Ok(format!("tocco-{arg}"))
    }
}

pub async fn main(root_args: &crate::Args, args: &Args) -> Result<()> {
    let (config, client_repo) = tokio::try_join!(
        common::ansible_config(root_args.update_repos_interval()),
        client::git::maybe_updated_repo(root_args.update_repos_interval()),
    )?;
    let releases = client::releases::releases(&client_repo)?;
    let tag_name_extractor = |status: &Status| {
        let Some(version) = status.client_packages.get(&args.package) else {
            return Ok(None);
        };
        let release = format!("{}@{version}", args.package);
        match releases.get(&release) {
            Some(id) => Ok(Some(id.to_string())),
            None => Err(Error::Custom(format!(
                "failed to find commit associated with release of {release}"
            ))),
        }
    };
    deploy_status_common::main(
        &args.common,
        &config,
        &client_repo,
        tag_name_extractor,
        Some(*client::git::oldest_commit_deployed()),
    )
    .await
}
