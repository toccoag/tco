use std::result::Result as StdResult;
use std::time::Duration;
use tco::ansible::config_quick;
use tco::{Error, Result, ansible, nice};

#[derive(Clone, clap::ValueEnum, PartialEq)]
pub enum EnvArg {
    All,
    Prod,
    Test,
}

impl EnvArg {
    pub fn includes(&self, env: nice::Env) -> bool {
        match (self, env) {
            (EnvArg::All, _)
            | (EnvArg::Prod, nice::Env::Prod)
            | (EnvArg::Test, nice::Env::Test) => true,
            (EnvArg::Prod, nice::Env::Test) | (EnvArg::Test, nice::Env::Prod) => false,
        }
    }
}

pub async fn ansible_config(update_interval: Option<Duration>) -> Result<config_quick::Config> {
    let repo = ansible::git::maybe_updated_repo(update_interval).await?;
    ansible::config_quick::load(&repo).await
}

#[cfg(test)]
#[cfg(feature = "test-with-repo-access")]
mod tests {
    use super::*;
    use test_utils::{LockMode, lock_caches};

    #[tokio::test]
    async fn includes() {
        let _guard = lock_caches(LockMode::Shared).await;
        let repo = ansible::git::repo().await.unwrap();
        let config = ansible::config_quick::load(&repo).await.unwrap();
        let prod = config.installations.get("tocco").unwrap().environment();
        let test = config.installations.get("toccotest").unwrap().environment();

        assert!(EnvArg::Prod.includes(prod));
        assert!(!EnvArg::Prod.includes(test));
        assert!(EnvArg::Test.includes(test));
        assert!(!EnvArg::Test.includes(prod));
    }
}

#[derive(Clone)]
pub enum DbOrServerLocation {
    Host { name: String, db: Option<String> },
    Installation { name: String, db: Option<String> },
}

/// Parse argument in form <INSTALLATION|DB_SERVER>[/DB_NAME]
#[allow(clippy::unnecessary_wraps)] // `Result` required by clap
pub fn parse_db_or_server_location_arg(value: &str) -> StdResult<DbOrServerLocation, String> {
    let (location, db_name) = match value.split_once('/') {
        Some((location, db_name)) => (location, Some(db_name)),
        None => (value, None),
    };
    let location = if location == "db3" || location == "db4" || location.contains('.') {
        DbOrServerLocation::Host {
            name: location.to_owned(),
            db: db_name.map(|n| n.to_owned()),
        }
    } else {
        DbOrServerLocation::Installation {
            name: location.to_owned(),
            db: db_name.map(|i| i.to_owned()),
        }
    };
    Ok(location)
}

#[derive(Clone)]
pub enum DbLocation {
    Host { name: String, db: String },
    Installation { name: String, db: Option<String> },
}

/// Parse argument in form <INSTALLATION[/DB_NAME]|DB_SERVER/DB_NAME>
pub fn parse_db_location_arg(value: &str) -> StdResult<DbLocation, String> {
    match parse_db_or_server_location_arg(value)? {
        DbOrServerLocation::Host { name, db: Some(db) } => Ok(DbLocation::Host { name, db }),
        DbOrServerLocation::Host { name: _, db: None } => {
            Err("DB name required when TARGET is a host name".to_string())
        }
        DbOrServerLocation::Installation { name, db } => Ok(DbLocation::Installation { name, db }),
    }
}

#[derive(Clone, Copy, clap::ValueEnum)]
pub enum TypeArg {
    /// Main database of installation.
    Main,

    /// History database of installation.
    History,
}

pub async fn db_server_and_name(
    location: &DbLocation,
    r#type: TypeArg,
    update_repos_interval: Option<Duration>,
) -> Result<(String, String)> {
    Ok(match location {
        DbLocation::Installation { name, db } => {
            let repo = tco::ansible::git::maybe_updated_repo(update_repos_interval).await?;
            let config = ansible::config_quick::load(&repo).await?;
            let Some(installation) = config.installations.get(name) else {
                return Err(Error::Custom(format!("installation {:?} not found", &name)));
            };
            let db_server = match r#type {
                TypeArg::Main => &installation.db_server,
                TypeArg::History => &installation.history_db_server,
            };
            let db_name = match db {
                Some(db_name) => db_name,
                None => match r#type {
                    TypeArg::Main => &installation.db_name,
                    TypeArg::History => &installation.history_db_name,
                },
            };
            (db_server.clone(), db_name.clone())
        }
        DbLocation::Host { name, db } => (name.clone(), db.clone()),
    })
}
