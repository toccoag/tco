use super::common::{self, DbLocation, TypeArg};
use clap::Parser;
use std::process::Command;
use tco::sql::ssh::SshProxyBuilder;
use tco::utils::log::DisplayCommand;
use tco::{Error, Result, utils};

#[derive(Parser)]
#[command()]
pub struct Args {
    /// Target installation and database
    ///
    /// Syntax: <INSTALLATION[/DB_NAME]|DB_SERVER>/DB_NAME>
    #[arg(value_parser = common::parse_db_location_arg)]
    target: DbLocation,

    /// Command to be executed
    command: String,

    /// Arguments passed to COMMAND
    #[allow(clippy::struct_field_names)]
    args: Vec<String>,

    /// Select to which DB to connect
    ///
    /// Ignored if a target DB is specified explicitly via TARGET.
    #[arg(long, short, value_enum, default_value_t = TypeArg::Main)]
    r#type: TypeArg,
}

pub async fn main(root_args: &crate::Args, args: &Args) -> Result<()> {
    let (db_server, db_name) =
        common::db_server_and_name(&args.target, args.r#type, root_args.update_repos_interval())
            .await?;

    let proxy = SshProxyBuilder::new(&db_server).open().await?;
    let mut cmd = Command::new(&args.command);
    cmd.args(&args.args)
        .env("PGDATABASE", &db_name)
        .env("PGHOST", proxy.host())
        .env("PGPORT", proxy.port().to_string())
        .env("PGUSER", proxy.user_name());
    debug!("Executing command: {}.", DisplayCommand(&cmd));
    let status = cmd.spawn()?.wait()?;

    utils::log::vpn_needed_info_message_on_ssh_timeout(status);
    if status.success() {
        Ok(())
    } else {
        Err(Error::CustomExitStatus(status.code().unwrap_or(1)))
    }
}
