#![allow(clippy::similar_names)]

use anstream::AutoStream;
use anstream::stream::{AsLockedWrite, RawStream};
use clap::{ArgAction, Parser};
use regex::{Regex, RegexBuilder};
use std::borrow::Cow;
use std::io::Write;
use std::str;
use tco::ansible;
use tco::{Error, Result};

#[derive(Parser)]
#[command()]
pub struct Args {
    /// Installation name
    #[arg()]
    installation: Option<String>,

    /// Show full config of customer
    ///
    /// This includes all installations associated with the
    /// customer and not only the one specified as INSTALLATION.
    #[arg(short, long)]
    all_installations: bool,

    /// Do not print line numbers
    #[arg(long = "no-number-lines", short, action = ArgAction::SetFalse)]
    number_lines: bool,
}

/// Extract the customer's config within the full config.
pub fn extract_customer_config(config: &str, customer: &str) -> (usize, usize) {
    let cust_start = format!("^{customer}:");
    let cust_start = RegexBuilder::new(&cust_start)
        .multi_line(true)
        .dot_matches_new_line(true)
        .build()
        .unwrap();
    let cust_start = cust_start.find(config).unwrap().start();
    let next_cust = RegexBuilder::new("\n[a-z]")
        .multi_line(true)
        .build()
        .unwrap();
    let mut cust_end = match next_cust.find(&config[cust_start..config.len()]) {
        Some(hit) => cust_start + hit.start(),
        None => config.len(), // EOF
    };
    while config.as_bytes()[cust_end - 1] == b'\n' {
        cust_end -= 1;
    }
    (cust_start, cust_end)
}

/// Extract location of an `installations:` block within a customer's configuration.
pub fn extract_installations_block(config: &str, mut start: usize, end: usize) -> (usize, usize) {
    let regex = RegexBuilder::new(r"^  installations:( +#.*)?\n")
        .multi_line(true)
        .build()
        .unwrap();
    start += regex.find(&config[start..end]).unwrap().end() - 1;
    let regex = RegexBuilder::new(r"\n  [a-z]")
        .dot_matches_new_line(true)
        .multi_line(true)
        .build()
        .unwrap();
    let end = match regex.find(&config[start..end]) {
        Some(hit) => start + hit.start(),
        None => end,
    };
    (start, end)
}

/// Extract location of a single installation within an `installations:` block.
pub fn extract_installation_config(
    config: &str,
    mut begin: usize,
    end: usize,
    installation: &str,
) -> (usize, usize) {
    let install_start = format!("^    {installation}:");
    let install_start = RegexBuilder::new(&install_start)
        .multi_line(true)
        .dot_matches_new_line(true)
        .build()
        .unwrap();
    begin += install_start.find(&config[begin..end]).unwrap().start();
    let install_end = RegexBuilder::new("\n    [a-z]").build().unwrap();
    let end = match install_end.find(&config[begin..end]) {
        Some(hit) => begin + hit.start(),
        None => end,
    };
    (begin, end)
}

pub fn print_lines<S>(
    writer: &mut AutoStream<S>,
    config: &str,
    mut line_no: usize,
    print_line_no: bool,
) where
    S: Write + RawStream + AsLockedWrite,
{
    use owo_colors::OwoColorize as _;

    for line in config.split('\n') {
        line_no += 1;
        if print_line_no {
            let to_color = format!("{line_no}:");
            write!(writer, "{:>6} ", to_color.blue()).unwrap();
        }
        writeln!(writer, "{line}").unwrap();
    }
}

pub fn line_count(config: &str) -> usize {
    config.bytes().filter(|c| *c == b'\n').count()
}

fn reverse_skip_comment_only_lines(
    config: &str,
    start: usize,
    mut end: usize,
    max_indent: Option<usize>,
) -> usize {
    let mut iter = config[start..end].rsplit('\n');
    if config[start..end].ends_with('\n') {
        assert_eq!(iter.next(), Some("")); // skip final NL
    }
    let max_indent = max_indent
        .map(|i| Cow::Owned(format!("{i}")))
        .unwrap_or(Cow::Borrowed(""));
    let regex = Regex::new(&format!(r"^\s{{0,{max_indent}}}#")).unwrap();
    for line in iter {
        if regex.is_match(line) {
            end -= line.len() + 1;
        } else {
            break;
        }
    }
    end
}

fn forward_skip_comment_only_lines(
    config: &str,
    mut start: usize,
    end: usize,
    min_indent: usize,
) -> usize {
    let regex = format!(r"^\s{{{min_indent},}}#");
    let regex = Regex::new(&regex).unwrap();
    let mut iter = config[start..end].split('\n');
    if config[start..end].starts_with('\n') {
        assert_eq!(iter.next(), Some("")); // skip initial NL
    }
    for line in iter {
        if regex.is_match(line) {
            start += line.len() + 1;
        } else {
            break;
        }
    }
    start
}

pub fn simplify_config<S>(
    writer: &mut AutoStream<S>,
    config: &str,
    customer: &str,
    installation: &str,
    print_line_no: bool,
    all_installations: bool,
) where
    S: Write + RawStream + AsLockedWrite,
{
    let (cust_start, cust_end) = extract_customer_config(config, customer);

    // Remove comment-only lines on top of customer config that belong to previous block.
    let cust_start = reverse_skip_comment_only_lines(config, 0, cust_start, Some(0));

    // Remove all comment-only lines at end of customer config and then re-add only those
    // that belong to the customer block (i.e. those indented).
    let cust_end = reverse_skip_comment_only_lines(config, cust_start, cust_end, None);
    let cust_end = forward_skip_comment_only_lines(config, cust_end, config.len(), 2);

    let line_no = line_count(&config[0..cust_start]);
    if all_installations {
        print_lines(
            writer,
            &config[cust_start..cust_end],
            line_no,
            print_line_no,
        );
        return;
    }

    let (installs_start, installs_end) = extract_installations_block(config, cust_start, cust_end);
    let (install_start, install_end) =
        extract_installation_config(config, installs_start, installs_end, installation);

    // Remove comment-only lines before installation that don't belong to this installation.
    let install_start =
        reverse_skip_comment_only_lines(config, installs_start, install_start, Some(4));

    // Remove all comment-only lines at end of installation config. Then re-add those
    // belonging to this installation.
    let install_end = reverse_skip_comment_only_lines(config, install_start, install_end, None);
    let install_end = forward_skip_comment_only_lines(config, install_end, cust_end, 6);

    print_lines(
        writer,
        &config[cust_start..installs_start],
        line_no,
        print_line_no,
    );

    let line_no = line_no + line_count(&config[cust_start..install_start]);
    print_lines(
        writer,
        &config[install_start..install_end],
        line_no,
        print_line_no,
    );

    let remainder = &config[installs_end..cust_end];
    if !remainder.is_empty() {
        // remove NL
        let remainder = &remainder[1..remainder.len()];
        let line_no = line_no + line_count(&config[install_start..=installs_end]);
        print_lines(writer, remainder, line_no, print_line_no);
    }
}

pub fn write_raw_config<S>(writer: &mut AutoStream<S>, config: &str, print_line_no: bool)
where
    S: Write + RawStream + AsLockedWrite,
{
    use owo_colors::OwoColorize as _;

    for (idx, line) in config.split_inclusive('\n').enumerate() {
        if print_line_no {
            let to_color = format!("{}:", idx + 1);
            write!(writer, "{:>6} ", to_color.blue()).unwrap();
        }
        write!(writer, "{line}").unwrap();
    }
}

pub async fn main(root_args: &crate::Args, args: &Args) -> Result<()> {
    let repo = tco::ansible::git::maybe_updated_repo(root_args.update_repos_interval()).await?;
    let config = ansible::config_quick::load(&repo).await?;
    let raw_config = repo
        .file_content("master", "tocco/config.yml")
        .expect("config.yml not found");
    let raw_config = raw_config.as_str().expect("config.yml not valid UTF-8");
    if let Some(installation) = &args.installation {
        let Some(config) = config.installations.get(installation) else {
            return Err(Error::Custom(format!(
                "installation {} not found",
                &installation
            )));
        };
        simplify_config(
            &mut anstream::stdout(),
            raw_config,
            &config.customer_name,
            installation,
            args.number_lines,
            args.all_installations,
        );
    } else {
        write_raw_config(&mut anstream::stdout(), raw_config, args.number_lines);
    }
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    #[cfg(feature = "test-with-repo-access")]
    use test_utils::{LockMode, lock_caches};

    const RAW_CONFIG: &str = r#"
xyz:
  a: b

abc:
  key: value
  something: else
  installations:
    abc:
      i_key1a: value
      i_key1b: value
    abctest:
      i_key2a: value
      i_key2b: value
  technically: valid

def:
  a: b
"#;

    const SIMPLIFIED_INSTALLATION_ABC: &str = r#"abc:
  key: value
  something: else
  installations:
    abc:
      i_key1a: value
      i_key1b: value
  technically: valid
"#;

    const SIMPLIFIED_INSTALLATION_ABCTEST: &str = r#"abc:
  key: value
  something: else
  installations:
    abctest:
      i_key2a: value
      i_key2b: value
  technically: valid
"#;

    const SIMPLIFIED_INSTALLATION_ABC_NUMBERED: &str = r#"    5: abc:
    6:   key: value
    7:   something: else
    8:   installations:
    9:     abc:
   10:       i_key1a: value
   11:       i_key1b: value
   15:   technically: valid
"#;

    const SIMPLIFIED_INSTALLATION_ABCTEST_NUMBERED: &str = r#"    5: abc:
    6:   key: value
    7:   something: else
    8:   installations:
   12:     abctest:
   13:       i_key2a: value
   14:       i_key2b: value
   15:   technically: valid
"#;

    const RAW_CONFIG_WITH_COMMENTS: &str = r#"
xyz:
  a: b

# A
# comment
abc:
  key: value
  something: else
  installations:
    # another comment
    # here
    abc:
      i_key1a: value
      i_key1b: value
    # some comment
    abctest:
      i_key2a: value
      i_key2b: value
  technically: valid

def:
  a: b
"#;

    const SIMPLIFIED_WITH_COMMENTS_INSTALLATION_ABC: &str = r#"# A
# comment
abc:
  key: value
  something: else
  installations:
    # another comment
    # here
    abc:
      i_key1a: value
      i_key1b: value
  technically: valid
"#;

    const SIMPLIFIED_WITH_COMMENTS_INSTALLATION_ABCTEST: &str = r#"# A
# comment
abc:
  key: value
  something: else
  installations:
    # some comment
    abctest:
      i_key2a: value
      i_key2b: value
  technically: valid
"#;

    const RAW_CONFIG_WITH_COMMENTS_ON_RIGHT: &str = r#"
xyz:
  a: b

# A
# comment
abc:
  key: value
  something: else
  installations:
    abc:
      i_key1a: value
      i_key1b: value  # a multiline
                      # comment
    abctest:
      i_key2a: value
      i_key2b: value
  technically: valid

def:
  a: b
"#;

    const SIMPLIFIED_WITH_COMMENTS_ON_RIGHT_INSTALLATION_ABC: &str = r#"# A
# comment
abc:
  key: value
  something: else
  installations:
    abc:
      i_key1a: value
      i_key1b: value  # a multiline
                      # comment
  technically: valid
"#;

    #[test]
    fn test_simplify_config_abc() {
        let mut writer = AutoStream::never(Vec::new());
        simplify_config(&mut writer, RAW_CONFIG, "abc", "abc", false, false);
        assert_eq!(
            str::from_utf8(&writer.into_inner()).unwrap(),
            SIMPLIFIED_INSTALLATION_ABC
        );
    }

    #[test]
    fn test_simplify_config_abctest() {
        let mut writer = AutoStream::never(Vec::new());
        simplify_config(&mut writer, RAW_CONFIG, "abc", "abctest", false, false);
        assert_eq!(
            str::from_utf8(&writer.into_inner()).unwrap(),
            SIMPLIFIED_INSTALLATION_ABCTEST
        );
    }

    #[test]
    fn test_simplify_config_abc_numbered() {
        let mut writer = AutoStream::never(Vec::new());
        simplify_config(&mut writer, RAW_CONFIG, "abc", "abc", true, false);
        assert_eq!(
            str::from_utf8(&writer.into_inner()).unwrap(),
            SIMPLIFIED_INSTALLATION_ABC_NUMBERED,
        );
    }

    #[test]
    fn test_simplify_config_abctest_numbered() {
        let mut writer = AutoStream::never(Vec::new());
        simplify_config(&mut writer, RAW_CONFIG, "abc", "abctest", true, false);
        assert_eq!(
            str::from_utf8(&writer.into_inner()).unwrap(),
            SIMPLIFIED_INSTALLATION_ABCTEST_NUMBERED,
        );
    }

    // There is a large number of unwrap() and slicing that could trigger a panic. Go through
    // every installation to ensure none tiggers one.
    #[cfg(feature = "test-with-repo-access")]
    #[tokio::test]
    async fn test_no_panic() {
        let _guard = lock_caches(LockMode::Shared).await;
        let repo = tco::ansible::git::repo().await.unwrap();
        let config = ansible::config_quick::load(&repo).await.unwrap();
        let raw_config = repo.file_content("master", "tocco/config.yml").unwrap();
        let raw_config = raw_config.as_str().unwrap();
        for (name, config) in config.installations {
            let mut writer = AutoStream::never(Vec::new());
            simplify_config(
                &mut writer,
                raw_config,
                &config.customer_name,
                &name,
                true,
                false,
            );
            let mut writer = AutoStream::never(Vec::new());
            simplify_config(
                &mut writer,
                raw_config,
                &config.customer_name,
                &name,
                false,
                false,
            );
            let writer = writer.into_inner();
            let result = str::from_utf8(&writer).unwrap();
            assert!(result.starts_with(&config.customer_name));
            assert!(result.ends_with('\n'));
        }
    }

    #[test]
    fn test_simplify_with_comments_config_abc() {
        let mut writer = AutoStream::never(Vec::new());
        simplify_config(
            &mut writer,
            RAW_CONFIG_WITH_COMMENTS,
            "abc",
            "abc",
            false,
            false,
        );
        assert_eq!(
            str::from_utf8(&writer.into_inner()).unwrap(),
            SIMPLIFIED_WITH_COMMENTS_INSTALLATION_ABC
        );
    }

    #[test]
    fn test_simplify_with_comments_config_abctest() {
        let mut writer = AutoStream::never(Vec::new());
        simplify_config(
            &mut writer,
            RAW_CONFIG_WITH_COMMENTS,
            "abc",
            "abctest",
            false,
            false,
        );
        assert_eq!(
            str::from_utf8(&writer.into_inner()).unwrap(),
            SIMPLIFIED_WITH_COMMENTS_INSTALLATION_ABCTEST
        );
    }

    #[test]
    fn test_simplify_with_comments_on_right_config_abc() {
        let mut writer = AutoStream::never(Vec::new());
        simplify_config(
            &mut writer,
            RAW_CONFIG_WITH_COMMENTS_ON_RIGHT,
            "abc",
            "abc",
            false,
            false,
        );
        assert_eq!(
            str::from_utf8(&writer.into_inner()).unwrap(),
            SIMPLIFIED_WITH_COMMENTS_ON_RIGHT_INSTALLATION_ABC
        );
    }
}
