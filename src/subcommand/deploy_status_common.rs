use crate::HTTP_SEMAPHORE;
use crate::subcommand::common::EnvArg;
use futures::future;
use std::cmp::Reverse;
use std::fmt;
use std::thread;
use tco::ansible::config_quick::{Config, InstallConfig};
use tco::git::{self, Repository};
use tco::nice::status::Status;
use tco::{Result, nice};
use tokio::sync::Semaphore;
use tokio::task;

#[derive(clap::Args)]
pub struct CommonArgs {
    /// Commit to check if deployed.
    ///
    /// rev_spec is usually a Git commit ID like
    /// "f40f82798be81f452926515f95a9275a539c4825", a shortened ID
    /// like "f40f82798be81" or a Git tag.
    ///
    /// When multiple rev_specs are given, the commit is considered
    /// deployed if any matches. When a change was cherry-picked, specify
    /// the original commit and all cherry-pick commits.
    ///
    /// Commits created with `git cherry-pick -x` have "cherry picked
    /// from commit …" appended to their commit message referencing the
    /// source of the commit. Such cherry-picks are detected
    /// automatically when looking for REV_SPEC / INTRODUCED_IN and
    /// do not need to be specified on the command line.
    #[arg(required = true, value_name = "REV_SPEC")]
    pub rev_specs: Vec<String>,

    /// Commit that introduced bug
    ///
    /// Any installation that has none of these commits is considered
    /// unaffected.
    ///
    /// If commit "aaaaaaaaaaaaa" introduced a bug and "bbbbbbbbbbbbb"
    /// fixes it, you can use this command to see all installations
    /// missing the fix:
    ///
    /// tco [client-]deploy-status --introduced-in aaaaaaaaaaaaa bbbbbbbbbbbbb
    ///
    /// Installations affected are marked "undeployed", those without
    /// the buggy commit "unaffected", and those with the fix "deployed".
    #[arg(short, long)]
    pub introduced_in: Vec<String>,

    /// Limit output to selected environment.
    #[arg(value_enum, long, short, default_value_t = EnvArg::All)]
    pub environment: EnvArg,

    /// Show only installations with given deployment status
    ///
    /// May be given multiple times.
    ///
    /// Show only "deployed" and "unknown":
    ///
    /// tco [client-]deploy-status -S deployed -S unknown …
    #[arg(
        value_enum,
        long,
        short = 'S',
        conflicts_with = "hide",
        value_name = "STATUS"
    )]
    pub show_only: Option<Vec<Verdict>>,

    /// Hide installations with given deployment status
    ///
    /// May be given multiple times.
    ///
    /// Show only "deployed" and "unaffected":
    ///
    /// tco [client-]deploy-status -H undeployed -H unaffected …
    #[arg(value_enum, long, short = 'H', value_name = "STATUS")]
    pub hide: Option<Vec<Verdict>>,

    /// Adjust secondary sorting of output
    ///
    /// Primary sorting is always by status
    #[arg(long, short, value_enum, default_value_t = SortBy::Name)]
    sort_by: SortBy,
}

#[derive(Clone, Copy, clap::ValueEnum)]
enum SortBy {
    Name,
    Version,
}

#[derive(clap::ValueEnum, Clone, Copy, Debug, Eq, PartialOrd, PartialEq, Ord)]
pub enum Verdict {
    /// Failed to determine status. See printed error message.
    Unknown,

    /// Fix deployed / REF_SPEC deployed
    Deployed,

    /// Fix not deployed / REF_SPEC not deployed
    Undeployed,

    /// Bug never deployed / INTRODUCED_IN never deployed
    Unaffected,

    /// Package not installated for installation.
    NotInstalled,
}

impl fmt::Display for Verdict {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let text = match self {
            Verdict::Deployed => "deployed",
            Verdict::Undeployed => "undeployed",
            Verdict::Unknown => "unknown",
            Verdict::Unaffected => "unaffected",
            Verdict::NotInstalled => "not installed",
        };
        fmt::Display::fmt(&text, f)
    }
}

async fn verdict(
    repo: &Repository,
    deployed_commit: &str,
    wanted_oids: &[git2::Oid],
    introducing_oids: &[git2::Oid],
    oldest_searched_commit: Option<git2::Oid>,
) -> Result<Verdict> {
    let regex = git::cherry_pick_regex();
    let repo = repo.try_clone()?;
    let deployed_commit = deployed_commit.to_owned();
    let wanted_oids = wanted_oids.to_vec();
    let introducing_oids = introducing_oids.to_vec();
    task::spawn_blocking(move || {
        let repo = repo.as_inner();
        let mut walker = repo.revwalk()?;

        if let Some(oldest_searched_commit) = oldest_searched_commit {
            debug!("Will not search commits older than {oldest_searched_commit}.");
            walker.push_range(&format!("{oldest_searched_commit}..{deployed_commit}"))?;
        } else {
            let deployed_oid = repo.revparse_single(&deployed_commit)?.id();
            walker.push(deployed_oid)?;
        }

        // Without --introduced-in, assume affected.
        let mut introducing_commit_seen = introducing_oids.is_empty();
        for oid in walker {
            let oid = oid?;
            if wanted_oids.contains(&oid) {
                return Ok(Verdict::Deployed);
            }
            if !introducing_commit_seen && introducing_oids.contains(&oid) {
                // Note that objects are traversed in arbitrary order. Thus,
                // we can't `break` here as we might observe the fix out of
                // order.
                introducing_commit_seen = true;
                continue;
            }

            let commit = repo
                .find_commit(oid)
                .expect("commit yielded by walker missing");
            let cherry_picks: Vec<_> = match commit.message() {
                Some(message) => regex
                    .captures_iter(message)
                    .map(|cap| {
                        let id = cap.get(1).expect("missing capture").as_str();
                        git2::Oid::from_str(id).expect("invalid object ID")
                    })
                    .collect(),
                None => {
                    debug!(
                        "Ignoring non-UTF8 message for commit {oid} while searching Cherry-Pick \
                         references."
                    );
                    Vec::new()
                }
            };
            for cherry_pick in cherry_picks {
                if wanted_oids.contains(&cherry_pick) {
                    return Ok(Verdict::Deployed);
                }
                if !introducing_commit_seen && introducing_oids.contains(&cherry_pick) {
                    introducing_commit_seen = true;
                }
            }
        }
        if introducing_commit_seen {
            Ok(Verdict::Undeployed)
        } else {
            Ok(Verdict::Unaffected)
        }
    })
    .await
    .unwrap()
}

async fn is_deployed<F>(
    repo: &Repository,
    installation: &InstallConfig,
    wanted_oids: &[git2::Oid],
    introducing_oids: &[git2::Oid],
    revwalk_semaphore: &Semaphore,
    rev_extractor: F,
    oldest_searched_commit: Option<git2::Oid>,
) -> Result<(Verdict, Status)>
where
    F: Fn(&Status) -> Result<Option<String>>,
{
    let status = {
        let base_url = format!("https://{}.tocco.ch", installation.installation_name);
        let _guard = HTTP_SEMAPHORE.acquire().await.unwrap();
        nice::status::get(&base_url).await?
    };
    let repo = repo.try_clone()?;
    let wanted_oids = wanted_oids.to_vec();
    let introducing_oids = introducing_oids.to_vec();
    let _guard = revwalk_semaphore.acquire().await.unwrap();
    let revision = rev_extractor(&status)?;
    if let Some(revision) = revision {
        Ok((
            verdict(
                &repo,
                &revision,
                &wanted_oids,
                &introducing_oids,
                oldest_searched_commit,
            )
            .await?,
            status,
        ))
    } else {
        Ok((Verdict::NotInstalled, status))
    }
}

#[allow(clippy::too_many_lines)]
pub async fn main<F>(
    args: &CommonArgs,
    config: &Config,
    repo: &Repository,
    rev_extractor: F,
    oldest_searched_commit: Option<git2::Oid>,
) -> Result<()>
where
    F: Fn(&Status) -> Result<Option<String>>,
{
    let installations: Vec<_> = config
        .installations
        .values()
        .filter(|i| args.environment.includes(i.environment()))
        .collect();

    let mut rev_specs = Vec::new();
    for rev_spec in &args.rev_specs {
        let oids = repo.resolve_cherry_picks_recursively(rev_spec)?;
        rev_specs.extend(oids);
    }
    debug!("After resolving cherry-picks, commits fixing bug are {rev_specs:?}");

    let mut introduced_in = Vec::new();
    for rev_spec in &args.introduced_in {
        let oids = repo.resolve_cherry_picks_recursively(rev_spec)?;
        introduced_in.extend(oids);
    }
    debug!("After resolving cherry-picks, commits introducing bug are {introduced_in:?}");

    let parallelism = thread::available_parallelism()
        .map(|c| usize::from(c))
        .unwrap_or_else(|e| {
            let default = 4;
            debug!("Failed to get parallelism, falling back to {default}: {e}.");
            default
        });
    debug!("Limiting number of Git revwalks to {parallelism}.");
    let revwalk_semaphore = Semaphore::new(parallelism);
    let tasks: Vec<_> = installations
        .iter()
        .map(|i| {
            is_deployed(
                repo,
                i,
                &rev_specs,
                &introduced_in,
                &revwalk_semaphore,
                &rev_extractor,
                oldest_searched_commit,
            )
        })
        .collect();

    let mut results: Vec<_> = installations
        .iter()
        .zip(future::join_all(tasks).await)
        .map(|(installation, result)| {
            let (verdict, status) = match result {
                Ok((verdict @ Verdict::Unaffected, status)) => {
                    debug_assert!(
                        !args.introduced_in.is_empty(),
                        "\"unaffected\" without --introduced-in"
                    );
                    (verdict, Some(status))
                }
                Ok((verdict, status)) => (verdict, Some(status)),
                Err(err) => {
                    error!(
                        "Failed to process installation {}: {err}",
                        installation.installation_name
                    );
                    (Verdict::Unknown, None)
                }
            };
            (installation, verdict, status)
        })
        .filter(|(_installation, verdict, _status)| {
            if let Some(show_only) = &args.show_only {
                debug_assert!(args.hide.is_none());
                show_only.contains(verdict)
            } else if let Some(hide) = &args.hide {
                debug_assert!(args.show_only.is_none());
                !hide.contains(verdict)
            } else {
                true
            }
        })
        .collect();

    results
        .sort_unstable_by_key(|(installation, _verdict, _status)| &installation.installation_name);
    match args.sort_by {
        SortBy::Name => { /* already sorted by name */ }
        SortBy::Version => results.sort_by_cached_key(|(installation, _, status)| {
            Reverse(
                status
                    .as_ref()
                    .map(|status| status.version.clone())
                    .unwrap_or_else(|| installation.version()),
            )
        }),
    }
    results.sort_by_key(|(_, verdict, _status)| *verdict);

    println!("VERDICT        VERSION  INSTALLATION");
    for (installation, verdict, status) in results {
        println!(
            "{verdict:13}  {:7}  {}",
            status
                .as_ref()
                .map(|status| status.version_pretty())
                .unwrap_or_else(|| installation.version_pretty()),
            installation.installation_name
        );
    }
    Ok(())
}

#[cfg(test)]
#[cfg(feature = "test-with-repo-access")]
mod tests {
    use super::*;
    use test_utils::{LockMode, lock_caches};

    #[tokio::test]
    async fn test_verdict() {
        let _guard = lock_caches(LockMode::Shared).await;
        let repo = tco::ansible::git::repo().await.unwrap();

        // Object in order they appear in Git. A commit contains
        // all commits listed below it.
        let a_str = "be3c7e7b4cc1e65d405cd715acd0306aae906651"; // newest commit
        let a = git2::Oid::from_str(a_str).unwrap();
        let b_str = "3d62a3ea504ad8217ad75c748d36ad0e801c1cc6";
        let b = git2::Oid::from_str(b_str).unwrap();
        let c_str = "29c412040df8a9e0cf539a072d0173d0dd3454f5"; // oldest commit
        let c = git2::Oid::from_str(c_str).unwrap();

        // Commit never deployed
        let never_str = "50c9bb7e6154f12d57abc28b1a0141495b21c7e4";
        let never = git2::Oid::from_str(never_str).unwrap();

        // Without introduced in
        assert_eq!(
            verdict(&repo, b_str, &[a], &[], None).await.unwrap(),
            Verdict::Undeployed
        );
        assert_eq!(
            verdict(&repo, b_str, &[b], &[], None).await.unwrap(),
            Verdict::Deployed
        );
        assert_eq!(
            verdict(&repo, b_str, &[c], &[], None).await.unwrap(),
            Verdict::Deployed
        );
        assert_eq!(
            verdict(&repo, b_str, &[a, c], &[], None).await.unwrap(),
            Verdict::Deployed
        );

        // Never introduced
        assert_eq!(
            verdict(&repo, b_str, &[a], &[never], None).await.unwrap(),
            Verdict::Unaffected
        );
        assert_eq!(
            verdict(&repo, b_str, &[b], &[never], None).await.unwrap(),
            Verdict::Deployed
        );
        assert_eq!(
            verdict(&repo, b_str, &[c], &[never], None).await.unwrap(),
            Verdict::Deployed
        );

        // Introduced at some point
        assert_eq!(
            verdict(&repo, b_str, &[a], &[b], None).await.unwrap(),
            Verdict::Undeployed
        );
        assert_eq!(
            verdict(&repo, b_str, &[b], &[b], None).await.unwrap(),
            Verdict::Deployed
        );
        assert_eq!(
            verdict(&repo, b_str, &[c], &[b], None).await.unwrap(),
            Verdict::Deployed
        );
        assert_eq!(
            verdict(&repo, b_str, &[never], &[a], None).await.unwrap(),
            Verdict::Unaffected
        );
        assert_eq!(
            verdict(&repo, b_str, &[never], &[b], None).await.unwrap(),
            Verdict::Undeployed
        );
        assert_eq!(
            verdict(&repo, b_str, &[never], &[c], None).await.unwrap(),
            Verdict::Undeployed
        );
    }

    #[tokio::test]
    async fn verdict_with_cherry_pick() {
        let _guard = lock_caches(LockMode::Shared).await;
        let repo = tco::client::git::repo().await.unwrap();

        // Original commit plus all its cherry picks
        let introducing_commits = [
            "49a172a680c971eaccf5825a8f0df5f5df7e5018", // original
            "5ab71e06fcfbfff125a712375673fb1f04a9ea34", // cherry-pick of original
            "ba691f51b16f68aae9d3a5294ab5060859ac3e4c", // cherry-pick of cherry-pick of original
            "360c1d56a8e2c9657d9571f0c3d6dfe195dac452", // …
            "75832e0af354c30c563ef62eac38ecc4c789ecda",
            "660c379ca42e2fde1db1d273b0c527df3fbdd40f",
            "58914b32e5c3a5ba81d4675579c4a118d291490a",
            "f02ffe6fd4937e1d83f55efe065552fb72c0db63",
            "41182da4e21029bec60566c7c5f386854e941b29",
        ];

        for i in 0..introducing_commits.len() {
            for j in 0..introducing_commits.len() {
                let deployed = introducing_commits[i];
                let wanted = git2::Oid::from_str(introducing_commits[j]).unwrap();
                let expected_verdict = if i < j {
                    Verdict::Undeployed
                } else {
                    Verdict::Deployed
                };
                assert_eq!(
                    verdict(&repo, deployed, &[wanted], &[], None)
                        .await
                        .unwrap(),
                    expected_verdict,
                );
            }
        }

        for i in 0..introducing_commits.len() {
            for j in 0..introducing_commits.len() {
                let deployed = introducing_commits[i];
                let introduced_in = git2::Oid::from_str(introducing_commits[j]).unwrap();
                let expected_verdict = if i < j {
                    Verdict::Unaffected
                } else {
                    Verdict::Undeployed
                };
                assert_eq!(
                    verdict(&repo, deployed, &[], &[introduced_in], None)
                        .await
                        .unwrap(),
                    expected_verdict,
                );
            }
        }
    }

    #[tokio::test]
    async fn nice2_repo_speed_up() {
        let _guard = lock_caches(LockMode::Shared).await;
        let repo = tco::nice::git::repo().await.unwrap();
        let deployed = "42020867983";

        // OLDEST_NICE2_COMMIT_SEARCHED and it's parents should not be searched.
        let search_newer_than = *nice::git::oldest_commit_deployed();
        {
            assert_eq!(
                verdict(
                    &repo,
                    deployed,
                    &[search_newer_than],
                    &[],
                    Some(search_newer_than)
                )
                .await
                .unwrap(),
                Verdict::Undeployed,
            );
        }

        {
            // Parent of OLDEST_NICE2_COMMIT_SEARCHED, this is one of the "oldest" commits searched.
            let oldest_searched =
                git2::Oid::from_str("41eb525aff5002317d549ec1f222c8d82faf2705").unwrap();
            assert_eq!(
                verdict(
                    &repo,
                    deployed,
                    &[oldest_searched],
                    &[],
                    Some(search_newer_than)
                )
                .await
                .unwrap(),
                Verdict::Deployed,
            );
        }
    }

    #[tokio::test]
    async fn oldest_seached_commit_reachable_from_master() {
        let _guard = lock_caches(LockMode::Shared).await;
        let repo = tco::client::git::repo().await.unwrap();

        let top = repo.as_inner().revparse_single("master").unwrap().id();
        let wanted = *tco::client::git::oldest_commit_deployed();

        // `wanted` (`SEARCH_TOCCO_CLIENT_COMMIT_NEWER_THAN`) has to be a
        // parent of `top` (`"master"`).
        let mut walker = repo.as_inner().revwalk().unwrap();
        walker.push(top).unwrap();
        assert!(walker.any(|commit| commit.unwrap() == wanted));
    }
}
