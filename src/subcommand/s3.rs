use clap::{Parser, Subcommand};
use futures::future;
use tco::ansible;
use tco::cloudscale;
use tco::{Error, Result};

#[derive(Parser)]
#[command()]
pub struct Args {
    #[command(subcommand)]
    command: Commands,
}

#[allow(clippy::enum_variant_names)]
#[derive(Subcommand)]
enum Commands {
    /// Create object / S3 user
    ///
    /// Developers keys use a "dev-" prefix. For instance,
    /// Jane Doe would likely have a key called "dev-jado".
    ///
    /// You should generally not touch any user with a
    /// "nice-" prefix. Those are managed by Ansible.
    CreateUser(CreateArgs),

    /// Show object / S3 user(s) and their keys
    ShowUser(ShowArgs),

    /// Remove object / S3 user
    ///
    /// You should generally not touch any user with a
    /// "nice-" prefix. Those are managed by Ansible.
    RemoveUser(RemoveArgs),

    /// Initialize key rotation
    ///
    /// This copies the current access_key ID to the old_key tag to
    /// allow Ansible to detect which key to use when a second
    /// key is added.
    ///
    /// Only do this if you know what you are doing.
    InitKeyRotation(InitKeyRotationArgs),

    /// Show per-key rotation status
    ///
    /// Show current key rotation state and indicate next step.
    ShowKeyRotationStatus(ShowKeyRotationStatusArgs),

    /// Complete key rotation
    ///
    /// Removes the old_key tag from object users. See
    /// also `tco s3 init-key-rotation`.
    CompleteKeyRotation(CompleteKeyRotationArgs),
}

#[derive(Parser)]
#[command()]
pub struct ShowArgs {
    /// Limit output to user with given ID.
    ///
    /// A user ID like 'dev-pege' or 'nice-bbg'.
    ///
    /// If omitted, all users are shown.
    id: Option<String>,
}

#[derive(Parser)]
#[command()]
pub struct CreateArgs {
    /// ID of user to create
    id: String,

    /// Allow ID without a "dev-" prefix
    ///
    /// Ansible manages "nice-"-prefixed users. Do not touch them.
    ///
    /// Useful to create users for tools other than Nice.
    #[arg(long)]
    force: bool,
}

#[derive(Parser)]
#[command()]
pub struct RemoveArgs {
    /// ID of user to create
    id: String,

    /// Allow ID without a "dev-" prefix
    ///
    /// Ansible manages "nice-"-prefixed users. Do not touch them.
    ///
    /// Useful to remove users for tools other than Nice.
    #[arg(long)]
    force: bool,
}

#[derive(Parser)]
#[command()]
pub struct InitKeyRotationArgs {
    /// Really initialize key rotation
    ///
    /// Safeguard to ensure rotation isn't triggered by accident. Run without
    /// this to see some details about what would be done.
    #[arg(long)]
    yes_init: bool,

    /// Only operate on keys whose ID starts with given prefix.
    ///
    /// If empty, key rotation is initialized for all users, even
    /// those without an ID.
    ///
    /// Users IDs starting with "nice-" are those used by Nice and
    /// can be rotated automatically by Ansible.
    #[arg(short, long, default_value = "nice-")]
    prefix: String,
}

#[derive(Parser)]
#[command()]
pub struct ShowKeyRotationStatusArgs {
    /// Only operate on keys whose ID starts with given prefix.
    ///
    /// If empty, key rotation is initialized for all users, even
    /// those without an ID.
    ///
    /// Users IDs starting with "nice-" are those used by Nice and
    /// can be rotated automatically by Ansible.
    #[arg(short, long, default_value = "nice-")]
    prefix: String,
}

#[derive(Parser)]
#[command()]
pub struct CompleteKeyRotationArgs {
    /// Only operate on keys whose ID starts with given prefix.
    ///
    /// If empty, key rotation is completed for all users, even
    /// those without an ID.
    #[arg(short, long, default_value = "nice-")]
    prefix: String,

    /// Complete key rotation even if key wasn't rotated.
    ///
    /// Remove old_key tag even if the only key still
    /// matches old_key.
    #[arg(long)]
    allow_not_rotated: bool,

    /// Complete key rotation for users with multiple keys.
    ///
    /// When rotating keys, it's usually expected that but
    /// one key is left when completing key rotation. Set
    /// this to remove old_key tag even when there
    /// is multiple keys.
    #[arg(long)]
    allow_multiple_keys: bool,
}

async fn print_users(api_token: &str) -> Result<()> {
    let mut users = cloudscale::get_object_users(api_token).await?;
    users.sort_by(|a, b| a.id_or_display().cmp(b.id_or_display()));
    for user in users {
        println!("{}", user.multi_line_display());
    }
    Ok(())
}

async fn print_user_with_id(api_token: &str, id: &str) -> Result<()> {
    if let Some(user) = cloudscale::get_object_user_by_id(api_token, id).await? {
        print!("{}", user.multi_line_display());
    } else {
        println!("<no such user>");
    }
    Ok(())
}

async fn create_user(api_token: &str, args: &CreateArgs) -> Result<()> {
    if !args.force && !args.id.starts_with("dev-") {
        return Err(Error::Custom(
            "Refusing to create user whose ID does not start with \"dev-\". See --help.."
                .to_string(),
        ));
    }
    let user = cloudscale::create_object_user(api_token, &args.id).await?;
    print!("{}", user.multi_line_display());
    Ok(())
}

async fn remove_user(api_token: &str, args: &RemoveArgs) -> Result<()> {
    if !args.force && !args.id.starts_with("dev-") {
        return Err(Error::Custom(
            "Refusing to remove user whose ID does not start with \"dev-\". See --help."
                .to_string(),
        ));
    }
    cloudscale::delete_object_user(api_token, &args.id).await
}

async fn init_key_rotation(api_token: &str, args: &InitKeyRotationArgs) -> Result<()> {
    let users = users_with_prefix(api_token, &args.prefix).await?;

    if !args.yes_init {
        println!();
        println!("--yes-init not given, aborting without initializing ...");
        return Ok(());
    }

    let tasks: Vec<_> = users
        .into_iter()
        .map(|u| async {
            (
                u.id_or_display().to_string(),
                u.set_old_key(api_token).await,
            )
        })
        .collect();
    let tasks = future::join_all(tasks).await;

    for (name, result) in &tasks {
        if let Err(e) = result {
            println!("Failed to set old_key for user {name}: {e}",);
        }
    }

    Ok(())
}

async fn complete_key_rotation(api_token: &str, args: &CompleteKeyRotationArgs) -> Result<()> {
    let users = users_with_prefix(api_token, &args.prefix).await?;
    let tasks: Vec<_> = users
        .into_iter()
        .map(|u| async {
            let name = u.id_or_display().to_string();
            let Some(old_key) = u.tags.get("old_key") else {
                return (name, Ok(()));
            };
            let result = match &u.keys[..] {
                [] => Err(Error::Custom("user has no key".to_string())),
                [key] if args.allow_not_rotated && &key.access_key == old_key => {
                    u.unset_old_key(api_token).await
                }
                [key] if &key.access_key == old_key => Err(Error::Custom(
                    "only key matches old_key (use --allow-not-rotated to force-complete)."
                        .to_string(),
                )),
                [_] => u.unset_old_key(api_token).await,
                [_, _, ..] if args.allow_multiple_keys => u.unset_old_key(api_token).await,
                [_, _, ..] => Err(Error::Custom(
                    "multiple keys exist (use --allow-multiple-keys to force-complete)".to_string(),
                )),
            };
            (name, result)
        })
        .collect();
    let tasks = future::join_all(tasks).await;

    for (name, result) in &tasks {
        if let Err(e) = result {
            error!("Failed to complete rotation for user {name}: {e}",);
        }
    }

    Ok(())
}

async fn show_rotation_status(api_token: &str, args: &ShowKeyRotationStatusArgs) -> Result<()> {
    let users = users_with_prefix(api_token, &args.prefix).await?;
    for user in users {
        print!("{:25} ", user.id_or_display());
        if let Some(old_key) = user.tags.get("old_key") {
            if user.keys.iter().any(|k| k.access_key == *old_key) {
                if user.keys.len() > 1 {
                    println!(
                        "phase 2: new key added - next: switch to new key and then remove old key \
                         {old_key}."
                    );
                } else {
                    println!("phase 1: rotation initialized - next: add new key.");
                }
            } else {
                println!("phase 3: old key removed - next: complete rotation.");
            }
        } else {
            println!("phase 0: key rotation not in progress - next: initialize rotation");
        }
    }
    Ok(())
}

async fn users_with_prefix(api_token: &str, prefix: &str) -> Result<Vec<cloudscale::ObjectUser>> {
    let mut users = cloudscale::get_object_users(api_token).await?;
    println!("Found a total of {} users.", users.len());

    if prefix.is_empty() {
        println!("Not filtering by prefix.");
    } else {
        users.retain(|i| i.id().map(|i| i.starts_with(prefix)).unwrap_or(false));
        println!("Found {} users with prefix {:?}.", users.len(), prefix);
    }
    users.sort_by(|a, b| a.id_or_display().cmp(b.id_or_display()));
    Ok(users)
}

pub async fn main(root_args: &crate::Args, args: &Args) -> Result<()> {
    let repo = ansible::git::maybe_updated_repo(root_args.update_repos_interval()).await?;
    let api_token = ansible::config::load(&repo)
        .await?
        .installations
        .remove("master")
        .expect("installation master not found")
        .cloudscale_api_token;

    match &args.command {
        Commands::CompleteKeyRotation(args) => complete_key_rotation(&api_token, args).await?,
        Commands::CreateUser(args) => create_user(&api_token, args).await?,
        Commands::InitKeyRotation(args) => init_key_rotation(&api_token, args).await?,
        Commands::RemoveUser(args) => remove_user(&api_token, args).await?,
        Commands::ShowKeyRotationStatus(args) => show_rotation_status(&api_token, args).await?,
        Commands::ShowUser(ShowArgs { id: None }) => print_users(&api_token).await?,
        Commands::ShowUser(ShowArgs { id: Some(id) }) => print_user_with_id(&api_token, id).await?,
    }
    Ok(())
}
