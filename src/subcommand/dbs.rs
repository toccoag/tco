use chrono::{DateTime, Duration, DurationRound, Local};
use clap::{Parser, Subcommand};
use futures::TryFutureExt;
use futures::future;
use owo_colors::{AnsiColors, OwoColorize};
use postgres::error::SqlState;
use std::cmp;
use std::collections::{HashMap, HashSet};
use std::io::{self, Write};
use tco::ansible;
use tco::ansible::config_quick::InstallConfig;
use tco::sql;
use tco::sql::SqlConnect;
use tco::sql::batch::BatchJob;
use tco::sql::query::{ClientExt, LatestLogin, UserLogin};
use tco::sql::ssh::{SshProxy, SshProxyBuilder};
use tco::utils;
use tco::{Error, Result};
use tokio::sync::Semaphore;
use tokio_postgres as postgres;

#[derive(Parser)]
#[command()]
pub struct Args {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
enum Commands {
    /// Show batch jobs across all DBs
    #[command(visible_alias = "bj")]
    BatchJobs(BatchJobsArgs),

    /// Show tasks that have failed the last N times
    ///
    /// This scans table nice_task_execution for failed tasks.
    /// Such tasks include batch jobs, mails, reports, etc.
    ///
    /// Tasks are limited to the last 6 months.
    FailingTasks(FailingTasksArgs),

    /// Show time of most recent login
    ///
    /// Shows time of most recent login and the corresponding
    /// user name.
    ///
    /// To see latest login and sessions for a single installation,
    /// use `tco sessions`.
    LatestLogin(LatestLoginArgs),

    /// Show all DBs and their sizes
    Sizes(SizesArgs),

    /// Show all unused DBs (=with no connections)
    Unused(UnusedArgs),
}

#[derive(Parser)]
#[command()]
pub struct BatchJobsArgs {
    /// Only show active jobs
    #[arg(long, short)]
    active_only: bool,

    /// Adjust sorting of output
    #[arg(long, short, value_enum, default_value_t = BatchJobsSortBy::Name)]
    sort_by: BatchJobsSortBy,
}

#[derive(Parser)]
#[command()]
pub struct LatestLoginArgs {
    /// Show all, Tocco or non-Tocco logins.
    ///
    /// A Tocco user is a user whose login ends with "@tocco.ch" or
    /// is "tocco". All other logins are considered Non-Tocco logins.
    #[arg(long, short, value_enum, default_value_t = LoginCategory::Both)]
    category: LoginCategory,

    /// Adjust sorting of output
    #[arg(long, short, value_enum, default_value_t = LatestLoginSortBy::Db)]
    sort_by: LatestLoginSortBy,
}

#[derive(Parser)]
#[command()]
pub struct SizesArgs {
    /// Adjust sorting of output
    #[arg(long, short, value_enum, default_value_t = SizeSortBy::Size)]
    sort_by: SizeSortBy,
}

#[derive(Parser)]
#[command()]
pub struct UnusedArgs {
    /// Adjust sorting of output
    #[arg(long, short, value_enum, default_value_t = UnusedSortBy::Modified)]
    sort_by: UnusedSortBy,

    /// Ask user if DBs should be removed
    ///
    /// Ask, for every DB, if it should be removed. Confirm removal
    /// with "y". For non-empty DBs used within the last three days,
    /// "yes!" needs to be used for confirmation.
    #[arg(long)]
    ask_delete: bool,
}

#[derive(Parser)]
#[command()]
pub struct FailingTasksArgs {
    /// Show only batch jobs that failed the last N times.
    #[arg(long, short, default_value_t = 3, value_parser(clap::value_parser!(i64).range(1..)))]
    max_fails: i64,

    /// Only consider task executions within the past N days.
    ///
    /// Lowering this value can greatly improve speed. However, consider that tasks need
    /// to have been executed at least MAX_FAILS times in this time interval to be shown.
    #[arg(long, default_value_t = 92, value_parser(clap::value_parser!(i64).range(1..)))]
    max_age_days: i64,

    /// Format in which time of last failure is shown
    #[arg(long, short, value_enum, default_value_t = TimeFormat::Relative)]
    format: TimeFormat,

    /// Adjust sorting of output
    #[arg(long, short, value_enum, default_value_t = TaskSortBy::Task)]
    sort_by: TaskSortBy,

    /// Max. number of queries executed in parallel per server
    #[arg(long, short, default_value_t = 8)]
    parallel: usize,

    /// Statement timeout in seconds
    ///
    /// Per-statement timeout for querying the database. Note that the server
    /// may have a statement timeout in place. This will override this timeout.
    ///
    /// Set to 0 to disable. However, disabling will not override any global
    /// statement timeout defined on the server.
    #[arg(long, short, value_parser = clap::value_parser!(i32).range(0..=1800), default_value_t = 30)]
    timeout: i32,
}

impl FailingTasksArgs {
    fn timeout_msecs(&self) -> Option<i32> {
        if self.timeout > 0 {
            Some(self.timeout * 1000)
        } else {
            debug_assert_eq!(self.timeout, 0);
            None
        }
    }
}

#[derive(Clone, Copy, clap::ValueEnum)]
enum BatchJobsSortBy {
    /// Sort by: job name, server name, db name
    Name,
    /// Sort by: hour, minute, server name, db name
    Time,
}

#[derive(Clone, Copy, clap::ValueEnum)]
enum LatestLoginSortBy {
    Db,
    Server,
    GlobalTime,
    NonToccoTime,
    ToccoTime,
}

#[derive(Clone, Copy, clap::ValueEnum)]
enum SizeSortBy {
    Db,
    Size,
    Server,
}

#[derive(Clone, Copy, clap::ValueEnum)]
enum UnusedSortBy {
    Db,
    Modified,
    Size,
    Server,
}

#[derive(Clone, Copy, clap::ValueEnum)]
enum TaskSortBy {
    Installation,
    Task,
    Version,
}

#[derive(Clone, Copy, clap::ValueEnum)]
enum TimeFormat {
    DateTime,
    Relative,
}

struct UnusedDb {
    name: String,
    size: u64,
    /// Either last login (main DB) or most recent history record (history DB)
    time: Option<DateTime<chrono::Local>>,
    empty: bool,
}

#[derive(Clone, Copy, clap::ValueEnum)]
enum LoginCategory {
    /// Show latest login across all users.
    Global,

    /// Show latest login by a Tocco and non-Tocco user.
    Both,

    /// Show latest login by a non-Tocco user.
    NonTocco,

    /// Show latest login by a Tocco user.
    Tocco,
}

impl LoginCategory {
    fn show_global(self) -> bool {
        match self {
            LoginCategory::Global => true,
            LoginCategory::Both | LoginCategory::NonTocco | LoginCategory::Tocco => false,
        }
    }

    fn show_tocco(self) -> bool {
        match self {
            LoginCategory::Both | LoginCategory::Tocco => true,
            LoginCategory::Global | LoginCategory::NonTocco => false,
        }
    }

    fn show_non_tocco(self) -> bool {
        match self {
            LoginCategory::Both | LoginCategory::NonTocco => true,
            LoginCategory::Global | LoginCategory::Tocco => false,
        }
    }
}

async fn query_times(proxy: &SshProxy, db_name: String) -> Result<Option<DateTime<chrono::Local>>> {
    let client = proxy.sql_connect(&db_name).await?;
    Ok(client.last_modified().await?)
}

async fn is_empty_history_db(client: &postgres::Client) -> Result<bool> {
    let sql = r#"
        SELECT
            NOT exists(SELECT * FROM nice_history)
            AND NOT exists(SELECT * FROM nice_history_data)
    "#;
    match client.query_one(sql, &[]).await {
        Ok(row) => Ok(row.get(0)),
        Err(e) if e.code() == Some(&SqlState::UNDEFINED_TABLE) => Ok(false),
        Err(e) => Err(e.into()),
    }
}

async fn is_empty(proxy: &SshProxy, db_name: String) -> Result<bool> {
    let client = proxy.sql_connect(&db_name).await?;
    let (empty_db, empty_history_db) = tokio::try_join!(
        client.is_empty().map_err(|e| e.into()),
        is_empty_history_db(&client),
    )?;
    Ok(empty_db || empty_history_db)
}

async fn unused_dbs(db_server: &str) -> Result<Vec<UnusedDb>> {
    let sql = r#"
        SELECT datname, pg_catalog.pg_database_size(datname)
        FROM pg_database
        WHERE
            NOT exists(SELECT *
                       FROM pg_stat_activity
                       WHERE pg_stat_activity.datname = pg_database.datname)
            AND NOT datistemplate;
    "#;
    let proxy = SshProxyBuilder::new(db_server).open().await?;
    let client = proxy.sql_connect("postgres").await?;

    let rows = client.query(sql, &[]).await?;
    let mut tasks = Vec::new();
    let mut unused_dbs = Vec::new();
    for row in rows {
        let db_name = row.get::<_, String>(0);
        let db_size = row.get::<_, i64>(1) as u64;
        let task = {
            let db_name1 = db_name.to_string();
            let db_name2 = db_name.to_string();
            async { tokio::try_join!(query_times(&proxy, db_name1), is_empty(&proxy, db_name2)) }
        };

        let db = UnusedDb {
            name: db_name,
            size: db_size,
            time: None,
            empty: false,
        };
        unused_dbs.push(db);
        tasks.push(task);
    }
    let tasks = future::try_join_all(tasks).await?;

    for (db, (time, empty)) in unused_dbs.iter_mut().zip(tasks) {
        db.time = time;
        db.empty = empty;
    }

    Ok(unused_dbs)
}

struct LatestLoginResult<'a> {
    db_server: &'a str,
    db_name: String,
    latest_login: LatestLogin,
}

async fn latest_login_for_db<'a>(
    proxy: &SshProxy,
    db_server: &'a str,
    db_name: String,
) -> Option<LatestLoginResult<'a>> {
    let client = match proxy.sql_connect(&db_name).await {
        Ok(client) => client,
        Err(e) => {
            error!("Failed to connect to {db_server}/{db_name}: {e}");
            return None;
        }
    };
    match client.latest_login().await {
        Ok(latest_login) => Some(LatestLoginResult {
            db_server,
            db_name,
            latest_login,
        }),
        Err(e) if e.code() == Some(&SqlState::UNDEFINED_TABLE) => {
            debug!("Ignoring database {db_name} which is not a Nice DB.");
            None
        }
        Err(e) => {
            error!("Failure while processing {db_server}/{db_name}: {e}");
            None
        }
    }
}

fn print_single_login(category: &str, login: Option<&UserLogin>) {
    if let Some(login) = login {
        let age = utils::human_duration_minutes(Local::now() - login.time);
        println!("{category:9}   {age:>17}  {:20}", login.login);
    } else {
        println!("{category:9}    <no login recorded>");
    }
}

async fn latest_login_for_server(db_server: &str) -> Result<Vec<LatestLoginResult>> {
    let proxy = SshProxyBuilder::new(db_server).open().await?;
    let client = proxy.sql_connect("postgres").await?;
    let dbs = client.list_dbs().await?;
    let tasks: Vec<_> = dbs
        .into_iter()
        .map(|db_name| latest_login_for_db(&proxy, db_server, db_name))
        .collect();
    let latest_logins = future::join_all(tasks)
        .await
        .into_iter()
        .flatten()
        .collect();
    Ok(latest_logins)
}

async fn show_latest_login(db_servers: &HashSet<&str>, args: &LatestLoginArgs) -> Result<()> {
    let sizes: Vec<_> = db_servers
        .iter()
        .map(|db_server| latest_login_for_server(db_server))
        .collect();
    let server_results = future::join_all(sizes).await;

    println!(
        "SERVER      DB                                   CATEGORY     TIME              USER"
    );
    let mut logins = Vec::new();
    for (db_server, results) in db_servers.iter().zip(server_results) {
        match results {
            Ok(results) => {
                logins.extend(results);
            }
            Err(e) => error!("Failed to process server {db_server}: {e}"),
        }
    }

    match args.sort_by {
        LatestLoginSortBy::Db => {
            logins.sort_unstable_by_key(|i| i.db_server);
            logins.sort_by(|a, b| a.db_name.cmp(&b.db_name));
        }
        LatestLoginSortBy::Server => {
            logins.sort_unstable_by(|a, b| a.db_name.cmp(&b.db_name));
            logins.sort_by_key(|i| i.db_server);
        }
        LatestLoginSortBy::GlobalTime => {
            logins.sort_unstable_by_key(|i| i.db_server);
            logins.sort_by(|a, b| a.db_name.cmp(&b.db_name));
            logins.sort_by_key(|i| i.latest_login.global().map(|l| l.time));
        }
        LatestLoginSortBy::NonToccoTime => {
            logins.sort_unstable_by_key(|i| i.db_server);
            logins.sort_by(|a, b| a.db_name.cmp(&b.db_name));
            logins.sort_by_key(|i| i.latest_login.non_tocco().map(|l| l.time));
        }
        LatestLoginSortBy::ToccoTime => {
            logins.sort_unstable_by_key(|i| i.db_server);
            logins.sort_by(|a, b| a.db_name.cmp(&b.db_name));
            logins.sort_by_key(|i| i.latest_login.tocco().map(|l| l.time));
        }
    }

    for login in &logins {
        let db_server = login
            .db_server
            .strip_suffix(".tocco.cust.vshn.net")
            .unwrap_or(login.db_server);
        print!("{db_server:10}  {:35}  ", login.db_name);
        if args.category.show_global() {
            print_single_login("global", login.latest_login.global());
        }
        if args.category.show_non_tocco() {
            print_single_login("non-tocco", login.latest_login.non_tocco());
        }
        if args.category.show_tocco() {
            if args.category.show_non_tocco() {
                print!("                                                 ");
            }
            print_single_login("tocco", login.latest_login.tocco());
        }
    }
    Ok(())
}

async fn db_sizes(db_server: &str) -> Result<Vec<(String, u64)>> {
    let sql = r#"
        SELECT datname, pg_catalog.pg_database_size(datname)
        FROM pg_database
        WHERE NOT datistemplate
    "#;
    let proxy = SshProxyBuilder::new(db_server).open().await?;
    let client = proxy.sql_connect("postgres").await?;

    let rows = client.query(sql, &[]).await?;
    let mut dbs = Vec::new();
    for row in rows {
        let db_name = row.get::<_, String>(0);
        let db_size = row.get::<_, i64>(1) as u64;
        dbs.push((db_name, db_size));
    }

    Ok(dbs)
}

async fn show_sizes(db_servers: &HashSet<&str>, sort_by: SizeSortBy) -> Result<()> {
    let sizes: Vec<_> = db_servers
        .iter()
        .map(|db_server| db_sizes(db_server))
        .collect();
    let sizes = future::join_all(sizes).await;

    let mut dbs = Vec::new();
    for (db_server, task) in db_servers.iter().zip(sizes) {
        match task {
            Ok(items) => {
                for db in items {
                    dbs.push((db_server.to_owned(), db));
                }
            }
            Err(e) => error!("Failed to fetch list from {db_server}: {e}"),
        }
    }
    match sort_by {
        SizeSortBy::Db => dbs.sort_unstable_by(|a, b| a.1.0.cmp(&b.1.0)),
        SizeSortBy::Server => {
            dbs.sort_unstable_by(|a, b| a.1.0.cmp(&b.1.0));
            dbs.sort_by(|a, b| a.0.cmp(b.0));
        }
        SizeSortBy::Size => dbs.sort_unstable_by(|a, b| b.1.1.cmp(&a.1.1)),
    }

    println!("SIZE      SERVER      DB");
    for (db_server, (db_name, size)) in dbs {
        let size = size as f32 / 1024.0 / 1024.0 / 1024.0;
        let db_server_short = db_server
            .strip_suffix(".tocco.cust.vshn.net")
            .unwrap_or(db_server);
        println!("{size:4.1} GiB  {db_server_short:10}  {db_name}");
    }
    Ok(())
}

fn ask_if_db_is_to_be_deleted(db: &UnusedDb, last_used: Option<Duration>) -> Result<bool> {
    let mut input = String::new();
    loop {
        let yes = if last_used
            .map(|d| d < Duration::try_days(3).expect("out of range"))
            .unwrap_or_else(|| !db.empty)
        {
            "yes!"
        } else {
            "y"
        };
        print!("Delete {:?}? [{yes}/N]: ", db.name);
        io::stdout().flush()?;
        io::stdin().read_line(&mut input)?;
        input.make_ascii_lowercase();
        let clean_input = input.trim_end();
        if clean_input == yes {
            break Ok(true);
        } else if ["n", ""].contains(&clean_input) {
            break Ok(false);
        }
        println!("Invalid input, type \"{yes}\", \"n\" or \"\".");
        input.truncate(0);
    }
}

async fn remove_dbs_on_server(db_server: &str, db_names: &[String]) -> Result<()> {
    let proxy = SshProxyBuilder::new(db_server).open().await?;
    let client = proxy.sql_connect("postgres").await?;
    let tasks = db_names.iter().map(|db| async {
        client
            .execute(&format!("DROP DATABASE {}", sql::quote_ident(db)), &[])
            .await
    });
    let task_results = future::join_all(tasks).await;
    for (db_name, result) in db_names.iter().zip(task_results) {
        if let Err(e) = result {
            error!("failed to remove DB {db_server}/{db_name}: {e}");
        }
    }
    Ok(())
}

async fn confirm_and_delete_dbs(dbs: &HashMap<String, Vec<String>>) -> Result<()> {
    println!();
    println!("DBs to remove:");
    println!();
    for (db_server, db_names) in dbs {
        for db_name in db_names {
            println!("{db_server:30}  {db_name}");
        }
    }
    print!("Is the list of DBs above correct? [yes, delete/N]: ");
    io::stdout().flush()?;
    let mut input = String::new();
    loop {
        io::stdin().read_line(&mut input)?;
        input.make_ascii_lowercase();
        match &input[..] {
            "yes, delete\n" => break,
            "no\n" => {
                println!("Aborting …");
                return Ok(());
            }
            _ => {
                println!("Invalid input, type \"yes, delete\", \"no\".");
            }
        }
        input.truncate(0);
    }
    let tasks = dbs
        .iter()
        .map(|(db_server, db_names)| remove_dbs_on_server(db_server, db_names));
    let task_results = future::join_all(tasks).await;
    for ((db_server, _), result) in dbs.iter().zip(task_results) {
        if let Err(e) = result {
            error!("failed to remove DBs on {db_server}: {e}");
        }
    }
    Ok(())
}

async fn show_unused(
    db_servers: &HashSet<&str>,
    sort_by: UnusedSortBy,
    ask_delete: bool,
) -> Result<()> {
    let tasks: Vec<_> = db_servers
        .iter()
        .map(|db_server| unused_dbs(db_server))
        .collect();
    let tasks = future::join_all(tasks).await;

    let mut dbs = Vec::new();
    for (db_server, task) in db_servers.iter().zip(tasks) {
        match task {
            Ok(items) => {
                for db in items {
                    dbs.push((db_server.to_owned(), db));
                }
            }
            Err(e) => error!("Failed to fetch list from {db_server}: {e}"),
        }
    }
    match sort_by {
        UnusedSortBy::Db => dbs.sort_unstable_by(|a, b| a.1.name.cmp(&b.1.name)),
        UnusedSortBy::Modified => {
            dbs.sort_by(|a, b| match (a.1.time, b.1.time) {
                (Some(a), Some(b)) => a.cmp(&b),
                (Some(_), None) => cmp::Ordering::Less,
                (None, Some(_)) => cmp::Ordering::Greater,
                (None, None) => a.1.name.cmp(&b.1.name),
            });
        }
        UnusedSortBy::Server => {
            dbs.sort_unstable_by(|a, b| a.1.name.cmp(&b.1.name));
            dbs.sort_by(|a, b| a.0.cmp(b.0));
        }
        UnusedSortBy::Size => dbs.sort_unstable_by(|a, b| b.1.size.cmp(&a.1.size)),
    }

    println!("SIZE       LAST MODIFIED     SERVER      DB");
    let mut to_be_deleted: HashMap<String, Vec<String>> = HashMap::new();
    let now = chrono::Local::now();
    for (db_server, unused) in dbs {
        let age = unused.time.map(|t| now - t);
        let age_string = if let Some(age) = age {
            utils::human_duration_minutes(age)
        } else if unused.empty {
            "     <empty>".to_string()
        } else {
            String::new()
        };
        let size = unused.size as f32 / 1024.0 / 1024.0 / 1024.0;
        let db_server = db_server
            .strip_suffix(".tocco.cust.vshn.net")
            .unwrap_or(db_server);
        println!(
            "{size:4.1} GiB  {age_string:17}  {db_server:10}  {}",
            unused.name
        );
        if ask_delete && ask_if_db_is_to_be_deleted(&unused, age)? {
            let db_list = to_be_deleted.entry(db_server.to_string()).or_default();
            db_list.push(unused.name.to_string());
        }
    }
    println!();
    println!("Heuristic used for LAST MODIFIED:");
    println!("    main DB:");
    println!("        Most recent time among last login, last task execution,");
    println!("        and last write test by /status-tocco");
    println!("    history DB:");
    println!("        Time of newest history record.");
    if !to_be_deleted.is_empty() {
        confirm_and_delete_dbs(&to_be_deleted).await?;
    }
    Ok(())
}

struct FailedTask {
    name: String,
    last_failure: DateTime<chrono::Local>,
}

async fn failed_tasks_on_db(
    proxy: &SshProxy,
    db_name: &str,
    args: &FailingTasksArgs,
    semaphore: &Semaphore,
) -> Result<Vec<FailedTask>> {
    let sql = r#"
    WITH last_n_jobs AS (
        SELECT
            te.name, te.start_date, te.fk_task_execution_status AS fk_status
        FROM
            nice_task_execution AS te
            LEFT OUTER JOIN nice_task_execution_status AS tes ON te.fk_task_execution_status = tes.pk
        WHERE
            te.start_date > $2
            AND te.pk in (SELECT pk
                          FROM nice_task_execution
                          WHERE
                            start_date > $2
                            AND te.name = name
                            AND tes.unique_id NOT IN ('pending', 'running')
                          ORDER BY start_date DESC
                          LIMIT $1))
    SELECT
        last_n_jobs.name, max(last_n_jobs.start_date) AS last_failure
    FROM
        last_n_jobs
    WHERE
        EXISTS (SELECT 1
                FROM nice_task_execution_status
                WHERE last_n_jobs.fk_status = pk AND unique_id = 'failed')
    GROUP BY
        last_n_jobs.name
    HAVING
        count(*) = $1
    "#;
    let max_age =
        Local::now() - Duration::try_days(args.max_age_days).expect("duration out of range");
    let client = proxy.sql_connect(db_name).await?;
    if let Some(timeout) = args.timeout_msecs() {
        // Parameter to SQL query should be passed as $1 but this doesn't appear to work.
        client
            .execute(&format!("SET statement_timeout TO {timeout}"), &[])
            .await?;
    }
    let _semaphore = semaphore.acquire().await.unwrap();
    debug!("Getting failed tasks from DB {db_name}.");
    let mut result = Vec::new();
    let rows = match client.query(sql, &[&args.max_fails, &max_age]).await {
        Ok(rows) => rows,
        Err(e) if e.code() == Some(&SqlState::UNDEFINED_TABLE) => {
            debug!("Ignoring missing table on DB {db_name:?}: {e}.");
            return Ok(result);
        }
        Err(e) => return Err(e.into()),
    };
    for row in rows {
        result.push(FailedTask {
            name: row.get("name"),
            last_failure: row.get("last_failure"),
        });
    }
    debug!("Got all failed tasks from DB {db_name}.");

    Ok(result)
}

async fn failed_tasks_on_server<'a>(
    db_server: &str,
    configs: &[&'a InstallConfig],
    args: &FailingTasksArgs,
) -> Result<Vec<(&'a InstallConfig, FailedTask)>> {
    debug_assert!(configs.iter().all(|i| db_server == i.db_server));
    let proxy = SshProxyBuilder::new(db_server).open().await?;
    let semaphore = Semaphore::new(args.parallel);
    debug!("Getting failed tasks from server {db_server}.");
    let tasks: Vec<_> = configs
        .iter()
        .map(|config| failed_tasks_on_db(&proxy, &config.db_name, args, &semaphore))
        .collect();
    let tasks = future::join_all(tasks).await;

    let mut result = Vec::new();
    for (config, task) in configs.iter().zip(tasks) {
        match task {
            Ok(failed_jobs) => {
                for job in failed_jobs {
                    result.push((*config, job));
                }
            }
            Err(e) => error!(
                "Failed to fetch failing batch jobs from {db_server}/{} : {e}",
                config.db_name
            ),
        }
    }
    debug!("Got all failed tasks from server {}.", db_server);
    Ok(result)
}

pub async fn show_failed_tasks(
    db_servers: &HashMap<&str, Vec<&InstallConfig>>,
    args: &FailingTasksArgs,
) -> Result<()> {
    let tasks = db_servers
        .iter()
        .map(|(db_server, config)| failed_tasks_on_server(db_server, config, args));
    let tasks = future::join_all(tasks).await;

    let mut failed_tasks = Vec::new();
    for (db_server, task) in db_servers.keys().zip(&tasks) {
        match task {
            Ok(ok) => failed_tasks.extend(ok),
            Err(e) => error!("Failed to fetch failing batch jobs from {db_server}: {e}"),
        }
    }
    match args.sort_by {
        TaskSortBy::Installation => {
            failed_tasks.sort_unstable_by_key(|(_, task)| &task.name[..]);
            failed_tasks.sort_by_key(|(installation, _)| &installation.installation_name);
        }
        TaskSortBy::Task => {
            failed_tasks.sort_unstable_by_key(|(installation, _)| &installation.installation_name);
            failed_tasks.sort_by_key(|(_, task)| &task.name[..]);
        }
        TaskSortBy::Version => {
            failed_tasks.sort_unstable_by_key(|(installation, _)| &installation.installation_name);
            failed_tasks.sort_by_key(|(_, task)| &task.name[..]);
            failed_tasks
                .sort_by_cached_key(|(installation, _)| cmp::Reverse(installation.version()));
        }
    }

    println!("LAST FAILURE         VERSION  INSTALLATION          TASK");
    let now = chrono::Local::now();
    for (installation, task) in failed_tasks {
        let time = match args.format {
            TimeFormat::DateTime => task
                .last_failure
                .naive_local()
                .duration_trunc(Duration::try_seconds(1).expect("out of range"))
                .unwrap()
                .to_string(),
            TimeFormat::Relative => {
                let age = now - task.last_failure;
                utils::human_duration_minutes(age)
            }
        };
        println!(
            "{time:19}  {:7}  {:20}  {}",
            installation.version_pretty(),
            installation.installation_name,
            task.name
        );
    }

    Ok(())
}

#[derive(Debug)]
struct BatchJobOnDb {
    server: String,
    db: String,
    job: BatchJob,
}

async fn retrieve_batch_jobs(
    proxy: &SshProxy,
    server: &str,
    db: &str,
) -> Result<Vec<BatchJobOnDb>> {
    let client = proxy.sql_connect(db).await?;
    match client.batch_jobs().await {
        Ok(jobs) => Ok::<_, Error>(
            jobs.into_iter()
                .map(|job| BatchJobOnDb {
                    server: server.to_owned(),
                    db: db.to_owned(),
                    job,
                })
                .collect(),
        ),
        Err(e) if e.code() == Some(&SqlState::UNDEFINED_TABLE) => Ok(Vec::new()),
        Err(e) => {
            error!("Failed to fetch batch jobs from {server}/{db}: {e}");
            Ok(Vec::new())
        }
    }
}

async fn batch_jobs_from_server(db_server: &str) -> Result<Vec<BatchJobOnDb>> {
    let mut batch_jobs = Vec::new();
    let proxy = SshProxyBuilder::new(db_server).open().await?;
    let dbs = {
        let client = proxy.sql_connect("postgres").await?;
        client.list_dbs().await?
    };
    let tasks: Vec<_> = dbs
        .iter()
        .map(|db| retrieve_batch_jobs(&proxy, db_server, db))
        .collect();
    let tasks = future::try_join_all(tasks).await?;
    for result in tasks {
        batch_jobs.extend(result);
    }
    Ok(batch_jobs)
}

async fn show_batch_jobs(db_servers: &HashSet<&str>, args: &BatchJobsArgs) -> Result<()> {
    let tasks: Vec<_> = db_servers
        .iter()
        .map(|db_server| async move {
            match batch_jobs_from_server(db_server).await {
                Ok(jobs) => jobs,
                Err(e) => {
                    error!("Failed to fetch batch job from server {db_server}: {e}");
                    Vec::new()
                }
            }
        })
        .collect();
    let mut batch_jobs: Vec<_> = future::join_all(tasks)
        .await
        .into_iter()
        .flatten()
        .filter(|job| !args.active_only || job.job.active)
        .collect();

    match args.sort_by {
        BatchJobsSortBy::Name => batch_jobs.sort_by(|a, b| {
            a.job
                .name
                .cmp(&b.job.name)
                .then(a.db.cmp(&b.db))
                .then(a.server.cmp(&b.server))
        }),
        BatchJobsSortBy::Time => batch_jobs.sort_by(|a, b| {
            a.job
                .hour
                .parse::<u8>()
                .ok()
                .cmp(&b.job.hour.parse::<u8>().ok())
                .then(a.job.hour.cmp(&b.job.hour))
                .then(
                    a.job
                        .minute
                        .parse::<u8>()
                        .ok()
                        .cmp(&b.job.minute.parse::<u8>().ok()),
                )
                .then(a.job.minute.cmp(&b.job.minute))
                .then(a.db.cmp(&b.db))
                .then(a.server.cmp(&b.server))
        }),
    }

    println!(
        "A MINUTE         HOUR           DoM      MONTH    DoW      SERVER     DATABASE                       NAME (TYPE)"
    );
    for job in &batch_jobs {
        let server = job.server.trim_end_matches(".tocco.cust.vshn.net");
        let (active_marker, active_color) = if job.job.active {
            ("✔", AnsiColors::Green)
        } else {
            ("✘", AnsiColors::Red)
        };
        anstream::println!(
            "{} {:14} {:14} {:8} {:8} {:8} {server:10} {:30} {} ({}) ",
            active_marker.color(active_color),
            job.job.minute,
            job.job.hour,
            job.job.dom,
            job.job.month,
            job.job.dow,
            job.db,
            job.job.name,
            job.job.r#type,
        );
    }
    println!();
    anstream::println!(
        "Column A(ctive): {} active, {} inactive",
        "✔".green(),
        "✘".red()
    );
    Ok(())
}

pub async fn main(root_args: &crate::Args, args: &Args) -> Result<()> {
    let repo = ansible::git::maybe_updated_repo(root_args.update_repos_interval()).await?;
    let config = ansible::config_quick::load(&repo).await?;
    let db_servers: HashSet<_> = config
        .installations
        .values()
        .map(|i| i.db_server.as_str())
        .collect();

    match &args.command {
        Commands::FailingTasks(sub_args) => {
            eprintln!("Fetching failed tasks, be patient …");
            let mut db_servers: HashMap<_, Vec<&InstallConfig>> = HashMap::new();
            for config in config.installations.values() {
                let list = db_servers.entry(config.db_server.as_str()).or_default();
                list.push(config);
            }
            show_failed_tasks(&db_servers, sub_args).await?;
        }
        Commands::LatestLogin(sub_args) => show_latest_login(&db_servers, sub_args).await?,
        Commands::Sizes(sub_args) => show_sizes(&db_servers, sub_args.sort_by).await?,
        Commands::Unused(sub_args) => {
            show_unused(&db_servers, sub_args.sort_by, sub_args.ask_delete).await?;
        }
        Commands::BatchJobs(sub_args) => {
            show_batch_jobs(&db_servers, sub_args).await?;
        }
    }
    Ok(())
}
