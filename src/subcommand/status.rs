use chrono::offset;
use clap::Parser;
use std::time::Instant;
use tco::Result;
use tco::nice;
use tco::utils;

#[derive(Parser)]
#[command()]
pub struct Args {
    /// Installation name
    #[arg()]
    installation: String,

    /// Print more details
    #[arg(long, short)]
    verbose: bool,
}

pub async fn main(_root_args: &crate::Args, args: &Args) -> Result<()> {
    let base_url = format!("https://{}.tocco.ch", &args.installation);
    let status = nice::status::get(&base_url);
    let settings = nice::client_settings::get(&base_url);
    let start_time = Instant::now();
    let (status, settings) = tokio::try_join!(status, settings)?;
    println!("Status of {:?}", &args.installation);
    println!();
    println!("Build:");
    println!("    Version:     {}", status.version_pretty());
    if args.verbose {
        let mut client_packages: Vec<_> = status
            .client_packages
            .iter()
            .map(|(key, value)| (key.strip_prefix("tocco-").unwrap_or(key), value))
            .collect();
        client_packages.sort_by(|(key1, _), (key2, _)| key1.cmp(key2));
        let max_len = client_packages
            .iter()
            .map(|(key, _)| key.len())
            .max()
            .unwrap_or(0);
        for (package, version) in client_packages {
            let width = max_len - package.len();
            println!("        {package}:{:width$} {version}", "");
        }
    }
    println!("    Revision:    {}", status.revision);
    let age = offset::Local::now() - status.build_time;
    println!(
        "    Build time:  {} ({} ago)",
        status.build_time,
        utils::human_duration_hours(age)
    );
    println!();
    println!("Environment:");
    println!("    Host / pod:  {}", status.host);
    println!("    Environment: {}", settings.environment);
    println!();
    println!("Java Heap Memory:");
    println!("    Max. memory:       {:5} MiB", status.max_mem);
    let pct = status.total_mem as f32 / status.max_mem as f32 * 100.0;
    println!(
        "    Total memory:      {:5} MiB ({pct:.0}%)",
        status.total_mem
    );
    println!("    Free memory:       {:5} MiB", status.free_mem);
    let used_mem = status.total_mem - status.free_mem;
    let total_free_mem = status.free_mem + (status.max_mem - status.total_mem);
    #[allow(clippy::cast_precision_loss)]
    let pct = total_free_mem as f32 / status.max_mem as f32 * 100.0;
    println!(
        "    Used memory:       {used_mem:5} MiB ({:.0}%)",
        100.0 - pct
    );
    println!("    Total free memory: {total_free_mem:5} MiB ({pct:.0}%)");
    println!();
    println!("    Percentages are relative to \"max. memory\".");
    println!();
    println!("Time elapsed: {:.3} s", start_time.elapsed().as_secs_f32());
    Ok(())
}
