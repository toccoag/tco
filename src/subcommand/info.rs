use clap::Parser;
use std::borrow::Cow;
use std::collections::HashMap;
use tco::ansible;
use tco::ansible::config_quick::InstallConfig;
use tco::sql::SqlConnect;
use tco::sql::query::ClientExt;
use tco::sql::ssh::SshProxyBuilder;
use tco::{Error, Result};
use tokio::try_join;

#[derive(Parser)]
#[command()]
pub struct Args {
    #[arg()]
    installation: String,

    /// Print only this, specific Ansible variable / setting
    #[arg()]
    var: Option<VarArg>,

    /// Print additional information
    ///
    /// Prints DB sizes.
    #[arg(long, short)]
    verbose: bool,
}

#[derive(Clone, clap::ValueEnum)]
enum VarArg {
    Branch,
    CustomerName,
    DbName,
    DbServer,
    DbUser,
    HistoryDbName,
    HistoryDbServer,
    HistoryDbUser,
    Version,
}

impl VarArg {
    fn value<'a>(&self, config: &'a InstallConfig) -> Cow<'a, str> {
        match self {
            VarArg::Branch => Cow::Borrowed(&config.branch),
            VarArg::CustomerName => Cow::Borrowed(&config.customer_name),
            VarArg::DbName => Cow::Borrowed(&config.db_name),
            VarArg::DbServer => Cow::Borrowed(&config.db_server),
            VarArg::DbUser => Cow::Borrowed(&config.db_user),
            VarArg::HistoryDbName => Cow::Borrowed(&config.history_db_name),
            VarArg::HistoryDbServer => Cow::Borrowed(&config.history_db_server),
            VarArg::HistoryDbUser => Cow::Borrowed(&config.history_db_user),
            VarArg::Version => Cow::Owned(format!("{}", config.version())),
        }
    }
}

async fn db_size(db_server: &str, db_name: &str) -> Result<String> {
    let db_server = SshProxyBuilder::new(db_server).open().await?;
    let conn = db_server.sql_connect("postgres").await?;
    let size = conn.db_size(db_name).await?;
    match size {
        Some(size) => Ok(format!("{:.1} GiB", size as f32 / 1024.0 / 1024.0 / 1024.0)),
        None => Ok("<DB missing>".to_string()),
    }
}

fn print_config(
    installations: &HashMap<String, InstallConfig>,
    config: &InstallConfig,
    db_size: &Option<(String, String)>,
) {
    println!(
        "Installation {:?} ({} / {})",
        config.installation_name,
        config.version_pretty(),
        config.environment(),
    );
    println!();
    println!("Main DB:");
    println!("    name:   {}", config.db_name);
    println!("    server: {}", config.db_server);
    if let Some((main, _history)) = db_size {
        println!("    size:   {main}");
    }
    println!();
    println!("History DB:");
    println!("    name:   {}", config.history_db_name);
    println!("    server: {}", config.history_db_server);
    if let Some((_main, history)) = db_size {
        println!("    size:   {history}");
    }
    if let Some(solr_server) = &config.solr_server {
        println!();
        println!("Solr:");
        println!("    name: {solr_server}");
    }
    if !config.sibling_installations.is_empty() {
        println!();
        println!("Other installations of customer {:?}", config.customer_name);
        for installation in &config.sibling_installations {
            let sibling_config = installations
                .get(installation)
                .expect("config for sibling missing");
            println!("    {installation} ({})", sibling_config.version_pretty());
        }
    }
}

pub async fn main(root_args: &crate::Args, args: &Args) -> Result<()> {
    let repo = tco::ansible::git::maybe_updated_repo(root_args.update_repos_interval()).await?;
    let config = ansible::config_quick::load(&repo).await?;
    match config.installations.get(&args.installation) {
        Some(install_config) => {
            if let Some(var) = &args.var {
                println!("{}", var.value(install_config));
            } else {
                let db_sizes = if args.verbose {
                    Some(try_join!(
                        db_size(&install_config.db_server, &install_config.db_name),
                        db_size(
                            &install_config.history_db_server,
                            &install_config.history_db_name
                        ),
                    )?)
                } else {
                    None
                };
                print_config(&config.installations, install_config, &db_sizes);
            }
            Ok(())
        }
        None => Err(Error::Custom(format!(
            "installation {} not found",
            &args.installation
        ))),
    }
}
