use anstream::ColorChoice;
use chrono::{DateTime, Local, SubsecRound};
use clap::{Parser, Subcommand};
use serde::Deserialize;
use std::process::{ExitStatus, Stdio};
use std::result::Result as StdResult;
use tco::Result;
use tco::error::Error;
use tco::utils;
use tco::utils::log::DisplayCommand;
use tokio::process::Command;

#[derive(Parser)]
#[command()]
pub struct Args {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
enum Commands {
    /// Dump database (experimental)
    ///
    /// The dump itself is created on given server or one
    /// of its slaves. Use printed command to restore DB on
    /// the same server.
    Dump(DumpArgs),

    /// List available database backups (experimental)
    List(ListArgs),

    /// Restore DB (experimental)
    ///
    /// Restore backup on given server. The ID must belong
    /// the same server cluster from which the backup ID
    /// was obtained.
    ///
    /// The name of the DB is picked based on name indicated
    /// in the backup ID with an random suffix appended.
    Restore(RestoreArgs),
}

#[derive(Parser)]
#[command()]
pub struct DumpArgs {
    /// Database to dump
    ///
    /// Syntax: <DB_SERVER/DB_NAME>
    #[arg(value_parser = parse_location)]
    location: Location,

    /// Print only backup ID
    ///
    /// By default the whole `tco` restore command
    /// is printed.
    #[arg(long, short = 'i')]
    print_id_only: bool,
}

#[derive(Parser)]
#[command()]
pub struct ListArgs {
    /// DB server whose dumps to list
    ///
    /// Request will be forwarded to slaves and their
    /// dumps included as well.
    db_server: String,

    #[arg(value_enum, long, short, default_value_t = SortBy::DbName)]
    sort_by: SortBy,
}

#[derive(Parser)]
#[command()]
pub struct RestoreArgs {
    /// Database server on which to restore
    db_server: String,

    /// Backup ID
    backup_id: String,

    /// Replace the existing DB
    ///
    /// The replaced DB is renamed before freshly
    /// restored DB is put into its place. The
    /// name of the renamed DB is printed.
    #[arg(hide = true, long)]
    replace: bool,
}

#[derive(Clone, Copy, clap::ValueEnum)]
enum SortBy {
    /// Database name
    DbName,

    /// Creation time and then database name
    Time,
}

#[derive(Clone)]
struct Location {
    host_name: String,
    db_name: String,
}

#[derive(Deserialize)]
struct DumpedDb {
    backup_id: String,
}

#[derive(Deserialize)]
struct RestoredDb {
    /// Name of restored DB.
    db_name: String,

    /// Name to which target DB was renamed before `--replace`ing it.
    renamed_db_name: Option<String>,
}

#[derive(Deserialize)]
pub struct BackupEntry {
    id: String,
    time: DateTime<Local>,
    db_name: String,
}

fn parse_location(value: &str) -> StdResult<Location, &'static str> {
    match value.split_once('/') {
        Some((host_name, db_name)) => Ok(Location {
            host_name: host_name.to_owned(),
            db_name: db_name.to_owned(),
        }),
        None => Err("invalid syntax, expected <DB_SERVER/DB_NAME>"),
    }
}

fn check_ssh_exit_status(status: ExitStatus) -> Result<()> {
    match status.code() {
        Some(0) => Ok(()),
        // Error returned by clap argument parser
        Some(2) => {
            info!(
                "The above argument parse error was emitted by tocco-backup-transfer on the DB \
                 server. The above message was not printed by tco."
            );
            Err(Error::Custom(
                "tocco-backup-transfer reported an error parsing passed arguments".to_string(),
            ))
        }
        _ => Err(Error::Custom(format!(
            "ssh or tocco-backup-transfer reported an error: {status}"
        ))),
    }
}

async fn dump(args: &DumpArgs) -> Result<DumpedDb> {
    let mut cmd = base_command(&args.location.host_name);
    cmd.arg("dump");
    cmd.arg("--");
    cmd.arg(&*shlex::try_quote(&args.location.db_name).expect("NUL in DB name"));
    cmd.stdout(Stdio::piped());
    debug!("About to execute command: {}", DisplayCommand(cmd.as_std()));
    let output = cmd.spawn()?.wait_with_output().await?;
    check_ssh_exit_status(output.status)?;
    Ok(serde_json::from_slice(&output.stdout)?)
}

async fn list(args: &ListArgs) -> Result<Vec<BackupEntry>> {
    let mut cmd = base_command(&args.db_server);
    cmd.arg("list");
    cmd.arg("--");
    cmd.stdout(Stdio::piped());
    debug!("About to execute command: {}", DisplayCommand(cmd.as_std()));
    let output = cmd.spawn()?.wait_with_output().await?;
    check_ssh_exit_status(output.status)?;
    Ok(serde_json::from_slice(&output.stdout)?)
}

fn print_list(host_name: &str, entries: &[BackupEntry]) {
    use anstream::println;
    use owo_colors::OwoColorize;

    let now = Local::now();
    for entry in entries {
        println!(
            "tco db-backup restore {} {}",
            host_name,
            shlex::try_quote(&entry.id).expect("NUL in backup ID")
        );
        println!("    db_name: {}", entry.db_name.green());
        println!(
            "    time:    {} ({} ago)",
            entry.time.naive_local().trunc_subsecs(0),
            utils::human_duration_minutes(now - entry.time)
        );
        println!();
    }
}

async fn restore(args: &RestoreArgs) -> Result<RestoredDb> {
    let mut cmd = base_command(&args.db_server);
    cmd.arg("restore");
    if args.replace {
        cmd.arg("--replace");
    }
    cmd.arg("--");
    cmd.arg(&*shlex::try_quote(&args.backup_id).expect("NUL in backup ID"));
    cmd.stdout(Stdio::piped());
    debug!("About to execute command: {}", DisplayCommand(cmd.as_std()));
    let output = cmd.spawn()?.wait_with_output().await?;
    check_ssh_exit_status(output.status)?;
    Ok(serde_json::from_slice(&output.stdout)?)
}

fn base_command(host_name: &str) -> Command {
    let mut cmd = Command::new("ssh");
    cmd.kill_on_drop(true);
    cmd.arg("-l");
    cmd.arg("_tocco_backup_transfer");
    cmd.arg("-o");
    cmd.arg("BatchMode=yes");
    cmd.arg(format!("ssh://{host_name}"));
    cmd.arg("tocco-backup-transfer");

    if log_enabled!(log::Level::Trace) {
        cmd.arg("--log-level");
        cmd.arg("trace");
    } else if log_enabled!(log::Level::Debug) {
        cmd.arg("--log-level");
        cmd.arg("debug");
    }

    // `Always`/`AlwaysAnsi` here simply means color is enabled
    // for stderr. Not necessarily via `--color always`.
    if let ColorChoice::AlwaysAnsi | ColorChoice::Always = anstream::stderr().current_choice() {
        cmd.arg("--color");
        cmd.arg("always");
    }

    cmd
}

pub async fn main(_root_args: &crate::Args, args: &Args) -> Result<()> {
    match &args.command {
        Commands::Dump(dump_args) => {
            let dumped_db = dump(dump_args).await?;
            if dump_args.print_id_only {
                println!("{}", dumped_db.backup_id);
            } else {
                println!(
                    "tco db-backup restore {} {}",
                    dump_args.location.host_name,
                    shlex::try_quote(&dumped_db.backup_id).expect("NUL in backup ID")
                );
            }
            Ok(())
        }
        Commands::List(list_args) => {
            let mut entries = list(list_args).await?;
            match list_args.sort_by {
                SortBy::DbName => entries
                    .sort_unstable_by(|a, b| a.db_name.cmp(&b.db_name).then(a.time.cmp(&b.time))),
                SortBy::Time => entries.sort_unstable_by_key(|e| e.time),
            }
            print_list(&list_args.db_server, &entries);
            Ok(())
        }
        Commands::Restore(restore_args) => {
            let restored_db = restore(restore_args).await?;
            if let Some(renamed_db_name) = restored_db.renamed_db_name {
                println!("Original, replaced DB:");
                println!(
                    "tco db-connect {}/{}",
                    restore_args.db_server,
                    shlex::try_quote(&renamed_db_name).expect("NUL in DB name")
                );
                println!();
                println!("Restored DB:");
            }
            println!(
                "tco db-connect {}/{}",
                restore_args.db_server,
                shlex::try_quote(&restored_db.db_name).expect("NUL in DB name")
            );
            Ok(())
        }
    }
}
