use crate::subcommand::common::EnvArg;
use chrono::{DateTime, Local};
use clap::Parser;
use futures::future;
use std::cmp::Ordering;
use tco::nice::pending_deployment::InstallationStatus;
use tco::nice::pending_deployment::pending_for_installation;
use tco::nice::pending_deployment::{Commit, UpdateSource};
use tco::{Error, Result, ansible, nice, utils};

#[derive(Parser)]
#[command()]
pub struct Args {
    /// Limit output to given installation
    installation: Option<String>,

    /// Limit output to selected environment
    #[arg(value_enum, long, short, default_value_t = EnvArg::All, conflicts_with = "installation")]
    environment: EnvArg,

    /// Hide installations with no pending, customer-specific changes
    #[arg(long, short = 'H')]
    hide_no_pending: bool,

    /// Format used for output
    #[arg(value_enum, long, short, default_value_t = Format::Full)]
    format: Format,

    /// Format to use for build date and time
    #[arg(value_enum, long, short, default_value_t = TimeFormat::Relative)]
    time_format: TimeFormat,

    /// Adjust sorting of output
    #[arg(value_enum, long, short, default_value_t = SortBy::Name)]
    sort_by: SortBy,
}

#[derive(Clone, Copy, clap::ValueEnum)]
enum Format {
    /// Show everything
    Full,

    /// Only show build date and hide commits
    BuildDateOnly,
}

impl Format {
    fn show_commits(self) -> bool {
        match self {
            Format::Full => true,
            Format::BuildDateOnly => false,
        }
    }
}

#[derive(Clone, Copy, clap::ValueEnum)]
enum TimeFormat {
    /// Show date
    Date,

    /// Show date and time
    DateTime,

    /// Show relative time
    Relative,
}

#[derive(Clone, Copy, clap::ValueEnum)]
enum SortBy {
    /// Count of pending commits
    CommitCount,

    /// Installation name
    Name,

    /// Newest commit pending deployment
    NewestCommit,

    /// Oldest commit pending deployment
    OldestCommit,

    /// Build time
    Time,
}

impl TimeFormat {
    fn format(self, timestamp: &DateTime<Local>) -> String {
        match self {
            TimeFormat::Date => timestamp.date_naive().to_string(),
            TimeFormat::DateTime => timestamp.naive_local().to_string(),
            TimeFormat::Relative => utils::human_duration_minutes(chrono::Local::now() - timestamp),
        }
    }
}

fn print_commit(commit: &Commit, format: TimeFormat) {
    print!(
        "    {:9} - {} - {:18} - {}",
        format.format(&commit.time),
        &commit.id.to_string()[0..11],
        commit.author,
        commit.message.split('\n').next().unwrap()
    );
    let tickets = commit.tickets();
    if !tickets.is_empty() {
        print!(" (");
        for (idx, ticket) in tickets.iter().enumerate() {
            if idx > 0 {
                print!(", ");
            }
            print!("{ticket}");
        }
        print!(")");
    }
    println!();
}

#[allow(clippy::too_many_lines)]
pub async fn main(root_args: &crate::Args, args: &Args) -> Result<()> {
    use anstream::println;
    use owo_colors::OwoColorize as _;

    let (ansible_repo, nice_repo) = tokio::try_join!(
        ansible::git::maybe_updated_repo(root_args.update_repos_interval()),
        nice::git::maybe_updated_repo(root_args.update_repos_interval()),
    )?;
    let config = ansible::config_quick::load(&ansible_repo).await?;

    let installations: Vec<_> = match &args.installation {
        Some(installation_name) => {
            let Some(installation) = config.installations.get(&installation_name[..]) else {
                return Err(Error::Custom(format!(
                    "installation {installation_name:?} not found"
                )));
            };
            vec![installation]
        }
        None => config
            .installations
            .values()
            .filter(|installation| args.environment.includes(installation.environment()))
            .collect(),
    };
    let tasks = installations.iter().map(|installation| {
        pending_for_installation(&config, &nice_repo, &installation.installation_name)
    });
    let tasks = future::join_all(tasks).await;

    let mut results: Vec<_> = installations
        .iter()
        .zip(tasks.into_iter())
        .filter(|(_, result)| {
            !args.hide_no_pending
                || !result
                    .as_ref()
                    .map(|r| r.commits.is_empty())
                    .unwrap_or(false)
        })
        .collect();

    results.sort_unstable_by(|(a, _), (b, _)| a.installation_name.cmp(&b.installation_name));
    match args.sort_by {
        SortBy::CommitCount => results.sort_by_key(|(_, result)| {
            if let Ok(status) = result {
                status.commits.len()
            } else {
                usize::MAX
            }
        }),
        SortBy::NewestCommit | SortBy::OldestCommit => {
            let time: fn(&InstallationStatus) -> _ = match args.sort_by {
                SortBy::OldestCommit => |status: &InstallationStatus| {
                    status.commits.iter().map(|commit| commit.time).min()
                },
                SortBy::NewestCommit => |status: &InstallationStatus| {
                    status.commits.iter().map(|commit| commit.time).max()
                },
                _ => unreachable!(),
            };
            results.sort_by(|(cfg_a, result_a), (cfg_b, result_b)| {
                // Sort by number of pending commits placing errors last.
                match (result_a, result_b) {
                    (Ok(status_a), Ok(status_b)) => time(status_a).cmp(&time(status_b)),
                    (Err(_), Err(_)) => cfg_a.installation_name.cmp(&cfg_b.installation_name),
                    (Ok(_), Err(_)) => Ordering::Less,
                    (Err(_), Ok(_)) => Ordering::Greater,
                }
            });
        }
        SortBy::Name => { /* already sorted by name */ }
        SortBy::Time => results.sort_by_key(|(_, result)| {
            result
                .as_ref()
                .map(|c| c.build_time)
                .unwrap_or_else(|_| chrono::DateTime::<chrono::Local>::MIN_UTC.into())
        }),
    }

    for (config, result) in &results {
        match result {
            Ok(result) => {
                debug_assert_eq!(config.installation_name, result.installation_name);

                println!(
                    "{:18} {:5} - built {:9}",
                    config.installation_name.green(),
                    result.version,
                    args.time_format.format(&result.build_time),
                );
                if args.format.show_commits() {
                    if result.commits.is_empty() {
                        println!("    {}", "<no pending customer-specfic commits>".cyan());
                    } else {
                        match &result.deploy_source {
                            UpdateSource::Branch(name) => {
                                println!("    Commits in branch {name} pending deployment:");
                            }
                            UpdateSource::Installation(name) => {
                                println!("    Commits on installation {name} pending deployment:");
                            }
                        }
                        for commit in &result.commits {
                            print_commit(commit, args.time_format);
                        }
                    }
                    println!();
                }
            }
            Err(e) => {
                print!(
                    "{:18} {:5}",
                    config.installation_name.green(),
                    config.version_pretty(),
                );
                if args.format.show_commits() {
                    println!();
                    println!("    {}: {}", "error".red(), e);
                    println!();
                } else {
                    println!(" - {}: {}", "error".red(), e);
                }
            }
        }
    }

    if results.iter().any(|r| r.1.is_err()) {
        // Error
        Err(Error::CustomExitStatus(1))
    } else if results
        .iter()
        .any(|r| matches!(r.1, Ok(ref status) if !status.commits.is_empty()))
    {
        // Pending commits found
        Err(Error::CustomExitStatus(2))
    } else {
        Ok(())
    }
}
