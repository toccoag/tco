use super::common::{self, DbLocation, TypeArg};
use clap::Parser;
use tco::Result;
use tco::exec::CommandExt;
use tco::exec::ssh;
use tco::exec::ssh::Terminal;
use tco::utils;

#[derive(Parser)]
#[command()]
pub struct Args {
    /// Target installation and database
    ///
    /// Syntax: <INSTALLATION[/DB_NAME]|DB_SERVER>/DB_NAME>
    ///
    /// Environment variable PGDATABASE is set server-side when
    /// connecting over SSH. Libpq will use it to determine the
    /// default DB when no DB is specified when connecting to
    /// Postgres. As result, psql, pg_dump, pg_restore, etc.
    /// will use the specified DB by default.
    ///
    /// See https://www.postgresql.org/docs/current/libpq-envars.html
    #[arg(value_parser = common::parse_db_location_arg)]
    target: DbLocation,

    /// Command to execute and arguments passed to it.
    ///
    /// An interactive shell is opened if omitted.
    ///
    /// Use "--" to be able to pass arguments that start
    /// with "-".
    ///
    /// Example:
    ///
    /// tco db-ssh master -- ls -lh /
    #[arg(last = true)]
    command: Option<Vec<String>>,

    /// Select whether to connect to server with main or history DB
    #[arg(long, short, value_enum, default_value_t = TypeArg::Main)]
    r#type: TypeArg,

    /// Explicitly request TTY allocation.
    ///
    /// Corresponds to the -t option of ssh.
    ///
    /// Use in combination with COMMAND to open command
    /// in interactive shell:
    ///
    /// tco db-ssh -T master -- top -H
    #[arg(long, short = 'T')]
    terminal: bool,
}

pub async fn main(root_args: &crate::Args, args: &Args) -> Result<()> {
    let (db_server, db_name) =
        common::db_server_and_name(&args.target, args.r#type, root_args.update_repos_interval())
            .await?;

    let mut cmd = ssh::Command::new(&db_server);
    cmd.stdin(true);
    cmd.env("PGDATABASE", &db_name);
    let status = if let Some(command) = &args.command {
        if args.terminal {
            cmd.terminal(Terminal::Always);
        }
        cmd.spawn(&command[0], &command[1..])
    } else {
        cmd.terminal(if args.terminal {
            Terminal::Always
        } else {
            Terminal::Auto
        });
        cmd.spawn_terminal()
    }?
    .wait()
    .await?;
    utils::log::vpn_needed_info_message_on_ssh_timeout(status);
    Ok(())
}
