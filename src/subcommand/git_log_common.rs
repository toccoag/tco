use clap::Parser;
use nix::sys::signal::Signal;
use std::os::unix::process::ExitStatusExt;
use std::process::Command;
use std::time::Duration;
use tco::ansible::config_quick::Config;
use tco::git::Repository;
use tco::utils::log::DisplayCommand;
use tco::{Error, Result, ansible, nice};

#[derive(Parser)]
#[command()]
pub struct Args {
    /// Installation name
    #[arg()]
    installation: String,

    /// Argument passed to `git log`
    ///
    /// Example a) show more details:
    ///
    ///      $ tco log master -- --format=fuller
    ///
    /// Example b) show only commits touching directory core/:
    ///
    ///      $ tco log master -- -- core/
    ///
    /// Example c) show one-line summary for commits touching directory core/:
    ///
    ///      $ tco log master -- --format=oneline -- core/
    ///
    /// See also git-log(1)
    #[arg(last = true, verbatim_doc_comment)]
    git_args: Vec<String>,
}

#[derive(Copy, Clone)]
pub enum Mode {
    CustomerLog,
    CustomerProdUndeployed,
    CustomerUndeployed,
    Log,
    ProdUndeployed,
    Undeployed,
}

impl Mode {
    fn show_non_customer_commits(self) -> bool {
        match self {
            Mode::Undeployed | Mode::ProdUndeployed | Mode::Log => true,
            Mode::CustomerUndeployed | Mode::CustomerProdUndeployed | Mode::CustomerLog => false,
        }
    }

    fn repo_paths(
        self,
        paths: &mut Vec<String>,
        repo: &Repository,
        customer_name: &str,
        rev: &str,
    ) -> Result<()> {
        paths.push(format!("customer/{customer_name}"));
        if self.show_non_customer_commits() {
            let tree = repo
                .as_inner()
                .revparse_single(rev)?
                .into_commit()
                .unwrap()
                .tree()
                .unwrap();
            for item in &tree {
                let name = item.name().expect("non-UTF8 file name");
                if name != "customer" && !paths.iter().any(|i| i == name) {
                    paths.push(name.to_owned());
                }
            }
        }
        Ok(())
    }
}

fn show_log(
    repo: &Repository,
    paths: Vec<String>,
    diff_spec: &str,
    git_args: &[String],
) -> Result<()> {
    let mut command = Command::new("git");
    command
        .current_dir(repo.as_inner().path())
        .arg("log")
        .arg("--no-merges")
        .args(git_args)
        .arg(diff_spec)
        .arg("--")
        .args(paths);
    debug!("Executing command: {}", DisplayCommand(&command));
    let status = command.status().unwrap();
    if let Some(signal) = status.signal() {
        // triggered by pager
        if Signal::try_from(signal) == Ok(Signal::SIGPIPE) {
            return Ok(());
        }
    }
    let ret = status.code().unwrap();
    if ret == 0 {
        Ok(())
    } else {
        Err(Error::Custom(format!("`git` exited with {ret}")))
    }
}

async fn ansible_load_config(interval: Option<Duration>) -> Result<Config> {
    let ansible_repo = tco::ansible::git::maybe_updated_repo(interval).await?;
    ansible::config_quick::load(&ansible_repo).await
}

async fn fetch_client_settings(installation: &str) -> Result<nice::client_settings::Settings> {
    let base_url = format!("https://{}.tocco.ch", &installation);
    match nice::client_settings::get(&base_url).await {
        Ok(v) => Ok(v),
        Err(Error::Reqwest(e)) if e.is_request() => Err(Error::Custom(format!(
            "installation {installation:?} may not exist: {e}"
        ))),
        Err(e) => Err(e),
    }
}

pub async fn main(root_args: &crate::Args, args: &Args, mode: Mode) -> Result<()> {
    let settings = fetch_client_settings(&args.installation);

    let test_settings = async {
        match mode {
            Mode::ProdUndeployed | Mode::CustomerProdUndeployed => {
                let installation = format!("{}test", &args.installation);
                Ok(Some(fetch_client_settings(&installation).await?))
            }
            _ => Ok(None),
        }
    };

    let nice_repo = nice::git::maybe_updated_repo(root_args.update_repos_interval());
    let ansible_config = ansible_load_config(root_args.update_repos_interval());
    let (nice_repo, ansible_config, settings, test_settings) =
        tokio::try_join!(nice_repo, ansible_config, settings, test_settings)?;

    let Some(installation_config) = ansible_config.installations.get(&args.installation) else {
        return Err(Error::Custom(format!(
            "installation {:?} not found",
            args.installation
        )));
    };

    let (src_rev, dst_rev) = match mode {
        Mode::ProdUndeployed | Mode::CustomerProdUndeployed => {
            let test_settings =
                test_settings.expect("fetch of client settings for test system not scheduled");
            (Some(settings.revision), test_settings.revision)
        }
        Mode::Undeployed | Mode::CustomerUndeployed => {
            (Some(settings.revision), installation_config.branch.clone())
        }
        Mode::Log | Mode::CustomerLog => (None, settings.revision),
    };

    let mut paths = Vec::new();
    if let Some(src_rev) = &src_rev {
        // Technically, we're missing paths created after `src_rev` and removed
        // again before `dst_rev`.
        mode.repo_paths(
            &mut paths,
            &nice_repo,
            &installation_config.customer_name_in_repo,
            src_rev,
        )?;
    }
    mode.repo_paths(
        &mut paths,
        &nice_repo,
        &installation_config.customer_name_in_repo,
        &dst_rev,
    )?;
    let rev_range = match src_rev {
        Some(src_rev) => format!("{src_rev}..{dst_rev}"),
        None => dst_rev,
    };
    show_log(&nice_repo, paths, &rev_range, &args.git_args)
}
