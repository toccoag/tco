use super::common::{self, DbLocation, TypeArg};
use clap::Parser;
use std::path::PathBuf;
use tco::exec::CommandExt;
use tco::exec::ssh;
use tco::exec::ssh::Terminal;
use tco::utils;
use tco::{Error, Result};

#[derive(Parser)]
#[command()]
pub struct Args {
    /// Target installation and database
    ///
    /// Syntax: <INSTALLATION[/DB_NAME]|DB_SERVER>/DB_NAME>
    #[arg(value_parser = common::parse_db_location_arg)]
    target: DbLocation,

    /// Select to which DB to connect
    ///
    /// Ignored if a target DB is specified explicitly via TARGET.
    #[arg(long, short, value_enum, default_value_t = TypeArg::Main)]
    r#type: TypeArg,

    /// Execute SQL from FILE
    ///
    /// Run SQL across all production installations, print output as CSV, and sort columns:
    ///
    ///     $ installations=$(tco list-installations --environment prod --name-only);
    ///     $ cat >sqlfile <<EOF
    ///     SELECT
    ///         current_database(),        -- column 1 (sort -k 1,1)
    ///         p.username,
    ///         p.last_login::timestamp    -- column 3 (sort -k 3,3)
    ///                                    -- (casting to a type which can be sorted)
    ///       FROM
    ///         nice_principal AS p
    ///         LEFT OUTER JOIN nice_user AS u ON p.fk_user = u.pk
    ///       WHERE
    ///         p.last_login IS NOT NULL
    ///         AND u.email NOT LIKE '%@tocco.ch';
    ///     EOF
    ///     $ for installation in $installations; do
    ///           tco db-connect "$installation" sqlfile -- --csv -t;
    ///       done | sort -t , -k 3,3 -k 1,1r >result.csv
    ///     $ csvtool readable result.csv
    ///
    /// Sort column 2 numerically (g) in reversed (r) order and column 1
    /// lexicographically:
    ///
    ///     $ sort -t , -k 2,2gr -k 1,1
    ///
    /// See --command for more examples.
    #[arg(conflicts_with = "command", verbatim_doc_comment)]
    file: Option<PathBuf>,

    /// Execute given SQL
    ///
    /// Extract a simple value:
    ///
    ///     $ tco db-connect master -c 'SELECT count(*) FROM nice_user' -- -qtA
    ///
    /// Print rows as CSV:
    ///
    ///     $ tco db-connect master -c 'SELECT firstname, lastname FROM nice_user' -- --csv
    ///
    /// Loop over all production installations:
    ///
    ///     $ installations=$(tco list-installations --environment prod --name-only);
    ///     $ for installation in $installations; do
    ///           tco db-connect -c 'SELECT current_database(), count(*) FROM nice_user' \
    ///             "$installation" -- --csv -t;
    ///       done
    ///
    /// See FILE for more examples. Use it for more complex SQL.
    #[arg(long, short, verbatim_doc_comment)]
    command: Option<String>,

    /// Arguments passed to psql.
    ///
    /// Use '--' to pass arguments starting with a '-'.
    ///
    /// Example:
    ///
    /// tco db-connect master -- --help
    #[allow(clippy::struct_field_names)]
    #[arg(last = true)]
    args: Vec<String>,
}

pub async fn main(root_args: &crate::Args, args: &Args) -> Result<()> {
    let (db_server, db_name) =
        common::db_server_and_name(&args.target, args.r#type, root_args.update_repos_interval())
            .await?;
    if db_name.contains('?') {
        return Err(Error::Custom(format!(
            "database {db_name:?} contains unsupported character '?'"
        )));
    }

    let uri = format!("postgresql:///{db_name}?application_name=tco / psql");
    let mut cmd_args = vec![&uri[..]];
    for arg in &args.args {
        cmd_args.push(arg);
    }
    if let Some(command) = &args.command {
        cmd_args.push("-c");
        cmd_args.push(command);
    }
    let mut command = ssh::Command::new(&db_server);
    command.terminal(if args.command.is_none() && args.file.is_none() {
        Terminal::Auto
    } else {
        Terminal::Never
    });
    command.stdin(true);
    let mut proc = if let Some(file_path) = &args.file {
        command.spawn_stdin_file("psql", cmd_args.iter(), file_path)?
    } else {
        command.spawn("psql", cmd_args.iter())?
    };
    let status = proc.wait().await?;

    utils::log::vpn_needed_info_message_on_ssh_timeout(status);
    if status.success() {
        Ok(())
    } else {
        Err(Error::CustomExitStatus(status.code().unwrap_or(1)))
    }
}
