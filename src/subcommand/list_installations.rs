use crate::subcommand::common::EnvArg;
use clap::Parser;
use std::cmp;
use tco::Result;
use tco::ansible;

#[derive(Parser)]
#[command()]
pub struct Args {
    /// Limit output to selected environment.
    #[arg(value_enum, long, short, default_value_t = EnvArg::All)]
    environment: EnvArg,

    /// Adjust sorting of output
    #[arg(value_enum, long, short, default_value_t = SortBy::Name)]
    sort_by: SortBy,

    /// Limit output to installation name
    #[arg(long, short)]
    name_only: bool,
}

#[derive(Clone, clap::ValueEnum)]
enum SortBy {
    Name,
    Version,
}

pub async fn main(root_args: &crate::Args, args: &Args) -> Result<()> {
    let repo = tco::ansible::git::maybe_updated_repo(root_args.update_repos_interval()).await?;
    let configs = ansible::config_quick::load(&repo).await?;
    let mut configs: Vec<_> = configs
        .installations
        .into_values()
        .filter(|c| args.environment.includes(c.environment()))
        .collect();
    match args.sort_by {
        SortBy::Name => {
            configs.sort_unstable_by(|a, b| a.installation_name.cmp(&b.installation_name));
        }
        SortBy::Version => {
            configs.sort_unstable_by(|a, b| a.installation_name.cmp(&b.installation_name));
            configs.sort_by_key(|i| cmp::Reverse(i.version()));
        }
    }
    for config in configs {
        if args.name_only {
            println!("{}", config.installation_name);
        } else {
            println!(
                "{:7}  {}",
                config.version_pretty(),
                config.installation_name
            );
        }
    }
    Ok(())
}
