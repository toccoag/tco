use crate::subcommand::common;
use crate::subcommand::deploy_status_common::{self, CommonArgs};
use clap::Parser;
use tco::Result;
use tco::nice;
use tco::nice::status::Status;

#[derive(Parser)]
#[command()]
pub struct Args {
    #[command(flatten)]
    common: CommonArgs,
}

#[allow(clippy::unnecessary_wraps)] // required by API
fn extract_git_revision(status: &Status) -> Result<Option<String>> {
    Ok(Some(status.revision.clone()))
}

pub async fn main(root_args: &crate::Args, args: &Args) -> Result<()> {
    let (config, nice_repo) = tokio::try_join!(
        common::ansible_config(root_args.update_repos_interval()),
        nice::git::maybe_updated_repo(root_args.update_repos_interval()),
    )?;
    deploy_status_common::main(
        &args.common,
        &config,
        &nice_repo,
        &extract_git_revision,
        Some(*nice::git::oldest_commit_deployed()),
    )
    .await
}
