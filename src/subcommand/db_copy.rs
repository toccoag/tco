use async_trait::async_trait;
use chrono::offset;
use clap::Parser;
use std::fmt::Write;
use std::result::Result as StdResult;
use std::time;
use std::time::Duration;
use tco::Result;
use tco::ansible;
use tco::ansible::config_quick;
use tco::error::Error;
use tco::nice::Env;
use tco::sql::dump::{
    self, Copier, CreateMode, DbMetadata, GenericProto, GenericProtoBuilder, Owner,
};
use tco::sql::query::ClientExt;
use tokio::sync::OnceCell;
use tokio_postgres as postgres;

#[derive(Parser)]
#[command()]
pub struct Args {
    /// Copy source
    ///
    /// Syntax: <INSTALLATION[/DB_NAME]|[DB_SERVER|localhost]/DB_NAME>
    ///
    /// An empty DB_SERVER is treated as if localhost was given. This
    /// is "localhost/test_db" and "/test_db" are equal.
    ///
    /// If INSTALLATION without DB_NAME is given, the DB
    /// name associated with the installation is used.
    #[arg(value_parser = parse_location)]
    source: Location,

    /// Copy target
    ///
    /// Syntax: [OWNER@][INSTALLATION|DB_SERVER|localhost][/DB_NAME]
    ///
    /// An empty DB_SERVER is treated as if localhost was given. This
    /// is "localhost/test_db" and "/test_db" are equal.
    ///
    /// When a INSTALLATION without a DB_NAME is given, the DB
    /// name associated with the installation is used. When a
    /// DB_SERVER without a DB_NAME is given, the DB_NAME of
    /// the SOURCE is used.
    #[arg(value_parser = parse_location, default_value = "localhost")]
    target: Location,

    /// Create backup of database
    ///
    /// Creates a backup / copy of the database on the same host
    /// appending a time-based suffix to the name of the copy.
    #[arg(short, long, conflicts_with = "target")]
    backup: bool,

    /// Create backup of database appending given suffix
    ///
    /// Creates backup / copy of the database on the same host
    /// appending given suffix.
    ///
    /// Note that an additional '_' is inserted before the suffix. This is, copying
    /// like so:
    ///
    ///     $ tco db-copy --backup-with-suffix copy1 db1.prod/nice_master
    ///
    /// Results in a DB with name "nice_master_copy1"
    #[arg(
        short = 'B',
        long,
        conflicts_with = "target",
        conflicts_with = "backup",
        verbatim_doc_comment
    )]
    backup_with_suffix: Option<String>,

    /// Arbitrary string set as comment on target DB.
    ///
    /// Stored as part of the usual JSON-based comment
    /// on the DB indicating from where DB was copied.
    #[arg(long, short)]
    comment: Option<String>,

    /// Mode used to create TARGET
    ///
    /// For safety reasons, `replace` is only allowed if TARGET is localhost.
    #[arg(value_enum, long, short = 'C', default_value_t = CreateMode::Auto)]
    create_mode: CreateMode,

    /// Which DB to copy
    #[arg(value_enum, long, short, default_value_t = TypeArg::Main)]
    r#type: TypeArg,

    /// Owner to use for TARGET
    ///
    /// By default, in auto mode, the following logic is used, first
    /// match wins:
    ///
    /// • If given via command line, use specified OWNER.
    /// • If target DB exists, use owner of target DB.
    /// • If target specified on command line is an installation, use
    ///   owner associated with installation.
    #[arg(
        value_enum, long,
        short,
        default_value_t = OwnerModeArg::Auto,
        conflicts_with = "backup",
        conflicts_with = "backup_with_suffix",
        verbatim_doc_comment,
    )]
    owner_mode: OwnerModeArg,

    /// Protocol used to copy DB
    ///
    /// `auto` selects the protocol as follows:
    ///
    /// • If target is localhost, choose `nice-partial-without-email`.
    /// • If target is a test system, choose `nice-partial-with-email`.
    /// • Otherwise, choose `generic`
    #[arg(value_enum, long, short, default_value_t = ProtocolArg::Auto, verbatim_doc_comment)]
    protocol: ProtocolArg,

    /// Exclude table content from copying
    ///
    /// Exclude data of table with given name from DB dump. Table,
    /// including indexes, sequences, etc. is created but no rows
    /// copied.
    ///
    /// May be given multiple times to exclude multiple tables.
    ///
    /// May be combined with --protocol nice-dev to exclude
    /// additional tables.
    #[arg(
        long = "exclude-table",
        short,
        value_name = "EXCLUDED_TABLE",
        value_delimiter = ','
    )]
    excluded_tables: Vec<String>,

    /// Print excluded tables and exit.
    ///
    /// Print excluded tables based on given --exclude-table and
    /// --protocol, then exit.
    #[arg(long)]
    show_excluded_tables: bool,

    /// Suppress informational output on stdout
    #[arg(long, short)]
    quiet: bool,
}

#[derive(Clone, Copy, clap::ValueEnum)]
enum TypeArg {
    /// Main database of installation.
    Main,

    /// History database of installation.
    History,
}

#[derive(Clone, Copy, clap::ValueEnum)]
enum OwnerModeArg {
    /// Try to detect owner to use
    Auto,

    /// Copy owner from SORUCE DB
    Copy,

    /// Use owner of TARGET DB. TARGET must exist.
    FromTarget,
}

#[derive(Clone, Copy, clap::ValueEnum, Eq, PartialEq)]
enum ProtocolArg {
    /// Automatically select protocol. See above.
    Auto,

    /// Generic protocol suited to copy any DB
    Generic,

    /// Protocol copying a Nice DB partially (skipping various tables)
    NicePartialWithEmail,

    /// Like `nice-partial-with-email` but also skip email-related tables
    NicePartialWithoutEmail,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
enum ProtocolChoice {
    Generic,
    NicePartialWithoutEmail,
    NicePartialWithEmail,
}

#[derive(Clone, Debug, PartialEq)]
struct Location {
    host: Host,
    db_name: Option<String>,
    owner: Option<String>,
}

#[derive(Clone, Debug, PartialEq)]
enum Host {
    Installation(String),
    Remote(String),
    Local,
}

fn parse_location(location: &str) -> StdResult<Location, String> {
    if location.chars().filter(|c| c == &'/').count() > 1 {
        return Err("expected at most one '/'".to_string());
    }
    if location.chars().filter(|c| c == &'@').count() > 1 {
        return Err("expected at most one '@'".to_string());
    }

    let (db_name, rest) = if let Some((rest, db_name)) = location.split_once('/') {
        if db_name.contains('@') {
            return Err("DB name cannot contain '@'".to_string());
        }
        if db_name.is_empty() {
            (None, rest)
        } else {
            (Some(db_name.to_string()), rest)
        }
    } else {
        (None, location)
    };

    let (owner, host) = if let Some((owner, host)) = rest.split_once('@') {
        if owner.is_empty() {
            (None, host.to_string())
        } else {
            (Some(owner.to_string()), host.to_string())
        }
    } else {
        (None, rest.to_string())
    };

    let host = if host.is_empty() || host == "localhost" {
        Host::Local
    } else if host == "db3" {
        // Expanding to FQDN to ensure, during copying, it is properly
        // detected when source and target are on the same machine. This
        // makes on-machine copying work properly when FQDN and non-FQDN
        // are mixed.
        Host::Remote("db3.tocco.cust.vshn.net".to_string())
    } else if host == "db4" {
        // ditto
        Host::Remote("db4.tocco.cust.vshn.net".to_string())
    } else if host.contains('.') {
        // false positive: not a file name
        #[allow(clippy::case_sensitive_file_extension_comparisons)]
        if host.ends_with(".prod") || host.ends_with(".stage") {
            // ditto
            Host::Remote(format!("{host}.tocco.cust.vshn.net"))
        } else {
            Host::Remote(host)
        }
    } else {
        Host::Installation(host)
    };

    Ok(Location {
        host,
        db_name,
        owner,
    })
}

#[derive(Debug, PartialEq, Eq)]
struct ResolvedLocation {
    source: dump::Location,
    target: dump::Location,
    owner: Owner,
    protocol: ProtocolChoice,
}

/// Resolve source, target and owner based on Ansible inventory
///
/// Resolves "master" to something like "nice_master@db7.stage.tocco.cust.vshn.net/nice_master".
#[allow(clippy::too_many_lines)]
async fn resolve_location(args: &Args, config_loader: &LoadConfig) -> Result<ResolvedLocation> {
    if args.source.owner.is_some() {
        return Err(Error::Custom(
            "no owner may be specified for SOURCE".to_string(),
        ));
    }

    let source = resolve_source(config_loader, args).await?;
    let target = resolve_target(config_loader, args, &source).await?;
    let owner = resolve_owner(config_loader, args).await?;
    let protocol = resolve_protocol(args, config_loader, &target).await?;

    debug!("Copying from {source} to {target}.");
    debug!("Owner mode is {owner:?}.");
    Ok(ResolvedLocation {
        source,
        target,
        owner,
        protocol,
    })
}

async fn resolve_source(config_loader: &LoadConfig, args: &Args) -> Result<dump::Location> {
    match &args.source.host {
        Host::Local => {
            if let Some(db_name) = &args.source.db_name {
                Ok(dump::Location::new_local(db_name))
            } else {
                Err(Error::Custom(
                    "when copying from a local SOURCE, a DB name is required".to_string(),
                ))
            }
        }
        Host::Remote(host_name) => {
            if let Some(db_name) = &args.source.db_name {
                Ok(dump::Location::new_ssh(host_name, db_name))
            } else {
                Err(Error::Custom(
                    "when a host is specified as SOURCE, a DB name is required".to_string(),
                ))
            }
        }
        Host::Installation(name) => {
            Ok(late_parse_installation(name, &args.source, args.r#type, config_loader).await?)
        }
    }
}

async fn resolve_target(
    config_loader: &LoadConfig,
    args: &Args,
    source: &dump::Location,
) -> Result<dump::Location> {
    if args.backup {
        debug_assert!(matches!(args.target.host, Host::Local)); // arg parser default
        let mut loc = source.clone();
        let suffix = offset::Local::now().format("_%Y_%m_%d_%H_%M_%S");
        write!(&mut loc.db_name, "{suffix}").expect("failed to format timestamp");
        Ok(loc)
    } else if let Some(suffix) = &args.backup_with_suffix {
        debug_assert!(matches!(args.target.host, Host::Local)); // arg parser default
        let mut loc = source.clone();
        loc.db_name.push('_');
        loc.db_name.push_str(suffix);
        Ok(loc)
    } else {
        match &args.target.host {
            Host::Local => {
                if let Some(db_name) = &args.target.db_name {
                    Ok(dump::Location::new_local(db_name))
                } else {
                    Ok(dump::Location::new_local(&source.db_name))
                }
            }
            Host::Remote(host_name) => {
                if let Some(db_name) = &args.target.db_name {
                    Ok(dump::Location::new_ssh(host_name, db_name))
                } else {
                    Ok(dump::Location::new_ssh(host_name, &source.db_name))
                }
            }
            Host::Installation(name) => {
                Ok(late_parse_installation(name, &args.target, args.r#type, config_loader).await?)
            }
        }
    }
}

async fn resolve_owner(config_loader: &LoadConfig, args: &Args) -> Result<Owner> {
    match args.owner_mode {
        OwnerModeArg::Auto => {
            if let Some(owner) = &args.target.owner {
                Ok(Owner::Name(owner.clone()))
            } else {
                match &args.target.host {
                    Host::Local => Ok(Owner::FromTargetOrCurrentUser),
                    Host::Remote(_) => Ok(Owner::FromTarget),
                    Host::Installation(name) => {
                        let config = config_loader
                            .get()
                            .await?
                            .installations
                            .get(name)
                            .ok_or_else(|| {
                                Error::Custom(format!("installation {name:?} not found"))
                            })?;
                        let owner = match args.r#type {
                            TypeArg::Main => &config.db_user,
                            TypeArg::History => &config.history_db_user,
                        };
                        Ok(Owner::FromTargetOr(owner.clone()))
                    }
                }
            }
        }
        OwnerModeArg::Copy => Ok(Owner::FromSource),
        OwnerModeArg::FromTarget => Ok(Owner::FromTarget),
    }
}

/// Resolve [`ProtocolArg::Auto`] to concrete [`ProtocolChoice`]
async fn resolve_protocol(
    args: &Args,
    config_loader: &LoadConfig,
    target: &dump::Location,
) -> Result<ProtocolChoice> {
    match args.protocol {
        ProtocolArg::Auto => {
            if args.backup || args.backup_with_suffix.is_some() {
                debug!(
                    "Protocol `Auto` selection: in backup mode, forcing complete copy by \
                     selecting `Generic` protocol."
                );
                Ok(ProtocolChoice::Generic)
            } else {
                match &args.target.host {
                    Host::Local => {
                        debug!(
                            "Protocol `Auto` selection: selecting protocol \
                             `NicePartialWithoutEmail` for target localhost."
                        );
                        Ok(ProtocolChoice::NicePartialWithoutEmail)
                    }
                    Host::Remote(_) => {
                        if target.db_name.contains("test") {
                            debug!(
                                "Protocol `Auto` selection: guessing that this is a test system \
                                 based on DB name {:?}, selecting `NicePartialWithEmail` protocol.",
                                target.db_name,
                            );
                            Ok(ProtocolChoice::NicePartialWithEmail)
                        } else {
                            debug!(
                                "Protocol `Auto` selection: guessing that this is a prod system \
                                 based on DB name {:?}, selecting `Generic` protocol.",
                                target.db_name,
                            );
                            Ok(ProtocolChoice::Generic)
                        }
                    }
                    Host::Installation(name) => {
                        let env = config_loader
                            .get()
                            .await?
                            .installations
                            .get(name)
                            .ok_or_else(|| {
                                Error::Custom(format!("installation {name:?} not found"))
                            })?
                            .environment();
                        match env {
                            Env::Prod => {
                                debug!(
                                    "Protocol `Auto` selection: {name:?} is a prod system \
                                     according to Ansible, selecting `Generic` protocol."
                                );
                                Ok(ProtocolChoice::Generic)
                            }
                            Env::Test => {
                                debug!(
                                    "Protocol `Auto` selection: {name:?} is a test system \
                                     according to Ansible, selecting `NicePartialWithEmail` \
                                     protocol."
                                );
                                Ok(ProtocolChoice::NicePartialWithEmail)
                            }
                        }
                    }
                }
            }
        }
        ProtocolArg::Generic => Ok(ProtocolChoice::Generic),
        ProtocolArg::NicePartialWithEmail => Ok(ProtocolChoice::NicePartialWithEmail),
        ProtocolArg::NicePartialWithoutEmail => Ok(ProtocolChoice::NicePartialWithoutEmail),
    }
}

async fn late_parse_installation(
    name: &str,
    host: &Location,
    r#type: TypeArg,
    config_loader: &LoadConfig,
) -> Result<dump::Location> {
    let config = config_loader
        .get()
        .await?
        .installations
        .get(name)
        .ok_or_else(|| Error::Custom(format!("installation {name:?} not found")))?;
    let host_name = match &r#type {
        TypeArg::Main => &config.db_server,
        TypeArg::History => &config.history_db_server,
    };
    let db_name = match &host.db_name {
        Some(db_name) => db_name,
        None => match &r#type {
            TypeArg::Main => &config.db_name,
            TypeArg::History => &config.history_db_name,
        },
    };
    Ok(dump::Location::new_ssh(host_name, db_name))
}

struct LoadConfig {
    update_interval: Option<Duration>,
    config: OnceCell<config_quick::Config>,
}

impl LoadConfig {
    fn new(update_interval: Option<Duration>) -> Self {
        LoadConfig {
            config: OnceCell::new(),
            update_interval,
        }
    }

    async fn get(&self) -> Result<&config_quick::Config> {
        self.config
            .get_or_try_init(|| async {
                let repo = ansible::git::maybe_updated_repo(self.update_interval).await?;
                ansible::config_quick::load(&repo).await
            })
            .await
    }
}

struct CopyCallback {
    start: time::Instant,
    quiet: bool,
}

#[async_trait(?Send)]
impl dump::Callback for CopyCallback {
    async fn pre_copy(
        &self,
        copier: &Copier,
        source_conn: &postgres::Client,
        _target_conn: &postgres::Client,
        source_metadata: Option<&DbMetadata>,
    ) -> Result<()> {
        if !self.quiet {
            println!();
            println!("source:");
            println!("    location: {}", copier.source());
            if let Some(size) = source_conn.db_size(&copier.source().db_name).await? {
                println!(
                    "    DB size:  {:.1} GiB",
                    size as f64 / 1024.0 / 1024.0 / 1024.0
                );
            }
            if let Some(metadata) = &source_metadata {
                let tainted_str = if metadata.tainted {
                    "true ⚠️ (source is partial DB copy)"
                } else {
                    "false"
                };
                println!("    tainted: {tainted_str}");
            }
            println!("target:");
            println!("    location: {}", copier.target());
            println!();
        }
        Ok(())
    }

    async fn post_copy(
        &self,
        _copier: &Copier,
        _source_conn: &postgres::Client,
        _target_conn: &postgres::Client,
    ) -> Result<()> {
        let duration = self.start.elapsed().as_secs();
        if !self.quiet {
            println!("duration: {} min {} s", duration / 60, duration % 60);
        }
        Ok(())
    }
}

fn check_if_args_conflict(
    args: &Args,
    location: &ResolvedLocation,
) -> StdResult<(), clap::error::Error> {
    use clap::CommandFactory;
    use clap::error::ErrorKind;

    if matches!(args.create_mode, CreateMode::Replace) && !location.target.is_local() {
        let mut cmd = Args::command();
        Err(cmd.error(
            ErrorKind::ArgumentConflict,
            "`--create-mode replace` only allowed when target is localhost",
        ))
    } else {
        Ok(())
    }
}

fn protocol(args: &Args, location: &ResolvedLocation) -> GenericProto {
    let mut proto_builder = GenericProtoBuilder::new();
    match location.protocol {
        ProtocolChoice::Generic => {}
        ProtocolChoice::NicePartialWithEmail => {
            proto_builder.exclude_var_tables();
        }
        ProtocolChoice::NicePartialWithoutEmail => {
            proto_builder.exclude_var_tables();
            proto_builder.exclude_email_tables();
        }
    }
    proto_builder.exclude_tables(&args.excluded_tables);
    let proto = proto_builder.build();

    let excluded_tables = proto.excluded_tables();
    if !excluded_tables.is_empty() {
        println!(
            "Creating ⚠️  partial copy ⚠️ . Will skip content of {} tables.",
            excluded_tables.len()
        );
        if args.protocol == ProtocolArg::Auto {
            println!();
            println!("Excluded tables were picked automatically. See --protocol in --help output.");
            println!();
            println!("Override automatic selection:");
            println!("    * Use `--protocol generic` to create a full DB copy.");
            if location.protocol != ProtocolChoice::NicePartialWithoutEmail {
                println!("    * Use `--protocol nice-partial-without-email` to also skip");
                println!("      email-related tables.");
            }
            if location.protocol != ProtocolChoice::NicePartialWithEmail {
                println!("    * Use `--protocol nice-partial-with-email` to additionally copy");
                println!("      email-related tables.");
            }
        }
        println!();
        println!("Use `--show-excluded-tables` to show skipped tables.");
    }
    if args.show_excluded_tables {
        println!();
        if excluded_tables.is_empty() {
            println!("Excluded tables: <none>");
        } else {
            println!("Excluded tables:");
            for table in excluded_tables {
                println!("    {table}");
            }
        }
    }

    proto
}

pub async fn main(root_args: &crate::Args, args: &Args) -> Result<()> {
    let config_loader = LoadConfig::new(root_args.update_repos_interval());
    let resolved_location = resolve_location(args, &config_loader).await?;
    if let Err(e) = check_if_args_conflict(args, &resolved_location) {
        e.exit();
    }
    let protocol = protocol(args, &resolved_location);

    if args.show_excluded_tables {
        println!();
        println!("--show-excluded-tables given, exiting without copying.");
        return Ok(());
    }

    let mut copier = Copier::new(resolved_location.source, resolved_location.target);
    copier.protocol(Box::new(protocol));
    copier.create_mode(args.create_mode);
    copier.owner(resolved_location.owner);
    if let Some(comment) = &args.comment {
        copier.comment(comment);
    }
    copier.callback(Box::new(CopyCallback {
        start: time::Instant::now(),
        quiet: args.quiet,
    }));
    copier.copy().await?;
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    #[cfg(feature = "test-with-repo-access")]
    use std::fmt;

    #[test]
    fn location_parsing_with_valid_locations() {
        {
            let expected_location = Location {
                host: Host::Installation("master".to_string()),
                db_name: None,
                owner: None,
            };
            let parsed_location = parse_location("master").unwrap();
            assert_eq!(expected_location, parsed_location);
        }
        {
            let expected_location = Location {
                host: Host::Installation("master".to_string()),
                db_name: Some("nice_master".to_string()),
                owner: None,
            };
            let parsed_location = parse_location("master/nice_master").unwrap();
            assert_eq!(expected_location, parsed_location);
        }
        {
            let expected_location = Location {
                host: Host::Installation("master".to_string()),
                db_name: None,
                owner: Some("peter".to_string()),
            };
            let parsed_location = parse_location("peter@master").unwrap();
            assert_eq!(expected_location, parsed_location);
        }
        {
            let expected_location = Location {
                host: Host::Installation("master".to_string()),
                db_name: Some("nice_master".to_string()),
                owner: Some("peter".to_string()),
            };
            let parsed_location = parse_location("peter@master/nice_master").unwrap();
            assert_eq!(expected_location, parsed_location);
        }
        {
            let expected_location = Location {
                host: Host::Remote("db1.stage.tocco.cust.vshn.net".to_string()),
                db_name: Some("nice_master".to_string()),
                owner: Some("peter".to_string()),
            };
            let parsed_location = parse_location("peter@db1.stage/nice_master").unwrap();
            assert_eq!(expected_location, parsed_location);
        }
    }

    #[test]
    fn location_parsing_with_invalid_locations() {
        assert_eq!(
            parse_location("master/nice_master/").unwrap_err(),
            "expected at most one '/'"
        );

        assert_eq!(
            parse_location("master@nice_master@").unwrap_err(),
            "expected at most one '@'"
        );

        assert_eq!(
            parse_location("user/host.name@database").unwrap_err(),
            "DB name cannot contain '@'"
        );
    }

    #[tokio::test]
    #[cfg(feature = "test-with-repo-access")]
    #[allow(clippy::too_many_lines)]
    async fn full_source_and_target_parsing() {
        // localhost SOURCE
        assert_eq!(
            parse_args(["db_copy", ""]).await.unwrap_err(),
            "when copying from a local SOURCE, a DB name is required"
        );
        assert_eq!(
            parse_args(["db_copy", "localhost"]).await.unwrap_err(),
            "when copying from a local SOURCE, a DB name is required"
        );
        assert_eq!(
            parse_args(["db_copy", "@/"]).await.unwrap_err(),
            "when copying from a local SOURCE, a DB name is required"
        );

        // source INSTALLATION
        assert_eq!(
            parse_args(["db_copy", "master"]).await.unwrap().location,
            ResolvedLocation {
                source: dump::Location::new_ssh("db5.stage.tocco.cust.vshn.net", "nice_master"),
                target: dump::Location::new_local("nice_master"),
                owner: Owner::FromTargetOrCurrentUser,
                protocol: ProtocolChoice::NicePartialWithoutEmail,
            }
        );
        assert_eq!(
            parse_args(["db_copy", "@master"]).await.unwrap().location,
            ResolvedLocation {
                source: dump::Location::new_ssh("db5.stage.tocco.cust.vshn.net", "nice_master"),
                target: dump::Location::new_local("nice_master"),
                owner: Owner::FromTargetOrCurrentUser,
                protocol: ProtocolChoice::NicePartialWithoutEmail,
            }
        );
        assert_eq!(
            parse_args(["db_copy", "master/"]).await.unwrap().location,
            ResolvedLocation {
                source: dump::Location::new_ssh("db5.stage.tocco.cust.vshn.net", "nice_master"),
                target: dump::Location::new_local("nice_master"),
                owner: Owner::FromTargetOrCurrentUser,
                protocol: ProtocolChoice::NicePartialWithoutEmail,
            }
        );
        assert_eq!(
            parse_args(["db_copy", "@master/"]).await.unwrap().location,
            ResolvedLocation {
                source: dump::Location::new_ssh("db5.stage.tocco.cust.vshn.net", "nice_master"),
                target: dump::Location::new_local("nice_master"),
                owner: Owner::FromTargetOrCurrentUser,
                protocol: ProtocolChoice::NicePartialWithoutEmail,
            }
        );
        assert_eq!(
            parse_args(["db_copy", "no-such-installation"])
                .await
                .unwrap_err(),
            "installation \"no-such-installation\" not found"
        );
        assert_eq!(
            parse_args(["db_copy", "no-such-installation/some_db"])
                .await
                .unwrap_err(),
            "installation \"no-such-installation\" not found"
        );

        // source DB_SERVER
        assert_eq!(
            parse_args(["db_copy", "db1.prod"]).await.unwrap_err(),
            "when a host is specified as SOURCE, a DB name is required"
        );
        assert_eq!(
            parse_args(["db_copy", "db1.prod.tocco.cust.vshn.net/"])
                .await
                .unwrap_err(),
            "when a host is specified as SOURCE, a DB name is required"
        );
        assert_eq!(
            parse_args(["db_copy", "db3/"]).await.unwrap_err(),
            "when a host is specified as SOURCE, a DB name is required"
        );
        assert_eq!(
            parse_args(["db_copy", "db4"]).await.unwrap_err(),
            "when a host is specified as SOURCE, a DB name is required"
        );

        // source INSTALLATION
        assert_eq!(
            parse_args(["db_copy", "master/alt_db"])
                .await
                .unwrap()
                .location,
            ResolvedLocation {
                source: dump::Location::new_ssh("db5.stage.tocco.cust.vshn.net", "alt_db"),
                target: dump::Location::new_local("alt_db"),
                owner: Owner::FromTargetOrCurrentUser,
                protocol: ProtocolChoice::NicePartialWithoutEmail,
            }
        );
        assert_eq!(
            parse_args(["db_copy", "@master/alt_db"])
                .await
                .unwrap()
                .location,
            ResolvedLocation {
                source: dump::Location::new_ssh("db5.stage.tocco.cust.vshn.net", "alt_db"),
                target: dump::Location::new_local("alt_db"),
                owner: Owner::FromTargetOrCurrentUser,
                protocol: ProtocolChoice::NicePartialWithoutEmail,
            }
        );
        assert_eq!(
            parse_args(["db_copy", "master/alt_db"])
                .await
                .unwrap()
                .location,
            ResolvedLocation {
                source: dump::Location::new_ssh("db5.stage.tocco.cust.vshn.net", "alt_db"),
                target: dump::Location::new_local("alt_db"),
                owner: Owner::FromTargetOrCurrentUser,
                protocol: ProtocolChoice::NicePartialWithoutEmail,
            }
        );

        // source OWNER
        assert_eq!(
            parse_args(["db_copy", "user-name@master"])
                .await
                .unwrap_err(),
            "no owner may be specified for SOURCE"
        );

        assert_eq!(
            parse_args(["db_copy", "user-name@master"])
                .await
                .unwrap_err(),
            "no owner may be specified for SOURCE"
        );

        // source DB_SERVER
        assert_eq!(
            parse_args(["db_copy", "db1.prod/alt_db"])
                .await
                .unwrap()
                .location,
            ResolvedLocation {
                source: dump::Location::new_ssh("db1.prod.tocco.cust.vshn.net", "alt_db"),
                target: dump::Location::new_local("alt_db"),
                owner: Owner::FromTargetOrCurrentUser,
                protocol: ProtocolChoice::NicePartialWithoutEmail,
            }
        );

        // localhost TARGET
        assert_eq!(
            parse_args(["db_copy", "master", ""])
                .await
                .unwrap()
                .location,
            ResolvedLocation {
                source: dump::Location::new_ssh("db5.stage.tocco.cust.vshn.net", "nice_master"),
                target: dump::Location::new_local("nice_master"),
                owner: Owner::FromTargetOrCurrentUser,
                protocol: ProtocolChoice::NicePartialWithoutEmail,
            }
        );
        assert_eq!(
            parse_args(["db_copy", "master", "/"])
                .await
                .unwrap()
                .location,
            ResolvedLocation {
                source: dump::Location::new_ssh("db5.stage.tocco.cust.vshn.net", "nice_master"),
                target: dump::Location::new_local("nice_master"),
                owner: Owner::FromTargetOrCurrentUser,
                protocol: ProtocolChoice::NicePartialWithoutEmail,
            }
        );
        assert_eq!(
            parse_args(["db_copy", "master", "/alt_db_name"])
                .await
                .unwrap()
                .location,
            ResolvedLocation {
                source: dump::Location::new_ssh("db5.stage.tocco.cust.vshn.net", "nice_master"),
                target: dump::Location::new_local("alt_db_name"),
                owner: Owner::FromTargetOrCurrentUser,
                protocol: ProtocolChoice::NicePartialWithoutEmail,
            }
        );
        assert_eq!(
            parse_args(["db_copy", "master", "alt_user@/alt_db_name"])
                .await
                .unwrap()
                .location,
            ResolvedLocation {
                source: dump::Location::new_ssh("db5.stage.tocco.cust.vshn.net", "nice_master"),
                target: dump::Location::new_local("alt_db_name"),
                owner: Owner::Name("alt_user".to_string()),
                protocol: ProtocolChoice::NicePartialWithoutEmail,
            }
        );
        assert_eq!(
            parse_args(["db_copy", "master", "alt_user@localhost/alt_db_name"])
                .await
                .unwrap()
                .location,
            ResolvedLocation {
                source: dump::Location::new_ssh("db5.stage.tocco.cust.vshn.net", "nice_master"),
                target: dump::Location::new_local("alt_db_name"),
                owner: Owner::Name("alt_user".to_string()),
                protocol: ProtocolChoice::NicePartialWithoutEmail,
            }
        );

        // INSTALLATION target
        assert_eq!(
            parse_args(["db_copy", "master", "tocco"])
                .await
                .unwrap()
                .location,
            ResolvedLocation {
                source: dump::Location::new_ssh("db5.stage.tocco.cust.vshn.net", "nice_master"),
                target: dump::Location::new_ssh("db7.prod.tocco.cust.vshn.net", "nice_tocco"),
                owner: Owner::FromTargetOr("nice_tocco".to_string()),
                protocol: ProtocolChoice::Generic,
            }
        );
        assert_eq!(
            parse_args(["db_copy", "master", "alt_user@tocco"])
                .await
                .unwrap()
                .location,
            ResolvedLocation {
                source: dump::Location::new_ssh("db5.stage.tocco.cust.vshn.net", "nice_master"),
                target: dump::Location::new_ssh("db7.prod.tocco.cust.vshn.net", "nice_tocco"),
                owner: Owner::Name("alt_user".to_string()),
                protocol: ProtocolChoice::Generic,
            }
        );
        assert_eq!(
            parse_args(["db_copy", "master", "tocco/alt_db"])
                .await
                .unwrap()
                .location,
            ResolvedLocation {
                source: dump::Location::new_ssh("db5.stage.tocco.cust.vshn.net", "nice_master"),
                target: dump::Location::new_ssh("db7.prod.tocco.cust.vshn.net", "alt_db"),
                owner: Owner::FromTargetOr("nice_tocco".to_string()),
                protocol: ProtocolChoice::Generic,
            }
        );

        // DB_SERVER target
        assert_eq!(
            parse_args(["db_copy", "master", "db7.prod"])
                .await
                .unwrap()
                .location,
            ResolvedLocation {
                source: dump::Location::new_ssh("db5.stage.tocco.cust.vshn.net", "nice_master"),
                target: dump::Location::new_ssh("db7.prod.tocco.cust.vshn.net", "nice_master"),
                owner: Owner::FromTarget,
                // Guessed based on DB name but master does not contain "test" in name. This is technically wrong.
                protocol: ProtocolChoice::Generic,
            }
        );

        // history DB
        assert_eq!(
            parse_args(["db_copy", "--type", "history", "master"])
                .await
                .unwrap()
                .location,
            ResolvedLocation {
                source: dump::Location::new_ssh(
                    "db5.stage.tocco.cust.vshn.net",
                    "nice_master_history"
                ),
                target: dump::Location::new_local("nice_master_history"),
                owner: Owner::FromTargetOrCurrentUser,
                protocol: ProtocolChoice::NicePartialWithoutEmail,
            }
        );
        assert_eq!(
            parse_args(["db_copy", "--type", "history", "master", "tocco"])
                .await
                .unwrap()
                .location,
            ResolvedLocation {
                source: dump::Location::new_ssh(
                    "db5.stage.tocco.cust.vshn.net",
                    "nice_master_history"
                ),
                target: dump::Location::new_ssh(
                    "db7.prod.tocco.cust.vshn.net",
                    "nice_tocco_history"
                ),
                owner: Owner::FromTargetOr("nice_tocco".to_string()),
                protocol: ProtocolChoice::Generic,
            }
        );
        assert_eq!(
            parse_args(["db_copy", "--type", "history", "master", "alt_user@tocco"])
                .await
                .unwrap()
                .location,
            ResolvedLocation {
                source: dump::Location::new_ssh(
                    "db5.stage.tocco.cust.vshn.net",
                    "nice_master_history"
                ),
                target: dump::Location::new_ssh(
                    "db7.prod.tocco.cust.vshn.net",
                    "nice_tocco_history"
                ),
                owner: Owner::Name("alt_user".to_string()),
                protocol: ProtocolChoice::Generic,
            }
        );
        assert_eq!(
            parse_args([
                "db_copy",
                "--type",
                "history",
                "master",
                "alt_user@toccotest/alt_db"
            ])
            .await
            .unwrap()
            .location,
            ResolvedLocation {
                source: dump::Location::new_ssh(
                    "db5.stage.tocco.cust.vshn.net",
                    "nice_master_history"
                ),
                target: dump::Location::new_ssh("db1.stage.tocco.cust.vshn.net", "alt_db"),
                owner: Owner::Name("alt_user".to_string()),
                protocol: ProtocolChoice::NicePartialWithEmail,
            }
        );

        // Protocol chosen based on DB name
        assert_eq!(
            parse_args([
                "db_copy",
                "--type",
                "history",
                "master",
                // no "test" in DB name
                "alt_user@db1.stage/nice_abc_history"
            ])
            .await
            .unwrap()
            .location,
            ResolvedLocation {
                source: dump::Location::new_ssh(
                    "db5.stage.tocco.cust.vshn.net",
                    "nice_master_history"
                ),
                target: dump::Location::new_ssh(
                    "db1.stage.tocco.cust.vshn.net",
                    "nice_abc_history"
                ),
                owner: Owner::Name("alt_user".to_string()),
                // Guessed based on target DB name, no installation name given.
                protocol: ProtocolChoice::Generic,
            }
        );
        assert_eq!(
            parse_args([
                "db_copy",
                "--type",
                "history",
                "master",
                // "test" in DB name
                "alt_user@db1.stage/nice_abctestnew_history"
            ])
            .await
            .unwrap()
            .location,
            ResolvedLocation {
                source: dump::Location::new_ssh(
                    "db5.stage.tocco.cust.vshn.net",
                    "nice_master_history"
                ),
                target: dump::Location::new_ssh(
                    "db1.stage.tocco.cust.vshn.net",
                    "nice_abctestnew_history"
                ),
                owner: Owner::Name("alt_user".to_string()),
                // Guessed based on target DB name, no installation name given.
                protocol: ProtocolChoice::NicePartialWithEmail,
            }
        );
    }

    #[tokio::test]
    #[cfg(feature = "test-with-repo-access")]
    async fn parse_with_backup_option() {
        {
            let args = parse_args(["db_copy", "--backup", "master"])
                .await
                .unwrap()
                .location;
            assert_eq!(
                args.source,
                dump::Location::new_ssh("db5.stage.tocco.cust.vshn.net", "nice_master")
            );
            assert!(args.target.db_name.starts_with("nice_master_20"));
            assert!(
                args.target
                    .host_name()
                    .unwrap()
                    .starts_with("db5.stage.tocco.cust.vshn.net")
            );
            assert_eq!(args.owner, Owner::FromTargetOrCurrentUser);
        }
        {
            let args = parse_args(["db_copy", "--backup", "--type", "history", "master"])
                .await
                .unwrap()
                .location;
            assert_eq!(
                args.source,
                dump::Location::new_ssh("db5.stage.tocco.cust.vshn.net", "nice_master_history")
            );
            assert!(args.target.db_name.starts_with("nice_master_history_20"));
            assert!(
                args.target
                    .host_name()
                    .unwrap()
                    .starts_with("db5.stage.tocco.cust.vshn.net")
            );
            assert_eq!(args.owner, Owner::FromTargetOrCurrentUser);
        }

        assert_eq!(
            Args::try_parse_from([
                "db_copy", "--backup", "master", "tocco", // TARGET conflicts
            ])
            .err()
            .unwrap()
            .kind(),
            clap::error::ErrorKind::ArgumentConflict,
        );
        assert_eq!(
            Args::try_parse_from([
                "db_copy",
                "--backup",
                "--owner-mode",
                "from-target", // conflicts
                "master",
            ])
            .err()
            .unwrap()
            .kind(),
            clap::error::ErrorKind::ArgumentConflict,
        );
    }

    #[tokio::test]
    #[cfg(feature = "test-with-repo-access")]
    async fn parse_with_backup_with_suffix_option() {
        assert_eq!(
            parse_args([
                "db_copy",
                "--backup-with-suffix",
                "right_hand_side_affix",
                "master"
            ])
            .await
            .unwrap()
            .location,
            ResolvedLocation {
                source: dump::Location::new_ssh("db5.stage.tocco.cust.vshn.net", "nice_master"),
                target: dump::Location::new_ssh(
                    "db5.stage.tocco.cust.vshn.net",
                    "nice_master_right_hand_side_affix"
                ),
                owner: Owner::FromTargetOrCurrentUser,
                // Always creating a full, Generic copy with --backup-with-suffix.
                protocol: ProtocolChoice::Generic,
            }
        );
        assert_eq!(
            Args::try_parse_from([
                "db_copy",
                "--backup-with-suffix",
                "suffix",
                "master",
                "tocco", // TARGET conflicts
            ])
            .err()
            .unwrap()
            .kind(),
            clap::error::ErrorKind::ArgumentConflict,
        );
        assert_eq!(
            Args::try_parse_from([
                "db_copy",
                "--backup-with-suffix",
                "suffix",
                "--backup", // conflicts
                "master",
            ])
            .err()
            .unwrap()
            .kind(),
            clap::error::ErrorKind::ArgumentConflict,
        );
        assert_eq!(
            Args::try_parse_from([
                "db_copy",
                "--backup-with-suffix",
                "suffix",
                "--owner-mode",
                "auto", // conflicts
                "master",
            ])
            .err()
            .unwrap()
            .kind(),
            clap::error::ErrorKind::ArgumentConflict,
        );
    }

    #[tokio::test]
    async fn expand_to_fqdn() {
        let data = [
            ("db1.prod", "db1.prod.tocco.cust.vshn.net"),
            ("db3.stage", "db3.stage.tocco.cust.vshn.net"),
            ("db3", "db3.tocco.cust.vshn.net"),
            ("db4", "db4.tocco.cust.vshn.net"),
        ];
        for (input, output) in data {
            let expected_location = Location {
                host: Host::Remote(output.to_string()),
                db_name: Some("nice_master".to_string()),
                owner: Some("peter".to_string()),
            };
            let parsed_location = parse_location(&format!("peter@{input}/nice_master")).unwrap();
            assert_eq!(expected_location, parsed_location);
        }
    }

    #[tokio::test]
    async fn do_not_expand_to_fqdn() {
        let data = [
            "db1.prod.tocco.cust.vshn.net",
            "db3.tocco.cust.vshn.net",
            "example.net",
        ];
        for host in data {
            let expected_location = Location {
                host: Host::Remote(host.to_string()),
                db_name: Some("nice_master".to_string()),
                owner: Some("peter".to_string()),
            };
            let parsed_location = parse_location(&format!("peter@{host}/nice_master")).unwrap();
            assert_eq!(expected_location, parsed_location);
        }
    }

    #[tokio::test]
    #[cfg(feature = "test-with-repo-access")]
    async fn check_if_args_conflict() {
        use test_utils::{LockMode, lock_caches};

        let _guard = lock_caches(LockMode::Shared).await;
        let config_loader = LoadConfig::new(Some(Duration::from_secs(86400)));

        // Target is localhost
        let ok = [
            &["db_copy", "--create-mode", "replace", "master"] as &[_],
            &["db_copy", "--create-mode", "replace", "master", "localhost"],
            &[
                "db_copy",
                "--create-mode",
                "replace",
                "master",
                "localhost/db_name",
            ],
            &["db_copy", "--create-mode", "replace", "master", "/db_name"],
        ];
        for args in ok {
            assert_replace_does_not_conflict(&config_loader, args).await;
        }

        // Target is not localhost
        let not_ok = [
            &[
                "db_copy",
                "--create-mode",
                "replace",
                "/nice_master",
                "master",
            ] as &[_],
            &["db_copy", "--create-mode", "replace", "master", "db1.prod"] as &[_],
            &[
                "db_copy",
                "--create-mode",
                "replace",
                "master",
                "db1.prod/db_name",
            ],
        ];
        for args in not_ok {
            assert_replace_conflicts(&config_loader, args).await;
        }
    }

    #[tokio::test]
    #[cfg(feature = "test-with-repo-access")]
    async fn excluded_tables_based_on_protocol() {
        async fn excluded_tables_for_protocol(selected_proto: &str) -> Vec<String> {
            let parsed_args = parse_args(["db-copy", "--protocol", selected_proto, "master"])
                .await
                .unwrap();
            protocol(&parsed_args.args, &parsed_args.location)
                .excluded_tables()
                .to_vec()
        }
        let generic_table = "nice_log_entry".to_string();
        let email_table = "nice_email_archive".to_string();

        assert!(excluded_tables_for_protocol("generic").await.is_empty());
        {
            let tables = excluded_tables_for_protocol("nice-partial-with-email").await;
            assert!(tables.contains(&generic_table));
            assert!(!tables.contains(&email_table));
        }
        {
            let tables = excluded_tables_for_protocol("nice-partial-without-email").await;
            assert!(tables.contains(&generic_table));
            assert!(tables.contains(&email_table));
        }
    }

    #[tokio::test]
    #[cfg(feature = "test-with-repo-access")]
    async fn never_auto_select_tainted_protocol_for_prod_target() {
        use tco::sql::dump::Protocol;

        let various_sources = ["/nice_master", "bbg/nice_abc", "agogistest", "bbg"];
        let prod_target = "bbg";
        for source in various_sources {
            let args = ["db-copy", "--protocol", "auto", source, prod_target];
            let args = Args::try_parse_from(args).unwrap();
            let config_loader = LoadConfig::new(None);
            let location = resolve_location(&args, &config_loader).await.unwrap();
            let protocol = protocol(&args, &location);
            assert!(!protocol.is_tainted());
        }
    }

    #[cfg(feature = "test-with-repo-access")]
    async fn assert_replace_does_not_conflict(config_loader: &LoadConfig, args: &[&str]) {
        let args = Args::try_parse_from(args).unwrap();
        let resolved_location = resolve_location(&args, config_loader).await.unwrap();
        assert!(super::check_if_args_conflict(&args, &resolved_location).is_ok());
    }

    #[cfg(feature = "test-with-repo-access")]
    async fn assert_replace_conflicts(config_loader: &LoadConfig, args: &[&str]) {
        let args = Args::try_parse_from(args).unwrap();
        let resolved_location = resolve_location(&args, config_loader).await.unwrap();
        let err = super::check_if_args_conflict(&args, &resolved_location).unwrap_err();
        assert_eq!(err.kind(), clap::error::ErrorKind::ArgumentConflict);
        assert!(
            err.to_string()
                .contains("`--create-mode replace` only allowed when target is localhost")
        );
    }

    #[cfg(feature = "test-with-repo-access")]
    struct ParsedArgs {
        args: Args,
        raw_args: Vec<String>,
        location: ResolvedLocation,
    }

    #[cfg(feature = "test-with-repo-access")]
    impl fmt::Debug for ParsedArgs {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            f.debug_struct("ParsedArgs")
                .field("args", &"...")
                .field("raw_args", &self.raw_args)
                .field("location", &self.location)
                .finish()
        }
    }

    #[cfg(feature = "test-with-repo-access")]
    async fn parse_args<I, T>(args: I) -> StdResult<ParsedArgs, String>
    where
        I: IntoIterator<Item = T>,
        T: Into<String> + Clone,
    {
        let raw_args = args.into_iter().map(|v| v.into()).collect();
        let args = Args::try_parse_from(&raw_args).map_err(|e| format!("{e}"))?;
        let config_loader = LoadConfig::new(None);
        let location = resolve_location(&args, &config_loader)
            .await
            .map_err(|e| format!("{e}"))?;
        Ok(ParsedArgs {
            args,
            raw_args,
            location,
        })
    }
}
