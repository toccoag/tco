//! Interaction with Ansible and *ansible* repository

pub mod config;
pub mod config_quick;
pub mod git;
