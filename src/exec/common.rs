use crate::utils;
use crate::utils::log::DisplayCommand;
use std::io;
use std::process::Stdio;
use tokio::process;

pub(crate) fn bytes_to_string(bytes: Vec<u8>) -> io::Result<String> {
    match String::from_utf8(bytes) {
        Ok(string) => Ok(string),
        Err(_) => Err(io::Error::new(
            io::ErrorKind::InvalidData,
            "failed to parse output as UTF-8",
        )),
    }
}

pub(crate) async fn execute(
    mut command: process::Command,
    trim_final_newline: bool,
) -> io::Result<Vec<u8>> {
    debug!("Executing command: {}", DisplayCommand(command.as_std()));
    let mut output = command
        .stdout(Stdio::piped())
        .spawn()?
        .wait_with_output()
        .await?;

    utils::log::vpn_needed_info_message_on_ssh_timeout(output.status);
    if !output.status.success() {
        return Err(io::Error::other("command returned with an error"));
    }
    if trim_final_newline
        && output
            .stdout
            .last()
            .map(|c| *c as char == '\n')
            .unwrap_or(false)
    {
        output.stdout.pop();
    }
    Ok(output.stdout)
}
