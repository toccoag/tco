//! Run command on remote server via SSH.
//!
//! # Examples
//!
//! **Collect output from remotely executed command:**
//!
//! ```
//! # #[tokio::main]
//! # async fn main() {
//! # #[cfg(feature = "test-with-secrets")] {
//! use crate::tco::exec::Command;
//! use crate::tco::exec::CommandExt;
//!
//! let server = "db1.stage.tocco.cust.vshn.net";
//! let command = "hostname";
//! let args = ["--fqdn"];
//! let output = tco::exec::ssh::Command::new(&server)
//!     .trim_final_newline(true)
//!     .output_string(command, args)
//!     .await
//!     .unwrap();
//!
//! assert_eq!(&output, server)
//! # }
//! # }
//! ```
//!
//! An `Error` is returned when the command exits with a non-zero exit code.
//!
//! **Execute command in shell:**
//!
//! ```
//! # #[tokio::main]
//! # async fn main() {
//! # #[cfg(feature = "test-with-secrets")] {
//! use crate::tco::exec::Command;
//!
//! let server = "db1.stage.tocco.cust.vshn.net";
//! let shell_script = "echo $$";
//! let output = tco::exec::ssh::Command::new(&server)
//!     .trim_final_newline(true)
//!     .output_string_shell(&shell_script)
//!     .await
//!     .unwrap();
//!
//! assert!(output.parse::<u64>().is_ok());
//! # }
//! # }
//! ```
//!
//! An `Error` is returned when the command exits with a non-zero exit code.
//!
//! **Execute interactive command:**
//!
//! ```no_run
//! # #[tokio::main]
//! # async fn main() {
//! use crate::tco::exec::ssh::Terminal;
//! use crate::tco::exec::{Command, CommandExt};
//!
//! let server = "db1.stage.tocco.cust.vshn.net";
//! let command = "top";
//! let status = tco::exec::ssh::Command::new(&server)
//!     .stdin(true) // allow keyboard input
//!     .terminal(Terminal::Auto) // allocate TTY / terminal if stdin is one
//!     .spawn(&command, [] as [&str; 0])
//!     .unwrap()
//!     .wait() // wait for comand to exit
//!     .await
//!     .unwrap();
//!
//! assert!(status.success());
//! # }
//! ```
use crate::exec;
use crate::exec::common;
use crate::utils;
use crate::utils::log::DisplayCommand;
use async_trait::async_trait;
use std::fs::File;
use std::io;
use std::io::IsTerminal;
use std::path::Path;
use std::process::Stdio;
use tokio::process;

/// Whether to request a terminal
#[derive(Clone, Copy)]
pub enum Terminal {
    /// Request terminal if stdin is connected to one.
    Auto,

    /// Unconditionally request a terminal.
    Always,

    /// Never request a terminal.
    Never,
}

/// Build command to execute remotely via SSH.
///
/// See also [module-level](`self`) documentation.
#[derive(Clone)]
pub struct Command {
    ssh_uri: String,
    terminal: Terminal,
    stdin: bool,
    trim_final_newline: bool,
    envs: Vec<(String, String)>,
}

impl Command {
    /// New command
    ///
    /// `command` is shell-quoted as needed before sending
    /// it over ssh.
    ///
    /// See also [module-level](`self`) documentation.
    #[must_use]
    pub fn new(ssh_uri: &str) -> Self {
        Command {
            terminal: Terminal::Never,
            ssh_uri: ssh_uri.to_string(),
            stdin: false,
            trim_final_newline: false,
            envs: Vec::new(),
        }
    }

    /// Whether to allocate a TTY
    ///
    /// Defaults to [`Terminal::Auto`].
    ///
    /// See example on [module-level](`self`).
    pub fn terminal(&mut self, value: Terminal) -> &mut Self {
        self.terminal = value;
        self
    }

    /// Whether to enable input from stdin.
    ///
    /// Defaults to `false`.
    pub fn stdin(&mut self, value: bool) -> &mut Self {
        self.stdin = value;
        self
    }

    /// Wheter to remove final '\n' from output collected from stdout.
    ///
    /// Only applicable for [`Command::output()`](`crate::exec::CommandExt::output()`)
    /// and [`Command::output_string()`](`crate::exec::CommandExt::output_string()`).
    ///
    /// Defaults to `false`.
    pub fn trim_final_newline(&mut self, value: bool) -> &mut Self {
        self.trim_final_newline = value;
        self
    }

    pub fn env(&mut self, key: &str, value: &str) -> &mut Command {
        assert!(!key.is_empty(), "empty env. var. key");
        assert!(!key.contains('='), "env. var. key contains '=' character");
        assert!(!key.contains('\0'), "env. var. key contains NUL character");
        assert!(
            !value.contains('\0'),
            "env. var. value contains NUL character"
        );

        self.envs.push((key.to_owned(), value.to_owned()));
        self
    }

    fn basic_command(&self, force_interactive: bool) -> process::Command {
        let mut cmd = process::Command::new("ssh");
        cmd.kill_on_drop(true).arg("-o").arg("BatchMode=yes");
        let terminal_arg = match self.terminal {
            Terminal::Auto if io::stdin().is_terminal() => "-t",
            Terminal::Auto | Terminal::Never => "-T",
            Terminal::Always => "-tt",
        };
        cmd.arg(terminal_arg);
        if !self.stdin && !force_interactive {
            cmd.arg("-n");
        }
        if let Some(arg) = utils::ssh_verbosity_arg() {
            cmd.arg(arg);
        }
        cmd.arg(&self.ssh_uri);
        for (key, value) in &self.envs {
            let key = shlex::try_quote(key).expect("NUL character in command");
            let value = shlex::try_quote(value).expect("NUL character in command");
            cmd.arg(format!("{key}={value}"));
        }
        cmd
    }

    fn shell_command(&self, shell_command: &str) -> process::Command {
        let mut cmd = self.basic_command(false);
        cmd.arg(shell_command);
        cmd
    }

    fn command<I, A>(&self, command: &str, args: I) -> process::Command
    where
        I: IntoIterator<Item = A>,
        A: AsRef<str>,
    {
        let mut shell_command = shlex::try_quote(command)
            .expect("NUL character in command")
            .into_owned();
        for arg in args {
            shell_command.push(' ');
            shell_command.push_str(
                shlex::try_quote(arg.as_ref())
                    .expect("NUL character in argument")
                    .as_ref(),
            );
        }
        self.shell_command(&shell_command)
    }

    /// Open interactive shell
    ///
    /// Calls `ssh $host` without a command to execute.
    pub fn spawn_terminal(&self) -> io::Result<process::Child> {
        let mut cmd = self.basic_command(true);
        cmd.arg(r#"exec "$SHELL""#);
        debug!("Executing command: {}", DisplayCommand(cmd.as_std()));
        cmd.spawn()
    }
}

#[async_trait]
impl exec::Command for Command {
    async fn output_shell(&self, shell_command: &str) -> io::Result<Vec<u8>> {
        exec::CommandExt::output(self, "bash", ["-c", shell_command]).await
    }

    async fn output_string_shell(&self, shell_command: &str) -> io::Result<String> {
        exec::CommandExt::output_string(self, "bash", ["-c", shell_command]).await
    }

    fn spawn_shell(&mut self, shell_command: &str) -> io::Result<process::Child> {
        exec::CommandExt::spawn(self, "bash", ["-c", shell_command])
    }

    fn spawn_shell_stdin(&mut self, shell_command: &str) -> io::Result<process::Child> {
        exec::CommandExt::spawn_stdin(self, "bash", ["-c", shell_command])
    }

    fn spawn_shell_stdout(&mut self, shell_command: &str) -> io::Result<process::Child> {
        exec::CommandExt::spawn_stdout(self, "bash", ["-c", shell_command])
    }
}

#[async_trait]
impl exec::CommandExt for Command {
    async fn output<I, A>(&self, command: &str, args: I) -> io::Result<Vec<u8>>
    where
        I: IntoIterator<Item = A> + Send,
        A: AsRef<str>,
    {
        common::execute(self.command(command, args), self.trim_final_newline).await
    }

    async fn output_string<I, A>(&self, command: &str, args: I) -> io::Result<String>
    where
        I: IntoIterator<Item = A> + Send,
        A: AsRef<str>,
    {
        common::bytes_to_string(self.output(command, args).await?)
    }

    fn spawn<I, A>(&mut self, command: &str, args: I) -> io::Result<process::Child>
    where
        I: IntoIterator<Item = A> + Send,
        A: AsRef<str>,
    {
        let mut command = self.command(command, args);
        debug!("Executing command: {}", DisplayCommand(command.as_std()));
        command.spawn()
    }

    fn spawn_stdin<I, A>(&mut self, command: &str, args: I) -> io::Result<process::Child>
    where
        I: IntoIterator<Item = A> + Send,
        A: AsRef<str>,
    {
        let mut command = self.command(command, args);
        command.stdin(Stdio::piped());
        debug!("Executing command: {}", DisplayCommand(command.as_std()));
        command.spawn()
    }

    fn spawn_stdin_file<I, A, P>(
        &mut self,
        command: &str,
        args: I,
        path: P,
    ) -> io::Result<process::Child>
    where
        I: IntoIterator<Item = A> + Send,
        A: AsRef<str>,
        P: AsRef<Path>,
    {
        let path = File::open(&path)?;
        let mut command = self.command(command, args);
        command.stdin(path);
        debug!("Executing command: {}", DisplayCommand(command.as_std()));
        command.spawn()
    }

    fn spawn_stdout<I, A>(&mut self, command: &str, args: I) -> io::Result<process::Child>
    where
        I: IntoIterator<Item = A> + Send,
        A: AsRef<str>,
    {
        let mut command = self.command(command, args);
        command.stdout(Stdio::piped());
        debug!("Executing command: {}", DisplayCommand(command.as_std()));
        command.spawn()
    }
}

#[cfg(test)]
#[cfg(feature = "test-with-secrets")]
mod tests {
    use crate::exec::Command as _;
    use crate::exec::ssh::Command;

    #[tokio::test]
    async fn env() {
        let output = Command::new("db1.stage.tocco.cust.vshn.net")
            .trim_final_newline(true)
            .env("ENV1", "1")
            .env("ENV2", "with space")
            .output_string_shell(r#"echo "$ENV1,$ENV2""#)
            .await
            .unwrap();
        assert_eq!(output, "1,with space");
    }
}
