//! Run command on local machine.
//!
//! # Examples
//!
//! **Collect output from executed command:**
//!
//! ```
//! # #[tokio::main]
//! # async fn main() {
//! # #[cfg(target_os = "linux")] {
//! use crate::tco::exec::Command;
//! use crate::tco::exec::CommandExt;
//!
//! let command = "uname";
//! let args = ["-s"];
//! let output = tco::exec::local::Command::new()
//!     .trim_final_newline(true)
//!     .output_string(command, args)
//!     .await
//!     .unwrap();
//!
//! assert_eq!(&output, "Linux");
//! # }
//! # }
//! ```
//!
//! An `Error` is returned when the command exits with a non-zero exit code.
//!
//! **Execute command in shell:**
//!
//! ```
//! # #[tokio::main]
//! # async fn main() {
//! use crate::tco::exec::Command;
//!
//! let shell_script = "echo $$";
//! let output = tco::exec::local::Command::new()
//!     .trim_final_newline(true)
//!     .output_string_shell(&shell_script)
//!     .await
//!     .unwrap();
//!
//! assert!(output.parse::<u64>().is_ok());
//! # }
//! ```
//!
//! An `Error` is returned when the command exits with a non-zero exit code.
//!
//! **Execute interactive command:**
//!
//! ```no_run
//! # #[tokio::main]
//! # async fn main() {
//! use crate::tco::exec::Command;
//! use crate::tco::exec::CommandExt;
//!
//! let command = "top";
//! let status = tco::exec::local::Command::new()
//!     .spawn(&command, [] as [&str; 0])
//!     .unwrap()
//!     .wait() // wait for comand to exit
//!     .await
//!     .unwrap();
//!
//! assert!(status.success());
//! # }
//! ```
use crate::exec;
use crate::exec::common;
use crate::utils::log::DisplayCommand;
use async_trait::async_trait;
use std::fs::File;
use std::io;
use std::path::Path;
use std::process::Stdio;
use tokio::process;

/// Build command to execute locally.
///
/// See also [module-level](`self`) documentation.
#[derive(Clone)]
pub struct Command {
    trim_final_newline: bool,
}

impl Command {
    /// New command
    ///
    /// See also [module-level](`self`) documentation.
    #[must_use]
    pub fn new() -> Self {
        Command {
            trim_final_newline: false,
        }
    }

    /// Wheter to remove final '\n' from output collected from stdout.
    ///
    /// Only applicable for [`Command::output()`](`crate::exec::CommandExt::output()`)
    /// and [`Command::output_string()`](`crate::exec::CommandExt::output_string()`).
    ///
    /// Defaults to `false`.
    pub fn trim_final_newline(&mut self, value: bool) -> &mut Self {
        self.trim_final_newline = value;
        self
    }

    #[allow(clippy::unused_self)] // this is consistent with self::ssh
    fn command<I, A>(&self, command: &str, args: I) -> process::Command
    where
        I: IntoIterator<Item = A>,
        A: AsRef<str>,
    {
        let mut cmd = process::Command::new(command);
        for arg in args {
            cmd.arg(arg.as_ref());
        }
        cmd
    }
}

#[async_trait]
impl exec::Command for Command {
    async fn output_shell(&self, shell_command: &str) -> io::Result<Vec<u8>> {
        exec::CommandExt::output(self, "bash", ["-c", shell_command]).await
    }

    async fn output_string_shell(&self, shell_command: &str) -> io::Result<String> {
        exec::CommandExt::output_string(self, "bash", ["-c", shell_command]).await
    }

    fn spawn_shell(&mut self, shell_command: &str) -> io::Result<process::Child> {
        exec::CommandExt::spawn(self, "bash", ["-c", shell_command])
    }

    fn spawn_shell_stdin(&mut self, shell_command: &str) -> io::Result<process::Child> {
        exec::CommandExt::spawn_stdin(self, "bash", ["-c", shell_command])
    }

    fn spawn_shell_stdout(&mut self, shell_command: &str) -> io::Result<process::Child> {
        exec::CommandExt::spawn_stdout(self, "bash", ["-c", shell_command])
    }
}

#[async_trait]
impl exec::CommandExt for Command {
    async fn output<I, A>(&self, command: &str, args: I) -> io::Result<Vec<u8>>
    where
        I: IntoIterator<Item = A> + Send,
        A: AsRef<str>,
    {
        common::execute(self.command(command, args), self.trim_final_newline).await
    }

    async fn output_string<I, A>(&self, command: &str, args: I) -> io::Result<String>
    where
        I: IntoIterator<Item = A> + Send,
        A: AsRef<str>,
    {
        common::bytes_to_string(self.output(command, args).await?)
    }

    fn spawn<I, A>(&mut self, command: &str, args: I) -> io::Result<process::Child>
    where
        I: IntoIterator<Item = A> + Send,
        A: AsRef<str>,
    {
        let mut command = self.command(command, args);
        debug!("Executing command: {}", DisplayCommand(command.as_std()));
        command.spawn()
    }

    fn spawn_stdin<I, A>(&mut self, command: &str, args: I) -> io::Result<process::Child>
    where
        I: IntoIterator<Item = A> + Send,
        A: AsRef<str>,
    {
        let mut command = self.command(command, args);
        command.stdin(Stdio::piped());
        debug!("Executing command: {}", DisplayCommand(command.as_std()));
        command.spawn()
    }

    fn spawn_stdin_file<I, A, P>(
        &mut self,
        command: &str,
        args: I,
        path: P,
    ) -> io::Result<process::Child>
    where
        I: IntoIterator<Item = A> + Send,
        A: AsRef<str>,
        P: AsRef<Path>,
    {
        let path = File::open(&path)?;
        let mut command = self.command(command, args);
        command.stdin(path);
        debug!("Executing command: {}", DisplayCommand(command.as_std()));
        command.spawn()
    }

    fn spawn_stdout<I, A>(&mut self, command: &str, args: I) -> io::Result<process::Child>
    where
        I: IntoIterator<Item = A> + Send,
        A: AsRef<str>,
    {
        let mut command = self.command(command, args);
        command.stdout(Stdio::piped());
        debug!("Executing command: {}", DisplayCommand(command.as_std()));
        command.spawn()
    }
}
