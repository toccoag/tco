//! Fetch index stats from Cluster
//!
//! This uses the [/_cat/indices] API.
//!
//! [/_cat/indices]: https://www.elastic.co/guide/en/elasticsearch/reference/current/cat-indices.html

use crate::Result;
use crate::http_client;
use crate::utils::deser::parse_str;
use serde::Deserialize;
use std::collections::HashMap;
use std::fmt;

/// Elasticsearch index stats
#[derive(Deserialize)]
pub struct IndexStats {
    pub health: Health,

    #[serde(rename = "docs.count", deserialize_with = "parse_str")]
    pub docs: u64,

    #[serde(rename = "docs.deleted", deserialize_with = "parse_str")]
    pub docs_deleted: u64,

    #[serde(rename = "index")]
    pub name: String,

    #[serde(rename = "pri", deserialize_with = "parse_str")]
    pub primaries: u32,

    #[serde(rename = "rep", deserialize_with = "parse_str")]
    pub replicas: u32,

    pub status: Status,

    /// Total size in bytes
    ///
    /// This includes all replicas.
    #[serde(rename = "store.size", deserialize_with = "parse_str")]
    pub total_size: u64,

    /// Primary size in bytes
    ///
    /// Size of primary only, excluding replicas.
    #[serde(rename = "pri.store.size", deserialize_with = "parse_str")]
    pub primary_size: u64,
}

/// Index health
#[derive(Clone, Copy, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum Health {
    Green,
    Yellow,
    Red,
}

impl Health {
    /// Style for terminal output (e.g. colored)
    #[must_use]
    pub fn term_styled(self) -> Box<dyn fmt::Display> {
        use owo_colors::OwoColorize as _;

        match self {
            Health::Green => Box::new("green".green()),
            Health::Yellow => Box::new("yellow".yellow()),
            Health::Red => Box::new("red".red()),
        }
    }
}

impl fmt::Display for Health {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let text = match self {
            Health::Green => "green",
            Health::Yellow => "yellow",
            Health::Red => "red",
        };
        fmt::Display::fmt(text, f)
    }
}

/// Index status
#[derive(Clone, Copy, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum Status {
    Open,
    Close,
}

impl Status {
    /// Style for terminal output (e.g. colored)
    #[must_use]
    pub fn term_styled(self) -> Box<dyn fmt::Display> {
        use owo_colors::OwoColorize as _;

        match self {
            Status::Open => Box::new("open".green()),
            Status::Close => Box::new("close".red()),
        }
    }
}

/// Load stats for all indexes on cluster
///
/// *Key* is the index name.
pub async fn load_all(
    user: &str,
    password: &str,
    host_name: &str,
) -> Result<HashMap<String, IndexStats>> {
    let endpoint = format!("https://{host_name}/_cat/indices?format=json&bytes=b");
    let result = http_client()
        .get(endpoint)
        .basic_auth(user, Some(password))
        .send()
        .await?
        .error_for_status()?
        .json()
        .await;
    let indexes: Vec<IndexStats> = match result {
        Ok(indexes) => indexes,
        Err(e) if e.is_timeout() => {
            warn!(
                "A connection to a Elasticsearch cluster timed out. This could mean that you are \
                 sending the request from an IP not whitelisted. Try sending the request from the \
                 office network. See also `tco elasticsearch --help`."
            );
            return Err(e.into());
        }
        Err(e) => return Err(e.into()),
    };
    Ok(indexes.into_iter().map(|i| (i.name.clone(), i)).collect())
}

#[cfg(test)]
#[cfg(all(feature = "test-with-secrets", feature = "test-with-ip-restriction"))]
mod tests {
    use crate::ansible;
    use test_utils::{LockMode, lock_caches};

    #[tokio::test]
    async fn load_all() {
        let config = {
            let _guard = lock_caches(LockMode::Shared).await;
            let repo = ansible::git::repo().await.unwrap();
            ansible::config::load(&repo).await.unwrap()
        };
        {
            let installation = &config.installations["tocco"];

            assert!(installation.elasticsearch_servers.len() >= 3);
            let host_name = &installation.elasticsearch_servers[0];
            assert!(host_name.ends_with(".prod.tocco.cust.vshn.net"));
            let indexes = super::load_all(
                "admin",
                &installation.elasticsearch_admin_password,
                host_name,
            )
            .await
            .unwrap();

            assert!(indexes.len() > 50);
            let index = &indexes["nice-tocco"];
            assert_eq!(index.name, "nice-tocco");
            assert!(index.docs > 10_000);
            assert!(index.replicas > 0);
            assert!(index.total_size > 100_000_000);
            assert!(index.total_size > index.primary_size);
        }

        {
            let installation = &config.installations["toccotest"];
            let host_name = &installation.elasticsearch_servers[0];
            assert!(host_name.ends_with(".stage.tocco.cust.vshn.net"));
        }
    }
}
