//! Execution of commands via OS processes

mod common;
pub mod local;
pub mod ssh;

use async_trait::async_trait;
use std::io;
use std::path::Path;
use tokio::process;

/// Execute a shell script
#[async_trait]
pub trait Command {
    /// Retrieve output of `shell_script
    ///
    /// `shell_script` is executed in Bash.
    async fn output_shell(&self, shell_script: &str) -> io::Result<Vec<u8>>;

    /// Retrieve output of `shell_script` as `String`
    async fn output_string_shell(&self, shell_script: &str) -> io::Result<String>;

    /// Spawn process running `shell_script`
    ///
    /// `shell_script` is executed in Bash.
    fn spawn_shell(&mut self, shell_script: &str) -> io::Result<process::Child>;

    /// Spawn process running `shell_script` and allow piping to stdin
    ///
    /// `shell_script` is executed in Bash.
    ///
    /// Write to [`process::Child::stdin`].
    fn spawn_shell_stdin(&mut self, shell_script: &str) -> io::Result<process::Child>;

    /// Spawn process running `shell_script` and allow piping from stdout
    ///
    /// `shell_script` is executed in Bash.
    ///
    /// Write to [`process::Child::stdout`]]
    fn spawn_shell_stdout(&mut self, shell_script: &str) -> io::Result<process::Child>;
}

/// Execute a command (a binary)
#[async_trait]
pub trait CommandExt {
    /// Retrieve output of command
    ///
    /// # Panics
    ///
    /// Panics if `command` or `args` contains a NUL character.
    async fn output<I, A>(&self, command: &str, args: I) -> io::Result<Vec<u8>>
    where
        I: IntoIterator<Item = A> + Send,
        A: AsRef<str>;

    /// Retrieve output of `command` as `String`
    ///
    /// # Panics
    ///
    /// Panics if `command` or `args` contains a NUL character.
    async fn output_string<I, A>(&self, command: &str, args: I) -> io::Result<String>
    where
        I: IntoIterator<Item = A> + Send,
        A: AsRef<str>;

    /// Spawn process running `command`
    ///
    /// # Panics
    ///
    /// Panics if `command` or `args` contains a NUL character.
    fn spawn<I, A>(&mut self, command: &str, args: I) -> io::Result<process::Child>
    where
        I: IntoIterator<Item = A> + Send,
        A: AsRef<str>;

    /// Spawn process running `command` and allow piping to stdin
    ///
    /// Write to [`process::Child::stdin`].
    ///
    /// # Panics
    ///
    /// Panics if `command` or `args` contains a NUL character.
    fn spawn_stdin<I, A>(&mut self, command: &str, args: I) -> io::Result<process::Child>
    where
        I: IntoIterator<Item = A> + Send,
        A: AsRef<str>;

    /// Spawn process running `command` connecting stdin to `path`.
    ///
    /// # Panics
    ///
    /// Panics if `command` or `args` contains a NUL character.
    fn spawn_stdin_file<I, A, P>(
        &mut self,
        command: &str,
        args: I,
        path: P,
    ) -> io::Result<process::Child>
    where
        I: IntoIterator<Item = A> + Send,
        A: AsRef<str>,
        P: AsRef<Path>;

    /// Spawn process running `command` and allow piping from stdout
    ///
    /// Write to [`process::Child::stdout`]]
    ///
    /// # Panics
    ///
    /// Panics if `command` or `args` contains a NUL character.
    fn spawn_stdout<I, A>(&mut self, command: &str, args: I) -> io::Result<process::Child>
    where
        I: IntoIterator<Item = A> + Send,
        A: AsRef<str>;
}
