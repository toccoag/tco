//! Manage [tocco-client][] Git repository
//!
//! See also documentation of [git](`crate::git`).
//!
//! [tocco-client]: https://gitlab.com/toccoag/tocco-client

use crate::Result;
use crate::git::{self, RepoBuilder, Repository};
use git2::Oid;
use std::path::PathBuf;
use std::sync::LazyLock;
use std::time::Duration;

/// Oldest version in active use.
///
/// Corresponds to the version to which [`oldest_commit_deployed()`] belongs.
#[macro_export]
macro_rules! oldest_client_version_deployed {
    () => {
        "3.1"
    };
}
pub use oldest_client_version_deployed;

// Make sure this is a commit that is part of the history of master too:
//
//     $ git merge-base origin/master origin/nice-releases/31
//
// (tocco-client does not merge older branches to newer ones.)
/// Oldest commit in active use.
///
/// Used internally to optimize searches (via [`git2::Repository::revwalk()`]). It's
/// rarely necessary to search past this commit. Note that `revwalk()` needs
/// to be used in combination with [`git2::Sort::TOPOLOGICAL`] to ensure search
/// is aborted in the right place.
#[must_use]
pub fn oldest_commit_deployed() -> &'static Oid {
    static VALUE: LazyLock<Oid> = LazyLock::new(|| {
        Oid::from_str("429e0716345ca665976324c03dae271a370222b5").expect("invalid object ID")
    });
    &VALUE
}

/// Name of cached Git repository
const CACHE_NAME: &str = "tocco-client";

/// URL of Git repository
pub const URL: &str = "ssh://git@gitlab.com/toccoag/tocco-client.git";

/// Path to cached *tocco-client* repository.
#[must_use]
pub fn repo_path() -> PathBuf {
    git::repo_cache_path(CACHE_NAME)
}

/// Obtain access to internally cached [tocco-client][] Git repository.
///
/// Repository is created as needed.
///
/// See also [`crate::git#important-repositories`]
///
/// [tocco-client]: https://gitlab.com/toccoag/tocco-client
pub async fn repo() -> Result<Repository> {
    let path = repo_path();
    RepoBuilder::new(path, URL).create_or_open().await
}

/// Obtain access to up-to-date, internally cached [tocco-client][]
/// Git repository.
///
/// Repository is created as needed and updated (`git fetch`) iff
/// `interval` has expired since the last update. An interval of
/// `None` indicates that the repository should not be updated.
///
/// This a wrapper around [`repo()`] offering a convenient way to
/// update the repository directly.
///
/// See also [`crate::git#important-repositories`]
///
/// [tocco-client]: https://gitlab.com/toccoag/tocco-client
pub async fn maybe_updated_repo(interval: Option<Duration>) -> Result<Repository> {
    let mut repo = repo().await?;
    repo.update_every(&[], interval).await?;
    Ok(repo)
}
