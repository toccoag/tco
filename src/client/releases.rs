/// Extract package releases from Git repository
use crate::client;
use crate::git::Repository;
use crate::git::deser::CommitId;
use crate::utils::log::soft_unreachable;
use crate::{Result, cache};
use git2::{Branch, Oid};
use regex::{Regex, RegexBuilder};
use serde::{Deserialize, Serialize};
use std::collections::{HashMap, HashSet};
use std::sync::LazyLock;
use xxhash_rust::const_xxh3;

const CACHE_NAME: &str = "tocco_client_releases";
const CACHE_SCHEMA_HASH: cache::SchemaHash = cache::cache_schema_hash(
    include_bytes!("releases.rs"),
    // invalidate cache when old branches becomes inactive
    const_xxh3::xxh3_128(client::git::oldest_client_version_deployed!().as_bytes()),
);

#[derive(Debug, Default, Deserialize, Serialize)]
struct Cache {
    /// Commits processed scanning for releases.
    ///
    /// Only commits whose ID starts with N zero bits are recorded
    /// to reduce the size of this list. See [`is_memorable_commit_id()`]
    seen_commits: HashSet<CommitId>,

    #[allow(clippy::doc_markdown)]
    /// All released packages
    ///
    /// Key is *{package_name}@{version}* (e.g. `"tocco-admin@3.0.2"`).
    releases: HashMap<String, CommitId>,

    #[serde(skip)]
    dirty: bool,
}

impl Cache {
    fn push_seen_commit(&mut self, id: Oid) {
        let inserted = self.seen_commits.insert(CommitId(id));
        debug_assert!(inserted);
        self.dirty = true;
    }

    fn push_release(&mut self, release: &str, id: Oid) {
        match self.releases.insert(release.to_owned(), CommitId(id)) {
            Some(value) if &id != value.id() => {
                soft_unreachable!(
                    "Release {release:?} referenced in two commits, {id} and {value}."
                );
            }
            Some(_) => (),
            None => self.dirty = true,
        }
    }
}

/// Extract all package releases
///
/// Scans active release branches for package releases, Returns a `HashMap`
/// where the key is the release (e.g. `"tocco-admin@3.0.5"`) and value is
/// the commit which is associated with the release.
pub fn releases(repo: &Repository) -> Result<HashMap<String, CommitId>> {
    let branches = release_branches(repo)?;
    let mut cache: Cache = crate::cache::load(CACHE_NAME, &CACHE_SCHEMA_HASH)?.unwrap_or_default();
    let lengths = (cache.seen_commits.len(), cache.releases.len());
    for branch in branches {
        debug!(
            "Processing branch {:?}",
            branch.name()?.expect("non-UTF8 branch name")
        );
        walk_branch(repo, &branch, &mut cache)?;
    }
    if cache.dirty {
        debug_assert!(lengths != (cache.seen_commits.len(), cache.releases.len()));
        debug!("Cache dirty, flushing to disk.");
        crate::cache::save(CACHE_NAME, &CACHE_SCHEMA_HASH, &cache)?;
    } else {
        debug_assert!(lengths == (cache.seen_commits.len(), cache.releases.len()));
        debug!("Cache up-to-date, not flushing to disk.");
    }
    debug!(
        "Cache status - seen commits: {}, releases: {}",
        cache.seen_commits.len(),
        cache.releases.len()
    );
    Ok(cache.releases)
}

/// Return list of all active release branches plus master.
fn release_branches(repo: &Repository) -> Result<Vec<Branch>> {
    let all_branches = repo.as_inner().branches(None)?;
    let mut filtered_branches: Vec<Branch> = Vec::new();
    for branch in all_branches {
        let (branch, _type) = branch?;
        let name = branch.name()?.expect("non-UTF8 branch name");
        if is_active_branch(name) {
            filtered_branches.push(branch);
        }
    }
    Ok(filtered_branches)
}

fn is_active_branch(name: &str) -> bool {
    static REGEX: LazyLock<Regex> =
        LazyLock::new(|| Regex::new(r"^nice-releases/(\d)(\d+)$").unwrap());

    if name == "master" {
        return true;
    }

    if let Some(capture) = REGEX.captures(name) {
        let major = capture.get(1).expect("missing major version").as_str();
        let minor = capture.get(2).expect("missing minor version").as_str();
        let version = format!("{major}.{minor}");
        let version = lenient_semver::parse(&version).expect("invalid version");

        let oldest_deployed_version =
            lenient_semver::parse(client::git::oldest_client_version_deployed!())
                .expect("invalid version");

        version >= oldest_deployed_version
    } else {
        false
    }
}

/// Walk down branch recording any seen releases.
fn walk_branch(repo: &Repository, branch: &Branch, cache: &mut Cache) -> Result<()> {
    let mut walker = repo.as_inner().revwalk()?;
    walker.set_sorting(git2::Sort::TOPOLOGICAL)?;
    walker.push(branch.get().target().unwrap())?;
    for id in walker {
        let id = id?;
        if cache.seen_commits.contains(&CommitId(id)) {
            debug!("Stop walking branch at already seen commit {id}");
            break;
        }
        if &id == client::git::oldest_commit_deployed() {
            debug!("Stop walking branch at oldest_commit_deployed().");
            break;
        }
        if is_memorable_commit_id(&id) {
            cache.push_seen_commit(id);
        }
        extract_releases_from_commit_message(repo, id, cache);
    }
    Ok(())
}

/// Check if commit ID starts with first 7 bits set to zero.
fn is_memorable_commit_id(id: &Oid) -> bool {
    let bit_count = 7;
    let bytes = id.as_bytes();
    let four_bytes: [u8; 4] = bytes[0..4].try_into().expect("short Git Object ID");
    let four_bytes = u32::from_be_bytes(four_bytes);
    four_bytes & !(!0 >> bit_count) == 0
}

fn extract_releases_from_commit_message(repo: &Repository, id: Oid, cache: &mut Cache) {
    static REGEX: LazyLock<Regex> = LazyLock::new(|| {
        RegexBuilder::new(r"^- (tocco-.*@.*)$")
            .multi_line(true)
            .build()
            .expect("invalid regex")
    });
    let commit = repo
        .as_inner()
        .find_commit(id)
        .expect("commit yielded by walker missing in repo");
    let message = commit.message().unwrap_or_default();
    if message.starts_with("chore: release packages\n") {
        let captures = REGEX.captures_iter(message);
        for capture in captures {
            let release = capture.get(1).expect("release not captured").as_str();
            cache.push_release(release, id);
        }
    }
}

#[cfg(test)]
#[cfg(feature = "test-with-repo-access")]
mod tests {
    use super::*;
    use test_utils::{LockMode, lock_caches};
    use tokio::fs;

    #[tokio::test]
    async fn releases() {
        let _ = env_logger::builder().is_test(true).try_init();
        let _guard = lock_caches(LockMode::Shared).await;
        let repo = client::git::repo().await.unwrap();
        let cache_path = cache::cache_file_with_name(CACHE_NAME);

        match fs::remove_file(&cache_path).await {
            Ok(()) => (),
            Err(e) if e.kind() == std::io::ErrorKind::NotFound => (),
            Err(e) => panic!("{e}"),
        }
        let uncached = super::releases(&repo).unwrap();
        let timestamp1 = fs::metadata(&cache_path).await.unwrap().modified().unwrap();
        let cached = super::releases(&repo).unwrap();
        let timestamp2 = fs::metadata(&cache_path).await.unwrap().modified().unwrap();
        assert_eq!(timestamp1, timestamp2);
        assert_eq!(cached, uncached);

        let packages_released = [
            "tocco-admin@3.9.24",
            "tocco-devcon@3.9.21",
            "tocco-widget-showcase@3.9.24",
        ];
        for release in packages_released {
            assert_eq!(
                cached[release].to_string(),
                "4aa96fd9ca6b4c98245982353dfe3784c5e4466c"
            );
        }

        // On inactive 3.0 branch
        assert!(!cached.contains_key("tocco-entity-browser@1.0.0-hotfix30.29"));
    }
}
