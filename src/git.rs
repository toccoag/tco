//! Manage Git repositories
//!
//! # Important Repositories
//!
//! The most important repositories are cached locally and
//! can be accessed via helper functions.
//!
//! | Repository     | Helper function                                                                 |
//! |----------------|---------------------------------------------------------------------------------|
//! | *ansible*      | [`crate::ansible::git::repo()`] / [`crate::ansible::git::maybe_updated_repo()`] |
//! | *nice2*        | [`crate::nice::git::repo()`] / [`crate::nice::git::maybe_updated_repo()`]       |
//! | *tocco-client* | [`crate::client::git::repo()`] / [`crate::client::git::maybe_updated_repo()`]   |
//!
//! Example:
//!
//! ```
//! # #[tokio::main]
//! # async fn main() {
//! # #[cfg(feature = "test-with-repo-access")] {
//! let repository = tco::ansible::git::repo().await.unwrap();
//!
//! // Full API is available via .as_inner()
//! let _ = repository.as_inner();
//! # }
//! # }
//! ```
//!
//! [`Repository::as_inner()`] returns [`git2::Repository`] providing full access to libgit2's API.
//!
//! # Working Trees
//!
//! Bare Git repositories (i.e. repositories with no files checked out) are created. Files are
//! often accessed via API most efficiently. If a working tree is required or more practical,
//! one can be created. See [`worktree`].
//!
//! # Concurrency-Safety
//!
//! Reposiory creation is protected by [`Flock`](`nix::fcntl::Flock`) as Git itself does
//! not provide any protection.
//!
//! Once created, concurrency-safety is provided by Git's internal locking
//! mechanism. Note that this locking isn't foolproof and races may occur,
//! particularly when it comes to worktrees.

pub mod deser;
pub mod worktree;

use crate::cache;
use crate::utils;
use crate::utils::log::soft_unreachable;
use crate::{Error, Result};
use git2::{ErrorClass, ErrorCode};
use regex::{Regex, RegexBuilder};
use std::fmt::Debug;
use std::fs::File;
use std::path::{Path, PathBuf};
use std::result::Result as StdResult;
use std::str;
use std::sync::LazyLock;
use std::time::Duration;
use std::time::SystemTime;
use tokio::fs::DirBuilder;
use tokio::sync::OnceCell;
use tokio::task;
use worktree::{Worktree, WorktreeDir};

/// Builder updating / cloning Git repositories.
pub struct RepoBuilder<'a> {
    path: PathBuf,
    url: &'a str,
}

impl RepoBuilder<'_> {
    pub fn new<P>(path: P, url: &str) -> RepoBuilder<'_>
    where
        P: AsRef<Path>,
    {
        RepoBuilder {
            path: path.as_ref().to_owned(),
            url,
        }
    }

    /// Open repository creating it if needed.
    pub async fn create_or_open(&self) -> Result<Repository> {
        if !self.path.try_exists()? {
            self.create().await?;
        }

        let path = self.path.clone();
        task::spawn_blocking(move || {
            let repo = git2::Repository::open_bare(path)?;
            Ok(Repository { inner: repo })
        })
        .await
        .unwrap()
    }

    /// Create repository
    ///
    /// The repository isn't returned because the directory
    /// is renamed and, thus, the repository needs to be reopened.
    async fn create(&self) -> Result<()> {
        debug!(
            "Preparing to clone repository {} to {}",
            &self.url,
            &self.path.display()
        );
        DirBuilder::new()
            .recursive(true)
            .mode(0o700)
            .create(&self.path.parent().unwrap())
            .await?;
        let path = self.path.clone();
        let path_tmp = path.with_extension("tmp");
        let url = self.url.to_owned();

        let lock_path = path.with_extension("create-lock");
        let _lock = crate::utils::flock_lock(lock_path).await?;

        utils::maybe_remove_dir_all(&path_tmp).await?;
        task::spawn_blocking(move || {
            if path.try_exists()? {
                debug!(
                    "Repository {} cloned meanwhile. Possibly by another thread.",
                    &path.display()
                );
                return Ok(());
            }
            info!(
                "Repository {url:?} not found at {:?}, cloning …",
                path.display()
            );
            let mut callbacks = git2::RemoteCallbacks::new();
            callbacks.credentials(|_url, username_from_url, _allowed_types| {
                git2::Cred::ssh_key_from_agent(username_from_url.unwrap())
            });

            let mut fo = git2::FetchOptions::new();
            fo.remote_callbacks(callbacks);

            let mut builder = git2::build::RepoBuilder::new();
            let result = builder
                .bare(true)
                .fetch_options(fo)
                .remote_create(|repo, name, url| {
                    repo.remote_with_fetch(name, url, "+refs/heads/*:refs/heads/*")?;
                    repo.remote_add_fetch(name, "+refs/tags/*:refs/tags/*")?;
                    repo.find_remote(name)
                })
                .clone(&url, &path_tmp);
            match result {
                Err(ref e)
                    if e.class() == git2::ErrorClass::Ssh
                        && e.code() == git2::ErrorCode::Certificate =>
                {
                    // `ssh` will likely find the key in GlobalKnownHostsFile installed by the
                    // tocco-known-hosts Debian package but libssh doesn't appear to support
                    // some of the configuration installed by that package and can't find the key.
                    // Disable GlobalKnownHostsFile to make sure the key gets added to
                    // ~/.ssh/known_hosts, which libssh supports.
                    error!(
                        "Clone failed because host isn't trusted. For GitLab repositories, trust \
                         host key like this: ssh -nT -o StrictHostKeyChecking=no -o \
                         GlobalKnownHostsFile=/dev/null -o UserKnownHostsFile=~/.ssh/known_hosts \
                         git@gitlab.com."
                    );
                }
                Err(ref e)
                    if e.class() == git2::ErrorClass::Ssh && e.code() == git2::ErrorCode::Auth =>
                {
                    error!(
                        "Failed to authenticate with server while cloning repository. For \
                         GitLab repositories, make sure your key is authorized. See \
                         https://docs.tocco.ch/ide/machine_setup.html#gitlab-account."
                    );
                }
                _ => (),
            }
            result?;

            // Creation of worktrees/ directory in Git is racy and
            // worktree creation will randomly fail if two threads
            // try to create the directory. Additionally, Git removes
            // the directory when no working tree remains. Prevent this
            // by creating an unused and, by Git, ignored directory in
            // worktrees/.
            std::fs::DirBuilder::new()
                .recursive(true)
                .create(path_tmp.join("worktrees/prevent_dir_removal"))?;

            std::fs::rename(&path_tmp, &path)?;
            Ok(())
        })
        .await
        .unwrap()
    }
}

/// A Git repository
pub struct Repository {
    inner: git2::Repository,
}

impl Repository {
    /// Return inner repository.
    #[must_use]
    pub fn as_inner(&self) -> &git2::Repository {
        &self.inner
    }

    /// Open repository another time
    ///
    /// The libgit2 C library used underneth [is not thread safe][0]. Thus,
    /// `Repository` is not [`Sync`]. To work around this, this method is
    /// provided to open `self` a second time. This instance can then be
    /// used in another thread.
    ///
    /// [0]: https://github.com/libgit2/libgit2/blob/main/docs/threading.md
    pub fn try_clone(&self) -> StdResult<Self, git2::Error> {
        Ok(Repository {
            inner: git2::Repository::open_bare(self.as_inner().path())?,
        })
    }

    /// Create working tree in a temporary directory.
    ///
    /// Temporary directory containing the working tree is removed
    /// on [`Drop`].
    ///
    /// See [`Self::create_worktree_dir()`] for an async version.
    ///
    /// See also documentation of module [`worktree`].
    pub fn create_worktree<O>(&self, refspec: &O) -> Result<Worktree>
    where
        O: ToOid,
    {
        worktree::create_worktree(self, refspec)
    }

    /// Extract file content from Git repository
    ///
    /// This is content extracted from a repository directly
    /// and not from a working tree.
    ///
    /// ```
    /// # #[tokio::main]
    /// # async fn main() {
    /// # #[cfg(feature = "test-with-repo-access")] {
    /// let repo = tco::ansible::git::repo().await.unwrap();
    /// let content = repo.file_content("master", "tocco/config.yml").unwrap();
    /// assert!(content.as_str().unwrap().contains("\ntest:\n"));
    /// # }
    /// # }
    /// ```
    pub fn file_content<P>(&self, rev: &str, path: P) -> Result<Content>
    where
        P: AsRef<Path>,
    {
        let blob = self
            .as_inner()
            .revparse_single(rev)?
            .as_commit()
            .ok_or_else(|| Error::Custom("rev not a commit".to_string()))?
            .tree()?
            .get_path(path.as_ref())?
            .to_object(self.as_inner())?
            .into_blob()
            .map_err(|_| Error::Custom("not a blob".to_string()))?;
        Ok(Content { blob })
    }

    /// Create working tree in a temporary directory.
    ///
    /// Temporary directory containing the working tree is removed
    /// on [`Drop`].
    ///
    /// See also documentation of module [`worktree`].
    pub async fn create_worktree_dir<O>(&self, refspec: &O) -> Result<WorktreeDir>
    where
        O: ToOid + Clone + Send + 'static,
    {
        let repo = self.try_clone()?;
        let refspec = refspec.to_owned();
        task::spawn_blocking(move || {
            let oid = refspec.to_oid(repo.as_inner())?;
            Ok(repo.create_worktree(&oid)?.into_worktree_dir())
        })
        .await
        .unwrap()
    }

    /// Update / Fetch given refspec.
    ///
    /// When `refspec.is_empty()`, the default references
    /// are updated.
    pub async fn update(&mut self, refspec: &[&str]) -> StdResult<(), git2::Error> {
        let refspec: Vec<_> = refspec.iter().map(|s| (*s).to_string()).collect();
        let repo = self.try_clone()?;
        debug!("Updating repository {}.", repo.inner.path().display());
        task::spawn_blocking(move || {
            let mut callbacks = git2::RemoteCallbacks::new();
            callbacks.credentials(|_url, username_from_url, _allowed_types| {
                git2::Cred::ssh_key_from_agent(username_from_url.unwrap())
            });

            let mut fo = git2::FetchOptions::new();
            fo.prune(git2::FetchPrune::On);
            fo.remote_callbacks(callbacks);

            // `git2::Repository` isn't `Sync`. So, just open it
            // a second time.
            let mut remote = repo.as_inner().find_remote("origin")?;
            remote.fetch(&refspec, Some(&mut fo), None)?;
            if let Err(e) = set_repo_modification_time(&repo) {
                soft_unreachable!("Failed to update modification timestamp for repository: {e}");
            }
            Ok(())
        })
        .await
        .unwrap()
    }

    /// Update / Fetch given refspec every `interval`.
    ///
    /// Updates the repository iff `interval` has expired since
    /// the last update. An interval of `None` indicates that the
    /// repository should not be updated.
    pub async fn update_every(
        &mut self,
        refspec: &[&str],
        interval: Option<Duration>,
    ) -> StdResult<(), git2::Error> {
        match interval {
            Some(interval) => match update_interval_expired(self, interval).await {
                Ok(true) => {}
                Ok(false) => return Ok(()),
                Err(e) => soft_unreachable!("Failed to check modification time of repository: {e}"),
            },
            None => return Ok(()),
        }
        self.update(refspec).await?;
        Ok(())
    }

    /// Update repository at most once.
    ///
    /// To ensure multiple calls don't trigger multiple updates,
    /// an [`UpdateOnce`] is passed. An update is attempted only
    /// once per instance of `UpdateOnce`. If the update fails, all
    /// calls will see the error that lead to the failure but the update
    /// is not reattempted.
    ///
    /// Use this to update repository lazily. For instance, only if
    /// commit can't be found locally.
    pub async fn update_once(
        &mut self,
        refspec: &[&str],
        once: &UpdateOnce,
    ) -> StdResult<(), git2::Error> {
        once.0
            .get_or_init(|| async { self.update(refspec).await })
            .await
            .as_ref()
            .map(|&()| ())
            .map_err(|e| {
                // `git2::Error` does not implement `Clone`, clone manually
                git2::Error::new(e.code(), e.class(), e.message())
            })
    }

    /// Path to repository
    #[must_use]
    pub fn path(&self) -> &Path {
        self.inner.path()
    }

    /// Resolve reference and follow cherry picks.
    ///
    /// Resolve `rev_spec` to a commit and follow all cherry-picks
    /// referenced in the commit message. References must follow
    /// the syntax used by `git cherry-pick -x`.
    pub fn resolve_cherry_picks_recursively(
        &self,
        rev_spec: &str,
    ) -> StdResult<Vec<git2::Oid>, git2::Error> {
        let commit = self
            .as_inner()
            .revparse_single(rev_spec)?
            .peel_to_commit()?;
        let mut commits = vec![commit.id()];
        recurse_cherry_pick_commit(&mut commits, self.as_inner(), &commit)?;
        Ok(commits)
    }
}

fn recurse_cherry_pick_commit(
    commits: &mut Vec<git2::Oid>,
    repo: &git2::Repository,
    commit: &git2::Commit,
) -> StdResult<(), git2::Error> {
    if let Some(message) = commit.message() {
        for capture in cherry_pick_regex().captures_iter(message) {
            let id = git2::Oid::from_str(capture.get(1).expect("missing capture").as_str())
                .expect("invalid object ID");
            if !commits.contains(&id) {
                commits.push(id);
                match repo.find_commit(id) {
                    Ok(commit) => recurse_cherry_pick_commit(commits, repo, &commit)?,
                    Err(e)
                        if e.class() == ErrorClass::Reference
                            && e.code() == ErrorCode::NotFound =>
                    {
                        soft_unreachable!(
                            "Commit {} references cherry-pick source {id} which was not found in \
                             repository. Ignoring reference ...",
                            commit.id(),
                        );
                    }
                    Err(e) => return Err(e),
                }
            }
        }
    } else {
        soft_unreachable!(
            "Ignoring non-UTF8 message for commit {} while searching Cherry-Pick references.",
            commit.id()
        );
    }
    Ok(())
}

/// Content of a file within Git
///
/// Obtain via [`Repository::file_content()`].
pub struct Content<'a> {
    blob: git2::Blob<'a>,
}

impl Content<'_> {
    #[must_use]
    pub fn as_bytes(&self) -> &[u8] {
        self.blob.content()
    }

    pub fn as_str(&self) -> StdResult<&str, str::Utf8Error> {
        str::from_utf8(self.as_bytes())
    }
}

/// Check if repository needs to be updated.
///
/// Returns `Ok(true)` if repository has not been updated for more than `interval`.
async fn update_interval_expired(repo: &Repository, interval: Duration) -> Result<bool> {
    let path = repo.inner.path().join("FETCH_HEAD");
    let modified = tokio::fs::metadata(&path).await?.modified()?;
    let modified_relative = match std::time::SystemTime::elapsed(&modified) {
        Ok(m) => m,
        Err(e) => {
            soft_unreachable!(
                "Repository {} last modifed {} s in future, assuming update is required.",
                repo.inner.path().display(),
                e.duration().as_secs()
            );
            return Ok(true);
        }
    };
    if interval < modified_relative {
        debug!(
            "Repository {} last modified {} s ago, update required.",
            repo.inner.path().display(),
            modified_relative.as_secs()
        );
        Ok(true)
    } else {
        debug!(
            "Repository {} last modified {} s ago, no update required.",
            repo.inner.path().display(),
            modified_relative.as_secs()
        );
        Ok(false)
    }
}

fn set_repo_modification_time(repo: &Repository) -> Result<()> {
    let path = repo.inner.path().join("FETCH_HEAD");
    File::open(path)?.set_modified(SystemTime::now())?;
    Ok(())
}

/// An object that can be convert to a Git commit ID.
pub trait ToOid: Debug {
    /// Obtain corresponding Git commit ID.
    fn to_oid(&self, repo: &git2::Repository) -> Result<git2::Oid>;
}

impl ToOid for git2::Oid {
    fn to_oid(&self, _repo: &git2::Repository) -> Result<git2::Oid> {
        Ok(*self)
    }
}

impl ToOid for &str {
    fn to_oid(&self, repo: &git2::Repository) -> Result<git2::Oid> {
        Ok(repo
            .resolve_reference_from_short_name(self)?
            .resolve()?
            .target()
            .expect("Git reference is symbolic"))
    }
}

/// Synchronization primitive to ensure repository
/// is only updated once.
///
/// See [`Repository::update_once()`]
pub struct UpdateOnce(OnceCell<StdResult<(), git2::Error>>);

impl UpdateOnce {
    #[must_use]
    pub const fn new() -> Self {
        UpdateOnce(OnceCell::const_new())
    }

    /// Returns true iff an update has been attempted already.
    ///
    /// The update may or may not have been successful.
    pub fn update_attempted(&self) -> bool {
        self.0.initialized()
    }
}

/// Regex finding "cherry picked from" references
#[must_use]
pub fn cherry_pick_regex() -> &'static Regex {
    static REGEX: LazyLock<Regex> = LazyLock::new(|| {
        RegexBuilder::new(r"^\(cherry picked from commit ([0-9a-f]{40})\)$")
            .multi_line(true)
            .build()
            .expect("invalid regex")
    });
    &REGEX
}

pub(crate) fn repo_cache_path(repo_name: &str) -> PathBuf {
    cache::cache_dir().join(format!("{repo_name}.git"))
}

pub(crate) fn git_time_to_chrono(time: git2::Time) -> chrono::DateTime<chrono::Local> {
    chrono::DateTime::from_timestamp(time.seconds(), 0)
        .expect("git time out of range")
        .into()
}

#[cfg(test)]
#[cfg(feature = "test-with-repo-access")]
mod tests {
    use super::*;
    use crate::{ansible, client};
    #[cfg(feature = "nightly")]
    use test_utils::lock_caches_sync;
    use test_utils::{LockMode, lock_caches};
    #[cfg(feature = "nightly")]
    use tokio::runtime::Runtime;

    #[tokio::test]
    async fn repo_checkout_and_update() {
        let _guard = lock_caches(LockMode::Exclusive).await;
        let mut repo = ansible::git::repo().await.unwrap();
        let a = modification_timestamp(&repo).await;
        repo.update(&[]).await.unwrap();
        let b = modification_timestamp(&repo).await;
        assert!(a < b);
        repo.update(&["master"]).await.unwrap();
        let c = modification_timestamp(&repo).await;
        assert!(b < c);
    }

    #[tokio::test]
    async fn update_once() {
        let mut guard = lock_caches(LockMode::Exclusive).await;
        let mut repo = ansible::git::repo().await.unwrap();

        let valid_url = repo
            .as_inner()
            .find_remote("origin")
            .unwrap()
            .url()
            .unwrap()
            .to_string();
        let invalid_url = "invalid@invalid.invalid:/invalid";

        {
            let a = modification_timestamp(&repo).await;
            let once = UpdateOnce::new();
            // Update once
            assert!(repo.update_once(&[], &once).await.is_ok());
            let b = modification_timestamp(&repo).await;
            assert!(a < b);

            guard.drop_caches_on_lock_release(true); // repo remote may be invalid
            repo.as_inner()
                .remote_set_url("origin", invalid_url)
                .unwrap();

            // Not failing, did not update
            assert!(repo.update_once(&[], &once).await.is_ok());
            let c = modification_timestamp(&repo).await;
            assert_eq!(b, c);
        }

        {
            let a = modification_timestamp(&repo).await;
            let once = UpdateOnce::new();

            // Fails because of invalid URL
            assert!(repo.update_once(&[], &once).await.is_err());
            let b = modification_timestamp(&repo).await;
            assert_eq!(a, b);

            repo.as_inner()
                .remote_set_url("origin", &valid_url)
                .unwrap();
            guard.drop_caches_on_lock_release(false); // URL fixed

            // Error persists, even with corrected URL
            assert!(repo.update_once(&[], &once).await.is_err());
            let c = modification_timestamp(&repo).await;
            assert_eq!(a, c);
        }

        // Ensure URL is really fixed
        assert!(repo.update_once(&[], &UpdateOnce::new()).await.is_ok());
    }

    async fn modification_timestamp(repo: &Repository) -> std::time::SystemTime {
        let path = repo.inner.path().join("FETCH_HEAD");
        tokio::fs::metadata(&path)
            .await
            .unwrap()
            .modified()
            .unwrap()
    }

    #[tokio::test]
    async fn resolve_cherry_picks_recursively() {
        let _guard = lock_caches(LockMode::Shared).await;
        let repo = client::git::repo().await.unwrap();

        {
            let id = "3a3ae66716104210acf0d3a9c84f4951e7f37ec0";
            let result: Vec<_> = repo
                .resolve_cherry_picks_recursively(id)
                .unwrap()
                .iter()
                .map(|id| id.to_string())
                .collect();
            assert_eq!(&["3a3ae66716104210acf0d3a9c84f4951e7f37ec0"], &result[..]);
        }

        {
            let id = "c2c30e30f98dfc912242a26d6cbddf043bd38177";
            let expected_result = &[
                "0838fb97c7fda28e1f6563bb51ff6d1703c59764",
                "7bd9ce8bd84034a2f0c7beb66009b01bb64e0eb8",
                "c2c30e30f98dfc912242a26d6cbddf043bd38177",
            ];
            let mut result: Vec<_> = repo
                .resolve_cherry_picks_recursively(id)
                .unwrap()
                .iter()
                .map(|id| id.to_string())
                .collect();
            result.sort();
            assert_eq!(expected_result, &result[..]);
        }
    }

    #[bench]
    #[cfg(feature = "nightly")]
    fn try_clone(b: &mut crate::test::Bencher) {
        let _guard = lock_caches_sync(LockMode::Shared);
        let rt = Runtime::new().unwrap();
        let repo = rt.block_on(async { ansible::git::repo().await.unwrap() });
        b.iter(|| {
            repo.try_clone().unwrap();
        });
    }
}

#[cfg(test)]
mod tests_local {
    use chrono::{DateTime, Local};

    #[test]
    fn git_time_to_chrono() {
        let test_data = [
            (0, 0, "1970-01-01T00:00:00Z"),
            (0, 75, "1970-01-01T00:00:00Z"),
            (1_585_885_335, -120, "2020-04-03 03:42:15Z"),
            (1_585_885_335, 120, "2020-04-03 03:42:15Z"),
        ];

        for (seconds, offset, expected) in test_data {
            let ts = git2::Time::new(seconds, offset);
            let expected: DateTime<Local> = expected.parse().unwrap();
            assert_eq!(super::git_time_to_chrono(ts), expected);
        }
    }
}
