//! Utilities to query DBs
use crate::sql;
use crate::sql::batch::BatchJob;
use crate::sql::dump::DbMetadata;
use crate::utils;
use async_trait::async_trait;
use chrono::{DateTime, Local};
use postgres::Error;
use postgres::error::SqlState;
use std::cmp;
use tokio_postgres as postgres;

/// Most recent login by a user
///
/// Obtain via [`ClientExt::latest_login()`].
pub struct LatestLogin {
    tocco: Option<UserLogin>,
    non_tocco: Option<UserLogin>,
}

impl LatestLogin {
    /// Latest login across all logins.
    #[must_use]
    pub fn global(&self) -> Option<&UserLogin> {
        cmp::max_by_key(self.tocco.as_ref(), self.non_tocco.as_ref(), |login| {
            login.map(|login| login.time)
        })
    }

    /// Latest login by a /support-tocco login.
    #[must_use]
    pub fn tocco(&self) -> Option<&UserLogin> {
        self.tocco.as_ref()
    }

    /// Latest login by a non-/support-tocco login.
    #[must_use]
    pub fn non_tocco(&self) -> Option<&UserLogin> {
        self.non_tocco.as_ref()
    }
}

/// Login of a user
#[derive(Eq)]
pub struct UserLogin {
    /// Time of login
    pub time: DateTime<Local>,

    /// Name of person
    pub name: String,

    /// User name / Login name
    pub login: String,

    /// Email address of user
    pub email: Option<String>,

    /// ID of login (=action of logging in)
    id: (i64, i64),
}

impl PartialEq for UserLogin {
    fn eq(&self, other: &UserLogin) -> bool {
        self.id == other.id
    }
}

/// Extensions to [`postgres::Client`]
#[async_trait]
pub trait ClientExt {
    /// DB size in bytes
    async fn db_size(&self, db_name: &str) -> Result<Option<u64>, Error>;

    /// Fetch metadata stored as comment on DB
    ///
    /// `None` is returned when:
    ///
    /// * the DB does not exist
    /// * the DB has no comment
    /// * the comment cannot be parsed
    async fn db_metadata(&self, db_name: &str) -> Result<Option<DbMetadata>, Error>;

    /// Check if DB is entirely empty
    ///
    /// This is, check if there is no tables in the "public" namespace.
    ///
    /// See also [`Self::is_empty_history()`]
    async fn is_empty(&self) -> Result<bool, Error>;

    /// Check if DB this is an empty history DB
    ///
    /// This is, check that this is a history DB, but the tables
    /// containing the history are empty (=Schema exists, data not).
    async fn is_empty_history(&self) -> Result<bool, Error>;

    /// Check if current DB is in use
    ///
    /// Returns `true` if there is any *other* connection to the DB.
    async fn is_in_use(&self) -> Result<bool, Error>;

    /// Check if role/user exists
    ///
    /// Returns `true` if DB is in use by anyone else.
    async fn role_exists(&self, name: &str) -> Result<bool, Error>;

    /// Create database
    async fn create_db(&self, db_name: &str, owner: Option<&str>) -> Result<(), Error>;

    /// Test if DB exists
    async fn db_exists(&self, db_name: &str) -> Result<bool, Error>;

    /// Get owner of current DB
    async fn db_owner(&self) -> Result<String, Error>;

    /// Get Owner of given DB
    ///
    /// `None` if DB is missing.
    async fn db_owner_of(&self, db_name: &str) -> Result<Option<String>, Error>;

    /// Obtain list of all DBs
    ///
    /// Excluded are template DBs and DB "postgres".
    ///
    /// Result is ordered arbitrarily.
    async fn list_dbs(&self) -> Result<Vec<String>, Error>;

    /// Obtain list of all tables in schema `"public"`
    ///
    /// Result is ordered arbitrarily.
    async fn list_tables(&self) -> Result<Vec<String>, Error>;

    /// Get time and name of user to login most recently
    ///
    /// `None` is returned when there never has been a login.
    async fn latest_login(&self) -> Result<LatestLogin, Error>;

    /// Time when DB was modified last
    ///
    /// Postgres does not track this information internally. Thus, various
    /// methods are tried to determine when the DB was modified.
    ///
    /// This only works reliable for Nice DBs (main and history) and only
    /// if the DB isn't empty.
    ///
    /// `None` is returned when last modification time can't be determined.
    async fn last_modified(&self) -> Result<Option<DateTime<chrono::Local>>, Error>;

    /// Retrieve list of all batch jobs.
    async fn batch_jobs(&self) -> Result<Vec<BatchJob>, Error>;
}

#[async_trait]
impl ClientExt for postgres::Client {
    async fn db_size(&self, db_name: &str) -> Result<Option<u64>, Error> {
        let row = self
            .query_one("SELECT pg_catalog.pg_database_size($1)", &[&db_name])
            .await;
        match row {
            Ok(row) => {
                let size: i64 = row.get(0);
                #[allow(clippy::cast_sign_loss)]
                Ok(Some(size as u64))
            }
            Err(e) if e.code() == Some(&SqlState::UNDEFINED_DATABASE) => Ok(None),
            Err(e) => Err(e),
        }
    }

    async fn db_metadata(&self, db_name: &str) -> Result<Option<DbMetadata>, Error> {
        let row = self
            .query_opt(
                "SELECT shobj_description(oid, 'pg_database') FROM pg_database WHERE datname = $1",
                &[&db_name],
            )
            .await?;
        let comment: Option<String> = match row {
            Some(row) => row.get(0),
            None => return Ok(None), // no such DB
        };
        Ok(match comment {
            Some(comment) => match serde_json::from_str(&comment) {
                Ok(metadata) => Some(metadata),
                Err(_) => {
                    debug!("Non-JSON DB comment: {comment:?}");
                    None
                }
            },
            None => None, // comment NULL on DB
        })
    }

    async fn is_empty(&self) -> Result<bool, Error> {
        let row = self
            .query_one(
                "SELECT NOT EXISTS(SELECT 1 FROM pg_tables WHERE schemaname = 'public')",
                &[],
            )
            .await?;
        Ok(row.get(0))
    }

    async fn is_empty_history(&self) -> Result<bool, Error> {
        let mut dbs = self.list_tables().await?;
        dbs.retain(|table| {
            !["nice_history", "nice_history_data", "schema_info"]
                .iter()
                .any(|v| v == table)
        });
        if !dbs.is_empty() {
            // unexpected table found
            return Ok(false);
        }

        let row = self
            .query_opt(
                "SELECT 1 FROM nice_history UNION SELECT 1 FROM nice_history_data LIMIT 1",
                &[],
            )
            .await;
        match row {
            Ok(Some(_)) => Ok(false),
            Ok(None) => Ok(true),
            Err(e) if e.code() == Some(&SqlState::UNDEFINED_TABLE) => Ok(false),
            Err(e) => Err(e),
        }
    }

    async fn is_in_use(&self) -> Result<bool, Error> {
        let query = r#"
            SELECT EXISTS(
                SELECT 1
                FROM pg_stat_activity
                WHERE
                    datname = current_database()
                    AND pid <> pg_backend_pid()
            )
        "#;
        let row = self.query_one(query, &[]).await?;
        Ok(row.get(0))
    }

    async fn role_exists(&self, name: &str) -> Result<bool, Error> {
        let row = self
            .query_opt("SELECT 1 FROM pg_roles WHERE rolname = $1", &[&name])
            .await?;
        Ok(row.is_some())
    }

    // Get owner of current DB
    async fn db_owner(&self) -> Result<String, Error> {
        let row = self
            .query_one(
                "SELECT pg_catalog.pg_get_userbyid(d.datdba)
                 FROM pg_catalog.pg_database AS d
                 WHERE d.datname = current_database()",
                &[],
            )
            .await?;
        Ok(row.get(0))
    }

    async fn db_owner_of(&self, db_name: &str) -> Result<Option<String>, Error> {
        let row = self
            .query_opt(
                "SELECT pg_catalog.pg_get_userbyid(d.datdba)
                 FROM pg_catalog.pg_database AS d
                 WHERE d.datname = $1",
                &[&db_name],
            )
            .await?;
        Ok(row.map(|r| r.get(0)))
    }

    async fn create_db(&self, db_name: &str, owner: Option<&str>) -> Result<(), Error> {
        let mut sql = format!("CREATE DATABASE {}", sql::quote_ident(db_name));
        if let Some(owner) = owner {
            sql.push_str(" WITH OWNER ");
            sql.push_str(&sql::quote_ident(owner));
        }
        self.execute(&sql, &[]).await?;
        Ok(())
    }

    async fn db_exists(&self, db_name: &str) -> Result<bool, Error> {
        let row = self
            .query_opt("SELECT 1 FROM pg_database WHERE datname = $1", &[&db_name])
            .await?;
        Ok(row.is_some())
    }

    async fn list_dbs(&self) -> Result<Vec<String>, Error> {
        let sql = r#"
            SELECT datname
            FROM pg_database
            WHERE NOT datistemplate AND datname <> 'postgres'
        "#;
        let rows = self.query(sql, &[]).await?;
        let dbs = rows.into_iter().map(|row| row.get(0)).collect();
        Ok(dbs)
    }

    async fn list_tables(&self) -> Result<Vec<String>, Error> {
        let sql = "SELECT tablename FROM pg_tables WHERE schemaname = 'public'";
        let rows = self.query(sql, &[]).await?;
        let tables = rows.into_iter().map(|row| row.get(0)).collect();
        Ok(tables)
    }

    async fn latest_login(&self) -> Result<LatestLogin, Error> {
        let tocco_sql = r#"
            SELECT
                p.pk AS login_pk,
                p._nice_version AS login_version,
                p.username AS user_name,
                p.last_login AS time,
                u.firstname || ' ' || u.lastname AS name,
                CASE WHEN u.email <> '' THEN u.email ELSE NULL END AS email
            FROM nice_principal AS p
                 LEFT OUTER JOIN nice_user AS u ON p.fk_user = u.pk
            WHERE
                last_login IS NOT NULL
                AND (username LIKE '%@tocco.ch'
                     OR username = 'tocco')
                AND u.firstname IS NOT NULL  -- Filter partially restored DB.
            ORDER BY last_login DESC
            LIMIT 1
        "#;
        let non_tocco_sql = r#"
            SELECT
                p.pk AS login_pk,
                p._nice_version AS login_version,
                p.username AS user_name,
                p.last_login AS time,
                u.firstname || ' ' || u.lastname AS name,
                CASE WHEN u.email <> '' THEN u.email ELSE NULL END AS email
            FROM nice_principal AS p
                 LEFT OUTER JOIN nice_user AS u ON p.fk_user = u.pk
            WHERE
                last_login IS NOT NULL
                AND username NOT LIKE '%@tocco.ch'
                AND username <> 'tocco'
                AND u.firstname IS NOT NULL  -- Filter partially restored DB.
            ORDER BY last_login DESC
            LIMIT 1
        "#;
        let (tocco, non_tocco) = tokio::try_join!(
            self.query_opt(tocco_sql, &[]),
            self.query_opt(non_tocco_sql, &[]),
        )?;

        Ok(LatestLogin {
            tocco: tocco.map(|login| {
                let untrusted_name: &str = login.get("name");
                let name = utils::term_sanitize_string_strict(untrusted_name).into_owned();
                UserLogin {
                    id: (login.get("login_pk"), login.get("login_version")),
                    login: login.get("user_name"),
                    name,
                    email: login.get("email"),
                    time: login.get("time"),
                }
            }),
            non_tocco: non_tocco.map(|login| {
                let untrusted_name: &str = login.get("name");
                let name = utils::term_sanitize_string_strict(untrusted_name).into_owned();
                UserLogin {
                    id: (login.get("login_pk"), login.get("login_version")),
                    login: login.get("user_name"),
                    name,
                    email: login.get("email"),
                    time: login.get("time"),
                }
            }),
        })
    }

    async fn last_modified(&self) -> Result<Option<DateTime<chrono::Local>>, Error> {
        let timestamps: [_; 4] = tokio::try_join!(
            query_time(self, "SELECT max(last_login) FROM nice_principal"),
            query_time(self, "SELECT max(insertion_time) FROM nice_history"),
            query_time(
                self,
                "SELECT max(_nice_update_timestamp) FROM nice_task_execution",
            ),
            query_time(
                self,
                "SELECT max(last_touched) FROM nice_status_tocco_write",
            ),
        )?
        .into();
        Ok(timestamps.into_iter().max().expect("empty iterator"))
    }

    /// Retrieve list of all batch jobs.
    async fn batch_jobs(&self) -> Result<Vec<BatchJob>, Error> {
        super::batch::batch_jobs(self).await
    }
}

async fn query_time(
    client: &postgres::Client,
    sql: &str,
) -> Result<Option<DateTime<chrono::Local>>, Error> {
    match client.query_one(sql, &[]).await {
        Ok(row) => Ok(row.get(0)),
        Err(ref e) if e.code() == Some(&SqlState::UNDEFINED_TABLE) => Ok(None),
        Err(e) => Err(e),
    }
}

#[cfg(test)]
#[cfg(feature = "test-with-secrets")]
mod tests {
    use super::*;
    use crate::sql::SqlConnect;
    use crate::sql::ssh::SshProxyBuilder;

    #[tokio::test]
    async fn db_size() {
        let db_server = "db5.stage.tocco.cust.vshn.net";
        let proxy = SshProxyBuilder::new(db_server).open().await.unwrap();
        let conn = proxy.sql_connect("postgres").await.unwrap();
        let master_size = conn.db_size("nice_master").await.unwrap().unwrap();
        let template_size = conn.db_size("template0").await.unwrap().unwrap();

        assert!(master_size > template_size);
        assert!(master_size > 100_000_000);
        assert!(template_size < 100_000_000);
    }

    #[tokio::test]
    async fn db_size_missing_db() {
        let db_server = "db5.stage.tocco.cust.vshn.net";
        let proxy = SshProxyBuilder::new(db_server).open().await.unwrap();
        let conn = proxy.sql_connect("postgres").await.unwrap();
        assert!(conn.db_size("DoeSNotExIsT").await.unwrap().is_none());
    }

    #[tokio::test]
    async fn is_empty() {
        let db_server = "db5.stage.tocco.cust.vshn.net";
        let proxy = SshProxyBuilder::new(db_server).open().await.unwrap();
        let template_conn = proxy.sql_connect("template1").await.unwrap();
        let master_conn = proxy.sql_connect("nice_master").await.unwrap();

        assert!(template_conn.is_empty().await.unwrap());
        assert!(!master_conn.is_empty().await.unwrap());
    }

    #[tokio::test]
    async fn list_dbs() {
        let db_server = "db5.stage.tocco.cust.vshn.net";
        let proxy = SshProxyBuilder::new(db_server).open().await.unwrap();
        let client = proxy.sql_connect("postgres").await.unwrap();
        let dbs = client.list_dbs().await.unwrap();

        assert!(dbs.iter().any(|i| i == "nice_master"));

        assert!(!dbs.iter().any(|i| i == "postgres"));
        assert!(!dbs.iter().any(|i| i == "template0"));
        assert!(!dbs.iter().any(|i| i == "template1"));
    }

    #[tokio::test]
    async fn latest_login() {
        let db_server = "db5.stage.tocco.cust.vshn.net";
        let proxy = SshProxyBuilder::new(db_server).open().await.unwrap();
        let client = proxy.sql_connect("nice_master").await.unwrap();
        let latest_login = client.latest_login().await.unwrap();

        let tocco = latest_login.tocco().unwrap();
        let non_tocco = latest_login.non_tocco().unwrap();

        assert!(tocco.login.ends_with("@tocco.ch"));
        if let Some(email) = &tocco.email {
            assert!(email.contains('@'));
        }

        assert!(!non_tocco.login.ends_with("@tocco.ch"));
        if let Some(email) = &non_tocco.email {
            assert!(email.contains('@'));
        }

        assert!(tocco != non_tocco);
    }
}

#[cfg(test)]
#[cfg(feature = "test-with-local-postgres")]
mod local_tests {
    use super::*;
    use crate::sql::SqlConnect;
    use crate::sql::local::Local;

    #[tokio::test]
    async fn is_in_use() {
        let db_name = "tco_test_su2hahghaequoorah0go";
        let host = Local::new();
        let admin_conn = host.sql_connect("postgres").await.unwrap();
        admin_conn
            .execute(&format!("DROP DATABASE IF EXISTS {db_name}"), &[])
            .await
            .unwrap();
        admin_conn
            .execute(&format!("CREATE DATABASE {db_name}"), &[])
            .await
            .unwrap();

        let conn1 = host.sql_connect(db_name).await.unwrap();
        assert!(!conn1.is_in_use().await.unwrap());

        let conn2 = host.sql_connect(db_name).await.unwrap();
        assert!(conn1.is_in_use().await.unwrap());
        assert!(conn2.is_in_use().await.unwrap());

        drop(conn1);
        drop(conn2);
        admin_conn
            .execute(&format!("DROP DATABASE {db_name}"), &[])
            .await
            .unwrap();
    }

    #[tokio::test]
    async fn role_exists() {
        let host = Local::new();
        let conn = host.sql_connect("postgres").await.unwrap();

        assert!(conn.role_exists("postgres").await.unwrap());

        let row = conn.query_one("SELECT current_user", &[]).await.unwrap();
        let current_user: &str = row.get(0);
        assert!(conn.role_exists(current_user).await.unwrap());

        assert!(!conn.role_exists("unlikely_to_ever_exist").await.unwrap());
    }

    #[tokio::test]
    async fn db_owner() {
        let role = "tco_test_gu6ruch8aizahj1na0qu";
        let db = "tco_test_bohghohyah1ieghohn6c";
        let host = Local::new();
        let admin_conn = host.sql_connect("postgres").await.unwrap();

        admin_conn
            .execute(&format!("DROP DATABASE IF EXISTS {db}"), &[])
            .await
            .unwrap();
        admin_conn
            .execute(&format!("DROP ROLE IF EXISTS {role}"), &[])
            .await
            .unwrap();
        admin_conn
            .execute(&format!("CREATE ROLE {role}"), &[])
            .await
            .unwrap();
        admin_conn
            .execute(&format!("CREATE DATABASE {db} WITH OWNER {role}"), &[])
            .await
            .unwrap();

        assert_eq!(admin_conn.db_owner_of(db).await.unwrap().unwrap(), role);
        assert!(
            admin_conn
                .db_owner_of("unlikely_to_ever_exist")
                .await
                .unwrap()
                .is_none()
        );

        {
            let conn = host.sql_connect(db).await.unwrap();
            assert_eq!(conn.db_owner().await.unwrap(), role);
        }

        admin_conn
            .execute(&format!("DROP DATABASE {db}"), &[])
            .await
            .unwrap();
        admin_conn
            .execute(&format!("DROP ROLE {role}"), &[])
            .await
            .unwrap();
    }

    #[tokio::test]
    async fn is_empty_history() {
        let history_db_name = "tco_test_ohsh7oox9a_history_db";
        let proxy = Local::new();
        let admin_conn = proxy.sql_connect("postgres").await.unwrap();

        admin_conn
            .execute(&format!("DROP DATABASE IF EXISTS {history_db_name}"), &[])
            .await
            .unwrap();
        admin_conn
            .execute(&format!("CREATE DATABASE {history_db_name}"), &[])
            .await
            .unwrap();

        let conn = proxy.sql_connect(history_db_name).await.unwrap();
        assert!(!conn.is_empty_history().await.unwrap());

        conn.execute("CREATE TABLE nice_history (x INT)", &[])
            .await
            .unwrap();
        assert!(!conn.is_empty_history().await.unwrap());

        conn.execute("CREATE TABLE nice_history_data (x INT)", &[])
            .await
            .unwrap();
        assert!(conn.is_empty_history().await.unwrap());

        conn.execute("CREATE TABLE another_table ()", &[])
            .await
            .unwrap();
        assert!(!conn.is_empty_history().await.unwrap());
        conn.execute("DROP TABLE another_table", &[]).await.unwrap();
        assert!(conn.is_empty_history().await.unwrap());

        conn.execute("INSERT INTO nice_history VALUES (1)", &[])
            .await
            .unwrap();
        assert!(!conn.is_empty_history().await.unwrap());
        conn.execute("DELETE FROM nice_history", &[]).await.unwrap();
        assert!(conn.is_empty_history().await.unwrap());

        conn.execute("INSERT INTO nice_history_data VALUES (1)", &[])
            .await
            .unwrap();
        assert!(!conn.is_empty_history().await.unwrap());

        drop(conn);
        admin_conn
            .execute(&format!("DROP DATABASE {history_db_name}"), &[])
            .await
            .unwrap();
    }

    #[tokio::test]
    async fn db_exists() {
        let db = "tco_test_m2hhhzewa3ak2t0z9b5l";
        let host = Local::new();
        let conn = host.sql_connect("postgres").await.unwrap();

        conn.execute(&format!("DROP DATABASE IF EXISTS {db}"), &[])
            .await
            .unwrap();
        assert!(!conn.db_exists(db).await.unwrap());

        conn.execute(&format!("CREATE DATABASE {db}"), &[])
            .await
            .unwrap();
        assert!(conn.db_exists(db).await.unwrap());

        conn.execute(&format!("DROP DATABASE IF EXISTS {db}"), &[])
            .await
            .unwrap();
    }
}
