//! Dumping and restoring DBs

mod generic;
pub use generic::{GenericProto, GenericProtoBuilder};

use crate::exec;
use crate::sql;
use crate::sql::SqlConnect;
use crate::sql::query::ClientExt;
use crate::sql::ssh::SshProxyBuilder;
use crate::utils::log::soft_unreachable;
use crate::{Error, Result};
use async_trait::async_trait;
use chrono::SubsecRound;
use futures::future;
use postgres::error::SqlState;
use serde::{Deserialize, Serialize};
use std::fmt;
use std::ops::Not;
use tokio_postgres as postgres;

/// Protocol used to copy a DB
#[async_trait]
pub trait Protocol {
    /// Execute copying
    async fn execute(
        &self,
        src_name: &str,
        src_conn: &mut postgres::Client,
        src_exec: &mut (dyn exec::Command + Send),
        dest_name: &str,
        dest_conn: &mut postgres::Client,
        dest_exec: &mut (dyn exec::Command + Send),
    ) -> Result<()>;

    /// Execute copying on same machine
    async fn execute_same_host(
        &self,
        src_name: &str,
        src_conn: &mut postgres::Client,
        dest_name: &str,
        dest_conn: &mut postgres::Client,
        exec: &mut (dyn exec::Command + Send),
    ) -> Result<()>;

    /// Whether this protocol will create a tainted DB
    ///
    /// See [`DbMetadata::tainted`]
    fn is_tainted(&self) -> bool;
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Location {
    host: Host,
    pub db_name: String,
}

impl Location {
    #[must_use]
    pub fn host_name(&self) -> Option<&str> {
        match &self.host {
            Host::Local => None,
            Host::Ssh(name) => Some(name),
        }
    }
}

impl fmt::Display for Location {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &self.host {
            Host::Local => write!(f, "localhost")?,
            Host::Ssh(name) => write!(f, "{name}")?,
        }
        write!(f, "/{}", self.db_name)
    }
}

#[derive(Clone, Debug, Eq, PartialEq)]
enum Host {
    Local,
    Ssh(String),
}

impl Location {
    /// DB on localhost
    #[must_use]
    pub fn new_local(db_name: &str) -> Self {
        Location {
            host: Host::Local,
            db_name: db_name.to_owned(),
        }
    }

    /// DB on remote host, connect via SSH
    #[must_use]
    pub fn new_ssh(db_server: &str, db_name: &str) -> Self {
        Location {
            host: Host::Ssh(db_server.to_owned()),
            db_name: db_name.to_owned(),
        }
    }

    /// Whether DB is on localhost
    #[must_use]
    pub fn is_local(&self) -> bool {
        matches!(self.host, Host::Local)
    }
}

/// Copy mode
#[derive(Clone, Copy)]
pub enum Mode {
    /// Copy a DB
    Copy,

    /// Move a DB
    Move,
}

/// Copy mode
#[derive(Clone, Copy, clap::ValueEnum)]
pub enum CreateMode {
    /// Create target DB. Err if DB already exists.
    Always,

    /// Use existing, empty DB creating it if missing. Err if DB is non-empty.
    Auto,

    /// Use existing, empty DB. Err if missing or non-empty.
    Never,

    /// Replace target DB, or create if missing.
    Replace,
}

/// Copy mode
#[derive(Clone, Debug, Eq, PartialEq)]
pub enum Owner {
    /// Copy owner from source DB.
    FromSource,

    /// Use owner of existing target DB or, if absent, connecting user.
    FromTargetOrCurrentUser,

    /// Use owner of existing target DB.
    FromTarget,

    /// Unconditionally use give owner.
    Name(String),

    /// Use owner of existing target DB or, if absent, given owner.
    FromTargetOr(String),
}

/// Metadata on database stored on target when copying
///
/// Structured metadata stored on DB as comment to help
/// track the origin of a copied DB.
///
/// Comments are shown by `psql -c '\l+'`.
#[derive(Deserialize, Serialize)]
pub struct DbMetadata {
    /// User that created DB
    pub creator: String,

    /// Creation timestamp
    pub creation_time: chrono::DateTime<chrono::Local>,

    #[allow(clippy::doc_markdown)]
    /// Source of database
    ///
    /// In format *<HOST_NAME>/<DB_NAME>*
    pub source: String,

    /// An arbitrary, user-supplied comment
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub comment: Option<String>,

    /// Whether this is a tainted DB copy
    ///
    /// `true` if this is a partial DB copy. For instance,
    /// one missing content for some tables.
    #[serde(default, skip_serializing_if = "<&bool>::not")]
    pub tainted: bool,
}

/// Copy a DB
pub struct Copier {
    source: Location,
    target: Location,
    mode: Mode,
    owner: Owner,
    create_mode: CreateMode,
    proto: Box<dyn Protocol>,
    callback: Option<Box<dyn Callback>>,
    comment: Option<String>,
}

impl Copier {
    /// From where DB is copied.
    #[must_use]
    pub fn source(&self) -> &Location {
        &self.source
    }

    /// To where DB is copied.
    #[must_use]
    pub fn target(&self) -> &Location {
        &self.target
    }
}

#[async_trait(?Send)]
pub trait Callback {
    /// Callback executed after creating database but before copying.
    async fn pre_copy(
        &self,
        copier: &Copier,
        source_conn: &postgres::Client,
        target_conn: &postgres::Client,
        metadata: Option<&DbMetadata>,
    ) -> Result<()>;

    /// Callback executed after copying database.
    ///
    /// Called even if pg_restore reported an error (e.g. as result of a warning).
    async fn post_copy(
        &self,
        _copier: &Copier,
        _source_conn: &postgres::Client,
        _target_conn: &postgres::Client,
    ) -> Result<()>;
}

impl Copier {
    #[must_use]
    pub fn new(source: Location, target: Location) -> Self {
        Copier {
            source,
            target,
            mode: Mode::Copy,
            owner: Owner::FromTarget,
            create_mode: CreateMode::Auto,
            proto: Box::new(GenericProtoBuilder::new().build()),
            callback: None,
            comment: None,
        }
    }

    /// Transfer protocol to use
    ///
    /// Defaults to [`GenericProto`].
    pub fn protocol(&mut self, proto: Box<dyn Protocol>) -> &mut Self {
        self.proto = proto;
        self
    }

    /// Transfer mode to use
    pub fn mode(&mut self, mode: Mode) -> &mut Self {
        self.mode = mode;
        self
    }

    /// How to set owner of target DB
    pub fn owner(&mut self, owner: Owner) -> &mut Self {
        self.owner = owner;
        self
    }

    /// Creation mode of target DB
    pub fn create_mode(&mut self, mode: CreateMode) -> &mut Self {
        self.create_mode = mode;
        self
    }

    /// Set callback
    pub fn callback(&mut self, callback: Box<dyn Callback>) -> &mut Self {
        self.callback = Some(callback);
        self
    }

    /// Set comment on database
    pub fn comment(&mut self, comment: &str) -> &mut Self {
        self.comment = Some(comment.to_owned());
        self
    }

    /// Copy DB
    pub async fn copy(&self) -> Result<()> {
        let source_connector: Box<dyn SqlConnect> = match &self.source.host {
            Host::Local => Box::new(sql::local::Local::new()),
            Host::Ssh(server) => Box::new(SshProxyBuilder::new(server).open().await?),
        };
        let mut source_conn = source_connector.sql_connect(&self.source.db_name).await?;
        let target_connector: Box<dyn SqlConnect> = match &self.target.host {
            Host::Local => Box::new(sql::local::Local::new()),
            Host::Ssh(server) => Box::new(SshProxyBuilder::new(server).open().await?),
        };

        let mut target_conn = self.create_db(&source_conn, &*target_connector).await?;

        match &self.owner {
            Owner::Name(name) | Owner::FromTargetOr(name) => {
                // `pg_restore --role …` will proceed with a warning
                // when role is missing. So, catch this here.
                err_on_missing_role(&target_conn, name).await?;
            }
            Owner::FromSource | Owner::FromTarget | Owner::FromTargetOrCurrentUser => (),
        }

        let expected_collation = "en_US.UTF-8";
        let collation = target_conn
            .query_one(
                "SELECT datcollate FROM pg_database WHERE datname = current_database()",
                &[],
            )
            .await;
        match collation {
            Ok(collation) => {
                let collation: &str = collation.get(0);
                if collation != expected_collation {
                    warn!(
                        "Target database uses collation (ordering) based on locale {collation:?} \
                         rather than {expected_collation:?}. Sorting columns will not work \
                         properly. See OPS-772."
                    );
                }
            }
            Err(e) => soft_unreachable!("Failed to check collation of DB: {e}"),
        }

        // Superuser role is required to restore comments on extensions and schemas. To avoid
        // errors because pg_restore lacks this role, remove all such comments in advance of
        // copying.
        let ((), (), source_metadata) = tokio::join!(
            remove_comments_on_extensions(&source_conn),
            remove_comment_on_schema_public(&source_conn),
            source_conn.db_metadata(&self.source.db_name),
        );
        let source_metadata = source_metadata?;

        if let Some(callback) = &self.callback {
            callback
                .pre_copy(self, &source_conn, &target_conn, source_metadata.as_ref())
                .await?;
        }

        let is_same_host = self.source.host == self.target.host;
        let mut target_executor: Box<dyn exec::Command + Send> = match &self.target().host {
            Host::Local => Box::new(exec::local::Command::new()),
            Host::Ssh(server) => {
                let mut executor = exec::ssh::Command::new(server);
                if !is_same_host {
                    executor.stdin(true);
                }
                Box::new(executor)
            }
        };

        let copy_result = if is_same_host {
            debug!("Source and target host identical, copying directly on host.");
            self.proto
                .execute_same_host(
                    &self.source.db_name,
                    &mut source_conn,
                    &self.target.db_name,
                    &mut target_conn,
                    &mut *target_executor, // same as source
                )
                .await
        } else {
            debug!("Source and target host differ, piping through this machine.");
            let mut source_executor: Box<dyn exec::Command + Send> = match &self.source.host {
                Host::Local => Box::new(exec::local::Command::new()),
                Host::Ssh(server) => Box::new(exec::ssh::Command::new(server)),
            };
            self.proto
                .execute(
                    &self.source.db_name,
                    &mut source_conn,
                    &mut *source_executor,
                    &self.target.db_name,
                    &mut target_conn,
                    &mut *target_executor,
                )
                .await
        };

        if let Err(e) = analyze_tables(&target_conn).await {
            soft_unreachable!("Failed to ANALYZE all tables: {e}");
        }

        let is_tainted =
            source_metadata.map(|m| m.tainted).unwrap_or(false) || self.proto.is_tainted();
        self.write_metadata(&target_conn, self.comment.as_deref(), is_tainted)
            .await?;

        if let Some(callback) = &self.callback {
            callback.post_copy(self, &source_conn, &target_conn).await?;
        }

        // Delay this check to ensure ANALYZE is executed even when
        // a warning is reported.
        copy_result?;

        Ok(())
    }

    async fn create_db(
        &self,
        source_conn: &postgres::Client,
        target_proxy: &dyn SqlConnect,
    ) -> Result<postgres::Client> {
        let target_admin_conn = target_proxy.sql_connect("postgres").await?;
        let owner = match &self.owner {
            Owner::FromTargetOr(name) => Some(
                target_admin_conn
                    .db_owner_of(&self.target.db_name)
                    .await?
                    .unwrap_or_else(|| name.clone()),
            ),
            Owner::FromTargetOrCurrentUser => {
                target_admin_conn.db_owner_of(&self.target.db_name).await?
            }
            Owner::FromTarget => Some(
                target_admin_conn
                    .db_owner_of(&self.target.db_name)
                    .await?
                    .ok_or_else(|| {
                        Error::Custom(
                            "cannot determine owner: target DB does not exist and no owner \
                             specified"
                                .to_string(),
                        )
                    })?,
            ),
            Owner::Name(name) => Some(name.to_string()),
            Owner::FromSource => Some(source_conn.db_owner().await?),
        };

        let conn = match self.create_mode {
            CreateMode::Never => {
                let conn = target_proxy.sql_connect(&self.target.db_name[..]).await?;
                if !conn.is_empty().await? {
                    return Err(Error::Custom(format!(
                        "target DB {} expected to be empty",
                        &self.target,
                    )));
                }
                conn
            }
            CreateMode::Always => {
                target_admin_conn
                    .create_db(&self.target.db_name[..], owner.as_deref())
                    .await?;
                target_proxy.sql_connect(&self.target.db_name[..]).await?
            }
            CreateMode::Auto => {
                self.create_db_or_connect_to_empty(
                    target_proxy,
                    &target_admin_conn,
                    owner.as_deref(),
                )
                .await?
            }
            CreateMode::Replace => {
                let sql = format!("DROP DATABASE {}", sql::quote_ident(&self.target.db_name));
                match target_admin_conn.execute(&sql, &[]).await {
                    Ok(_) => debug!("Dropped target DB {}", self.target),
                    Err(e) if e.code() == Some(&SqlState::UNDEFINED_DATABASE) => {
                        debug!("Could not drop target DB {}: no such database", self.target);
                    }
                    Err(e) => return Err(e.into()),
                }
                self.create_db_or_connect_to_empty(
                    target_proxy,
                    &target_admin_conn,
                    owner.as_deref(),
                )
                .await?
            }
        };
        if let Some(owner) = owner {
            let sql = format!(
                "ALTER DATABASE {} OWNER TO {}",
                sql::quote_ident(&self.target.db_name),
                sql::quote_ident(&owner)
            );
            conn.execute(&sql, &[]).await?;
        }
        Ok(conn)
    }

    async fn create_db_or_connect_to_empty(
        &self,
        target_proxy: &dyn SqlConnect,
        target_admin_conn: &postgres::Client,
        owner: Option<&str>,
    ) -> Result<postgres::Client> {
        match target_admin_conn
            .create_db(&self.target.db_name[..], owner)
            .await
        {
            Ok(()) => Ok(target_proxy.sql_connect(&self.target.db_name[..]).await?),
            Err(e) if e.code() == Some(&SqlState::DUPLICATE_DATABASE) => {
                let conn = target_proxy.sql_connect(&self.target.db_name[..]).await?;
                if !conn.is_empty().await? {
                    return Err(Error::Custom(format!(
                        "target DB {} expected to be empty or missing",
                        &self.target,
                    )));
                }
                Ok(conn)
            }
            Err(e) => Err(e.into()),
        }
    }

    async fn write_metadata(
        &self,
        target_conn: &postgres::Client,
        comment: Option<&str>,
        tainted: bool,
    ) -> Result<()> {
        let data = DbMetadata {
            creator: target_conn
                .query_one("SELECT current_user", &[])
                .await?
                .get(0),
            creation_time: chrono::Local::now().round_subsecs(0),
            source: format!("{}", self.source),
            comment: comment.map(|s| s.to_owned()),
            tainted,
        };
        target_conn
            .execute(
                &format!(
                    "COMMENT ON DATABASE {} IS {}",
                    sql::quote_ident(&self.target.db_name),
                    // not accepted when passed as parameter (e.g. "$1") to execute().
                    sql::quote_literal(&serde_json::to_string(&data)?),
                ),
                &[],
            )
            .await?;
        Ok(())
    }
}

async fn err_on_missing_role(client: &postgres::Client, name: &str) -> Result<()> {
    if client.role_exists(name).await? {
        Ok(())
    } else {
        Err(Error::Custom(format!(
            "role {name:?} not found on target server"
        )))
    }
}

async fn remove_comments_on_extensions(conn: &postgres::Client) {
    match conn
        .execute(
            "DELETE FROM pg_catalog.pg_description WHERE classoid = \
             'pg_catalog.pg_extension'::pg_catalog.regclass",
            &[],
        )
        .await
    {
        Ok(0) => {}
        Ok(count) => debug!("Removed comments from {count} extension(s)."),
        Err(e) if e.code() == Some(&SqlState::READ_ONLY_SQL_TRANSACTION) => {
            debug!(
                "Cannot remove comments from extensions: read-only transaction. You are probably \
                 copying from a read-only slave."
            );
        }
        Err(e) if e.code() == Some(&SqlState::INSUFFICIENT_PRIVILEGE) => {
            debug!(
                "Cannot remove comments from extensions: permission denied. You may not have \
                 write permission for source DB."
            );
        }
        Err(e) => {
            soft_unreachable!("Failed to remove comments from extensions: {e}.");
        }
    }
}

async fn remove_comment_on_schema_public(conn: &postgres::Client) {
    match conn
        .execute(
            r#"
            DELETE FROM pg_catalog.pg_description
            WHERE classoid = 'pg_catalog.pg_namespace'::pg_catalog.regclass
                  AND objoid = (SELECT oid FROM pg_namespace WHERE nspname = 'public');
            "#,
            &[],
        )
        .await
    {
        Ok(0) => {}
        Ok(1) => debug!("Removed comment from schema public."),
        Ok(_) => unreachable!(),
        Err(e) if e.code() == Some(&SqlState::READ_ONLY_SQL_TRANSACTION) => {
            debug!(
                "Cannot remove comment from schema public: read-only transaction. You are \
                 probably copying from a read-only slave."
            );
        }
        Err(e) if e.code() == Some(&SqlState::INSUFFICIENT_PRIVILEGE) => {
            debug!(
                "Cannot remove comment from schema public: permission denied. You may not have \
                 write permission for source DB."
            );
        }
        Err(e) => {
            soft_unreachable!("Failed to remove comment from schema public: {e}.");
        }
    }
}

/// `ANALYZE` tables in schema "public"
// A simple `ANALYZE` could be used, without specifying a table
// but such a statement will issue various warnings when executed
// as non-superuser. This because it tries to `ANALYZE` tables
// in other schemas too.
async fn analyze_tables(conn: &postgres::Client) -> Result<()> {
    let tables = conn.list_tables().await?;
    let tasks: Vec<_> = tables
        .iter()
        .map(|table| {
            let query = format!("ANALYZE public.{}", sql::quote_ident(table));
            async move { conn.execute(&query, &[]).await }
        })
        .collect();
    future::try_join_all(tasks).await?;
    Ok(())
}

#[cfg(test)]
#[cfg(feature = "test-with-local-postgres")]
mod tests_local {
    use super::*;

    const TEST_ROLE_NAME: &str = "tco_test_role_Ze9du4luy2ohrohoh2i1";

    #[tokio::test]
    async fn copy() {
        let machine = sql::local::Local::new();
        let admin_conn = machine.sql_connect("postgres").await.unwrap();
        let name = "1Xcwvxltg9gv3tdu4za1";

        drop_dbs(&admin_conn, name).await;
        let (source_db, target_db) = create_db(&admin_conn, name, None).await;

        let source = Location::new_local(&source_db);
        let target = Location::new_local(&target_db);
        Copier::new(source, target)
            .owner(Owner::FromTargetOrCurrentUser)
            .copy()
            .await
            .unwrap();
        assert_row_copied(name).await;
        assert_owner(name, None).await;

        drop_dbs(&admin_conn, name).await;
    }

    #[tokio::test]
    async fn copy_with_existing_target() {
        let machine = sql::local::Local::new();
        let admin_conn = machine.sql_connect("postgres").await.unwrap();
        let name = "Efowunle8jp0V0vykbwb";

        drop_dbs(&admin_conn, name).await;
        create_role(&admin_conn, TEST_ROLE_NAME).await;
        let (source_db, target_db) = create_db(&admin_conn, name, Some(TEST_ROLE_NAME)).await;

        let source = Location::new_local(&source_db);
        let target = Location::new_local(&target_db);
        Copier::new(source, target)
            .owner(Owner::FromTargetOrCurrentUser)
            .copy()
            .await
            .unwrap();
        assert_row_copied(name).await;
        assert_owner(name, Some(TEST_ROLE_NAME)).await;

        drop_dbs(&admin_conn, name).await;
    }

    #[tokio::test]
    async fn copy_with_specific_owner() {
        let machine = sql::local::Local::new();
        let admin_conn = machine.sql_connect("postgres").await.unwrap();
        let name = "aexO0hal1eev3ohhaes2";

        drop_dbs(&admin_conn, name).await;
        create_role(&admin_conn, TEST_ROLE_NAME).await;
        let (source_db, target_db) = create_db(&admin_conn, name, Some(TEST_ROLE_NAME)).await;

        let source = Location::new_local(&source_db);
        let target = Location::new_local(&target_db);
        Copier::new(source, target)
            .owner(Owner::Name(TEST_ROLE_NAME.to_string()))
            .copy()
            .await
            .unwrap();
        assert_row_copied(name).await;
        assert_owner(name, Some(TEST_ROLE_NAME)).await;

        drop_dbs(&admin_conn, name).await;
    }

    #[tokio::test]
    async fn copy_with_non_empty_target() {
        let machine = sql::local::Local::new();
        let admin_conn = machine.sql_connect("postgres").await.unwrap();
        let name_a = "PeeluoFahC0miphaith4-a";
        let name_b = "PeeluoFahC0miphaith4-b";

        drop_dbs(&admin_conn, name_a).await;
        drop_dbs(&admin_conn, name_b).await;
        create_role(&admin_conn, TEST_ROLE_NAME).await;
        let (source_db, _) = create_db(&admin_conn, name_a, None).await;
        let (non_empty_target_db, _) = create_db(&admin_conn, name_b, None).await;

        let source = Location::new_local(&source_db);
        let target = Location::new_local(&non_empty_target_db);
        let err = Copier::new(source, target)
            .owner(Owner::FromTarget)
            .copy()
            .await
            .unwrap_err();
        assert_eq!(
            format!("{err}"),
            "target DB localhost/tco_test_PeeluoFahC0miphaith4-b_orig expected to be empty or \
             missing"
        );
        drop_dbs(&admin_conn, name_a).await;
        drop_dbs(&admin_conn, name_b).await;
    }

    #[tokio::test]
    async fn copy_mode_replace() {
        async fn does_table_from_old_db_exist(conn: &postgres::Client) -> bool {
            match conn
                .execute("SELECT * FROM table_in_db_being_replaced", &[])
                .await
            {
                Ok(_) => true,
                Err(e) if e.code() == Some(&SqlState::UNDEFINED_TABLE) => false,
                Err(e) => panic!("{e}"),
            }
        }

        let machine = sql::local::Local::new();
        let admin_conn = machine.sql_connect("postgres").await.unwrap();
        let name = "e8EaccuzXwjmmBPeJfXw";

        drop_dbs(&admin_conn, name).await;
        create_role(&admin_conn, TEST_ROLE_NAME).await;
        let (source_db, target_db) = create_db(&admin_conn, name, Some(TEST_ROLE_NAME)).await;

        // Create table in old DB, before replacing it, to be able to
        // detect if DB was replaced properly later.
        {
            let conn = machine.sql_connect(&target_db).await.unwrap();
            conn.execute("CREATE TABLE table_in_db_being_replaced ()", &[])
                .await
                .unwrap();
            assert!(does_table_from_old_db_exist(&conn).await);
        }

        let source = Location::new_local(&source_db);
        let target = Location::new_local(&target_db);
        Copier::new(source, target)
            .owner(Owner::FromTarget)
            .create_mode(CreateMode::Replace)
            .copy()
            .await
            .unwrap();

        // Check if DB was replaced.
        {
            let conn = machine.sql_connect(&target_db).await.unwrap();
            assert!(!does_table_from_old_db_exist(&conn).await);
        }

        drop_dbs(&admin_conn, name).await;
    }

    #[tokio::test]
    async fn copy_mode_never() {
        let machine = sql::local::Local::new();
        let admin_conn = machine.sql_connect("postgres").await.unwrap();
        let name = "mot8peg4aeyohph3ais7";

        drop_dbs(&admin_conn, name).await;
        create_role(&admin_conn, TEST_ROLE_NAME).await;
        let (source_db, target_db) = create_db(&admin_conn, name, None).await;

        let source = Location::new_local(&source_db);
        let target = Location::new_local(&target_db);
        let mut copier = Copier::new(source, target);
        copier.owner(Owner::FromTargetOrCurrentUser);
        copier.create_mode(CreateMode::Never);

        // error, target does not exist
        let err = copier.copy().await.unwrap_err();
        assert!(err.to_string().contains("does not exist"));

        // create empty target
        admin_conn
            .execute(&format!("CREATE DATABASE {target_db}"), &[])
            .await
            .unwrap();

        // ok, restoring to empty target
        copier.copy().await.unwrap();
        assert_row_copied(name).await;

        // error, non-empty target
        let err = copier.copy().await.unwrap_err();
        assert!(err.to_string().contains("expected to be empty"));

        drop_dbs(&admin_conn, name).await;
    }

    #[tokio::test]
    async fn copy_mode_always() {
        let machine = sql::local::Local::new();
        let admin_conn = machine.sql_connect("postgres").await.unwrap();
        let name = "eehaip5weejae2ohg4ch";

        drop_dbs(&admin_conn, name).await;
        create_role(&admin_conn, TEST_ROLE_NAME).await;
        let (source_db, target_db) = create_db(&admin_conn, name, None).await;

        let source = Location::new_local(&source_db);
        let target = Location::new_local(&target_db);
        let mut copier = Copier::new(source, target);
        copier.owner(Owner::FromTargetOrCurrentUser);
        copier.create_mode(CreateMode::Always);

        // ok, creating target
        copier.copy().await.unwrap();
        assert_row_copied(name).await;

        // error, non-empty target
        let err = copier.copy().await.unwrap_err();
        assert!(err.to_string().contains("already exists"));

        // create empty target
        admin_conn
            .execute(&format!("DROP DATABASE {target_db};"), &[])
            .await
            .unwrap();
        admin_conn
            .execute(&format!("CREATE DATABASE {target_db};"), &[])
            .await
            .unwrap();

        // error, target exists
        let err = copier.copy().await.unwrap_err();
        assert!(err.to_string().contains("already exists"));

        drop_dbs(&admin_conn, name).await;
    }

    #[tokio::test]
    async fn tainted_propagation_from_source_to_target_db() {
        const SOURCE_DB: &str = "tocco_tco_tkcbk5_source";
        const TARGET_DB: &str = "tocco_tco_tkcbk5_target";

        async fn cleanup_dbs(admin_conn: &postgres::Client) {
            admin_conn
                .execute(&format!("DROP DATABASE IF EXISTS {SOURCE_DB}"), &[])
                .await
                .unwrap();
            admin_conn
                .execute(&format!("DROP DATABASE IF EXISTS {TARGET_DB}"), &[])
                .await
                .unwrap();
        }

        async fn copy() {
            let source = Location::new_local(SOURCE_DB);
            let target = Location::new_local(TARGET_DB);

            let mut copier = Copier::new(source, target);
            copier.owner(Owner::FromTargetOrCurrentUser);
            copier.copy().await.unwrap();
        }

        #[must_use]
        async fn target_tainted(admin_conn: &postgres::Client) -> bool {
            let metadata = admin_conn.db_metadata(TARGET_DB).await.unwrap().unwrap();
            metadata.tainted
        }

        async fn drop_target_db(admin_conn: &postgres::Client) {
            admin_conn
                .execute(&format!("DROP DATABASE {TARGET_DB}"), &[])
                .await
                .unwrap();
        }

        async fn write_comment_on_source(admin_conn: &postgres::Client, comment: &str) {
            admin_conn
                .execute(
                    &format!(
                        "COMMENT ON DATABASE {SOURCE_DB} IS {}",
                        sql::quote_literal(comment)
                    ),
                    &[],
                )
                .await
                .unwrap();
        }

        let machine = sql::local::Local::new();
        let admin_conn = machine.sql_connect("postgres").await.unwrap();

        cleanup_dbs(&admin_conn).await;
        admin_conn
            .execute(&format!("CREATE DATABASE {SOURCE_DB}"), &[])
            .await
            .unwrap();

        /* Source without COMMENT will not taint target. */

        copy().await;
        assert!(!target_tainted(&admin_conn).await);
        drop_target_db(&admin_conn).await;

        /* Source with COMMENT but without "tainted" will not taint target. */

        write_comment_on_source(
            &admin_conn,
            r#"{"creator":"user","creation_time":"2024-12-20T12:07:44+01:00","source":"db5.stage.tocco.cust.vshn.net/nice_master"}"#,
        ).await;
        copy().await;
        assert!(!target_tainted(&admin_conn).await);
        drop_target_db(&admin_conn).await;

        /* Source with COMMENT indicating "tainted" will taint target. */

        write_comment_on_source(
             &admin_conn,
             r#"{"creator":"user","creation_time":"2024-12-20T12:07:44+01:00","source":"db5.stage.tocco.cust.vshn.net/nice_master", "tainted": true}"#,
         ).await;
        copy().await;
        assert!(target_tainted(&admin_conn).await);
        drop_target_db(&admin_conn).await;

        cleanup_dbs(&admin_conn).await;
    }

    async fn create_db(
        conn: &postgres::Client,
        name: &str,
        target_owner: Option<&str>,
    ) -> (String, String) {
        let (source_db, target_db) = db_names(name);
        conn.execute(&format!("CREATE DATABASE \"{source_db}\""), &[])
            .await
            .unwrap();

        if let Some(target_owner) = target_owner {
            conn.execute(
                &format!("CREATE DATABASE \"{target_db}\" WITH OWNER \"{target_owner}\""),
                &[],
            )
            .await
            .unwrap();
        }

        let content_sql = r#"
            CREATE TABLE a_table (
                a_row INT
            );

            INSERT INTO a_table VALUES (0);
        "#;
        let conn = sql::local::Local::new()
            .sql_connect(&source_db)
            .await
            .unwrap();
        conn.batch_execute(content_sql).await.unwrap();

        (source_db, target_db)
    }

    async fn create_role(conn: &postgres::Client, name: &str) {
        match conn.execute(&format!("CREATE ROLE \"{name}\""), &[]).await {
            Ok(_) => (),
            Err(e) if e.code() == Some(&SqlState::DUPLICATE_OBJECT) => (), // Postgres 15
            Err(e) if e.code() == Some(&SqlState::UNIQUE_VIOLATION) => (), // Postgres 13
            Err(e) => panic!("{e:?}"),
        }
    }

    async fn drop_dbs(conn: &postgres::Client, name: &str) {
        let (source_db, target_db) = db_names(name);
        conn.execute(&format!("DROP DATABASE IF EXISTS \"{source_db}\""), &[])
            .await
            .unwrap();
        conn.execute(&format!("DROP DATABASE IF EXISTS \"{target_db}\""), &[])
            .await
            .unwrap();
    }

    fn db_names(name: &str) -> (String, String) {
        (
            format!("tco_test_{name}_orig"),
            format!("tco_test_{name}_copy"),
        )
    }

    async fn assert_row_copied(name: &str) {
        let (_source_db, target_db) = db_names(name);
        let conn = sql::local::Local::new()
            .sql_connect(&target_db)
            .await
            .unwrap();
        let row = conn
            .query_one("SELECT a_row FROM a_table", &[])
            .await
            .unwrap();
        let value: i32 = row.get(0);
        assert_eq!(value, 0);
    }

    async fn assert_owner(name: &str, owner: Option<&str>) {
        let (_source_db, target_db) = db_names(name);
        let conn = sql::local::Local::new()
            .sql_connect(&target_db)
            .await
            .unwrap();

        let db_owner = conn.db_owner().await.unwrap();
        let row = conn
            .query_one(
                "SELECT t.tableowner FROM pg_catalog.pg_tables AS t WHERE t.tablename = 'a_table'",
                &[],
            )
            .await
            .unwrap();
        let table_owner: String = row.get(0);

        if let Some(owner) = owner {
            assert_eq!(db_owner, owner);
            assert_eq!(table_owner, owner);
        } else {
            assert_ne!(db_owner, TEST_ROLE_NAME);
            assert_ne!(table_owner, TEST_ROLE_NAME);
        }
    }
}

#[cfg(test)]
#[cfg(feature = "test-with-local-postgres")]
mod tests_remote {
    use super::*;
    #[cfg(feature = "test-with-secrets")]
    use std::result::Result as StdResult;

    #[cfg(feature = "test-with-secrets")]
    async fn count_rows(conn: &postgres::Client, table: &str) -> StdResult<i64, postgres::Error> {
        let row = conn
            .query_one(
                &format!("SELECT COUNT(*) FROM {}", sql::quote_ident(table)),
                &[],
            )
            .await?;
        Ok(row.get(0))
    }

    #[tokio::test]
    #[cfg(feature = "test-with-secrets")]
    async fn copy() {
        // Name contains upper-case letter to test quoting. If unquoted,
        // names are converted to lower-case.
        let target_name = "tco_test_m7zcrv0ivcz7ltnxqS5j";

        let target_machine = sql::local::Local::new();
        let admin_conn = target_machine.sql_connect("postgres").await.unwrap();
        admin_conn
            .execute(&format!("DROP DATABASE IF EXISTS \"{target_name}\""), &[])
            .await
            .unwrap();

        let source = Location::new_ssh("db5.stage.tocco.cust.vshn.net", "nice_master");
        let target = Location::new_local(target_name);
        let mut copier = Copier::new(source, target);
        copier.owner(Owner::FromTargetOrCurrentUser);

        copier.copy().await.unwrap();

        let conn = target_machine.sql_connect(target_name).await.unwrap();
        assert!(count_rows(&conn, "nice_principal").await.unwrap() > 0);

        let metadata = conn.db_metadata(target_name).await.unwrap().unwrap();
        assert_eq!(metadata.source, "db5.stage.tocco.cust.vshn.net/nice_master");
        assert!(metadata.comment.is_none());
        assert!(!metadata.tainted);
        assert!(admin_conn.db_size(target_name).await.unwrap().unwrap() > 100 * 1024 * 1024);

        drop(conn);
        admin_conn
            .execute(&format!("DROP DATABASE \"{target_name}\""), &[])
            .await
            .unwrap();
    }

    #[tokio::test]
    #[cfg(feature = "test-with-secrets")]
    async fn copy_with_default_set_of_excluded_tables_for_dev_dbs() {
        let target_name = "tco_test_iq6iev4xmen5esnauhrb";

        let target_machine = sql::local::Local::new();
        let admin_conn = target_machine.sql_connect("postgres").await.unwrap();
        admin_conn
            .execute(&format!("DROP DATABASE IF EXISTS \"{target_name}\""), &[])
            .await
            .unwrap();

        let source = Location::new_ssh("db5.stage.tocco.cust.vshn.net", "nice_master");
        let target = Location::new_local(target_name);
        let mut proto_builder = GenericProtoBuilder::new();
        proto_builder.exclude_var_tables();
        proto_builder.exclude_email_tables();
        let mut copier = Copier::new(source, target);
        let protocol = proto_builder.build();
        assert!(protocol.is_tainted());
        copier.protocol(Box::new(protocol));
        copier.owner(Owner::FromTargetOrCurrentUser);

        copier.copy().await.unwrap();

        /*  Excluded tables should be empty. */

        let conn = target_machine.sql_connect(target_name).await.unwrap();
        for table in generic::EXCLUDED_TABLES_VAR
            .iter()
            .chain(generic::EXCLUDED_TABLES_EMAIL)
        {
            match count_rows(&conn, table).await {
                Ok(0) => (),
                Ok(count) => {
                    panic!("excluded table {table} not empty as expected, found {count} rows.")
                }
                Err(ref e) if e.code() == Some(&SqlState::UNDEFINED_TABLE) => {
                    panic!(
                        "Excluded table {table} not found. Was it renamed? Is there (an)other \
                         table(s) in need of exclusion?"
                    );
                }
                Err(e) => panic!("{e}"),
            }
        }

        /* Other tables should contain content. */

        assert!(count_rows(&conn, "nice_principal").await.unwrap() > 0);
        assert!(admin_conn.db_size(target_name).await.unwrap().unwrap() > 100 * 1024 * 1024);

        /* Indexes/constraints/... should exist, even on excluded tables. */

        let an_excluded_table = "nice_email_archive_to_recipient";
        assert!(generic::EXCLUDED_TABLES_EMAIL.contains(&an_excluded_table));

        match conn
            .execute(
                &format!(
                    r#"INSERT INTO {an_excluded_table} (fk_email_archive, fk_recipient)
                       VALUES (-1, -1)"#
                ),
                &[],
            )
            .await
        {
            Err(e) if e.code() == Some(&SqlState::FOREIGN_KEY_VIOLATION) => (),
            Ok(_) => panic!("foreign key constraint missing on excluded table"),
            Err(e) => panic!("{e}"),
        }

        let an_index_on_excluded_table = format!("{an_excluded_table}_fk_recipient_idx");
        match conn
            .execute(
                &format!("CREATE INDEX {an_index_on_excluded_table} ON {an_excluded_table} ((1))"),
                &[],
            )
            .await
        {
            Err(e) if e.code() == Some(&SqlState::DUPLICATE_TABLE) => (), // really duplicate index
            Ok(_) => panic!("index missing"),
            Err(e) => panic!("{e:?}"),
        }

        /* Partially copied DB should be marked tainted. */

        let metadata = conn.db_metadata(target_name).await.unwrap().unwrap();
        assert!(metadata.tainted);

        drop(conn);
        admin_conn
            .execute(&format!("DROP DATABASE \"{target_name}\""), &[])
            .await
            .unwrap();
    }

    #[tokio::test]
    async fn metadata_with_comment() {
        // Names contain upper-case letters to test quoting. If unquoted,
        // names are converted to lower-case.
        let source_name = "tco_test_yee4aivie2OoCah_a";
        let target_name = "tco_test_yee4aivie2OoCah_b";

        let comment = "a comment on a database";
        let target_machine = sql::local::Local::new();

        let admin_conn = target_machine.sql_connect("postgres").await.unwrap();
        admin_conn
            .execute(&format!("DROP DATABASE IF EXISTS \"{source_name}\""), &[])
            .await
            .unwrap();
        admin_conn
            .execute(&format!("DROP DATABASE IF EXISTS \"{target_name}\""), &[])
            .await
            .unwrap();
        admin_conn
            .execute(&format!("CREATE DATABASE \"{source_name}\""), &[])
            .await
            .unwrap();

        let source = Location::new_local(source_name);
        let target = Location::new_local(target_name);
        let mut copier = Copier::new(source, target);
        copier.owner(Owner::FromTargetOrCurrentUser);
        copier.comment(comment);

        copier.copy().await.unwrap();

        let conn = target_machine.sql_connect(target_name).await.unwrap();
        let metadata = conn.db_metadata(target_name).await.unwrap().unwrap();
        assert_eq!(metadata.source, format!("localhost/{source_name}"));
        assert_eq!(metadata.comment.unwrap(), comment);

        drop(conn);
        admin_conn
            .execute(&format!("DROP DATABASE \"{source_name}\""), &[])
            .await
            .unwrap();
        admin_conn
            .execute(&format!("DROP DATABASE \"{target_name}\""), &[])
            .await
            .unwrap();
    }

    #[tokio::test]
    async fn metadata_missing_db() {
        let db_name = "tco_test_iegazea8teebuph7ooch_missing_db";
        let machine = sql::local::Local::new();
        let admin_conn = machine.sql_connect("postgres").await.unwrap();
        assert!(admin_conn.db_metadata(db_name).await.unwrap().is_none());
    }

    #[tokio::test]
    async fn metadata_invalid_comment() {
        let db_name = "tco_test_shoong0oothai3yai3ok";
        let machine = sql::local::Local::new();
        let admin_conn = machine.sql_connect("postgres").await.unwrap();

        admin_conn
            .execute(&format!("DROP DATABASE IF EXISTS {db_name}"), &[])
            .await
            .unwrap();
        admin_conn
            .execute(&format!("CREATE DATABASE {db_name}"), &[])
            .await
            .unwrap();

        // no comment
        assert!(admin_conn.db_metadata(db_name).await.unwrap().is_none());

        // invalid comment
        admin_conn
            .execute(
                &format!(r#"COMMENT ON DATABASE {db_name} IS '{{ creator: "fields missing" }}'"#),
                &[],
            )
            .await
            .unwrap();
        assert!(admin_conn.db_metadata(db_name).await.unwrap().is_none());

        admin_conn
            .execute(&format!("DROP DATABASE {db_name}"), &[])
            .await
            .unwrap();
    }
}
