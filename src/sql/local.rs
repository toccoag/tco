//! Connect to Postgres on local machine.

use crate::sql::SqlConnect;
use async_trait::async_trait;
use nix::unistd;
use std::result::Result as StdResult;
use tokio_postgres as postgres;

/// The local host
pub struct Local {
    port: Option<u16>,
}

impl Local {
    #[must_use]
    pub fn new() -> Self {
        Local { port: None }
    }

    pub fn port(&mut self, port: Option<u16>) -> &mut Self {
        self.port = port;
        self
    }
}

#[async_trait]
impl SqlConnect for Local {
    async fn sql_connect(&self, db_name: &str) -> StdResult<postgres::Client, postgres::Error> {
        let mut config = postgres::config::Config::new();
        let host_path = if cfg!(target_os = "linux") {
            // Linux distros appear to have realized that using
            // a hard-coded path in /tmp is insecure but …
            "/var/run/postgresql"
        } else {
            // … it's still the default upstream
            "/tmp"
        };
        config
            .application_name("tco")
            .user(current_user_name())
            .dbname(db_name)
            .host_path(host_path);

        if let Some(port) = self.port {
            config.port(port);
        }
        let (client, connection) = config.connect(postgres::tls::NoTls).await?;
        tokio::spawn(async move {
            if let Err(e) = connection.await {
                debug!("Postgres connection closed: {e}");
            }
        });
        Ok(client)
    }
}

fn current_user_name() -> String {
    let uid = unistd::Uid::current();
    unistd::User::from_uid(uid)
        .expect("failure while resolving current user's name")
        .expect("current user has no name")
        .name
}

#[cfg(test)]
#[cfg(feature = "test-with-local-postgres")]
mod tests {
    use super::*;

    #[tokio::test]
    async fn connect_no_port() {
        let conn = Local::new().sql_connect("postgres").await.unwrap();
        let row = conn.query_one("SELECT 1", &[]).await.unwrap();
        let value: i32 = row.get(0);
        assert_eq!(value, 1);
    }

    #[tokio::test]
    async fn connect_with_port() {
        let conn = Local::new()
            .port(Some(5432))
            .sql_connect("postgres")
            .await
            .unwrap();
        let row = conn.query_one("SELECT 1", &[]).await.unwrap();
        let value: i32 = row.get(0);
        assert_eq!(value, 1);
    }
}
