//! Retrieve information about batch jobs from DB
use postgres::Error;
use std::fmt;
use tokio_postgres as postgres;

#[derive(Debug, PartialEq)]
pub enum Type {
    Static,
    Dynamic,
}

impl fmt::Display for Type {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let text = match self {
            Type::Static => "static",
            Type::Dynamic => "dynamic",
        };
        write!(f, "{text}")
    }
}

#[derive(Debug)]
pub struct BatchJob {
    pub active: bool,
    pub name: String,
    pub r#type: Type,
    pub description: String,
    /// Day of month
    pub dom: String,
    pub month: String,
    /// Day of week
    pub dow: String,
    pub hour: String,
    pub minute: String,
}

pub(crate) async fn batch_jobs(conn: &postgres::Client) -> Result<Vec<BatchJob>, Error> {
    let mut result = Vec::new();

    let rows = conn
        .query(
            "SELECT active, id, description, days_of_month, months, days_of_week, hours, minutes \
             FROM nice_batch_job",
            &[],
        )
        .await?;
    for row in rows {
        result.push(BatchJob {
            active: row.get("active"),
            name: row.get("id"),
            r#type: Type::Static,
            description: row.get("description"),
            dom: row.get("days_of_month"),
            month: row.get("months"),
            dow: row.get("days_of_week"),
            hour: row.get("hours"),
            minute: row.get("minutes"),
        });
    }

    let rows = conn
        .query(
            "SELECT active, name, description, cron_days_of_month, cron_months, \
             cron_days_of_week, cron_hours, cron_minutes FROM nice_dynamic_batch_job",
            &[],
        )
        .await?;
    for row in rows {
        result.push(BatchJob {
            active: row.get("active"),
            name: row.get("name"),
            r#type: Type::Dynamic,
            description: row.get("description"),
            dom: row.get("cron_days_of_month"),
            month: row.get("cron_months"),
            dow: row.get("cron_days_of_week"),
            hour: row.get("cron_hours"),
            minute: row.get("cron_minutes"),
        });
    }

    Ok(result)
}

#[cfg(test)]
#[cfg(feature = "test-with-secrets")]
mod tests {
    use super::*;
    use crate::sql::SqlConnect;
    use crate::sql::ssh::SshProxyBuilder;

    #[tokio::test]
    async fn fetch_batch_jobs() {
        let proxy = SshProxyBuilder::new("db5.stage.tocco.cust.vshn.net")
            .open()
            .await
            .unwrap();
        let conn = proxy.sql_connect("nice_master").await.unwrap();
        let jobs = batch_jobs(&conn).await.unwrap();
        let job = jobs
            .iter()
            .find(|j| j.name == "BirthdayCongratulateBatchJob")
            .unwrap();
        assert_eq!(job.r#type, Type::Static);
    }
}
