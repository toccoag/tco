//! Manage SSH tunnel to connect to Postgres
//!
//! Port forwarding is used to create a local Unix socket
//! which forwards all traffic to Postgres on a remote machine.
//! The SSH connection is kept open until [`SshProxy`] is
//! [`Drop`]ed. An arbitrary number of connections to
//! Postgres can be opened via [`SqlConnect::sql_connect()`].
//!
//! # User / Role
//!
//! The name of the Linux user on the server is used to connect
//! via Unix socket. To execute queries with under different role
//! use [SET ROLE]. This may be required to ensure the owner of
//! a newly created object is correct.
//!
//! # Example
//!
//! ```
//! # #[tokio::main]
//! # async fn main() {
//! # #[cfg(feature = "test-with-secrets")] {
//! use tco::sql::SqlConnect;
//! use tco::sql::ssh::SshProxyBuilder; // for sql_connect()
//!
//! let server = "db1.stage.tocco.cust.vshn.net";
//! let database = "postgres";
//! let sql = "SELECT 5 + $1::BIGINT + $2::TEXT::BIGINT AS a_name";
//! let param1 = 15_i64;
//! let param2 = "8";
//! let proxy = SshProxyBuilder::new(server).open().await.unwrap();
//! let client = proxy.sql_connect(&database).await.unwrap();
//! let row = client.query_one(sql, &[&param1, &param2]).await.unwrap();
//! let number: i64 = row.get(0);
//! assert_eq!(number, 28);
//!
//! let number: i64 = row.get("a_name");
//! assert_eq!(number, 28);
//! # }
//! # }
//! ```
//!
//! Note that supplied types must match the ones expected by Postgres. Use
//! [type casting], as shown in the example above (`expression::type`), to
//! ensure postgres expects the right type.
//!
//! [SET ROLE]: https://www.postgresql.org/docs/current/sql-set-role.html
//! [type casting]: https://www.postgresql.org/docs/15/sql-expressions.html#SQL-SYNTAX-TYPE-CASTS
use crate::exec::{CommandExt, ssh};
use crate::sql::SqlConnect;
use crate::utils;
use crate::utils::log::DisplayCommand;
use async_trait::async_trait;
use std::io;
use std::path::{Path, PathBuf};
use std::result::Result as StdResult;
use std::time::Duration;
use tempfile::TempDir;
use tokio::process;
use tokio_postgres as postgres;

/// Build an [`SshProxy`]
///
/// See [module-level](`self`) documentation.
pub struct SshProxyBuilder {
    ssh_user: Option<String>,
    ssh_host: String,
    ssh_port: Option<u16>,
    postgres_port: u16,
}

impl SshProxyBuilder {
    #[must_use]
    pub fn new(ssh_host: &str) -> Self {
        SshProxyBuilder {
            ssh_user: None,
            ssh_host: ssh_host.to_string(),
            ssh_port: None,
            postgres_port: 5432,
        }
    }

    pub async fn open(self) -> io::Result<SshProxy> {
        let temp_dir = utils::temp_dir()?;
        let socket_path = temp_dir.path().join(".s.PGSQL.5432");

        let mut conn_spec = socket_path.clone().into_os_string();
        conn_spec.push(":/var/run/postgresql/.s.PGSQL.");
        conn_spec.push(format!("{}", self.postgres_port));

        let ssh_uri = self.ssh_uri();

        let mut child = process::Command::new("ssh");
        if let Some(arg) = utils::ssh_verbosity_arg() {
            child.arg(arg);
        }
        child
            .arg("-n")
            .arg("-T")
            .arg("-N")
            .arg("-o")
            .arg("BatchMode=yes")
            .arg("-o")
            .arg("ExitOnForwardFailure=yes")
            .arg("-L")
            .arg(conn_spec)
            .arg(&ssh_uri)
            .kill_on_drop(true);
        debug!("Executing command: {}", DisplayCommand(child.as_std()));
        let mut child = child.spawn()?;

        let _abort_handle = utils::log::schedule_vpn_needed_for_ssh_info_message();
        let (_wait, user_name) = tokio::try_join!(
            wait_for_proxy(&mut child, &socket_path),
            ssh_fetch_user_name(&ssh_uri),
        )?;

        let proxy = SshProxy {
            _child: child,
            socket_path,
            user_name,
            _temp_dir: temp_dir,
        };

        Ok(proxy)
    }

    pub fn ssh_user(&mut self, value: &str) -> &mut Self {
        self.ssh_user = Some(value.to_string());
        self
    }

    pub fn ssh_port(&mut self, value: u16) -> &mut Self {
        self.ssh_port = Some(value);
        self
    }

    pub fn postgres_port(&mut self, value: u16) -> &mut Self {
        self.postgres_port = value;
        self
    }

    fn ssh_uri(&self) -> String {
        use std::fmt::Write;

        let mut uri = "ssh://".to_string();
        if let Some(user) = &self.ssh_user {
            write!(&mut uri, "{user}@").unwrap();
        }
        write!(&mut uri, "{}", &self.ssh_host).unwrap();
        if let Some(port) = self.ssh_port {
            write!(&mut uri, ":{port}").unwrap();
        }
        uri
    }
}

/// SSH connection tunnelling a Postgres connection
///
/// Use built in port-forwarding of SSH to forward
/// remote ssh port to local machine.
///
/// See [module-level](`self`) documentation.
pub struct SshProxy {
    _child: process::Child,
    socket_path: PathBuf,
    user_name: String,
    _temp_dir: TempDir,
}

impl SshProxy {
    /// Host of Unix socket
    ///
    /// Unix sockets don't really have a host but
    /// this is what needs to be passed as host to
    /// libpq.
    #[must_use]
    pub fn host(&self) -> &str {
        self.socket_path
            .parent()
            .unwrap()
            .as_os_str()
            .to_str()
            .expect("non-UTF8 socket path")
    }

    /// Postgres user name
    #[must_use]
    pub fn user_name(&self) -> &str {
        &self.user_name
    }

    /// Port of the Unix
    ///
    /// Unix sockets don't really have ports but
    /// this is what needs to be passed as port to
    /// libpq.
    #[must_use]
    pub fn port(&self) -> u16 {
        5432
    }
}

#[async_trait]
impl SqlConnect for SshProxy {
    async fn sql_connect(&self, db_name: &str) -> StdResult<postgres::Client, postgres::Error> {
        let (client, connection) = postgres::config::Config::new()
            .application_name("tco")
            .user(&self.user_name)
            .host(self.host())
            .port(self.port())
            .dbname(db_name)
            .connect(postgres::tls::NoTls)
            .await?;
        tokio::spawn(async move {
            if let Err(e) = connection.await {
                debug!("Postgres connection closed: {e}");
            }
        });
        Ok(client)
    }
}

/// Wait for SSH to open forwarded socket.
async fn wait_for_proxy(child: &mut process::Child, socket_path: &Path) -> io::Result<()> {
    loop {
        if let Some(exit_status) = child.try_wait()? {
            return Err(io::Error::other(format!(
                "`ssh` process terminated unexpectedly with code {exit_status}."
            )));
        }
        match tokio::fs::metadata(&socket_path).await {
            Ok(_) => break Ok(()),
            Err(e) if e.kind() == io::ErrorKind::NotFound => {}
            Err(e) => break Err(e),
        }
        tokio::time::sleep(Duration::from_millis(50)).await;
    }
}

/// Fetch OS user name
async fn ssh_fetch_user_name(ssh_uri: &str) -> io::Result<String> {
    ssh::Command::new(ssh_uri)
        .trim_final_newline(true)
        .output_string("id", ["-un"].iter())
        .await
}

#[cfg(test)]
#[cfg(feature = "test-with-secrets")]
mod tests {
    use super::*;

    #[tokio::test]
    async fn ssh_proxy() {
        let db_name = "nice_agogistest";
        let proxy = SshProxyBuilder::new("db5.stage.tocco.cust.vshn.net")
            .open()
            .await
            .unwrap();
        let client = proxy.sql_connect(db_name).await.unwrap();

        let row = client.query_one("SELECT current_user", &[]).await.unwrap();
        let user_name: &str = row.get(0);
        assert_eq!(user_name, &proxy.user_name);

        let row = client
            .query_one("SELECT current_database()", &[])
            .await
            .unwrap();
        let observed_db_name: &str = row.get(0);
        assert_eq!(db_name, observed_db_name);
    }
}
