//! Implementation of a generic DB copy protocol

use crate::exec;
use crate::sql::dump::Protocol;
use crate::sql::query::ClientExt;
use crate::{Error, Result};
use async_trait::async_trait;
use std::str::FromStr;
use tokio_postgres as postgres;

pub(crate) const EXCLUDED_TABLES_VAR: &[&str] = &[
    "nice_entity_history",
    "nice_log_entry",
    "nice_log_entry_var",
    "nice_notification",
    "nice_progress_var",
    "nice_system_activity",
    "nice_task_execution",
    "nice_task_progress",
];

pub(crate) const EXCLUDED_TABLES_EMAIL: &[&str] = &[
    "nice_email_archive",
    "nice_email_archive_to_address",
    "nice_email_archive_to_email_attachment",
    "nice_email_archive_to_recipient",
];

struct DbScriptOpts<'a> {
    src_db: &'a str,
    dest_db: &'a str,
    dest_role: &'a str,
    excluded_tables: &'a [String],
}

fn copy_scripts(opts: &DbScriptOpts) -> Vec<(String, String)> {
    let base_dump_command = format!(
        "pg_dump -Fc -Z0 --no-owner --no-acl --quote-all-identifiers -d {}",
        shlex::try_quote(opts.src_db).expect("NUL character in source DB name"),
    );
    let base_restore_command = format!(
        r#"PGOPTIONS="-c synchronous_commit=off" pg_restore --no-owner --role {} --no-acl -d {}"#,
        shlex::try_quote(opts.dest_role).expect("NUL character in role name"),
        shlex::try_quote(opts.dest_db).expect("NUL character in target DB name"),
    );

    if opts.excluded_tables.is_empty() {
        // TODO consider using separate pre-data, data and post-data copies (as used in `else` branch) for everything
        //
        // I'd be more consistent but this is better tested. Let's keep using
        // this for full dumps for a while longer.
        vec![(base_dump_command, base_restore_command)]
    } else {
        vec![
            // pre-data section
            (
                format!("{base_dump_command} --section pre-data"),
                base_restore_command.clone(),
            ),
            // data section
            (
                {
                    let mut command = format!("{base_dump_command} --section data");
                    for table in opts.excluded_tables {
                        command.push_str(" --exclude-table ");
                        command.push_str(
                            &shlex::try_quote(table).expect("NUL character in table name"),
                        );
                    }
                    command
                },
                base_restore_command.clone(),
            ),
            // post-data section
            (
                format!("{base_dump_command} --section post-data"),
                base_restore_command,
            ),
        ]
    }
}

/// Construct dump and restore scripts
///
/// Takes a generic `dump_script` and `restore_script` and
/// readies it for use over SSH. Output of `dump_script` is
/// meant to be piped to `restore_script`.
///
/// Check exit status of both commands.
fn host_to_host_copy_scripts(dump_script: &str, restore_script: &str) -> (String, String) {
    (
        format!("set -Eeu -o pipefail && ({dump_script}) | zstd --adapt -c"),
        format!("set -Eeu -o pipefail && zstd -cd | ({restore_script})"),
    )
}

/// Construct dump and restore script to execute one a single host
///
/// Takes a generic `dump_script` and `restore_script` and
/// readies it for use over SSH. Both are combined into
/// one script piping from `dump_script` to `restore_script`
/// on remote machine directly.
///
/// Exit status of dump and restore, in this order, are returned on
/// stdout separated by semicolon. For example, `"0;0"` is retuned on
/// success, and `"0;32"` may be returned to indicate an error during restore.
fn on_host_copy_script(dump_script: &str, restore_script: &str) -> String {
    // PIPESTATUS is bash-specific.
    format!(r#"({dump_script}) | ({restore_script}); printf "%d;%d" "${{PIPESTATUS[@]}}""#)
}

/// Generic protocol usable for any DB (even non-Nice DBs)
///
/// Create via [`GenericProtoBuilder::new()`].
pub struct GenericProto {
    excluded_tables: Vec<String>,
}

impl GenericProto {
    #[must_use]
    pub fn excluded_tables(&self) -> &[String] {
        &self.excluded_tables
    }
}

pub struct GenericProtoBuilder {
    inner: GenericProto,
}

/// Builder for [`GenericProto`]
impl GenericProtoBuilder {
    #[must_use]
    pub fn new() -> Self {
        Self {
            inner: GenericProto {
                excluded_tables: Vec::new(),
            },
        }
    }

    pub fn exclude_tables<L, T>(&mut self, tables: L) -> &mut Self
    where
        L: IntoIterator<Item = T>,
        T: AsRef<str>,
    {
        self.inner
            .excluded_tables
            .extend(tables.into_iter().map(|table| table.as_ref().to_owned()));
        self
    }

    /// Exclude various tables from copying
    pub fn exclude_var_tables(&mut self) -> &mut Self {
        self.exclude_tables(EXCLUDED_TABLES_VAR);
        self
    }

    /// Exclude email-related tables from copying
    pub fn exclude_email_tables(&mut self) -> &mut Self {
        self.exclude_tables(EXCLUDED_TABLES_EMAIL);
        self
    }

    #[must_use]
    pub fn build(mut self) -> GenericProto {
        self.inner.excluded_tables.sort_unstable();
        self.inner.excluded_tables.dedup();
        self.inner
    }
}

#[async_trait]
impl Protocol for GenericProto {
    // TODO Try to merge `execute()` and `execute_same_host()`. Code is very similar.

    async fn execute(
        &self,
        src_name: &str,
        _src_conn: &mut postgres::Client,
        src_exec: &mut (dyn exec::Command + Send),
        dest_name: &str,
        dest_conn: &mut postgres::Client,
        dest_exec: &mut (dyn exec::Command + Send),
    ) -> Result<()> {
        let role = dest_conn.db_owner().await?;
        let scripts = copy_scripts(&DbScriptOpts {
            src_db: src_name,
            dest_db: dest_name,
            dest_role: &role,
            excluded_tables: &self.excluded_tables[..],
        });

        execute_scripts(&scripts, src_exec, dest_exec).await
    }

    async fn execute_same_host(
        &self,
        src_name: &str,
        _src_conn: &mut postgres::Client,
        dest_name: &str,
        dest_conn: &mut postgres::Client,
        exec: &mut (dyn exec::Command + Send),
    ) -> Result<()> {
        let role = dest_conn.db_owner().await?;
        let scripts = &copy_scripts(&DbScriptOpts {
            src_db: src_name,
            dest_db: dest_name,
            dest_role: &role,
            excluded_tables: &self.excluded_tables[..],
        });

        execute_scripts_same_host(scripts, exec).await
    }

    fn is_tainted(&self) -> bool {
        !self.excluded_tables().is_empty()
    }
}

// TODO Try to merge `execute_scripts()` and `execute_scripts_same_host()`. Code is very similar.

async fn execute_scripts(
    scripts: &[(String, String)],
    src_exec: &mut (dyn exec::Command + Send),
    dest_exec: &mut (dyn exec::Command + Send),
) -> Result<()> {
    let mut first_restore_error = None;
    for (i, (dump_script, restore_script)) in scripts.iter().enumerate() {
        let no = i + 1;
        let (dump_script, restore_script) = host_to_host_copy_scripts(dump_script, restore_script);

        debug!("Executing dump script #{no} on source machine: {dump_script:?}");
        let mut dump_proc = src_exec.spawn_shell_stdout(&dump_script)?;
        debug!("Executing restore script #{no} on target machine: {restore_script:?}");
        let mut restore_proc = dest_exec.spawn_shell_stdin(&restore_script)?;
        tokio::io::copy(
            &mut dump_proc.stdout.take().expect("pg_dump: stdout not a pipe"),
            &mut restore_proc
                .stdin
                .take()
                .expect("pg_restore: stdin not a pipe"),
        )
        .await?;
        let dump_proc_status = dump_proc.wait().await?;
        let restore_proc_status = restore_proc.wait().await?;

        if !dump_proc_status.success() {
            return Err(Error::Custom(format!(
                "pg_dump #{no} failed: {dump_proc_status}"
            )));
        }
        match restore_proc_status.code() {
            Some(0) => (),
            Some(1..=125) => {
                // Code returned by pg_restore on SQL-level error appears to be 1 but
                // this is not formally documented. Be a bit more inclusive, just in case.

                debug!(
                    "Restore script #{no} failed: {restore_proc_status}, delaying error \
                     propagation ..."
                );
                // `pg_restore` can report harmless warnings which trigger a failure
                // exit status. Just continue and let the user decide what to do about it
                // at the end.
                first_restore_error.get_or_insert((no, restore_proc_status));
            }
            Some(_) | None => {
                // Codes used by shell when command not found, terminated by signal,
                // etc. pg_restore hopefully doesn't use this.

                return Err(Error::Custom(format!(
                    "pg_restore #{no} failed: {restore_proc_status}"
                )));
            }
        }
    }
    if let Some((no, restore_proc_status)) = first_restore_error {
        warn!(
            "pg_restore reported one or more errors during restore. Check output for errors. Such \
             errors may be harmless."
        );
        Err(Error::Custom(format!(
            "pg_restore #{no} failed: {restore_proc_status}"
        )))
    } else {
        Ok(())
    }
}

async fn execute_scripts_same_host(
    scripts: &[(String, String)],
    exec: &mut (dyn exec::Command + Send),
) -> Result<()> where {
    let mut first_restore_error = None;
    for (i, (dump_script, restore_script)) in scripts.iter().enumerate() {
        let no = i + 1;
        let script = on_host_copy_script(dump_script, restore_script);

        debug!("Executing script #{no} on source/target machine: {script:?}");
        let status_str = exec.output_string_shell(&script);
        let status_str = status_str
            .await
            .map_err(|e| Error::Custom(format!("copy script #{no} failed: {e}")))?;
        let (dump_status, restore_status) = status_str
            .split_once(';')
            .expect("invalid status from copy script");
        let (dump_status, restore_status) = (
            u8::from_str(dump_status).expect("cannot parse pg_dump exit status"),
            u8::from_str(restore_status).expect("cannot parse pg_restore exit status"),
        );
        if dump_status != 0 {
            return Err(Error::Custom(format!(
                "pg_dump #{no} failed: exit status: {dump_status}"
            )));
        }
        match restore_status {
            0 => (),
            1..=125 => {
                // Code returned by pg_restore on SQL-level error appears to be 1 but
                // this is not formally documented. Be a bit more inclusive, just in case.

                warn!(
                    "Script #{no}: restore failed: {restore_status}, delaying error propagation \
                     ..."
                );
                // `pg_restore` can report harmless warnings which trigger a failure
                // exit status. Just continue and let the user decide what to do about it
                // at the end.
                first_restore_error.get_or_insert((no, restore_status));
            }
            status @ 126..=255 => {
                // Codes used by shell when command not found, terminated by signal,
                // etc. pg_restore hopefully doesn't use this.

                return Err(Error::Custom(format!(
                    "pg_restore #{no} failed: exit status: {status}"
                )));
            }
        }
    }

    if let Some((no, restore_proc_status)) = first_restore_error {
        warn!(
            "pg_restore reported one or more errors during restore. Check output for errors. Such \
             errors may be harmless."
        );
        Err(Error::Custom(format!(
            "pg_restore #{no} failed: exit status: {restore_proc_status}"
        )))
    } else {
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::utils;

    #[tokio::test]
    #[allow(clippy::too_many_lines)]
    async fn script_errors() {
        fn scripts(dump_status: u8, restore_status: u8) -> (String, String) {
            (
                format!("sh -c 'exit {dump_status}'"),
                format!("sh -c 'exit {restore_status}'"),
            )
        }

        #[derive(Clone, Copy, Debug)]
        enum Expect<'a> {
            Ok,
            Err(&'a str),
        }

        async fn test(scripts: &[(String, String)], expected: Expect<'_>) {
            {
                dbg!(scripts);
                dbg!(expected);
                let mut executor_a = exec::local::Command::new();
                let mut executor_b = exec::local::Command::new();

                let remote_result =
                    execute_scripts(scripts, &mut executor_a, &mut executor_b).await;
                let same_host_result = execute_scripts_same_host(scripts, &mut executor_a).await;
                match expected {
                    Expect::Ok => {
                        remote_result.unwrap();
                        same_host_result.unwrap();
                    }
                    Expect::Err(expected_err) => {
                        let remote_err = same_host_result.unwrap_err();
                        let remote_err = format!("{remote_err}");

                        let same_host_err = remote_result.unwrap_err();
                        let same_host_err = format!("{same_host_err}");

                        assert_eq!(remote_err, expected_err);
                        assert_eq!(same_host_err, expected_err);
                    }
                }
            }
        }

        let _ = env_logger::builder().is_test(true).try_init();

        // Ok
        {
            let scripts = [scripts(0, 0), scripts(0, 0), scripts(0, 0)];
            test(&scripts, Expect::Ok).await;
        }

        // Restore error (code <126, delayed reporting)
        {
            let dir = utils::temp_dir().unwrap();
            let path_a = dir.path().join("a");
            let path_b = dir.path().join("b");

            let scripts = [
                scripts(0, 0),
                // reporting of this error is delayed to end, after script #3
                scripts(0, 16),
                // reachable
                (
                    format!(r#"touch "{}""#, path_a.display()),
                    format!(r#"touch "{}""#, path_b.display()),
                ),
            ];
            let expect = Expect::Err("pg_restore #2 failed: exit status: 16");
            test(&scripts, expect).await;

            assert!(path_a.try_exists().unwrap());
            assert!(path_b.try_exists().unwrap());
        }

        // Restore error (code >= 126, immediate reporting)
        {
            let scripts = [
                scripts(0, 0),
                scripts(0, 126),
                scripts(64, 64), // unreachable
            ];
            let expect = Expect::Err("pg_restore #2 failed: exit status: 126");
            test(&scripts, expect).await;
        }

        // Delayed reporting of restore error (code <126)
        {
            let scripts = [
                // pg_restore error ignored (would be reported at end, after script #3)
                scripts(0, 125),
                // pg_dump is reported immediately
                scripts(32, 0),
                // unreachable
                scripts(64, 64),
            ];
            let expect = Expect::Err("pg_dump #2 failed: exit status: 32");
            test(&scripts, expect).await;
        }

        // Dump command not found
        {
            let scripts = [
                (
                    "a_command-that_is-unlikely_to-exist 2>/dev/null".to_string(),
                    "true".to_string(),
                ),
                // unreachable
                scripts(128, 128),
            ];
            let expect = Expect::Err("pg_dump #1 failed: exit status: 127");
            test(&scripts, expect).await;
        }

        // Restore command not found
        {
            let scripts = [
                (
                    "true".to_string(),
                    "a_command-that_is-unlikely_to-exist 2>/dev/null".to_string(),
                ),
                // unreachable
                scripts(128, 128),
            ];
            let expect = Expect::Err("pg_restore #1 failed: exit status: 127");
            test(&scripts, expect).await;
        }
    }
}
