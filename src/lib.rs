#![cfg_attr(all(test, feature = "nightly"), feature(test))]
#![cfg_attr(feature = "nightly", feature(exit_status_error))]

pub mod ansible;
pub mod cache;
pub mod client;
pub mod cloudscale;
pub mod elasticsearch;
pub mod error;
pub mod exec;
pub mod git;
pub mod k8s;
pub mod nice;
pub mod sql;
pub mod utils;

#[macro_use]
extern crate log;
#[cfg(all(test, feature = "nightly"))]
extern crate test;

pub use error::Error;
pub use error::Result;

use std::sync::LazyLock;
use std::time::Duration;

/// Obtain global, shared HTTP client
#[must_use]
pub fn http_client() -> &'static reqwest::Client {
    static CLIENT: LazyLock<reqwest::Client> = LazyLock::new(|| {
        reqwest::ClientBuilder::new()
            .user_agent(concat!("Tocco tco/", env!("CARGO_PKG_VERSION")))
            .connect_timeout(Duration::from_secs(30))
            .https_only(true)
            .build()
            .expect("failed to initialize HTTP client")
    });
    &CLIENT
}
