//! Generic, versioned cache
use crate::Result;
use nix::fcntl;
use serde::Serialize;
use serde::de::DeserializeOwned;
use std::fs;
use std::io::{self, Read, Write};
use std::path::{Path, PathBuf};
use std::sync::LazyLock;
use xxhash_rust::const_xxh3;

/// Hash included in cache to detect schema changes
/// (=when the data structure in Rust changes).
pub(crate) type SchemaHash = [u8; SCHEMA_HASH_SIZE];

/// Size of `SchemaHash` in bytes.
const SCHEMA_HASH_SIZE: usize = 16;

/// Get path to cache directory
#[must_use]
pub fn cache_dir() -> &'static Path {
    static VALUE: LazyLock<PathBuf> = LazyLock::new(|| {
        let mut path = dirs::cache_dir().expect("failed to find OS-specific cache dir");
        path.push("tocco-tco");
        path
    });
    &VALUE
}

/// Get path to cache file with given name
#[must_use]
pub(crate) fn cache_file_with_name(name: &str) -> PathBuf {
    cache_dir().join(format!("{name}.cache"))
}

/// Hash over struct/enums/etc. representing config in Rust.
///
/// Pass the content of the \*.rs file that defines the structures of
/// the configuration to allow invalidating the cache when the
/// schema changes internally.
///
/// Additionally, the hash of this file is included to account for
/// changes in how the cache is stored.
pub(crate) const fn cache_schema_hash(file_content: &[u8], mixin: u128) -> SchemaHash {
    let this_file = const_xxh3::xxh3_128(include_bytes!("cache.rs"));
    let supplied_file = const_xxh3::xxh3_128(file_content);
    (this_file ^ supplied_file ^ mixin).to_le_bytes()
}

pub(crate) fn load<C>(cache_name: &str, schema_hash: &SchemaHash) -> Result<Option<C>>
where
    C: DeserializeOwned,
{
    debug!(
        "Loading cache {cache_name:?} of type {} and with schema {:#x}.",
        std::any::type_name::<C>(),
        u128::from_le_bytes(*schema_hash),
    );
    let path = cache_file_with_name(cache_name);
    let mut file = match fs::File::open(path) {
        Ok(file) => file,
        Err(e) if e.kind() == io::ErrorKind::NotFound => {
            debug!("Cache {cache_name:?} not found.");
            return Ok(None);
        }
        Err(e) => {
            return Err(e.into());
        }
    };
    let mut version: SchemaHash = Default::default();
    file.read_exact(&mut version)?;
    if schema_hash != &version {
        debug!("Cache version mismatched for cache {cache_name:?}, ignoring cache …");
        return Ok(None);
    }
    Ok(Some(rmp_serde::from_read(&mut file)?))
}

pub(crate) fn save<C>(cache_name: &str, schema_hash: &SchemaHash, config: &C) -> Result<()>
where
    C: Serialize,
{
    debug!(
        "Saving cache {cache_name:?} of type {} and with schema {:#x}.",
        std::any::type_name::<C>(),
        u128::from_le_bytes(*schema_hash),
    );
    let path = cache_file_with_name(cache_name);
    let tmp_path = path.with_extension("tmp");
    let lock_path = path.with_extension("lock");
    let lock_file = std::fs::File::create(lock_path)?;
    let _flock = fcntl::Flock::lock(lock_file, fcntl::FlockArg::LockExclusive)
        .map_err(|(_, errno)| errno)?;
    {
        let mut file = fs::File::create(&tmp_path)?;
        file.write_all(&schema_hash[..])?;
        rmp_serde::encode::write(&mut file, &config)?;
        file.flush()?;
        file.sync_all()?;
    }
    fs::rename(&tmp_path, path)?;
    Ok(())
}

#[cfg(test)]
#[cfg(feature = "test-with-repo-access")]
mod tests {
    use super::*;
    use crate::ansible::{self, config_quick};
    use test_utils::{LockMode, lock_caches};

    #[tokio::test]
    async fn config_parsing_and_cache() {
        let _guard = lock_caches(LockMode::Exclusive).await;
        let repo = ansible::git::repo().await.unwrap();
        let cache_path = cache_file_with_name(config_quick::CACHE_NAME);

        match std::fs::remove_file(&cache_path) {
            Ok(()) => {}
            Err(e) if e.kind() == std::io::ErrorKind::NotFound => {}
            Err(e) => panic!("{}", e),
        }

        let cache: Option<config_quick::Config> =
            load(config_quick::CACHE_NAME, &config_quick::CACHE_SCHEMA_HASH).unwrap();
        assert!(cache.is_none());
        let uncached_config = config_quick::load(&repo).await.unwrap();
        assert!(cache_path.try_exists().unwrap());
        let cached_config: config_quick::Config =
            load(config_quick::CACHE_NAME, &config_quick::CACHE_SCHEMA_HASH)
                .unwrap()
                .unwrap();
        assert_eq!(
            uncached_config.installations.len(),
            cached_config.installations.len()
        );

        let master1 = uncached_config.installations.get("master").unwrap();
        let master2 = cached_config.installations.get("master").unwrap();
        assert_eq!(master1.branch, "master");
        assert_eq!(master2.branch, "master");
        assert_eq!(master1.customer_name, "test");
        assert_eq!(master2.customer_name, "test");
        assert_eq!(master1.version().to_string(), "1000.0.0");
        assert_eq!(master2.version().to_string(), "1000.0.0");
    }

    #[tokio::test]
    async fn cache_invalidation() {
        let _guard = lock_caches(LockMode::Shared).await;
        let cache_name = "cache-invalidation-test";

        // save
        let hash = 0_u128.to_le_bytes();
        save(cache_name, &hash, &"sample payload").unwrap();

        // load with same hash
        let loaded: Option<String> = load(cache_name, &hash).unwrap();
        assert_eq!(loaded.as_deref(), Some("sample payload"));

        // try to load with different hash
        let mismatching_hash = 1_u128.to_le_bytes();
        let loaded: Option<String> = load(cache_name, &mismatching_hash).unwrap();
        assert_eq!(loaded, None);
    }
}
