# Tco

Multi-purpose tool aimed at Tocco employees. Simplifies various work tasks.

I've mostly written this for myself but anyone is free to use it and contribute.

## Documentation

API: https://toccoag.gitlab.io/tco

## Install

1. Install cargo (see https://rustup.rs/).
2. Compile and install:

   ```
   cargo install --git https://gitlab.com/toccoag/tco.git
   ```

## Update

1. Update compiler:

   ```
   rustup update
   ```

2. Update tco:

   ```
   cargo install --git https://gitlab.com/toccoag/tco.git
   ```

## Development

Make sure cargo is installed (see https://rustup.rs/).

Syntax check:

```
cargo check
```

Linter:

```
cargo clippy
```

Run all tests:

```
cargo test
```

Exclude tests that need local secrets (Vault password, ssh key, etc.)

```
cargo test --no-default-features
```

Run application:

```
cargo run -- help
```

Install from repository:

```
cargo install --path .
```
